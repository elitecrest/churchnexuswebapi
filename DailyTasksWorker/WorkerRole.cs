using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Diagnostics;
using Microsoft.WindowsAzure.ServiceRuntime;
 
using System.Net.Http;
using System.Net.Http.Headers;

namespace DailyTasksWorker
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);


    //     <!--test server url-->
    //<!--<add key = "baseurl" value="http://churchapitest.azurewebsites.net/" />-->

    //<!--production server url-->
    //<add key = "baseurl" value="http://churchapi.azurewebsites.net/" />


        public override void Run()
        {
            Trace.TraceInformation("DailyTasksWorker is running");

            try
            {
                //while (true)
                //{
                    sendPushToMembers();
                    
                    Thread.Sleep(TimeSpan.FromDays(1));
                    Trace.TraceInformation("Working", "Information");
               // }

                //this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }


        public void sendPushToMembers()
        {
            HttpClient client = new HttpClient();

            //string baseURL = CloudConfigurationManager.GetSetting("BaseURL").ToString();

            string baseURL = "http://churchapi.azurewebsites.net/";
            client.BaseAddress = new Uri(baseURL);
            string Api = "/Users/sendWishesPushToChurchMembers";
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client.GetAsync(Api).Result;
            if (response1.IsSuccessStatusCode)
            {
            }


        }
        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("DailyTasksWorker has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("DailyTasksWorker is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("DailyTasksWorker has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }


       
    }
}
