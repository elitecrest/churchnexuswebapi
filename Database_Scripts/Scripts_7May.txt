 
 

 
ALTER PROCEDURE [cp].[CreateSermons] 
   @name nvarchar(255),
   @author nvarchar(64),
   @series_id int,
   @gallery_id int,  
   @sermon_url nvarchar(1024),
   @is_used bit,
   @updated datetime,
   @duration varchar(20),
   @description nvarchar(4000),	 
   @sermon_format varchar(20),
   @created datetime,
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
	  BEGIN TRY

	    BEGIN TRAN
			insert into sermons(name,author,series_id,gallery_id,sermon_url,is_used,updated,duration,description,sermon_format,status,created)
            values(@name,@author,@series_id,@gallery_id,@sermon_url,@is_used,@updated,@duration,@description,@sermon_format,1,@created)
            
		set @Id = @@IDENTITY

		select sr.*, s.church_id,isnull(c.PushNotificationAccept,0) as PushNotificationAccept--,d.UDID,fdd.FBDeviceToken
		,c.name as ChurchName
		from  cp.sermons sr
		left join cp.series s on sr.series_id = s.id
		left join cp.churches c on c.id  = s.church_id
		 --left join cp.device d on d.ChurchId   = c.Id
		--left join FirebaseDeviceDetails fdd on fdd.REfDeviceId  = d.UDID 
		where sr.id = @Id  

	   COMMIT TRAN
    END TRY
	BEGIN CATCH
		set @Id = -1
		ROLLBACK TRAN
	END CATCH 
END
 

GO


CREATE TABLE [cp].[OptionalFeatures](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[FeatureName] [varchar](50) NOT NULL 
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO



CREATE TABLE [cp].[Plan_OptionalFeatures](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PlanId] [int] NULL,
	[FeatureId] [int] NULL
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO



INSERT INTO [cp].[OptionalFeatures]
           ([FeatureName])
     VALUES
           ('Kids Registration')
GO
INSERT INTO [cp].[OptionalFeatures]
           ([FeatureName])
     VALUES
           ('Church Store')
GO

INSERT INTO [cp].[OptionalFeatures]
           ([FeatureName])
     VALUES
           ('Live Streaming')
GO



INSERT INTO [cp].[Plan_OptionalFeatures]
           ([PlanId]
           ,[FeatureId])
     VALUES
           (2
           ,1)
GO
INSERT INTO [cp].[Plan_OptionalFeatures]
           ([PlanId]
           ,[FeatureId])
     VALUES
           (3
           ,1)
GO
INSERT INTO [cp].[Plan_OptionalFeatures]
           ([PlanId]
           ,[FeatureId])
     VALUES
           (3
           ,2)
GO
INSERT INTO [cp].[Plan_OptionalFeatures]
           ([PlanId]
           ,[FeatureId])
     VALUES
           (4
           ,1)
GO
INSERT INTO [cp].[Plan_OptionalFeatures]
           ([PlanId]
           ,[FeatureId])
     VALUES
           (4
           ,2)
GO
INSERT INTO [cp].[Plan_OptionalFeatures]
           ([PlanId]
           ,[FeatureId])
     VALUES
           (4
           ,3)
GO


 
create PROCEDURE [cp].[GetPlansOptionalFeatures] 
@planid int 
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 
	--SELECT id, name, gallery_id, (select count(*) from cp.plays p where p.sermon_id = s.id) as play_count
	--from cp.sermons s
	 
	 select pof.*,opf.FeatureName from Plan_OptionalFeatures pof
	 left join optionalfeatures opf on pof.featureid = opf.id
	 where Planid = @planid

END


GO


 Alter table cp.Plan_OptionalFeatures add isExists int  
 
 

ALTER proc [dbo].[spGetChurchMembersHavingBirthDay]
as 
begin


select cmi.*,fdd.FBDeviceToken,isnull(c.PushNotificationAccept,0) PushNotificationAccept from cp.ChurchMembersInfo cmi 
left join FirebaseDeviceDetails fdd on fdd.REfDeviceId  = cmi.UDID 
left join cp.churches c on c.id = cmi.churchid
where (datepart(DAY,dateOfBirth)=datepart(DAY,GETDATE()) 
and datepart(MONTH,dateOfBirth)=datepart(MONTH,GETDATE())) 

end


GO


 
 

ALTER proc [dbo].[spGetChurchMembersHavingWeddingDay]
as 
begin


select cmi.*,fdd.FBDeviceToken,isnull(c.PushNotificationAccept,0) PushNotificationAccept from cp.ChurchMembersInfo cmi 
left join cp.churches c on c.id = cmi.churchid
left join FirebaseDeviceDetails fdd on fdd.REfDeviceId  = cmi.UDID 
where (datepart(DAY,weddingDate)=datepart(DAY,GETDATE()) and datepart(MONTH,weddingDate)=datepart(MONTH,GETDATE())) 

end

GO


 
ALTER PROCEDURE [cp].[GetFirebaseDevices]
 AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	Select fdd.*,isnull(c.PushNotificationAccept,0) PushNotificationAccept from FirebaseDeviceDetails  fdd
	left join cp.churches c on c.id = fdd.UserId

END 


GO

CREATE TABLE [cp].[ChurchSignUpInfo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ChurchName] [nvarchar](255) NULL,
	[Email] [nvarchar](100) NULL,
	[MobileNumber] [nvarchar](50) NULL,
	[CountryId] [int] NULL,
	[PlanId] [int]  NULL,
	[SignupCompleted] [bit] NULL,
	[CreateDate] [datetime] NULL 
)

GO
 
ALTER PROCEDURE [cp].[CreateEvent] 
   @name nvarchar(255),
   @author nvarchar(64),
   @church_id int,
   @gallery_id int,
   @is_used bit,
   @starts datetime,
   @ends datetime,
   @description nvarchar(512),	 
   @Id int  

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  BEGIN TRY
			insert into events(name,author,church_id,gallery_id,is_used,starts,ends,description,status)
            values(@name,@author,@church_id,@gallery_id,@is_used,@starts,@ends,@description,1)
            
			set @Id = @@Identity

		select e.id,isnull(c.PushNotificationAccept,0) as PushNotificationAccept ,c.name as ChurchName
		from cp.events e 
		left join cp.churches c on c.id  = e.church_id
		where e.id = @Id
   
   	  END TRY
		BEGIN CATCH
		       set @Id = -1
	    END CATCH 
END









GO


CREATE PROCEDURE [cp].[AddChurchSignupInfo] 
	-- Add the parameters for the stored procedure here
	@ChurchName nvarchar(255),
	@Email nvarchar(100),
	@MobileNumber nvarchar(50),
	@CountryId int,
	@PlanId int,
	@SignUpCompleted bit, 
	@Id int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--select * from  cp.storeitems where church_id = @church_id

INSERT INTO [cp].[ChurchSignUpInfo]
           ([ChurchName]
           ,[Email]
           ,[MobileNumber]
           ,[CountryId]
           ,[PlanId]
           ,[SignupCompleted]
           ,[CreateDate])
     VALUES
           (@ChurchName
           ,@Email
           ,@MobileNumber
           ,@CountryId
           ,@PlanId
           ,@SignUpCompleted
           ,getutcdate())

	set @Id = @@Identity
END


GO


  
Create PROCEDURE [cp].[UpdateChurchSignupInfo] 
	-- Add the parameters for the stored procedure here
	@ChurchName nvarchar(255),
	@Email nvarchar(100),
	@MobileNumber nvarchar(50),
	@CountryId int,
	@PlanId int,
	@SignUpCompleted bit, 
	@Id int ,
	@output int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--select * from  cp.storeitems where church_id = @church_id

UPDATE [cp].[ChurchSignUpInfo]
   SET [ChurchName] = @ChurchName
      ,[Email] = @Email
      ,[MobileNumber] = @MobileNumber
      ,[CountryId] = @CountryId
      ,[PlanId] = @PlanId
      ,[SignupCompleted] = @SignUpCompleted
      
 WHERE Id = @Id

 set @output = @Id
 
END
 

GO




 
CREATE PROCEDURE [cp].[GetChurchSignupInfo] 
	-- Add the parameters for the stored procedure here 
	@Id int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--select * from  cp.storeitems where church_id = @church_id

 select * from [cp].[ChurchSignUpInfo] where id = @Id
END
 
GO



 

ALTER PROCEDURE [cp].[GetEvents]  --[cp].[GetEvents] 102282,0
@churchid int,
@eventid int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 if(@eventid = 0)
	select * from cp.events where church_id = @churchid and status = 1
	 and convert(varchar,starts,102) >= convert(varchar,getutcdate(),102)
	 order by starts asc
	else
	select * from cp.events where id=@eventid and status = 1
	 and convert(varchar,starts,102) >= convert(varchar,getutcdate(),102)
	 order by starts asc
END
 



GO
 


  

CREATE PROCEDURE [cp].[GetChurchIdByChurchSignUpId]  
@ChurchName varchar(100),
@Email varchar(100)
AS
BEGIN
 
	SET NOCOUNT ON;
	 


 select Id from cp.churches where email=@Email and Name=@ChurchName

END











GO


