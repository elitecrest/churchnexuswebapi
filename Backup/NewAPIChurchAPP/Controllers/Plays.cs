﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class PlaysController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse Plays(int sermonid)
        {
            try
            {
                
                List<PlaysDTO> plays = ChurchDAL.GetAllPlays(sermonid);
                if (plays.ToList().Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = plays;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        [HttpGet]
        public Utility.CustomResponse GetPlay(int playId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var play = ChurchDAL.GetPlay(playId);
                if (play.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = play;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse CreatePlay(PlaysDTO playsDTO)
        {
            try
            {
                int playId = ChurchDAL.CreatePlay(playsDTO);

                if (playId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = playId;
                    Result.Message = CustomConstants.PlaysAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPut]
        public Utility.CustomResponse EditPlay(PlaysDTO playsDTO)
        {
            try
            {
                int playId = ChurchDAL.EditPlay(playsDTO);

                if (playId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = playId;
                    Result.Message = CustomConstants.PlaysUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeletePlay(int PlayId)
        {
            try
            {
                int playId = ChurchDAL.DeletePlay(PlayId);

                if (playId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = playId;
                    Result.Message = CustomConstants.PlaysDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
    }
}
