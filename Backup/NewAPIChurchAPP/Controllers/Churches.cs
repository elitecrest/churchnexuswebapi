﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using NewAPIChurchAPP.Models;
using Microsoft.WindowsAzure.Storage;
using System.Configuration;
using System.Xml.XPath;
using Newtonsoft.Json.Linq;
using System.Net.Http.Headers;
using System.IO;
using Newtonsoft.Json; 
using System.Web.Script.Serialization;
using System.Runtime.Serialization; 
using System.Text;
using System.Web.Security;
using WebMatrix.WebData;
using System.Data.SqlClient;
using System.Runtime.Serialization.Json;
using GoogleMaps.LocationServices;
using System.Web;

namespace NewAPIChurchAPP.Controllers
{
    public class ChurchesController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();
     
        [HttpGet]
        public Utility.CustomResponse Login(string userName, string password,int ChurchId)
        {
            try
            {
               

                bool isLogin = WebSecurity.Login(userName, password, persistCookie: true);
                if (isLogin)
                {
                    
                    //var count = ChurchDAL.ChurchLoginValidation(userName, password, ChurchId);
                    //if(count > 0)
                    //{
                        var currentUser = ChurchDAL.Login(userName, password, ChurchId);
                        if (currentUser.Id > 0)
                        {
                           // currentUser.ChurchFeatures = ChurchDAL.GetFeaturesByChurchId(ChurchId);
                            Result.Status = Utility.CustomResponseStatus.Successful;
                            Result.Response = currentUser;
                            Result.Message = CustomConstants.DetailsGetSuccessfully;
                        }
                    //}
                    //else if(count == -1)
                    //{
                    //    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    //    Result.Message = CustomConstants.PasswordMisMatch;
                    //}
                    else 
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.UserDoesNotExist;
                    }
                }
                else
                {
                    bool validate = WebSecurity.UserExists(userName);
                    if (validate)
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.PasswordMisMatch;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.UserDoesNotExist;
                    }
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; 
            }
        }

        [HttpGet]
        public Utility.CustomResponse Churches(string geo)
        {
            try
            {
                 
                if(geo == null)
                {
                    geo = string.Empty;
                }
                List<ChurchDTO> newChurches = new List<ChurchDTO>();
                if (geo.Equals("0.0,0.0") || geo.Equals("0.000000,0.000000") || geo.Equals(string.Empty))
                {
                    newChurches = ChurchDAL.GetAllChurches(geo);
                }
                else
                {
                    
                    string countryName = GetCountryName(geo);
                    string[] arrLatlong = geo.Split(',');
                    //Get the current country by geolocation
                   
                    //if country is Us
                    if (countryName == "United States")
                    {
                        List<ApiDataDTO> ZipCodeList = GetZipDataFromLatLong(arrLatlong[0], arrLatlong[1]);
                        if (ZipCodeList.Count > 0)
                        {
                            List<ChurchDTO> churchesFromDB = ChurchDAL.GetAllChurches(geo);
                            newChurches = GetChurchesBasesOnGeo(ZipCodeList, churchesFromDB);
                            if(newChurches.Count <= 0)
                            {
                                //newChurches = ChurchDAL.GetChurchesByCountry(countryName);
                                newChurches = ChurchDAL.GetChurchesByCountry(countryName,geo);
                            }
                        }
                    }
                    else
                    { 
                        //Get Non US churches 
                        //newChurches = ChurchDAL.GetChurchesByCountry(countryName);
                        newChurches = ChurchDAL.GetChurchesByCountry(countryName, geo);
                    }
                }
                if (newChurches.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = newChurches;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        //public static T JsonDeserialize<T>(string jsonString)
        //{
        //       DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
        //    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
        //    T obj = (T)ser.ReadObject(ms);
        //    return obj;
        //}

        internal string GetCountryName(string geo)
        {
            string countryName = string.Empty;
            string[] arrLatlong = geo.Split(',');
            string content1 = string.Empty;
            string downloadApi = "maps/api/geocode/json?latlng=" + arrLatlong[0] + "," + arrLatlong[1] + "&sensor = false";
            JavaScriptSerializer serializer1 = new JavaScriptSerializer();
            serializer1.MaxJsonLength = 1000000000;
            Newtonsoft.Json.Linq.JArray resp1 = null;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://maps.google.com/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client.GetAsync(downloadApi).Result;
            if (response1.IsSuccessStatusCode)
            {
                Stream st1 = response1.Content.ReadAsStreamAsync().Result;
                StreamReader reader1 = new StreamReader(st1);
                content1 = reader1.ReadToEnd();

                LocationDTO locations = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<LocationDTO>(content1);
                List<Result> result = locations.results.ToList();

                if (result.Count > 0)
                {
                    List<AddressComponent> addressComponent = result.First().address_components;

                    for (int i = 0; i < addressComponent.Count; i++)
                    {
                        List<string> types = addressComponent[i].types;
                        for (int j = 0; j < types.Count; j++)
                        {
                            if (types[j].Contains("country"))
                            {
                                countryName = addressComponent[i].long_name;
                            }
                        }
                    }

                }

            }
            return countryName;
        }

      

        private List<ChurchDTO> GetChurchesBasesOnGeo(List<ApiDataDTO> ZipCodeList, List<ChurchDTO> churchesFromDB)
        {
            List<ChurchDTO> newChurchesList = new List<ChurchDTO>();

            for (int i = 0; i < churchesFromDB.Count;i++)
            {
                for (int j = 0; j < ZipCodeList.Count; j++)
                { 
                    if (churchesFromDB[i].Zip == ZipCodeList[j].Code)
                    {
                        newChurchesList.Add(churchesFromDB[i]);
                    }
                }
            }

                return newChurchesList;

        }
         
        [HttpGet]
        public Utility.CustomResponse ChurchesBySearch(string searchText)
        {
            try
            {

                List<ChurchDTO> churches = ChurchDAL.GetChurchesBySearch(searchText);
                if (churches.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churches;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse ChurchesBySearchCountry(string country,string searchtext)
        {
            try
            {

                List<ChurchDTO> churches = ChurchDAL.GetChurchesBySearchCountry(country, searchtext);
                if (churches.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churches;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse Country()
        {
            try
            {

                List<CountryDTO> Countries = ChurchDAL.GetCountryNames();
                if (Countries.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Countries;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse ChurchesForAdmin()
        {
            try
            {

                List<ChurchDTO> churches = ChurchDAL.GetAllChurchesForAdmin(string.Empty);
                if (churches.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churches;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetChurch(int churchid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var church = ChurchDAL.GetChurch(churchid);
              if(church.Id > 0)
              {
                //church.ChurchFeatures = ChurchDAL.GetFeaturesByChurchId(churchid);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = church;
                Result.Message = CustomConstants.DetailsGetSuccessfully;
              }
              else
              {
                  Result.Status = Utility.CustomResponseStatus.UnSuccessful;                  
                  Result.Message = CustomConstants.NoRecordsFound;
              }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetGiveUrl(int churchid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var urls = ChurchDAL.GetGiveUrl(churchid);
                
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = urls;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;               

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
        [HttpGet]
        public Utility.CustomResponse GetWordoftheDay(int churchid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var word = ChurchDAL.GetWordoftheDay(churchid);
                if (word != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = word;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse Announcements(int ChurchId)
        {
            try
            {

                List<AnnouncementDTO> announcement = ChurchDAL.GetAnnouncements(ChurchId);
                if (announcement.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = announcement;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {

                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        public Utility.CustomResponse CreateChurch(ChurchDTO churchDTO)
        {
            SqlConnection conn = ChurchDAL.ConnectTODB();
            SqlTransaction trans = conn.BeginTransaction();
            SqlCommand command = conn.CreateCommand();
            command.Transaction = trans;
            try
            {
                if (!(churchDTO.Banners.ToString() == string.Empty))
                {
                    if (!churchDTO.Banners.StartsWith("http"))
                    {
                        churchDTO.Banners = GetImageURL(churchDTO.Banners, churchDTO.BannerFormat);
                    }
                }
                if (!(churchDTO.Splashscreen.ToString() == string.Empty))
                {
                    if (!churchDTO.Splashscreen.StartsWith("http"))
                    {
                        churchDTO.Splashscreen = GetImageURL(churchDTO.Splashscreen, churchDTO.SplashScreenFormat);
                    }
                }
                if (churchDTO.Address != null)
                {
                    string address = string.Empty;
                    if (churchDTO.City != null && churchDTO.State != null && churchDTO.Country != null && churchDTO.Zip != null)
                    {
                         address = churchDTO.Address + "," + churchDTO.City + ","  + churchDTO.Country + "," + churchDTO.Zip;
                    }
                    else
                    {
                        address = churchDTO.Address;
                    }
                    var locationService = new GoogleLocationService();
                    var point = locationService.GetLatLongFromAddress(address);
                    if (point != null)
                    {
                        churchDTO.Longitude = Convert.ToString(point.Longitude);
                        churchDTO.Latitude = Convert.ToString(point.Latitude);
                    }
                }
                if (churchDTO.Banners == string.Empty)
                {
                    churchDTO.Banners = "";
                    churchDTO.BannerFormat = "jpg";
                }
                if (churchDTO.Splashscreen == string.Empty)
                {
                    churchDTO.Splashscreen = "";
                    churchDTO.SplashScreenFormat = "jpg";
                }

                int churchid = ChurchDAL.CreateChurch(churchDTO);
                if (churchid == -1)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.UserExistWithEmail;
                }
                else
                    if (churchid > 0)
                    {

                        var church = ChurchDAL.GetChurch(churchid);
                        if (!WebSecurity.UserExists(churchDTO.Username))
                        {
                            WebSecurity.CreateUserAndAccount(churchDTO.Username, churchDTO.Password);
                        }
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = church;
                        Result.Message = CustomConstants.ChurchAddedSuccessfully;
                    }

                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.AccessDenied;
                    }
                //}
                //else
                //{
                //    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                //    Result.Message = CustomConstants.User_Exist_With_Email;
                //}

                trans.Commit();

                return Result;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPut]
        public Utility.CustomResponse EditChurch(ChurchDTO churchDTO)
        {
            SqlConnection conn = ChurchDAL.ConnectTODB();
            SqlTransaction trans = conn.BeginTransaction();
            SqlCommand command = conn.CreateCommand();
            command.Transaction = trans;
            try
            {
                if (!(churchDTO.Banners.ToString() == string.Empty))
                {
                    if (!churchDTO.Banners.StartsWith("http"))
                    {
                        churchDTO.Banners = GetImageURL(churchDTO.Banners, churchDTO.BannerFormat);
                    }
                }
                if (!(churchDTO.Splashscreen.ToString() == string.Empty))
                {
                    if (!churchDTO.Splashscreen.StartsWith("http"))
                    {
                        churchDTO.Splashscreen = GetImageURL(churchDTO.Splashscreen, churchDTO.SplashScreenFormat);
                    }
                }

                if (WebSecurity.UserExists(churchDTO.Username))
                {

                    int churchid = ChurchDAL.EditChurch(churchDTO);
                    if (churchid == -1)
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.UserExistWithEmail;
                    }
                    else
                        if (churchid > 0)
                        {
                            var church = ChurchDAL.GetChurch(churchid);
                            Result.Status = Utility.CustomResponseStatus.Successful;
                            Result.Response = church;
                            Result.Message = CustomConstants.ChurchUpdatedSuccessfully;
                        }

                        else
                        {
                            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                            Result.Message = CustomConstants.AccessDenied;
                        }
                }

                trans.Commit();
                return Result;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpDelete]
        public Utility.CustomResponse DeleteChurch(int ChurchId)
        {
            try
            {
                int churchid = ChurchDAL.DeleteChurch(ChurchId);

                if (churchid > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchid;
                    Result.Message = CustomConstants.ChurchDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse CreateAnnouncement(AnnouncementDTO announcementDTO)
        {
            try
            {
                int announcementid = ChurchDAL.CreateAnnouncement(announcementDTO);

                if (announcementid > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = announcementid;
                    Result.Message = CustomConstants.AnnouncementsAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPut]
        public Utility.CustomResponse EditAnnouncement(AnnouncementDTO announcementDTO)
        {
            try
            {
                int announceId = ChurchDAL.EditAnnouncement(announcementDTO);

                if (announceId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = announceId;
                    Result.Message = CustomConstants.AnnouncementsUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteAnnouncement(int announcementId)
        {
            try
            {
                int announceId = ChurchDAL.DeleteAnnouncement(announcementId);

                if (announceId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = announceId;
                    Result.Message = CustomConstants.AnnouncementsDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DBInsertVerseOftheDay(VerseDTO verseDTO)
        {
            try
            {

                ChurchDAL.InsertVerse(verseDTO);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.VerseInsertedSuccessfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
       

        [HttpGet]
        public Utility.CustomResponse GetVerseOftheDay()
        {
            try
            {

                VerseDTO verse = ChurchDAL.GetVerseOftheDay();
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = verse;
                Result.Message = CustomConstants.DetailsGetSuccessfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        [HttpGet]
        public Utility.CustomResponse GetLastModifiedDate()
        {
            try
            {

                DateTime lastModifiedDate =  ChurchDAL.GetLastModifiedDate();
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.VerseInsertedSuccessfully;
                Result.Response = lastModifiedDate;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        [HttpPost]
        public Utility.CustomResponse InsertVerseOftheDay()
        {
            try
            {

                ChurchDAL.InsertVerseOftheDay();
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.VerseInsertedSuccessfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse RegisterDevice(DeviceDTO deviceDTO)
        { 
            try
            {
              
                var deviceId = ChurchDAL.RegisterDevice(deviceDTO);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.DeviceRegisteredSuccessfully;
                Result.Response = deviceId;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddPushNotifications(string message, int badge)
        {

            // string filename = GetFileURL("D:\\iKKOSAdminPushCertificates.p12");
            try
            {
                //   rating.UserId =Convert.ToInt32(Userid);
                ChurchDAL.AddPushNotifications(message, badge);
                Result.Status = Utility.CustomResponseStatus.Successful;
               // Result.Message = CustomConstants.PushNotifications;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetCountValues(int ChurchId)
        {
            try
            {

                var Counts = ChurchDAL.GetCountValues(ChurchId);
                if (Counts != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Counts;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        public string GetImageURL(string binary, string format)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond + format;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(binary);

                //using (var fileStream = binarydata)
                //{
                blockBlob.Properties.ContentType = format;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                imageurl = blockBlob1.Uri.ToString();
               
            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
            }

            return imageurl;
        }


        internal LatLongDTO GetLatLongValues(string address)
        {
            LatLongDTO latlong = new LatLongDTO();
            string url = "http://maps.googleapis.com/maps/api/geocode/" + "xml?address="+address+"&sensor=false";

            WebResponse response = null;
            bool is_geocoded = true;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            response = request.GetResponse();
            string lat = "";
            string lng = "";
            string loc_type = "";
            if (response != null)
            {
                XPathDocument document = new XPathDocument(response.GetResponseStream());
                XPathNavigator navigator = document.CreateNavigator();

                // get response status
                XPathNodeIterator statusIterator = navigator.Select("/GeocodeResponse/status");
                while (statusIterator.MoveNext())
                {
                    if (statusIterator.Current.Value != "OK")
                    {
                        is_geocoded = false;
                    }
                }

                // get results
                if (is_geocoded)
                {
                    XPathNodeIterator resultIterator = navigator.Select("/GeocodeResponse/result");
                    while (resultIterator.MoveNext())
                    {


                        XPathNodeIterator geometryIterator = resultIterator.Current.Select("geometry");
                        while (geometryIterator.MoveNext())
                        {
                            XPathNodeIterator locationIterator = geometryIterator.Current.Select("location");
                            while (locationIterator.MoveNext())
                            {
                                XPathNodeIterator latIterator = locationIterator.Current.Select("lat");
                                while (latIterator.MoveNext())
                                {
                                    lat = latIterator.Current.Value;
                                    latlong.Latitude = lat;
                                }

                                XPathNodeIterator lngIterator = locationIterator.Current.Select("lng");
                                while (lngIterator.MoveNext())
                                {
                                    lng = lngIterator.Current.Value;
                                    latlong.Longitude = lng;

                                }
                                XPathNodeIterator locationTypeIterator = geometryIterator.Current.Select("location_type");
                                while (locationTypeIterator.MoveNext())
                                {
                                    loc_type = locationTypeIterator.Current.Value;
                                }
                            }

                        }
                    }

                }

            }

            return latlong;
        }

         

        internal string GetAddressWithLatLong(string latitude,string longitude)
        {
          
            string url = "http://maps.google.com/maps/api/geocode/xml?latlng=" + latitude + "," + longitude + "&sensor=false";

            WebResponse response = null;
            bool is_geocoded = true;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            response = request.GetResponse();
            string address = "";
          
            if (response != null)
            {
                XPathDocument document = new XPathDocument(response.GetResponseStream());
                XPathNavigator navigator = document.CreateNavigator();

                // get response status
                XPathNodeIterator statusIterator = navigator.Select("/GeocodeResponse/status");
                while (statusIterator.MoveNext())
                {
                    if (statusIterator.Current.Value != "OK")
                    {
                        is_geocoded = false;
                    }
                }

                // get results
                if (is_geocoded)
                {
                    XPathNodeIterator resultIterator = navigator.Select("/GeocodeResponse/result");
                    if(resultIterator.MoveNext())
                    {


                        XPathNodeIterator formatted_address_iterator = resultIterator.Current.Select("formatted_address");
                       if (formatted_address_iterator.MoveNext())
                        {

                            address = formatted_address_iterator.Current.Value;
 
                        }
                    }

                }

            }

            return address;
        }

        internal List<ApiDataDTO> GetZipDataFromLatLong(string latitude, string longitude)
        {
            List<ApiDataDTO> objApiDataDTO = new List<ApiDataDTO>();
            
            Newtonsoft.Json.Linq.JArray resp1 = null;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://api.zip-codes.com/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // HTTP GET
            string content = string.Empty;
            HttpResponseMessage response = client.GetAsync("/ZipCodesAPI.svc/1.0/FindZipCodesInRadius/ByLatLon?latitude=" + latitude + "&longitude=" + longitude + "&maximumradius=15&minimumradius=0&key=ITDS4E4MSYGL3LEJ7U8G").Result;
            if (response.IsSuccessStatusCode)
            {
                Stream st = response.Content.ReadAsStreamAsync().Result;
                StreamReader reader = new StreamReader(st);
                content = reader.ReadToEnd();
                var values = JsonConvert.DeserializeObject<dynamic>(content);
                var resp = values["DataList"];
                if (resp != null)
                {
                    foreach (var p in resp)
                    {
                        ApiDataDTO obj = new ApiDataDTO();
                        obj.Code = p.Code;
                        obj.City = p.City;
                        obj.State = p.State;
                        obj.Latitude = p.Latitude;
                        obj.Longitude = p.Longitude;
                        obj.Distance = p.Distance;
                        obj.County = p.County; 
                        objApiDataDTO.Add(obj);

                    }
                }
            }
            return objApiDataDTO.ToList();
        }


        [HttpGet]
        public Utility.CustomResponse TermsandConditions()
        {
            try
            {
                string TermsURL = ConfigurationManager.AppSettings["TermsURL"].ToString();
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = TermsURL;
                        Result.Message = CustomConstants.DetailsGetSuccessfully;
                 
                
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ProgramSheet(int ChurchId)
        {
            try
            {

                var prgSheet = ChurchDAL.GetProgramSheet(ChurchId);
                if (prgSheet.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = prgSheet;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = null;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ProgramSheetPortal(int ChurchId)
        {
            try
            {

                var prgSheet = ChurchDAL.GetProgramSheetPortal(ChurchId);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = prgSheet;
                Result.Message = CustomConstants.DetailsGetSuccessfully;


                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse ProgramSheetDetails(int ProgramSheetId)
        {
            try
            {

                var prgSheetDet = ChurchDAL.GetProgramSheetDetails(ProgramSheetId);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = prgSheetDet;
                Result.Message = CustomConstants.DetailsGetSuccessfully;


                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse CreateProgramSheet(ProgramSheetDTO programSheetDTO)
        {
            try
            {
                int programId = ChurchDAL.CreateProgramSheet(programSheetDTO);

                if (programId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = programId;
                    Result.Message = CustomConstants.ProgramSheetAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse CreateProgramSheetDetails(List<ProgramSheetDetailsDTO> programSheetDetailsDTO)
        {
            try
            {

                int numOfRows = ChurchDAL.DeleteProgramSheetDetails(programSheetDetailsDTO[0].ProgramSheetId);
                for (int i = 0; i < programSheetDetailsDTO.Count; i++)
                {
                    int programId = ChurchDAL.CreateProgramSheetDetails(programSheetDetailsDTO[i]);
                    if (programId > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = programId;
                        Result.Message = CustomConstants.ProgramSheetAddedSuccessfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.AccessDenied;
                    }
                }
               

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteProgramSheet(int ProgramSheetId)
        {
            try
            {
                int programSheetId = ChurchDAL.DeleteProgramSheet(ProgramSheetId);

                if (programSheetId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = programSheetId;
                    Result.Message = CustomConstants.ProgramSheetDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse UpdateChurchEnabled(int churchId, Boolean enabled)
        {
            try
            {
                int churchid = ChurchDAL.UpdateChurchEnabled(churchId, enabled);

                if (churchid > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchid;
                    Result.Message = CustomConstants.ChurchEnableSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
         

        public Utility.CustomResponse GetUserSubscription(int ChurchId)
        {
            try
            {
           string customerkey = System.Configuration.ConfigurationManager.AppSettings["CustomerKey"].ToString();
           string customersecret = System.Configuration.ConfigurationManager.AppSettings["CustomerSecret"].ToString();

                bool isaccess = false;
                string content = string.Empty;
                string content1 = string.Empty;
                CustomerDTO customerDTO = new CustomerDTO();
                 
                var church = ChurchDAL.GetChurch(ChurchId);
                string api = "wc-api/v2/customers/email/" + church.Username + "?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                //string api = "/demo/wc-api/v2/customers/email/" + church.Username + "?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                JavaScriptSerializer serializer1 = new JavaScriptSerializer();
                serializer1.MaxJsonLength = 1000000000;
                Newtonsoft.Json.Linq.JArray resp1 = null;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://www.rohanlam.com/churchnexus_copy"); //http:www.churchnexus.com
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync(api).Result;
                if (response.IsSuccessStatusCode)
                {
                    Stream st = response.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(st);
                    content = reader.ReadToEnd();
                    if (content.Contains("errors"))
                    {
                        isaccess = false;
                    }
                    else
                    {
                        customerDTO = JsonDeserialize<CustomerDTO>(content);
                        if (customerDTO != null)
                        {
                            int custid = customerDTO.customer.id;
                            string downloadApi = "wc-api/v2/customers/" + custid + "/downloads?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                            // string downloadApi = "/demo/wc-api/v2/customers/" + custid + "/downloads?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                            HttpResponseMessage response1 = client.GetAsync(downloadApi).Result;
                            if (response1.IsSuccessStatusCode)
                            {
                                Stream st1 = response1.Content.ReadAsStreamAsync().Result;
                                StreamReader reader1 = new StreamReader(st1);
                                content1 = reader1.ReadToEnd();
                                DownloadDTO downloadDTO = JsonDeserialize<DownloadDTO>(content1);
                                List<Download> download = downloadDTO.downloads.ToList();
                                if (download.Count == 0)
                                {
                                    isaccess = false;
                                }
                                else
                                {
                                    string accessExpries = download.First().access_expires;
                                    if (Convert.ToDateTime(accessExpries).AddDays(1) >= DateTime.Now)
                                    {
                                        isaccess = true;
                                    }
                                }

                            }

                        }
                    }

                }


                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = isaccess;


                // todo: add other conditions
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        public static T JsonDeserialize<T>(string jsonString)
        {
            System.Runtime.Serialization.Json.DataContractJsonSerializer ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        [HttpGet]
        public Utility.CustomResponse GetChurchByName(string churchName)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var church = ChurchDAL.GetChurchByName(churchName);
                if (church.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = church;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
    }
      
}
