﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class PrayersController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse  Prayers(int churchid)
        {
            try
            {
                
                List<PrayersDTO> prayers = ChurchDAL.GetAllPrayers(churchid);
                if (prayers.ToList().Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = prayers;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {

                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetPrayer(int prayerid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var prayer = ChurchDAL.GetPrayer(prayerid);
                if (prayer.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = prayer;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse CreatePrayer(PrayersDTO prayerDTO)
        {
            try
            {
                int prayerId = ChurchDAL.CreatePrayer(prayerDTO);

                if (prayerId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = prayerId;
                    Result.Message = CustomConstants.PrayerAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
              
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; 
            }
        }

    }
}
