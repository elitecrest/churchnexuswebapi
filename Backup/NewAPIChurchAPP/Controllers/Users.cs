﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Http;
using WebMatrix.WebData;

namespace NewAPIChurchAPP.Controllers
{
    public class UsersController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse CreateUser(UsersDTO usersDTO)
        {
            try
            {
                int userId = ChurchDAL.CreateUser(usersDTO);

                if (userId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = userId;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
             
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; 
            }
        }

        [HttpPost]
        public Utility.CustomResponse PaymentTracking(PaymentTrackingDTO paymentTrackingDTO)
        {
            try
            {
                int PaymentID = ChurchDAL.CreatePaymentTracking(paymentTrackingDTO);

                if (PaymentID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = PaymentID;
                    Result.Message = CustomConstants.PaymentTrackingAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        // Add Usage Tracking
        [HttpPost]
        public Utility.CustomResponse UsageTracking(IEnumerable<UsageDTO> usageDTO)
        {
            try
            {
                foreach (var u in usageDTO)
                {
                    ChurchDAL.AddUsageTracking(u);
                }


                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.UsageTrackingSuccessfully;


            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetUsersCount(int ChurchId)
        {
            try
            {

                int AppUsersCount = ChurchDAL.GetUsersCount(ChurchId);
                if (AppUsersCount != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = AppUsersCount;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
             
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse ForgotPassword(string userName)
        {
            //algoritham 1. we need to search for corresponding email id and prepare message and send it to related email.
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                if (WebSecurity.UserExists(userName))
                {
                    string token = WebSecurity.GeneratePasswordResetToken(userName);

                    var emailAddress = ChurchDAL.GetEmailByUserId(userName);
                    //need to send a mail to user

                    if (SendMail(userName, token, emailAddress) == 0)
                    {
                        res.Status = Utility.CustomResponseStatus.Successful;
                        res.Message = CustomConstants.ForgotPasswordSuccessfully;
                    }
                    else
                    {
                        res.Status = Utility.CustomResponseStatus.UnSuccessful;
                        res.Message = Message;
                    }
                }
                else
                {
                    res.Status = Utility.CustomResponseStatus.UnSuccessful;
                    res.Message = CustomConstants.UserDoesNotExist;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }

            return res;
        }

        [HttpGet] //For Woocommerce site
        public Utility.CustomResponse UpdatePassword(string userName, string password)
        {
           
            bool isReset = false;
            try
            {
                if (WebSecurity.UserExists(userName))
                {
                    string token = WebSecurity.GeneratePasswordResetToken(userName);

                    isReset = WebSecurity.ResetPassword(token, password);
                    if (isReset)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Message = CustomConstants.ResetPassword;

                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.ResetPasswordFailed;
                    }

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.UserDoesNotExist;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }

            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse ResetPassword(string token, string confirmPassword)
        {
            bool isReset = false;
            try
            {
                isReset = WebSecurity.ResetPassword(token, confirmPassword);
                if (isReset)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.ResetPassword;

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.ResetPasswordFailed;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
            return Result;
        }

        public static int SendMail(string username, string token, string emailAddress)
        {
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ResetPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$Token$$", token);
                //   myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + emailAddress + ""));
                Msg.Subject = " Password Reset REquested";
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password"]),
                    Timeout = 1000000
                };
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                smtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ChangePassword(string username, string currentpassword, string newpassword)
        {
            try
            {
                if (WebSecurity.ChangePassword(username,currentpassword,newpassword))
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.UpdateUserUpdateSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.PasswordMisMatch;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse InAppPurchase(InAppPurchaseDTO inAppPurchaseDto)
        {
            try
            {
                int PaymentID = ChurchDAL.CreatePaymentsTracking(inAppPurchaseDto);

                if (PaymentID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = PaymentID;
                    Result.Message = CustomConstants.PaymentTrackingAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
    }
}
