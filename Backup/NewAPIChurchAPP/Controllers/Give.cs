﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class GiveController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse Offerings(int ChurchId,int Flag)
        {
            try
            {

                List<OfferingsDTO> Offerings = ChurchDAL.GetAllOfferings(ChurchId, Flag);
                if (Offerings.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Offerings;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {

                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetOfferings(int OfferingsId)
        {
            try
            {

                OfferingsDTO  Offerings = ChurchDAL.GetOfferings(OfferingsId);
                if (Offerings.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Offerings;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {

                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse CreateOfferings(OfferingsDTO OfferingsDTO)
        {
            try
            {
                int OfferingsID = ChurchDAL.CreateOfferings(OfferingsDTO);

                if (OfferingsID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = OfferingsID;
                    Result.Message = CustomConstants.OfferingsAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPut]
        public Utility.CustomResponse EditOfferings(OfferingsDTO OfferingsDTO)
        {
            try
            {
                int OfferingsID = ChurchDAL.EditOfferings(OfferingsDTO);

                if (OfferingsID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = OfferingsID;
                    Result.Message = CustomConstants.OfferingsUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteOfferings(int OfferingsId)
        {
            try
            {
                int offeringsId = ChurchDAL.DeleteOfferings(OfferingsId);

                if (offeringsId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = offeringsId;
                    Result.Message = CustomConstants.OfferingsDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

    }
}
