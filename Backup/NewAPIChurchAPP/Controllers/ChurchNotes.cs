﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace NewAPIChurchAPP.Controllers
{
    public class ChurchNotesController : ApiController
    {
        //
        // GET: /ChurchNotes/

        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetNotes(int churchId)
        {
            try
            {

                List<ChurchNotesDto> notes = ChurchDAL.GetNotesByChurchId(churchId);
                if (notes.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = notes;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpPost]
        public Utility.CustomResponse CreateNotes(ChurchNotesDto churchNotesDto)
        {
            try
            {

                var notes = ChurchDAL.CreateChurchNotes(churchNotesDto);

                if (notes > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = notes;
                    Result.Message = CustomConstants.NotesAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpPost]
        public Utility.CustomResponse UpdateNotes(ChurchNotesDto churchNotesDto)
        {
            try
            {

                var notes = ChurchDAL.UpdateChurchNotes(churchNotesDto);

                if (notes > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = notes;
                    Result.Message = CustomConstants.NotesUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetChurchNotes(int churchId)
        {
            try
            {

                var notes = ChurchDAL.GetChurchNotesByChurchId(churchId);
                if (notes.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = notes;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
