﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.WindowsAzure.MediaServices.Client;
using System.Threading;

namespace NewAPIChurchAPP
{
    public partial class UploadedFiles : System.Web.UI.Page
    {

        Utility.CustomResponse Result = new Utility.CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Result = Handlefile();
                var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                string jsonString = javaScriptSerializer.Serialize(Result);
                Response.Write(jsonString);
            }
        }

        //protected void Button1_Click(object sender, EventArgs e)
        //{
        //    Result = Handlefile();
        //    var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //    string jsonString = javaScriptSerializer.Serialize(Result);
        //    Response.Write(jsonString);

        //}

        private Utility.CustomResponse Handlefile()
        {
            string baseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            int length = (int)Request.InputStream.Length; 
            byte[] buffer = new byte[length]; 
            Stream stream = Request.InputStream;
            Sermons sermons = new Sermons();
            string sermonformat = Request.QueryString["sermonformat"].Trim();
            //string sermonformat = "stream";
            string ext = Request.QueryString["ext"].Trim();
            //string ext = "mp3";

            string sermonurl = streamaudio(stream, ext); 
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

             
                sermons.Id = 0; 
                sermons.Name = Request.QueryString["Name"].Trim();
                //sermons.Name = "RaviKiran";
                sermons.Description = Request.QueryString["Description"].Trim();
                //sermons.Description = "Testing Sermon audio files";
                sermons.Enabled = Request.QueryString["Enabled"].ToString().Contains("true") ? true : false;
                //sermons.Enabled = true;
                sermons.Author = Convert.ToString(Request.QueryString["Author"]);
                //sermons.Author = "RaviKiranManda";
                sermons.SeriesId = Convert.ToInt32(Request.QueryString["SeriesId"]);
                //sermons.SeriesId = 3508;
                sermons.GalleryId = Convert.ToInt16(Request.QueryString["GalleryId"]);
                sermons.SermonUrl = sermonurl;
                sermons.Updated = Convert.ToDateTime(Request.QueryString["Updated"].ToString());
                //sermons.Updated = DateTime.Now;
                sermons.Duration = "1";// Convert.ToDateTime(Request.Form["Ends"].ToString()); 
                sermons.SermonFormat = sermonformat;

                var response = client.PostAsJsonAsync("Sermons/CreateSermon", sermons).Result;

                if (response.IsSuccessStatusCode)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermons;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;

                   
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                 
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
        }
      

        public string streamaudio(Stream stream, string format)
        {

            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);

            var fileStream = System.IO.File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();

            var ext = format;
             
            Stream fileStreambanners = stream;

            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string sermonURL = UploadImage1(fileNamebanners, path, ext, fileStreambanners);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);

            }
            return sermonURL;
        }

        public string UploadImage(string fileName, string path, string ext, Stream InputStream, string ContentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

             

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                string uniqueBlobName = string.Format(fileName, Guid.NewGuid().ToString(), ext);
                CloudBlockBlob blob = container.GetBlockBlobReference(uniqueBlobName);
                blob.Properties.ContentType = ContentType;
                blob.UploadFromStream(InputStream);


                imageurl = blob.Uri.ToString();

            }
            catch (Exception ex)
            {

            }

            return imageurl;

        }


         public string UploadImage1(string fileName, string path, string ext, Stream InputStream)
         {

             string account = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountName"].ToString();
             string key = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountKey"].ToString();

             CloudMediaContext context = new CloudMediaContext(account, key);

             var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

             var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));

             assetFile.Upload(path);
             var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
             var daysForWhichStreamingUrlIsActive = 365;
             var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
             var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                                                      AccessPermissions.Read);
             string streamingUrl = string.Empty;
             var assetFiles = streamingAsset.AssetFiles.ToList();
             var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith("." + ext)).FirstOrDefault();
             if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
             {
                 var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                 var mp4Uri = new UriBuilder(locator.Path);
                 mp4Uri.Path += "/" + streamingAssetFile.Name;
                 streamingUrl = mp4Uri.ToString();
             }
             return streamingUrl;
 
         }
        
     }


         


    public partial class Sermons
    {
        public Int32 Id { get; set; }
        public Int32 SeriesId { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public Boolean Enabled { get; set; }
        public String Description { get; set; }
        public String SermonUrl { get; set; }
        public String Duration { get; set; }
        public DateTime? Updated { get; set; }
        public Int16? GalleryId { get; set; }
        public String SermonFormat { get; set; }
        public Stream stream { get; set; }
    }
}