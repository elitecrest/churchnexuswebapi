﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[subscription_coupons](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[coupon_code] [varchar](25) NULL,
	[days] [int] NULL,
	[expiry_date] [datetime] NULL,
	[created_date] [datetime] NULL,
	[is_redeemed] [bit] NULL,
	[redeemed_church_id] [int] NULL,
	[status] [bit] NULL,
 CONSTRAINT [PK_subscription_coupons] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[subscription_coupons] ADD  CONSTRAINT [DF_subscription_coupons_created_date]  DEFAULT (getdate()) FOR [created_date]
GO

ALTER TABLE [dbo].[subscription_coupons] ADD  CONSTRAINT [DF_subscription_coupons_is_redeemed]  DEFAULT ((0)) FOR [is_redeemed]
GO

ALTER TABLE [dbo].[subscription_coupons] ADD  CONSTRAINT [DF_subscription_coupons_status]  DEFAULT ((0)) FOR [status]
GO

ALTER TABLE [dbo].[subscription_coupons]  WITH CHECK ADD  CONSTRAINT [FK_church_subscription_coupons] FOREIGN KEY([redeemed_church_id])
REFERENCES [cp].[churches] ([id])
GO

ALTER TABLE [dbo].[subscription_coupons] CHECK CONSTRAINT [FK_church_subscription_coupons]
GO

ALTER TABLE [dbo].[subscription_coupons]  WITH CHECK ADD  CONSTRAINT [FK_subscription_coupons_subscription_coupons] FOREIGN KEY([id])
REFERENCES [dbo].[subscription_coupons] ([id])
GO

ALTER TABLE [dbo].[subscription_coupons] CHECK CONSTRAINT [FK_subscription_coupons_subscription_coupons]
GO

