﻿ 

/****** Object:  StoredProcedure [cp].[AddChurchItems]    Script Date: 9/16/2017 1:08:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[AddSubscriptionCoupon] 
	-- Add the parameters for the stored procedure here
	@coupon_code varchar(25),
	@days int,
	@expiry_date datetime,
	@Id int output
AS
BEGIN
	SET NOCOUNT ON;
	insert into dbo.subscription_coupons([coupon_code], [days],[expiry_date])
	values(@coupon_code, @days, @expiry_date)

	set @Id = @@Identity
END


GO


