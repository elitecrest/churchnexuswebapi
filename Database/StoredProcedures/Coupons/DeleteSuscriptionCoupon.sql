﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeleteSubscriptionCoupon] 
	-- Add the parameters for the stored procedure here
	@id bigint
AS
BEGIN
	SET NOCOUNT ON;
	update dbo.subscription_coupons
	set [status]=0
	where [id]=@id
END

GO
