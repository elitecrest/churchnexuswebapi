﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[AddSubscriptionCouponToChurch] 
	-- Add the parameters for the stored procedure here
	@churchId int,
	@coupon_code varchar(25),
	@status int output
AS
BEGIN
	Declare @coupon varchar(25);
	select @coupon = coupon_code 
	from dbo.subscription_coupons sc where sc.coupon_code=@coupon_code and sc.status=1  and sc.is_redeemed =0 and sc.expiry_date > getdate()

	IF(@coupon IS NOT NULL)
	BEGIN
	update dbo.subscription_coupons set redeemed_church_id=@churchId , [is_redeemed] =1 where coupon_code=@coupon
	set @status=0
	END
	ELSE
	BEGIN
	set @status=-1
	END

END

GO