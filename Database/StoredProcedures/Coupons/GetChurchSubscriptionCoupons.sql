﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetChurchSubscriptionCoupons] 
	-- Add the parameters for the stored procedure here
	@churchId int
AS
BEGIN
	SET NOCOUNT ON;
	select *,c.name from dbo.subscription_coupons sc
	left join [cp].[churches] c on c.id=sc.redeemed_church_id
	where [redeemed_church_id]=@churchId and [status]=1 and [is_redeemed]=1
END

GO