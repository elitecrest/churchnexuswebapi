﻿-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdateSubscriptionCoupon] 
	-- Add the parameters for the stored procedure here
	@id bigint,
	@coupon_code varchar(25),
	@days int,
	@expiry_date datetime
AS
BEGIN
	SET NOCOUNT ON;
	update dbo.subscription_coupons
	set [coupon_code]=@coupon_code,
	    [days]=@days,
		[expiry_date]=@expiry_date
	where [id]=@id
END

GO