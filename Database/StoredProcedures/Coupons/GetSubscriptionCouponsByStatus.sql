﻿
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetSubscriptionCouponsByStatus] 
	-- Add the parameters for the stored procedure here
	@status bigint
AS
BEGIN
	SET NOCOUNT ON;
	select sc.id,sc.coupon_code,sc.days,sc.expiry_date,sc.created_date,sc.is_redeemed,sc.redeemed_church_id,c.name from dbo.subscription_coupons sc
	left join [cp].[churches] c on c.id=sc.redeemed_church_id
	where [status]=@status
END

GO
