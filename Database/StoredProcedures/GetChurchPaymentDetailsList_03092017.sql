﻿USE [ChurchNexus]
GO
/****** Object:  StoredProcedure [cp].[GetChurchPaymentDetailsList]    Script Date: 9/3/2017 3:26:35 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [cp].[GetChurchPaymentDetailsList] --102282
@churchid int  
AS
BEGIN
	 
	SET NOCOUNT ON;

	 --select * from cp.Payment where churchid = @churchid

	 select mp.id, mp.church_id as ChurchId,mp.donor_name as Name,mp.donor_email as Email,mp.payment_service_transactionId as TransactionId,mp.donation_cause as PaymentType, mp.donation_amount as Amount,mp.payment_date as CreatedDate,mp.[donation_description] as DonationDescription,mp.[donor_address] as Address,dc.donation_cause as Productinfo
	 from [cp].[membertochurchpayments] mp
	 left join [dbo].[church_donation_cause] dc
	 on mp.donation_cause= dc.id
	 where mp.church_id=@churchid and [payment_status] =1
	  order by id desc
END