﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [cp].[GetCountryPlanIdByCountryAndPlanId]
 @planId int,
 @countryCode varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
select cp1.ID from  [cp].[Country_Plans] cp
inner join [cp].[plans] p on p.ID=cp.PlanId
inner join [cp].[Country_Plans] cp1 on p.ID=cp1.PlanId
inner join [cp].[country] c on cp1.Country=c.code
where cp.ID=@planId and c.name= @countryCode

END
 
GO