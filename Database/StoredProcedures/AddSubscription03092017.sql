﻿ ALTER PROCEDURE [cp].[AddSubscriptionInfo]
	-- Add the parameters for the stored procedure here
	@church_id int,
	@planid int,
	@paidamount decimal,
	@paymentorderid nvarchar(255),
	@subscriptionMonth datetime,
	@isPartialPayment bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	insert into  [dbo].[subscriptionsInfo](churchId, planId, paidPlanAmount, payment_order_id,subscriptionMonth, payment_date,is_partial_payment)
	values(@church_id,@planid, @paidamount, @paymentorderid,@subscriptionMonth,getutcdate(),@isPartialPayment)
END
SET ANSI_NULLS ON
