﻿<?xml version="1.0" encoding="utf-8"?>
<serviceModel xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" name="DailyWorkerRole" generation="1" functional="0" release="0" Id="f3c1bfb5-242f-4c18-a911-4d989eee376b" dslVersion="1.2.0.0" xmlns="http://schemas.microsoft.com/dsltools/RDSM">
  <groups>
    <group name="DailyWorkerRoleGroup" generation="1" functional="0" release="0">
      <settings>
        <aCS name="DailyTasksWorker:APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="">
          <maps>
            <mapMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/MapDailyTasksWorker:APPINSIGHTS_INSTRUMENTATIONKEY" />
          </maps>
        </aCS>
        <aCS name="DailyTasksWorker:BaseUrl" defaultValue="">
          <maps>
            <mapMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/MapDailyTasksWorker:BaseUrl" />
          </maps>
        </aCS>
        <aCS name="DailyTasksWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="">
          <maps>
            <mapMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/MapDailyTasksWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </maps>
        </aCS>
        <aCS name="DailyTasksWorkerInstances" defaultValue="[1,1,1]">
          <maps>
            <mapMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/MapDailyTasksWorkerInstances" />
          </maps>
        </aCS>
      </settings>
      <maps>
        <map name="MapDailyTasksWorker:APPINSIGHTS_INSTRUMENTATIONKEY" kind="Identity">
          <setting>
            <aCSMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/DailyTasksWorker/APPINSIGHTS_INSTRUMENTATIONKEY" />
          </setting>
        </map>
        <map name="MapDailyTasksWorker:BaseUrl" kind="Identity">
          <setting>
            <aCSMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/DailyTasksWorker/BaseUrl" />
          </setting>
        </map>
        <map name="MapDailyTasksWorker:Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" kind="Identity">
          <setting>
            <aCSMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/DailyTasksWorker/Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" />
          </setting>
        </map>
        <map name="MapDailyTasksWorkerInstances" kind="Identity">
          <setting>
            <sCSPolicyIDMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/DailyTasksWorkerInstances" />
          </setting>
        </map>
      </maps>
      <components>
        <groupHascomponents>
          <role name="DailyTasksWorker" generation="1" functional="0" release="0" software="E:\WorkingCopies\churchnexuswebapi\DailyWorkerRole\csx\Release\roles\DailyTasksWorker" entryPoint="base\x64\WaHostBootstrapper.exe" parameters="base\x64\WaWorkerHost.exe " memIndex="-1" hostingEnvironment="consoleroleadmin" hostingEnvironmentVersion="2">
            <settings>
              <aCS name="APPINSIGHTS_INSTRUMENTATIONKEY" defaultValue="" />
              <aCS name="BaseUrl" defaultValue="" />
              <aCS name="Microsoft.WindowsAzure.Plugins.Diagnostics.ConnectionString" defaultValue="" />
              <aCS name="__ModelData" defaultValue="&lt;m role=&quot;DailyTasksWorker&quot; xmlns=&quot;urn:azure:m:v1&quot;&gt;&lt;r name=&quot;DailyTasksWorker&quot; /&gt;&lt;/m&gt;" />
            </settings>
            <resourcereferences>
              <resourceReference name="DiagnosticStore" defaultAmount="[4096,4096,4096]" defaultSticky="true" kind="Directory" />
              <resourceReference name="EventStore" defaultAmount="[1000,1000,1000]" defaultSticky="false" kind="LogStore" />
            </resourcereferences>
          </role>
          <sCSPolicy>
            <sCSPolicyIDMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/DailyTasksWorkerInstances" />
            <sCSPolicyUpdateDomainMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/DailyTasksWorkerUpgradeDomains" />
            <sCSPolicyFaultDomainMoniker name="/DailyWorkerRole/DailyWorkerRoleGroup/DailyTasksWorkerFaultDomains" />
          </sCSPolicy>
        </groupHascomponents>
      </components>
      <sCSPolicy>
        <sCSPolicyUpdateDomain name="DailyTasksWorkerUpgradeDomains" defaultPolicy="[5,5,5]" />
        <sCSPolicyFaultDomain name="DailyTasksWorkerFaultDomains" defaultPolicy="[2,2,2]" />
        <sCSPolicyID name="DailyTasksWorkerInstances" defaultPolicy="[1,1,1]" />
      </sCSPolicy>
    </group>
  </groups>
</serviceModel>