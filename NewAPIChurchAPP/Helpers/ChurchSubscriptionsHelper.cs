﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NewAPIChurchAPP.Models;

namespace NewAPIChurchAPP.Helpers
{
    /// <summary>
    /// Helper class for Church Subscriptions
    /// </summary>
    public sealed class ChurchSubscriptionsHelper
    {

        /// <summary>
        /// Singleton instance reference to class
        /// </summary>
        private static ChurchSubscriptionsHelper instance;

        #region Constructors

        /// <summary>
        /// Creates a singlton instance of <see cref="ChurchSubscriptionsHelper"/>
        /// </summary>
        private ChurchSubscriptionsHelper()
        {
        }

        public static ChurchSubscriptionsHelper GetInstance()
        {
            if (null == instance)
            {
                instance = new ChurchSubscriptionsHelper();
            }

            return instance;
        }

        #endregion Constructors

        #region Methods

        /// <summary>
        /// Validates ChurchSubscription and returns a flag to indicate the same.
        /// </summary>
        /// <returns>True / False</returns>
        public void ValidateChurchSubscription(ChurchDTO model)
        {
            var _validityDate = model.SignupDate.Value.AddDays(model.totalPromotionDays);

            var subscriptionStartDate = GetSubscriptionStartDate(model);

            if (_validityDate > DateTime.Now)
            {
                model.HasValidSubscription = true;
            }
            else
            {

                // first month subscription 
                if (DateTime.Now.Month < subscriptionStartDate.Month && DateTime.Now.Year <= subscriptionStartDate.Year)
                {
                    model.HasValidSubscription = true;
                }
                else
                model.HasValidSubscription = ChurchDAL.HasValidSubscription(model.Id, DateTime.Now);

            }

            if (model.HasValidSubscription == false)
            {
                try
                {
                    PopulateTotalDueAmount(subscriptionStartDate, model);

                }
                catch (Exception)
                {
                    // do nothing                     
                }
            }

        }

        /// <summary>
        /// Returns subscription start date of given church
        /// </summary>
        /// <returns></returns>
        private DateTime GetSubscriptionStartDate(ChurchDTO model)
        {
            var _validityDate = model.SignupDate.Value.AddDays(model.totalPromotionDays);

            var roundOffDate = _validityDate.AddMonths(1);

            return new DateTime(roundOffDate.Year, roundOffDate.Month, 1);
        }

        /// <summary>
        /// Returns total subscription pending amount
        /// </summary>
        /// <param name="subscriptionStartdate"></param>
        /// <param name="churchId"></param>
        /// <returns></returns>
        public void PopulateTotalDueAmount(DateTime subscriptionStartdate, ChurchDTO dto)
        {
            var subscriptions = ChurchDAL.GetSubscriptionsByChurch(dto.Id);
            double totalSubscriptionAmount = 0;
            if (dto.PlanId == 0)
            {
                throw new InvalidOperationException("Plan not configured");
            }

            dto.PlanAmount = ChurchDAL.GetPlanAmount(dto.PlanId, dto.Country);

            for (var year = subscriptionStartdate; year <= DateTime.Now; year = year.AddMonths(1))
            {

                if (!subscriptions.Any(o => o.SubscriptionMonth.Month == year.Month && o.SubscriptionMonth.Year == year.Year && o.PaymentStatus == true))
                {
                    totalSubscriptionAmount += Convert.ToDouble(dto.PlanAmount);
                }
                else if (subscriptions.Any(o => o.SubscriptionMonth.Month == year.Month && o.SubscriptionMonth.Year == year.Year && o.PaymentStatus == true && o.IsPartialPayment == true))
                {
                    var partialPaidAmount = subscriptions.Where(o => o.SubscriptionMonth.Month == year.Month && o.SubscriptionMonth.Year == year.Year && o.PaymentStatus == true).Select(p=> p.PaidPlanAmount).Sum();
                    totalSubscriptionAmount += Convert.ToDouble(dto.PlanAmount - partialPaidAmount);
                }
            }

            dto.TotalSubscriptionDue = totalSubscriptionAmount;
        }

        /// <summary>
        /// Calculates Subscription months from given amount
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public List<SubscriptionInfo> CalcSubscriptionMonthsFromPaidAmount(SubscriptionInfo info)
        {
            List<SubscriptionInfo> subscriptionsList = new List<SubscriptionInfo>();


            var churchInfo = ChurchDAL.GetChurch(info.ChurchId);
            var planAmount = ChurchDAL.GetPlanAmount(info.PlanId, churchInfo.Country);
            var subscriptions = ChurchDAL.GetSubscriptionsByChurch(info.ChurchId);

            var subscriptionStartdate = GetSubscriptionStartDate(churchInfo);

            // add partial payment , pending amount , if any 

            var partialPayments = subscriptions.Where(s => s.IsPartialPayment == true && s.PaymentStatus == true).ToList();

            partialPayments.ForEach(p => {
                var totalSubscriptionAmount = subscriptions.Where(s => s.SubscriptionMonth.Month == p.SubscriptionMonth.Month && s.SubscriptionMonth.Year == p.SubscriptionMonth.Year).Sum(s => s.PaidPlanAmount);
                if (totalSubscriptionAmount < planAmount)
                {
                    var amounttoBePaid = planAmount - totalSubscriptionAmount;
                    subscriptionsList.Add(new SubscriptionInfo
                    {
                        SubscriptionMonth = p.SubscriptionMonth,
                        ChurchId = info.ChurchId,
                        PaidPlanAmount = amounttoBePaid,
                        Remarks = info.Remarks,
                        PaymentOrderId = info.PaymentOrderId,
                        PlanId = info.PlanId,
                        IsPartialPayment=(amounttoBePaid<=info.PaidPlanAmount?false:true)
                    });

                    info.PaidPlanAmount -= subscriptionsList[0].PaidPlanAmount;
                }
            });

            for (var year = subscriptionStartdate; year <= DateTime.Now; year = year.AddMonths(1))
            {

                // get all pending subscriptions info into list
                if (!subscriptions.Any(o => o.SubscriptionMonth.Month == year.Month && o.SubscriptionMonth.Year == year.Year))
                {
                    if (info.PaidPlanAmount >= planAmount)
                    {
                        subscriptionsList.Add(new SubscriptionInfo
                        {
                            ChurchId = info.ChurchId,
                            PaidPlanAmount = planAmount,
                            PlanId = info.PlanId,
                            SubscriptionMonth = new DateTime(year.Year, year.Month, 1),
                            Remarks = info.Remarks,
                            PaymentOrderId = info.PaymentOrderId,
                        });

                        info.PaidPlanAmount -= planAmount;
                    }
                    else if (info.PaidPlanAmount > 0)
                    {
                        subscriptionsList.Add(new SubscriptionInfo
                        {
                            ChurchId = info.ChurchId,
                            PaidPlanAmount = info.PaidPlanAmount,
                            PlanId = info.PlanId,
                            SubscriptionMonth = new DateTime(year.Year, year.Month, 1),
                            Remarks = info.Remarks,
                            PaymentOrderId = info.PaymentOrderId,
                            IsPartialPayment = true
                        });

                        info.PaidPlanAmount = 0;
                        break;
                    }
                }


            }

            return subscriptionsList;
        }

        #endregion Methods

    }
}