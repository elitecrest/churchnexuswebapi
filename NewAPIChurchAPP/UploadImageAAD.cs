﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using Microsoft.WindowsAzure.MediaServices.Client;
using System.Collections.Generic; 
using System.Web;

namespace NewAPIChurchAPP
{
    public class UploadImageAAD
    {

        internal static string streamaudio(Stream stream, string format)
        {
            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);

            var fileStream = System.IO.File.Create(path);
            stream.Seek(0, SeekOrigin.Begin);
            stream.CopyTo(fileStream);
            fileStream.Close();
            var ext = format;
            Stream fileStreambanners = stream;
            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string videoURL = UploadImageThroughAAD(fileNamebanners, path, ext, fileStreambanners);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);

            }
            //finalVideoUrl = videoURL;
            return videoURL;
        }


        internal static string UploadImageThroughAAD(string fileName, string path, string ext, Stream inputStream)
        {
            string tenent = ConfigurationManager.AppSettings["Tenent"];
            string clientId = ConfigurationManager.AppSettings["ClientId"];
            string clientKey = ConfigurationManager.AppSettings["ClientKey"];

            var tokenCredentials = new AzureAdTokenCredentials(tenent,
                           new AzureAdClientSymmetricKey(clientId, clientKey),
                           AzureEnvironments.AzureCloudEnvironment);

            var tokenProvider = new AzureAdTokenProvider(tokenCredentials);

            CloudMediaContext context = new CloudMediaContext(new Uri(ConfigurationManager.AppSettings["AADURI"]), tokenProvider);

            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);
            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));
            assetFile.Upload(path);
            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 10000;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive), AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith("." + ext)).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;

        }
    }
}