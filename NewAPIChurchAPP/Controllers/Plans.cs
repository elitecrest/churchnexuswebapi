﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{

    public class PlansController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();


        #region Plans
        [HttpGet]
        public Utility.CustomResponse GetPlans()
        {
            try
            {

                var plans = ChurchDAL.GetAllPlans();
                if (plans.ToList().Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = plans;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse CreatePlan(PlanDTO planDTO)
        {
            try
            {
                int planid = ChurchDAL.CreatePlan(planDTO);

                if (planid > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = planid;
                    Result.Message = CustomConstants.PlanAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPut]
        public Utility.CustomResponse EditPlan(PlanDTO planDTO)
        {
            try
            {
                int planid = ChurchDAL.EditPlan(planDTO);

                if (planid > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = planid;
                    Result.Message = CustomConstants.PlanUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeletePlan(int PlanId)
        {
            try
            {
                int planId = ChurchDAL.DeletePlan(PlanId);

                if (planId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = planId;
                    Result.Message = CustomConstants.PlanDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetCountryPlans(string Country)
        {
            try
            {

                var plans = ChurchDAL.GetCountryPlans(Country);
                if (plans.ToList().Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = plans;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        #endregion


        #region PlanFeatures
        [HttpGet]
        public Utility.CustomResponse GetPlanFeatures(int planid)
        {
            try
            {

                var planFeatures = ChurchDAL.GetPlanFeatures(planid);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = planFeatures;
                Result.Message = CustomConstants.DetailsGetSuccessfully;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse CreatePlanFeatures(PlanFeaturesDTO PlanFeaturesDTO)
        {
            try
            {
                int PlanFeatureId = ChurchDAL.CreatePlanFeatures(PlanFeaturesDTO);

                if (PlanFeatureId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = PlanFeatureId;
                    Result.Message = CustomConstants.PlanFeaturesAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPut]
        public Utility.CustomResponse EditPlanFeatures(PlanFeaturesDTO PlanFeaturesDTO)
        {
            try
            {
                int PlanFeatureId = ChurchDAL.EditPlanFeatures(PlanFeaturesDTO);

                if (PlanFeatureId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = PlanFeatureId;
                    Result.Message = CustomConstants.PlanFeaturesUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeletePlanFeatures(int PlanFeatureId)
        {
            try
            {
                int planFeatureId = ChurchDAL.DeletePlanFeatures(PlanFeatureId);

                if (planFeatureId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = planFeatureId;
                    Result.Message = CustomConstants.PlanFeaturesDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetPlansandFeatures()
        {
            try
            {

                var planFeatures = ChurchDAL.GetPlansandFeatures();
                if (planFeatures.ToList().Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = planFeatures;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        #endregion
    }
}
