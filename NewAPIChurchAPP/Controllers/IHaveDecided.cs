﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class IHaveDecidedController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse IHaveDecided(DecidedDTO decidedDTO)
        {
            try
            {
                int userId = ChurchDAL.SubmitIHavedecided(decidedDTO);

                if (userId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = userId;
                    Result.Message = CustomConstants.HaveDecidedAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
               
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result; 
            }
        }



        [HttpGet]
        public Utility.CustomResponse GetIHaveDecided(int churchId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                List<DecidedDTO> decidedDTO = ChurchDAL.GetIHaveDecided(churchId);
             
                if (decidedDTO.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = decidedDTO;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
    }
}
