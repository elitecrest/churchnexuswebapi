﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
  

namespace NewAPIChurchAPP.Controllers
{
    public class SermonsController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse Sermons(int seriesid)
        {
            try
            {
                
                List<SermonSeriesDTO> sermonSeries = ChurchDAL.GetAllSermonSeries(seriesid);
                if (sermonSeries.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermonSeries;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        [HttpGet]
        public Utility.CustomResponse GetSermon(int sermonId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var sermon  = ChurchDAL.GetSermonSeries(sermonId);
                if (sermon.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermon;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse CreateSermon(SermonSeriesDTO sermonSeriesDTO)
        {
            try
            {

               
                ChurchesController churches = new ChurchesController();
                SermonSeriesDTO sermons = new SermonSeriesDTO();
                if (!sermonSeriesDTO.SermonUrl.StartsWith("http") && (sermonSeriesDTO.GalleryId != 5))
                {
                    sermonSeriesDTO.ContentType = "video/" + sermonSeriesDTO.SermonFormat;
                    sermonSeriesDTO.SermonUrl =  GetImageURL(sermonSeriesDTO.SermonUrl, sermonSeriesDTO.SermonFormat,sermonSeriesDTO.ContentType);
                }

                if (sermonSeriesDTO.GalleryId == 1)
                {
                    sermonSeriesDTO.SermonFormat = "audio";
                }
                else if (sermonSeriesDTO.GalleryId == 2)
                {
                    sermonSeriesDTO.SermonFormat = "video";
                }
                else if (sermonSeriesDTO.GalleryId == 3)
                { 
                    sermonSeriesDTO.SermonFormat = "youtube";
                }
                else
                {
                    sermonSeriesDTO.SermonFormat = "text";
                }

                sermons = ChurchDAL.CreateSermonSeries(sermonSeriesDTO);

                if (sermons.Id > 0)
                {
                     
                        SendPushNotificationForSermon(sermons);
                     
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermons;
                    Result.Message = CustomConstants.SermonAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        private void SendPushNotificationForSermon(SermonSeriesDTO sermonSeriesDTO)
        {
            UsersController usersCtrl = new UsersController();
           if(sermonSeriesDTO.PushNotificationAccept == true)
            {
                List<string> fbdeviceTokens = ChurchDAL.GetChurchFBDeviceTokens(sermonSeriesDTO.ChurchId);
                foreach (var v in fbdeviceTokens)
                {
                    string Message = "A new sermon is posted by your Church, please watch it !!";
                    bool send = usersCtrl.PushNotification(v, Message,sermonSeriesDTO.ChurchName);
                }
            }
        }

        public string GetImageURL(string binary, string SermonFormat, string contentType)
        {
            Random r = new Random();
            //var s = DateTime.Now.Millisecond.ToString();
            //string filename = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() + "." + SermonFormat;
            string filename = "" + r.Next(999999) + r.Next(99999).ToString() + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + SermonFormat;
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccountvideo = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
            // Create the blob client.
            Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientvideo = storageAccountvideo.CreateCloudBlobClient();

            // Retrieve a reference to a container. 
            Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containervideo = blobClientvideo.GetContainerReference("mycontainer");

            // Create the container if it doesn't already exist.
            containervideo.CreateIfNotExists();


            containervideo.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
            {
                PublicAccess = Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
            });


            // Retrieve reference to a blob named "myblob".
            Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideo = containervideo.GetBlockBlobReference(filename);

            // Create or overwrite the "myblob" blob with contents from a local file.

            byte[] binarydatavideo = Convert.FromBase64String(binary);

            if (contentType == string.Empty || contentType == null)
            {
                contentType = "video/" + SermonFormat;
            }
            blockBlobvideo.Properties.ContentType = contentType;

            blockBlobvideo.UploadFromByteArray(binarydatavideo, 0, binarydatavideo.Length);
  
            // Retrieve reference to a blob named "myblob".
            Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideourl = containervideo.GetBlockBlobReference(filename);

            string imageurlvideo = blockBlobvideourl.Uri.ToString();

            return imageurlvideo;
        }

        [HttpPut]
        public Utility.CustomResponse EditSermon(SermonSeriesDTO sermonSeriesDTO)
        {
            try
            {
                ChurchesController churches = new ChurchesController();
                SermonSeriesDTO sermons = new SermonSeriesDTO();
                if (!sermonSeriesDTO.SermonUrl.StartsWith("http") && (sermonSeriesDTO.GalleryId != 5))
                {
                    sermonSeriesDTO.ContentType = "video/" + sermonSeriesDTO.SermonFormat;
                    sermonSeriesDTO.SermonUrl = GetImageURL(sermonSeriesDTO.SermonUrl, sermonSeriesDTO.SermonFormat, sermonSeriesDTO.ContentType);
                }

                if (sermonSeriesDTO.GalleryId == 1)
                {
                    sermonSeriesDTO.SermonFormat = "audio";
                }
                else if (sermonSeriesDTO.GalleryId == 2)
                {
                    sermonSeriesDTO.SermonFormat = "video";
                }
                else if (sermonSeriesDTO.GalleryId == 3)
                {
                    sermonSeriesDTO.SermonFormat = "youtube";
                }
                else
                {
                    sermonSeriesDTO.SermonFormat = "text";
                }

                sermons = ChurchDAL.EditSermonSeries(sermonSeriesDTO);

                if (sermons.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermons;
                    Result.Message = CustomConstants.SermonUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteSermon(int SermonId)
        {
            try
            {
                int sermonId = ChurchDAL.DeleteSermonSeries(SermonId);

                if (sermonId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermonId;
                    Result.Message = CustomConstants.SermonDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse GetURLFromBaseString(Base64DTO base64DTO)
        { 
            try
            { 
                string imageURL = GetImageURL(base64DTO.Base64String, base64DTO.format,base64DTO.ContentType);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.DetailsGetSuccessfully;
                Result.Response = imageURL;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        // POST api/files
        public async Task<HttpResponseMessage> Post()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            string value = string.Empty;

            try
            {
                // Read the form data and return an async data.
                var result = await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the form data.
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        // return multiple value from FormData
                        if (key == "value")
                            value = val;
                    }
                }

                if (result.FileData.Any())
                {
                    // This illustrates how to get the file names for uploaded files.
                    foreach (var file in result.FileData)
                    {
                        FileInfo fileInfo = new FileInfo(file.LocalFileName);
                        if (fileInfo.Exists)
                        {
                            //do somthing with file
                        }
                    }
                }


                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, value);
             
                return response;
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }





      
        [HttpPost]
        public Utility.CustomResponse StreamAudioGet(Stream streamData)
        {
            try            
            {
                string url = string.Empty;
                Random ran = new Random();
                 var fileName = "fn"+ ran.Next();

                string sermonurl = string.Empty;

                //Create a stream for the file
                Stream stream = null;

                //This controls how many bytes to read at a time and send to the client
                int bytesToRead = 10000;

                // Buffer to read bytes in chunk size specified above
                byte[] buffer = new Byte[bytesToRead];

                // The number of bytes read
                try
                {
                    //Create a WebRequest to get the file
                    FileWebRequest fileReq = (FileWebRequest)FileWebRequest.Create(url);

                    //Create a response for this request
                   // HttpWebResponse fileResp = (HttpWebResponse)fileReq.GetResponse();
                    System.Net.FileWebResponse fileResp = (System.Net.FileWebResponse)fileReq.GetResponse();
                    if (fileReq.ContentLength > 0)
                        fileResp.ContentLength = fileReq.ContentLength;

                    //Get the Stream returned from the response
                   // stream = fileResp.GetResponseStream();

                    stream = streamData;
                    // prepare the response to the client. resp is the client Response
                    var resp = HttpContext.Current.Response;

                    //Indicate the type of data being sent
                    resp.ContentType = "application/octet-stream";

                    //Name the file 
                    string Filen = resp.Headers.Get("uploadedfile");
                    resp.AddHeader("Content-Disposition", "attachment; filename=\"" + Filen + "\"");
                    resp.AddHeader("Content-Length", fileResp.ContentLength.ToString());

                    int length;
                    do
                    {
                        // Verify that the client is connected.
                        if (resp.IsClientConnected)
                        {
                            // Read data into the buffer.
                            length = stream.Read(buffer, 0, bytesToRead);

                            // and write it out to the response's output stream
                            resp.OutputStream.Write(buffer, 0, length);

                            // Flush the data
                            resp.Flush();

                            //Clear the buffer
                            buffer = new Byte[bytesToRead];
                        }
                        else
                        {
                            // cancel the download if client has disconnected
                            length = -1;
                        }
                    } while (length > 0); //Repeat until no data is read
                }
                finally
                {
                    if (stream != null)
                    {
                        //Close the input stream
                        stream.Close();
                    }
                }


                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob". 

                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
                blockBlob.Properties.ContentType = "mp3";
                blockBlob.UploadFromByteArray(buffer, 0, buffer.Length);

                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(fileName);
                sermonurl = blockBlob1.Uri.ToString();

                SermonSeriesDTO sermons = new SermonSeriesDTO();
                //sermonurl = streamaudio(fileStream);
                sermons.Name = "Sermn";
                sermons.Author = "auth";
                sermons.Description = "Desc";
                sermons.Duration = "1";
                sermons.Enabled = true;
                sermons.GalleryId = 0;
                sermons.SermonUrl = sermonurl;
                sermons.SermonFormat = "3gp";
                sermons.Updated = DateTime.Now;
                sermons.SeriesId = 3010;
                
                sermons = ChurchDAL.CreateSermonSeries(sermons);
                if (sermons.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermons.Id;
                    Result.Message = CustomConstants.SermonAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse StreamingFromPost()
        {

            HttpRequestMessage request = this.Request;
            if (!request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = System.Web.HttpContext.Current.Server.MapPath("~/");
            var provider = new MultipartFormDataStreamProvider(root);

            var task = request.Content.ReadAsMultipartAsync(provider).
                ContinueWith<HttpResponseMessage>(o =>             {

                    string file1 = provider.Contents.First().ToString();
                    // this is the file name on the server where the file was saved 

                    return new HttpResponseMessage()
                    {
                        Content = new StringContent("File uploaded.")
                    };
                }
            );
            Result.Status = Utility.CustomResponseStatus.Successful;
            Result.Response = task;
            Result.Message = CustomConstants.SermonAddedSuccessfully;
            return Result; 
        }

        [HttpPost]
        public Utility.CustomResponse StreamSermonAudio([FromBody]string value)
        {
            try
            {
                string sermonurl = string.Empty;
                SermonSeriesDTO sermons = new SermonSeriesDTO();
                var task = this.Request.Content.ReadAsStreamAsync();
                task.Wait();
                Stream requestStream = task.Result;

                try
                {
                    Stream fileStream = System.IO.File.Create(HttpContext.Current.Server.MapPath("~/FileEx.3gp"));
                    sermonurl = streamaudio(fileStream);
                    requestStream.CopyTo(fileStream);
                    fileStream.Close();
                    requestStream.Close();
                }
                catch (IOException)
                {
                    //  throw new HttpResponseException("A generic error occured. Please try again later.", HttpStatusCode.InternalServerError);
                }

         
              

                //SermonSeriesDTO sermons = new SermonSeriesDTO();
                //System.Web.HttpRequest Request;
                //int length = (int)this.Request.InputStream.Length;
                //byte[] buffer = new byte[length];
                //Stream stream = Request.InputStream;
                // Request.InputStream.Read(buffer, 0, length);
                // string sermonurl = streamaudio(stream);


               // Save(buffer);
              //   Request.Files["uploadedfile"] = null;
                //if (Request.Files["uploadedfile"] != null)
                //{
                //    var fileName = Path.GetFileName(Request.Files["uploadedfile"].FileName);
                //    var path = Path.Combine(Server.MapPath("~/App_Data/UploadPlays"), fileName);
                //    var ext = Path.GetExtension(Request.Files["SermonUrl"].FileName);
                //    string mimeTypebanners = Request.Files["SermonUrl"].ContentType;
                //    Stream fileStreambanners = Request.Files["SermonUrl"].InputStream;
                //    string fileNamebanners = Path.GetFileName(Request.Files["SermonUrl"].FileName);
                //    int fileLengthbanners = Request.Files["SermonUrl"].ContentLength;
                //    fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
                //    sermonurl = UploadImage(fileNamebanners, path, ext, fileStreambanners, mimeTypebanners);
                     
                //}
           
                sermons.Author = "auth";
                sermons.Description = "Desc";
                sermons.Duration = "1";
                sermons.Enabled = true;
                sermons.GalleryId = 0;
                sermons.SermonUrl = sermonurl;
                sermons.SermonFormat = "3gp";
                sermons.Updated = DateTime.Now;
                sermons.SeriesId = 3010;
                sermons = ChurchDAL.CreateSermonSeries(sermons);
                if (sermons.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermons.Id;
                    Result.Message = CustomConstants.SermonAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
        void Save(byte[] bytes)
        {
            System.IO.File.WriteAllBytes(Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/UploadPlays"),"fn1"), bytes);
        }
        public string streamaudio(Stream stream)
        {
             
            Random ran = new Random();
            var fileName = "fn"+ ran.Next();
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/UploadPlays"), fileName);
            var ext = "mp3";
            string mimeTypebanners = "audio/mpeg3";
            Stream fileStreambanners = stream;
            string fileNamebanners = "fn" + ran.Next();         
            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string sermonURL = UploadImage(fileNamebanners, path, ext, fileStreambanners, mimeTypebanners);
            return sermonURL;
         }

        public string UploadImage(string fileName, string path, string ext, Stream InputStream, string ContentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                string uniqueBlobName = string.Format(fileName, Guid.NewGuid().ToString(), ext);
                CloudBlockBlob blob = container.GetBlockBlobReference(uniqueBlobName);
                blob.Properties.ContentType = ContentType;
                blob.UploadFromStream(InputStream);


                imageurl = blob.Uri.ToString();

            }
            catch (Exception ex)
            {

            }

            return imageurl;

        }


        [HttpGet]
        public Utility.CustomResponse AdminSermons(int seriesid)
        {
            try
            {

                List<SermonSeriesDTO> sermonSeries = ChurchDAL.GetAllAdminSermonSeries(seriesid);
                if (sermonSeries.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermonSeries;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse CreateAdminSermon(SermonSeriesDTO sermonSeriesDTO)
        {
            try
            {

                ChurchesController churches = new ChurchesController();
                SermonSeriesDTO sermons = new SermonSeriesDTO();
                if (!sermonSeriesDTO.SermonUrl.StartsWith("http"))
                {
                    sermonSeriesDTO.ContentType = "video/" + sermonSeriesDTO.SermonFormat;
                    sermonSeriesDTO.SermonUrl = GetImageURL(sermonSeriesDTO.SermonUrl, sermonSeriesDTO.SermonFormat,sermonSeriesDTO.ContentType);
                }

                sermons = ChurchDAL.CreateAdminSermon(sermonSeriesDTO);

                if (sermons.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermons;
                    Result.Message = CustomConstants.SermonAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


    }
}
