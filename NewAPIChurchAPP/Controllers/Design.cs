﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace NewAPIChurchAPP.Controllers
{
    public class DesignController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();
        [HttpPost] 
        public Utility.CustomResponse UpdateTemplate(DesignDTO designDto)
        {
            try
            {

                designDto = ChurchDAL.UpdateTemplate(designDto);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = designDto;
                Result.Message = CustomConstants.TemplateUpdatedSuccessfully;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }

        [HttpGet]
        public Utility.CustomResponse GetTemplate(int ChurchId)
        {
            try
            {
                DesignDTO design = ChurchDAL.GetTemplate(ChurchId);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = design;
                Result.Message = CustomConstants.DetailsGetSuccessfully; 
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse UpdatePlanFeature(ChurchPlanFeatureDTO planFeature)
        {
            try
            {

                planFeature = ChurchDAL.UpdatePlanFeature(planFeature);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = planFeature;
                Result.Message = CustomConstants.TemplateUpdatedSuccessfully;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }

        [HttpGet]
        public Utility.CustomResponse GetPlanFeature(int ChurchId)
        {
            try
            {
               ChurchPlanFeatureDTO design = ChurchDAL.GetPlanFeature(ChurchId);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = design;
                Result.Message = CustomConstants.DetailsGetSuccessfully;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
