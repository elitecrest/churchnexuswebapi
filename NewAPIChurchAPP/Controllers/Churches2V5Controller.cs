﻿using NewAPIChurchAPP.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Xml.XPath;
using WebMatrix.WebData;
using static NewAPIChurchAPP.Models.Utility;

namespace NewAPIChurchAPP.Controllers
{
    public class Churches2V5Controller : ApiController
    {

        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse CreateChurchPdfFiles(ChurchPdfFileDto churchPdfFileDto)
        {
            try
            {
                int pdfFileId = ChurchDAL.CreateChurchPdfFiles(churchPdfFileDto);

                if (pdfFileId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = pdfFileId;
                    Result.Message = CustomConstants.PdfFilesAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }



        [HttpGet]
        public Utility.CustomResponse GetAboutChurchPdfFiles(int ChurchId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                AboutChurchPdfFileDto churchPdf = new AboutChurchPdfFileDto();
                var about = ChurchDAL.GetAboutChurch(ChurchId);
                var pdfFiles = ChurchDAL.GetChurchPdfFiles(ChurchId);
                churchPdf.About = about;
                churchPdf.ChurchPdfFiles = pdfFiles;

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = churchPdf;
                Result.Message = CustomConstants.DetailsGetSuccessfully;

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse UpdateChurchPaymentMode(ChurchPaymentModeDTO paymentdto)
        {
            try
            {
                int Res = ChurchDAL.UpdateChurchPaymentMode(paymentdto);

                if (Res > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Res;
                    Result.Message = CustomConstants.UpdateChurchPaymentMode;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }



        [HttpGet]
        public Utility.CustomResponse GetPaymentModebyChurchId(int ChurchId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var paymentmode = ChurchDAL.GetPaymentModebyChurchId(ChurchId);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = paymentmode;
                Result.Message = CustomConstants.DetailsGetSuccessfully;

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse EliteChurchEnable()
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var response = ChurchDAL.EliteChurchEnable();
                if (response == 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = response;
                    Result.Message = CustomConstants.Elite_Church_Enable;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

            }
            catch (Exception e)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = e.Message;
                return Result;
            }

            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse EliteChurchDisable()
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var response = ChurchDAL.EliteChurchDisable();
                if (response == 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = response;
                    Result.Message = CustomConstants.Elite_Church_Disable;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

            }
            catch (Exception e)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = e.Message;
                return Result;
            }

            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse EliteChurchDisable123456()
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var response = ChurchDAL.EliteChurchDisable();
                if (response == 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = response;
                    Result.Message = CustomConstants.Elite_Church_Disable;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

            }
            catch (Exception e)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = e.Message;
                return Result;
            }

            return Result;
        }
  
        [HttpGet]
        public Utility.CustomResponse UpdateChurchPushNotificationAccept(int ChurchId,bool PushNotificationAccept)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                ChurchDAL.UpdateChurchPushNotificationAccept(ChurchId, PushNotificationAccept);

                Result.Status = Utility.CustomResponseStatus.Successful; 
                Result.Message = CustomConstants.DetailsGetSuccessfully;

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }



        [HttpPost]
        public Utility.CustomResponse AddChurchSignUpInfo(ChurchSignUpInfo churchSignUpInfo)
        {
            try
            {
                int Res = ChurchDAL.CreateChurchSignUpInfo(churchSignUpInfo);

                if (Res > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Res;
                    Result.Message = CustomConstants.ChurchSignUpAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse UpdateChurchSignUpInfo(ChurchSignUpInfo churchSignUpInfo)
        {
            try
            {
                int Res = ChurchDAL.UpdateChurchSignUpInfo(churchSignUpInfo);

                if (Res > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Res;
                    Result.Message = CustomConstants.ChurchSignUpUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetChurchSignUpInfo(int ChurchSignUpId)
        {
            try
            {
                ChurchSignUpInfo Res = ChurchDAL.GetChurchSignUpInfo(ChurchSignUpId);
                if(Res.SignUpStepId > 0)
                {
                    Res.ChurchId= ChurchDAL.GetChurchIdByChurchSignUpId(Res.ChurchName,Res.Email);
                }
                if (Res.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Res;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse UpdateChurchSigupInfoStatus(int ChurchSignUpId, bool Status)
        {
            try
            {
                int Res = ChurchDAL.UpdateChurchSigupInfoStatus(ChurchSignUpId, Status);

                if (Res > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Res;
                    Result.Message = CustomConstants.ChurchSignUpStatusUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse UpdateChurchSignupStepId(int ChurchSignUpId, int StepId)
        {
            try
            {
                int Res = ChurchDAL.UpdateChurchSignupStepId(ChurchSignUpId, StepId);

                if (Res > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Res;
                    Result.Message = CustomConstants.ChurchSignUpStatusUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

    }
}
