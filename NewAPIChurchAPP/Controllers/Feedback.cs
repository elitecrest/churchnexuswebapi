﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Net.Mail;
using System.Configuration;
using System.Web;
using System.Text;

namespace NewAPIChurchAPP.Controllers
{
    public class FeedbackController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();
         


        [HttpPost]
        public Utility.CustomResponse Feedback(FeedbackDTO feedbackDTO)
        {
            try
            {
                int id = ChurchDAL.CreateFeedback(feedbackDTO);
                string churchName = ChurchDAL.GetChurchName(feedbackDTO.ChurchId);
                var email = SendMail(feedbackDTO, churchName);
                if (id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = id;
                    Result.Message = CustomConstants.FeedbackAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        public static int SendMail(FeedbackDTO feedBackDTO, string churchName)
        {
            try
            {
                string myString = "<b>Church Name: </b>" + churchName + ", <br />" + "<b>User Name: </b>" + feedBackDTO.Username + ", <br />" + "<b>Email: </b>" + feedBackDTO.Email + ", <br />" + "<b>Comments: </b>" + feedBackDTO.Comments + ". ";

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + "sridevilam@elitecrest.com" + ""));
                Msg.Subject = "Feedback from " + churchName;
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password"]),
                    Timeout = 1000000
                };
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                smtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

    }
}
