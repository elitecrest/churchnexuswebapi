﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class AnnouncementsController : ApiController
    {


        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse getAnnouncements(int churchId)
        {
            try
            {

                List<AnnouncmentDataDTO> notes = ChurchDAL.getAnnouncementsByChurchId(churchId);
                if (notes.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = notes;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }



        [HttpPost]
        public Utility.CustomResponse createAnnouncement(AnnouncmentDataDTO annoucementDataDto)
        {
            try
            {

                int id = ChurchDAL.CreateCustomAnnouncement(annoucementDataDto);
                if (id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = id;
                    Result.Message = CustomConstants.AnnouncementAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }


        [HttpGet]
        public Utility.CustomResponse getAnnouncementById(int id)
        {
            try
            {

                AnnouncmentDataDTO notes = ChurchDAL.getAnnouncementById(id);
                if (notes !=null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = notes;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteAnnouncement(int Id)
        {
            try
            {
                int sermonId = ChurchDAL.DeleteCustomAnnouncement(Id);

                if (sermonId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = sermonId;
                    Result.Message = CustomConstants.AnnouncementDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse editAnnouncement(AnnouncmentDataDTO annoucementDataDto)
        {
            try
            {

                AnnouncmentDataDTO annDto = ChurchDAL.EditCustomAnnouncement(annoucementDataDto);
                if (annDto != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = annDto;
                    Result.Message = CustomConstants.AnnouncementsUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;

        }


    }
}