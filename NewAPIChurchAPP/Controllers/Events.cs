﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class EventsController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();


        [HttpGet]
        public Utility.CustomResponse Events(int ChurchId)
        {
            try
            {

                List<EventsDTO> events = ChurchDAL.GetAllEvents(ChurchId);
                if (events.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = events;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {

                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetEvent(int eventid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var events = ChurchDAL.GetEvent(eventid);
                if(events.Id > 0)
                { 
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = events;
                Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse CreateEvent(EventsDTO eventsDTO)
        {
            try
            {
                EventsDTO eventDet = ChurchDAL.CreateEvent(eventsDTO);

                if (eventDet.Id > 0)
                {
                    SendPushNotificationForEvent(eventDet.ChurchId,eventDet.PushNotificationAccept,eventDet.ChurchName);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = eventDet.Id;
                    Result.Message = CustomConstants.EventsAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        private void SendPushNotificationForEvent(int churchId, bool PushNotificationAccept,string churchName)
        {
            UsersController usersCtrl = new UsersController();
            if (PushNotificationAccept == true)
            {
                List<string> fbdeviceTokens = ChurchDAL.GetChurchFBDeviceTokens(churchId);
                foreach (var v in fbdeviceTokens)
                {
                    string Message = "A new event is posted by your Church, please watch it !!";
                    bool send = usersCtrl.PushNotification(v, Message, churchName);
                }
            }
        }


        [HttpPut]
        public Utility.CustomResponse EditEvent(EventsDTO eventsDTO)
        {
            try
            {
                int eventId = ChurchDAL.EditEvent(eventsDTO);

                if (eventId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = eventId;
                    Result.Message = CustomConstants.EventsUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteEvent(int EventId)
        {
            try
            {
                int eventId = ChurchDAL.DeleteEvent(EventId);

                if (eventId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = eventId;
                    Result.Message = CustomConstants.EventsDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

    }
}
