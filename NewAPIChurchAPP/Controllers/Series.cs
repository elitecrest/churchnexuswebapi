﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewAPIChurchAPP.Controllers
{
    public class SeriesController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse Series(int Churchid)
        {
            try
            {

                List<SeriesDTO> series = ChurchDAL.GetAllSeries(Churchid);
                if (series.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = series;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetSeries(long seriesId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var series = ChurchDAL.GetSeries(seriesId);
                if (series.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = series;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse CreateSeries(SeriesDTO seriesDTO)
        {
            try
            {
                ChurchesController churchcon = new ChurchesController();
                if (!seriesDTO.ThumbNail.StartsWith("http"))
                {
                    seriesDTO.ThumbNail = churchcon.GetImageURL(seriesDTO.ThumbNail, seriesDTO.ThumbNailFormat);
                }
                int seriesId = ChurchDAL.CreateSeries(seriesDTO);

                if (seriesId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = seriesId;
                    Result.Message = CustomConstants.SeriesAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.SeriesAddedUnSuccessfully;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPut]
        public Utility.CustomResponse EditSeries(SeriesDTO seriesDTO)
        {
            try
            {
                ChurchesController churchcon = new ChurchesController();
                if (!seriesDTO.ThumbNail.StartsWith("http"))
                { 
                    seriesDTO.ThumbNail = churchcon.GetImageURL(seriesDTO.ThumbNail, seriesDTO.ThumbNailFormat);
                }
                int seriesId = ChurchDAL.EditSeries(seriesDTO);

                if (seriesId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = seriesId;
                    Result.Message = CustomConstants.SeriesUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.SeriesUpdatedUnSuccessfully;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteSeries(int seriesId)
        {
            try
            {
                int SeriesId = ChurchDAL.DeleteSeries(seriesId);

                if (SeriesId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = SeriesId;
                    Result.Message = CustomConstants.SeriesDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.SeriesDeletionFailureMsg;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetTopSeriesSermons(int churchid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var series = ChurchDAL.GetTopSeriesSermons(churchid);
                if (series.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = series;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

    }
}
