﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Web.Http;
using NewAPIChurchAPP.Models;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using Microsoft.Azure.NotificationHubs;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Mail;

namespace NewAPIChurchAPP.Controllers
{
    public class AdminClassesController : ApiController
    {
        //
        // GET: /AdminClasses/
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse CreateClass(AdminClassDto adminClasses)
        {
            try
            {
                var classId = ChurchDAL.CreateClassesByAdmin(adminClasses);

                if (classId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = classId;
                    Result.Message = CustomConstants.ClassCreatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetAllClassesInfo(int churchId)
        {
            try
            {
                var classList = ChurchDAL.GetAllClassInfo(churchId);

                if (classList.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = classList;
                    Result.Message = CustomConstants.ClassCreatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GenerateClassPin(int classId, int createdBy)
        {
            try
            {
                var classList = ChurchDAL.GenerateClassAuthCode(classId, createdBy);

                if (classList != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = classList;
                    Result.Message = CustomConstants.ClassGenerated;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ValidateClassPin(int classId, int pin)
        {
            try
            {
                var classList = ChurchDAL.ValidateClassPin(classId, pin);

                if (classList >= 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = classList;
                    Result.Message = CustomConstants.ClassValidated;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = "Class is not Validated";
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetClassesByChurchId(int churchId)
        {
            try
            {
                var classList = ChurchDAL.GetClassesInfoByChurch(churchId);

                if (classList.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = classList;
                    Result.Message = CustomConstants.ClassRetrieved;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse CreateParent(ParentsDto parent)
        {
            try
            {
                var userValidate = ChurchDAL.UserValidation(parent.EmailAddress);
                if (userValidate == 0)
                {
                    var parentDetails = ChurchDAL.CreateParents(parent);

                    if (parentDetails.Id > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = parentDetails;
                        Result.Message = CustomConstants.UserCreatedSuccessfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.AccessDenied;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = "User already Exists";
                    Result.Message = CustomConstants.UserExistWithEmail;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpDelete]
        public Utility.CustomResponse DeleteClasses(int classId)
        {
            try
            {
                var classes = ChurchDAL.DeleteClass(classId);

                if (classes > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = classes;
                    Result.Message = CustomConstants.ClassDeleted;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse CreateKids(KidsDto kid)
        {
            try
            {
                //SermonsController sermon = new SermonsController();
                // var imageUrl = sermon.GetImageURL(kid.Base64Image, kid.Format);
                //kid.ImageUrl = imageUrl;
                var kidDetails = ChurchDAL.CreateKids(kid);

                if (kidDetails.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = kidDetails;
                    Result.Message = CustomConstants.KidRegisteredSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        //[HttpGet]
        //public Utility.CustomResponse GetKids(int parentId)
        //{
        //    try
        //    {
        //        var kidsList = ChurchDAL.GetKidsByParentId(parentId);

        //        if (kidsList.Count > 0)
        //        {
        //            Result.Status = Utility.CustomResponseStatus.Successful;
        //            Result.Response = kidsList;
        //            Result.Message = CustomConstants.KidRetrieved;
        //        }
        //        else
        //        {
        //            Result.Status = Utility.CustomResponseStatus.Successful;
        //            Result.Message = CustomConstants.NoRecordsFound;
        //        }

        //        return Result;
        //    }
        //    catch (Exception ex)
        //    {
        //        Result.Status = Utility.CustomResponseStatus.Exception;
        //        Result.Message = ex.Message;
        //        return Result;
        //    }
        //}

        [HttpGet]
        public Utility.CustomResponse UsersValidation(string emailAddress, string password, int churchId)
        {
            try
            {
                var usersCount = ChurchDAL.UserExits(emailAddress, password, churchId);
                if (usersCount > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = ChurchDAL.UserLoginValidation(emailAddress, password, churchId);
                    Result.Message = CustomConstants.UserValidated;
                }

                else if (usersCount == -2)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = "";
                    Result.Message = CustomConstants.PasswordMisMatch;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = "";
                    Result.Message = CustomConstants.UserDoesNotExist;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ForgotPassword(string emailAddress)
        {
            UsersController users = new UsersController();
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                string code = ChurchDAL.GetUserExists(emailAddress);
                if (code != string.Empty)
                {
                    string token = code;

                    //need to send a mail to user

                    if (SendMailForForgotPassword(emailAddress, token) == 0)
                    {
                        res.Status = Utility.CustomResponseStatus.Successful;
                        res.Message = CustomConstants.ForgotPasswordSuccessfully;
                    }
                    else
                    {
                        res.Status = Utility.CustomResponseStatus.UnSuccessful;
                        res.Message = Message;

                        //"Failed to Send Email, Please try again later";
                    }
                }
                else
                {
                    res.Status = Utility.CustomResponseStatus.UnSuccessful;
                    res.Message = CustomConstants.UserDoesNotExist;
                }
            }
            catch (Exception ex)
            {
                res.Status = Utility.CustomResponseStatus.Exception;
                res.Message = ex.Message;
                return res;// javaScriptSerializer.Serialize(_result);
            }

            return res;
        }

        public static int SendMailForForgotPassword(string username, string token)
        {
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ForgotPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$Token$$", token);
                //   myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + username + ""));
                Msg.Subject = " Forgot Password";
                Msg.Body = myString.ToString();
                Msg.IsBodyHtml = true;
                SmtpClient SmtpMail = new SmtpClient();
                SmtpMail.Host = "smtp.gmail.com";
                //SmtpMail.Host = "smtp.oxcs.net";
                SmtpMail.Port = 587;
                SmtpMail.EnableSsl = true;
                SmtpMail.UseDefaultCredentials = false;
                SmtpMail.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"].ToString(), ConfigurationManager.AppSettings["Password"].ToString());
                SmtpMail.Timeout = 1000000;
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                SmtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }

        }

        [HttpGet]
        public Utility.CustomResponse ResetPassword(string userName, string token, string confirmPassword)
        {

            try
            {
                //isReset = WebSecurity.ResetPassword(token, confirmPassword);
                string email = ChurchDAL.UpdateParentPassword(userName, token, confirmPassword);
                if (email != string.Empty)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.ResetPassword;

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.ResetPasswordFailed;
                }


            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(_result);
            }
            return Result;
        }

        //[HttpGet]
        //public Utility.CustomResponse GetKidswithClasses(int parentId, int churchId)
        //{
        //    try
        //    {
        //        KidsListDto kidsList = new KidsListDto();
        //        //isReset = WebSecurity.ResetPassword(token, confirmPassword);
        //        //kidsList.Kids = ChurchDAL.GetKidsByParentId(parentId);
        //        kidsList.Classes = ChurchDAL.GetClassesInfoByChurch(churchId);
        //        Result.Status = Utility.CustomResponseStatus.Successful;
        //        Result.Response = kidsList;
        //        Result.Message = CustomConstants.ResetPassword;
        //    }
        //    catch (Exception ex)
        //    {
        //        Result.Status = Utility.CustomResponseStatus.Exception;
        //        Result.Message = ex.Message;
        //        return Result;// javaScriptSerializer.Serialize(_result);
        //    }
        //    return Result;
        //}

        [HttpPost]
        public Utility.CustomResponse KidsCheckin(KidsCheckInDto checkInDto)
        {
            var resList = new KidsCheckInDto();
            resList.KidId = new List<int>();
            try
            {
                if (checkInDto != null && checkInDto.KidId.Count > 0)
                {
                    foreach (var kids in checkInDto.KidId)
                    {
                        var result = ChurchDAL.CheckInKids(kids, checkInDto.QrCode, checkInDto.Status, checkInDto.DeviceType);
                        if (result > 0)
                        {
                            resList.KidId.Add(result);
                        }
                    }
                    if (resList.KidId != null)
                    {
                        resList.QrCode = checkInDto.QrCode;
                    }
                }

                if (resList.KidId != null && resList.KidId.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = resList;
                    Result.Message = CustomConstants.KidsCheckIn;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = "QR Code mis matched";
                    Result.Message = CustomConstants.QrCodeError;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetClassesInformation(int churchId)
        {
            try
            {
                Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                var temp = unixTimestamp.ToString();
                var classsList = ChurchDAL.GetClassInfoByChurch(churchId);
                if (classsList.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = classsList;
                    Result.Message = CustomConstants.ClassRetrieved;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = "No Classes Exist for this Church";
                    Result.Message = CustomConstants.ClassNotExists;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(_result);
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse KidsCheckOut(KidsCheckInDto checkInDto)
        {
            var resList = new KidsCheckInDto();
            resList.KidId = new List<int>();
            try
            {
                if (checkInDto != null && checkInDto.KidId.Count > 0)
                {
                    foreach (var kids in checkInDto.KidId)
                    {
                        var result = ChurchDAL.CheckInKids(kids, checkInDto.QrCode, checkInDto.Status,checkInDto.DeviceType);
                        if (result > 0)
                        {
                            resList.KidId.Add(result);
                        }
                    }
                    if (resList.KidId != null)
                    {
                        resList.QrCode = checkInDto.QrCode;
                    }
                }

                if (resList.KidId != null && resList.KidId.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = resList;
                    Result.Message = CustomConstants.KidsCheckOut;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = "QR Code mis matched";
                    Result.Message = CustomConstants.QrCodeError;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetKidsListByClass(int classId, string tokenId)
        {
            try
            {
                var res = ChurchDAL.ValidateClassPin(classId, Convert.ToInt32(tokenId));
                if (res > 0)
                {
                    var kidList = ChurchDAL.GetKidsByClass(classId, Convert.ToInt32(tokenId));
                    if (kidList.Count > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = kidList;
                        Result.Message = CustomConstants.KidRetrieved;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = "No Kids Found for this Class";
                        Result.Message = CustomConstants.KidsNotExists;
                    }
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = "In valid class pin";
                    Result.Message = CustomConstants.ClassInvalid;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetKidsCheckedOut(int churchId)
        {
            try
            {
                var kidList = ChurchDAL.GetKidsCheckedOut(churchId);
                if (kidList.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = kidList;
                    Result.Message = CustomConstants.KidsCheckOut;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = "No Kids Checked Out";
                    Result.Message = CustomConstants.KidsCheckOut;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetKidsByClassId(int classId, int churchId)
        {
            try
            {
                var kidList = ChurchDAL.GetKidsByClassId(classId, churchId);
                if (kidList.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = kidList;
                    Result.Message = CustomConstants.KidRetrieved;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = "No Kids Found in this class";
                    Result.Message = CustomConstants.KidsNotExists;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetKidsListByKid(int kidId)
        {
            try
            {
                var kidList = ChurchDAL.GetParentEmail(kidId);
                var kidDetails = ChurchDAL.GetKidsByClass(kidList.ClassId, 123);
                if (kidDetails.Count > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = kidDetails;
                        Result.Message = CustomConstants.KidRetrieved;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = "No Kids Found for this Class";
                        Result.Message = CustomConstants.KidsNotExists;
                    }
               
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse PushToParents(int kidId)
        {
            try
            {
                var kidList = ChurchDAL.GetParentEmail(kidId);
                var emailAddress = GetParentEmailId(kidList.EmailAddress);
                var kidDetails = ChurchDAL.GetKidsByClass(kidList.ClassId, 123);
                Result.Response = kidDetails;
                var response = PushNotifications(kidList, "needs attention, please come to your child's class room asap. Thanks.", emailAddress.ToLower().Trim());
                if (!string.IsNullOrEmpty(emailAddress))
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = kidDetails;
                    Result.Message = CustomConstants.PushToNotify;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = 0;
                    Result.Message = CustomConstants.PushNotNotify;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
            return Result;
        }

        public string GetParentEmailId(string EmailAddress)
        {
            if (EmailAddress.Contains("@"))
            {
                var replace = EmailAddress.Replace("@", "");
                if (!replace.Contains(".")) return EmailAddress;
                var str = replace.Replace(".", "");
                EmailAddress = str;
            }
            
            return EmailAddress;
        }

        public async Task<HttpResponseMessage> PushNotifications(ParentKidsDto kidsInfo, [FromBody]string message, string to_tag)
        {
            var user = HttpContext.Current.User.Identity.Name;
            string[] userTag = new string[2];
            userTag[0] = "username:" + to_tag;
            userTag[1] = "from:" + user;

            var usersTag = to_tag;

            Microsoft.Azure.NotificationHubs.NotificationOutcome outcome = null;
            HttpStatusCode ret = HttpStatusCode.InternalServerError;

            switch (kidsInfo.DeviceType)
            {
                case 3:
                    // Windows 8.1 / Windows Phone 8.1
                    var toast = @"<toast><visual><binding template=""ToastText01""><text id=""1"">" +
                                "From " + user + ": " + message + "</text></binding></visual></toast>";
                    outcome = await Notifications.Instance.Hub.SendWindowsNativeNotificationAsync(toast, usersTag);
                    break;
                case 1:
                    // iOS
                    var alert = "{\"aps\":{\"alert\":\"" + "Hello, " + kidsInfo.KidName + " needs attention, please come to your child class room asap. Thanks. " + "\"}" + ",\"KidInfo\"" + ": " + "{\"KidId\":\"" + kidsInfo.Id + "\"}}";
                    outcome = await Notifications.Instance.Hub.SendAppleNativeNotificationAsync(alert, to_tag);
                    //outcome = await Notifications.Instance.Hub.SendAppleNativeNotificationAsync(alert);
                    break;
                case 2:
                    // Android
                    var notif = "{ \"data\" : { \"message\" : {\"KidId\":\"" + kidsInfo.Id + "\""  + ", "  + "\"Reason\": \"" + "Hello, " + kidsInfo.KidName + " needs attention, please come to your child's class room ASAP. Thanks. " + "\"}}}";
                    outcome = await Notifications.Instance.Hub.SendGcmNativeNotificationAsync(notif, usersTag);
                    break;
            }

            if (outcome != null)
            {
                if (!((outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Abandoned) ||
                    (outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Unknown)))
                {
                    ret = HttpStatusCode.OK;
                }
            }

            return Request.CreateResponse(ret);
        }

        [HttpGet]
        public Utility.CustomResponse GetChurchInformation(int churchId)
        {
            try
            {
                var churchList = ChurchDAL.GetChurchBranchByChurch(churchId);
                if (churchList.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchList;
                    Result.Message = CustomConstants.ChurchesRetrieved;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = churchList;
                    Result.Message = CustomConstants.BranchesNotExists;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(_result);
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse CreateChurchBranch(ChurchBranchesDto churchBranchesDto)
        {
            try
            {

                var churchBranch = ChurchDAL.CreateChurchBranches(churchBranchesDto);

                if (churchBranch > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchBranch;
                    Result.Message = CustomConstants.CreatedChurchSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse UpdateChurchBranch(ChurchBranchesDto churchBranchesDto)
        {
            try
            {
                var churchBranch = ChurchDAL.UpdateChurchBranches(churchBranchesDto);
                if (churchBranch > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchBranch;
                    Result.Message = CustomConstants.UpdateChurchSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpDelete]
        public Utility.CustomResponse DeleteChurchBranch(int branchId)
        {
            try
            {
                int ItemId = ChurchDAL.DeleteChurchBranch(branchId);

                if (ItemId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = ItemId;
                    Result.Message = CustomConstants.DeleteChurchSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        //[HttpGet]
        //public Utility.CustomResponse GetKidsByClassId(int classId)
        //{
        //    try
        //    {
        //        var kidList = ChurchDAL.GetCheckedKidsByClassId(classId);
        //        if (kidList.Count > 0)
        //        {
        //            Result.Status = Utility.CustomResponseStatus.Successful;
        //            Result.Response = kidList;
        //            Result.Message = CustomConstants.KidRetrieved;
        //        }
        //        else
        //        {
        //            Result.Status = Utility.CustomResponseStatus.UnSuccessful;
        //            Result.Response = "No Kids Found in this class";
        //            Result.Message = CustomConstants.KidsNotExists;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Result.Status = Utility.CustomResponseStatus.Exception;
        //        Result.Message = ex.Message;
        //        return Result;
        //    }
        //    return Result;
        //}
    }
}
