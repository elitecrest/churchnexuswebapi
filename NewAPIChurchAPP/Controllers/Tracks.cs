﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using Microsoft.WindowsAzure.Storage;
using NewAPIChurchAPP.Models;

namespace NewAPIChurchAPP.Controllers
{
    public class TracksController : ApiController
    {
        //
        // GET: /Tracks/
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();
       
        [System.Web.Http.HttpGet]
        public Utility.CustomResponse Tracks(int albumId)
        {
            try
            {

                List<TracksAlbumsDTO> albumsTracks = ChurchDAL.GetAllAlbumsTracks(albumId);
                if (albumsTracks.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = albumsTracks;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetTracks(int albumId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var track = ChurchDAL.GetTrackByAlbum(albumId);
                if (track.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = track;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpPost]
        public Utility.CustomResponse CreateTrack(TracksAlbumsDTO trackAlbumDto)
        {
            try
            {

                ChurchesController churches = new ChurchesController();
                TracksAlbumsDTO tracks = new TracksAlbumsDTO();
                if (!trackAlbumDto.TrackUrl.StartsWith("http"))
                {
                    trackAlbumDto.ContentType = "video/" + trackAlbumDto.TrackFormat;
                    trackAlbumDto.TrackUrl = GetImageURL(trackAlbumDto.TrackUrl, trackAlbumDto.TrackFormat, trackAlbumDto.ContentType);
                }

                if (trackAlbumDto.GalleryId == 1)
                {
                    trackAlbumDto.TrackFormat = "audio";
                }
                else if (trackAlbumDto.GalleryId == 2)
                {
                    trackAlbumDto.TrackFormat = "video";
                }
                else if (trackAlbumDto.GalleryId == 3)
                {
                    trackAlbumDto.TrackFormat = "youtube";
                }

                tracks = ChurchDAL.CreateTrackAlbums(trackAlbumDto);

                if (tracks.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = tracks;
                    Result.Message = CustomConstants.TrackAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        public string GetImageURL(string binary, string SermonFormat,string contentType)
        {
            Random r = new Random();
            //var s = DateTime.Now.Millisecond.ToString();
            //string filename = "" + r.Next(999999) + DateTime.Now.Millisecond.ToString() + r.Next(99999).ToString() + "." + SermonFormat;
            string filename = "" + r.Next(999999) + r.Next(99999).ToString() + "_" + Convert.ToString(DateTime.Now, CultureInfo.InvariantCulture) + "." + SermonFormat;
            // Retrieve storage account from connection string.
            CloudStorageAccount storageAccountvideo = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
            // Create the blob client.
            Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClientvideo = storageAccountvideo.CreateCloudBlobClient();

            // Retrieve a reference to a container. 
            Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer containervideo = blobClientvideo.GetContainerReference("mycontainer");

            // Create the container if it doesn't already exist.
            containervideo.CreateIfNotExists();


            containervideo.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
            {
                PublicAccess = Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
            });


            // Retrieve reference to a blob named "myblob".
            Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideo = containervideo.GetBlockBlobReference(filename);

            // Create or overwrite the "myblob" blob with contents from a local file.

            byte[] binarydatavideo = Convert.FromBase64String(binary);

            //using (var fileStream = binarydata)
            //{
            if(contentType == string.Empty || contentType == null)
            {
                contentType = "video/" + SermonFormat;
            }
            blockBlobvideo.Properties.ContentType = contentType;
            blockBlobvideo.UploadFromByteArray(binarydatavideo, 0, binarydatavideo.Length);

            // Retrieve reference to a blob named "myblob".
            Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlobvideourl = containervideo.GetBlockBlobReference(filename);

            string imageurlvideo = blockBlobvideourl.Uri.ToString();

            return imageurlvideo;
        }


        [System.Web.Http.HttpPut]
        public Utility.CustomResponse EditTracks(TracksAlbumsDTO tracksAlbumsDto)
        {
            try
            {
                ChurchesController churches = new ChurchesController();
                TracksAlbumsDTO tracks = new TracksAlbumsDTO();
                if (!tracksAlbumsDto.TrackUrl.StartsWith("http"))
                {
                    tracksAlbumsDto.ContentType = "video/" + tracksAlbumsDto.TrackFormat;
                    tracksAlbumsDto.TrackUrl = GetImageURL(tracksAlbumsDto.TrackUrl, tracksAlbumsDto.TrackFormat, tracksAlbumsDto.ContentType);
                }

                if (tracksAlbumsDto.GalleryId == 1)
                {
                    tracksAlbumsDto.TrackFormat = "audio";
                }
                else if (tracksAlbumsDto.GalleryId == 2)
                {
                    tracksAlbumsDto.TrackFormat = "video";
                }
                else
                    tracksAlbumsDto.TrackFormat = "youtube";

                tracks = ChurchDAL.EditTrackAlbums(tracksAlbumsDto);

                if (tracks.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = tracks;
                    Result.Message = CustomConstants.TrackUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [System.Web.Http.HttpDelete]
        public Utility.CustomResponse DeleteTrack(int trackId)
        {
            try
            {
                int track = ChurchDAL.DeleteTrackLabel(trackId);

                if (track > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = track;
                    Result.Message = CustomConstants.TrackDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetTracksByTrackId(int trackId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var track = ChurchDAL.GetTrackAlbums(trackId);
                if (track.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = track;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpPost]
        public Utility.CustomResponse GetURLFromBaseString(Base64DTO base64DTO)
        {
            try
            {
                
                string imageURL = GetImageURL(base64DTO.Base64String, base64DTO.format, base64DTO.ContentType);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.DetailsGetSuccessfully;
                Result.Response = imageURL;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
