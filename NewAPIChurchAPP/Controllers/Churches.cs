﻿using GoogleMaps.LocationServices;
using Microsoft.WindowsAzure.Storage;
using NewAPIChurchAPP.Helpers;
using NewAPIChurchAPP.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using System.Web.Security;
using System.Xml.XPath;
using WebMatrix.WebData;
using static NewAPIChurchAPP.Models.Utility; 
using Google.Apis.Services;
using Google.Apis.YouTube.v3;



namespace NewAPIChurchAPP.Controllers
{
    public class ChurchesController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpGet]
        public Utility.CustomResponse Login(string userName, string password, int ChurchId)
        {
            try
            {
                bool isLogin = WebSecurity.Login(userName, password, persistCookie: true);
                if (isLogin)
                {
                    var currentUser = ChurchDAL.Login(userName, password, ChurchId);
                    if (currentUser.Id > 0)
                    {

                        ChurchSubscriptionsHelper.GetInstance().ValidateChurchSubscription(currentUser);
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = currentUser;
                        Result.Message = CustomConstants.DetailsGetSuccessfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.UserDoesNotExist;
                    }
                }
                else
                {
                    bool validate = WebSecurity.UserExists(userName);
                    if (validate)
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.PasswordMisMatch;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.UserDoesNotExist;
                    }
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse Churches(string geo)
        {
            try
            {

                if (geo == null)
                {
                    geo = string.Empty;
                }
                List<ChurchDTO> newChurches = new List<ChurchDTO>();
                if (geo.Equals("0.0,0.0") || geo.Equals("0.000000,0.000000") || geo.Equals(string.Empty))
                {
                    newChurches = ChurchDAL.GetAllChurches(geo);
                }
                else
                {

                    string countryName = GetCountryName(geo);
                    string[] arrLatlong = geo.Split(',');
                    //Get the current country by geolocation

                    //if country is Us
                    if (countryName == "United States")
                    {
                        List<ApiDataDTO> ZipCodeList = GetZipDataFromLatLong(arrLatlong[0], arrLatlong[1]);
                        if (ZipCodeList.Count > 0)
                        {
                            List<ChurchDTO> churchesFromDB = ChurchDAL.GetAllChurches(geo);
                            newChurches = GetChurchesBasesOnGeo(ZipCodeList, churchesFromDB);
                            if (newChurches.Count <= 0)
                            {
                                //newChurches = ChurchDAL.GetChurchesByCountry(countryName);
                                newChurches = ChurchDAL.GetChurchesByCountry(countryName, geo);
                            }
                        }
                    }
                    else
                    {
                        //Get Non US churches 
                        //newChurches = ChurchDAL.GetChurchesByCountry(countryName);
                        newChurches = ChurchDAL.GetChurchesByCountry(countryName, geo);
                    }


                }
                if (newChurches.Count > 0)
                {


                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = newChurches;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        /// <summary>
        /// Returns plan details of a given church
        /// </summary>
        /// <param name="churchId"></param>
        /// <returns></returns>
        [HttpGet]
        public CustomResponse GetChurchPlanDetails(int churchId)
        {
            try
            {

                var plan = ChurchDAL.GetChurchPlanDetails(churchId);
                if (plan != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = plan;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        /// <summary>
        /// Returns plan details of a given church
        /// </summary>
        /// <param name="churchId"></param>
        /// <returns></returns>
        [HttpGet]
        public CustomResponse GetChurchPlanAmount(int planId, string country)
        {
            try
            {
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = ChurchDAL.GetPlanAmount(planId, country);
                Result.Message = CustomConstants.DetailsGetSuccessfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        //public static T JsonDeserialize<T>(string jsonString)
        //{
        //       DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
        //    MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
        //    T obj = (T)ser.ReadObject(ms);
        //    return obj;
        //}

        internal string GetCountryName(string geo)
        {
            string countryName = string.Empty;
            string[] arrLatlong = geo.Split(',');
            string content1 = string.Empty;
            string downloadApi = "maps/api/geocode/json?latlng=" + arrLatlong[0] + "," + arrLatlong[1] + "&sensor = false";
            JavaScriptSerializer serializer1 = new JavaScriptSerializer();
            serializer1.MaxJsonLength = 1000000000;
            Newtonsoft.Json.Linq.JArray resp1 = null;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://maps.google.com/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client.GetAsync(downloadApi).Result;
            if (response1.IsSuccessStatusCode)
            {
                Stream st1 = response1.Content.ReadAsStreamAsync().Result;
                StreamReader reader1 = new StreamReader(st1);
                content1 = reader1.ReadToEnd();

                LocationDTO locations = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<LocationDTO>(content1);
                List<Result> result = locations.results.ToList();

                if (result.Count > 0)
                {
                    List<AddressComponent> addressComponent = result.First().address_components;

                    for (int i = 0; i < addressComponent.Count; i++)
                    {
                        List<string> types = addressComponent[i].types;
                        for (int j = 0; j < types.Count; j++)
                        {
                            if (types[j].Contains("country"))
                            {
                                countryName = addressComponent[i].long_name;
                            }
                        }
                    }

                }

            }
            return countryName;
        }

        private List<ChurchDTO> GetChurchesBasesOnGeo(List<ApiDataDTO> ZipCodeList, List<ChurchDTO> churchesFromDB)
        {
            List<ChurchDTO> newChurchesList = new List<ChurchDTO>();

            for (int i = 0; i < churchesFromDB.Count; i++)
            {
                for (int j = 0; j < ZipCodeList.Count; j++)
                {
                    if (churchesFromDB[i].Zip == ZipCodeList[j].Code)
                    {
                        newChurchesList.Add(churchesFromDB[i]);
                    }
                }
            }

            return newChurchesList;

        }

        [HttpGet]
        public Utility.CustomResponse ChurchesBySearch(string searchText)
        {
            try
            {

                List<ChurchDTO> churches = ChurchDAL.GetChurchesBySearch(searchText);
                if (churches.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churches;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse ChurchesBySearchCountry(string country, string searchtext)
        {
            try
            {

                List<ChurchDTO> churches = ChurchDAL.GetChurchesBySearchCountry(country, searchtext);
                if (churches.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churches;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse Country()
        {
            try
            {

                List<CountryDTO> Countries = ChurchDAL.GetCountryNames();
                if (Countries.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Countries;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse ChurchesForAdmin()
        {
            try
            {

                List<ChurchDTO> churches = ChurchDAL.GetAllChurchesForAdmin(string.Empty);
                if (churches.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churches;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetChurch(int churchid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var church = ChurchDAL.GetChurch(churchid);

                ChurchSubscriptionsHelper.GetInstance().ValidateChurchSubscription(church);

                if (church.Id > 0)
                {
                    //church.ChurchFeatures = ChurchDAL.GetFeaturesByChurchId(churchid);
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = church;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetGiveUrl(int churchid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var urls = ChurchDAL.GetGiveUrl(churchid);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = urls;
                Result.Message = CustomConstants.DetailsGetSuccessfully;

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
        [HttpGet]
        public Utility.CustomResponse GetWordoftheDay(int churchid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var word = ChurchDAL.GetWordoftheDay(churchid);
                if (word != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = word;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse Announcements(int ChurchId)
        {
            try
            {

                List<AnnouncementDTO> announcement = ChurchDAL.GetAnnouncements(ChurchId);
                if (announcement.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = announcement;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {

                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        public Utility.CustomResponse CreateChurch(ChurchDTO churchDTO)
        {
            SqlConnection conn = ChurchDAL.ConnectTODB();
            SqlTransaction trans = conn.BeginTransaction();
            SqlCommand command = conn.CreateCommand();
            command.Transaction = trans;
            try
            {
                if (!(churchDTO.Banners.ToString() == string.Empty))
                {
                    if (!churchDTO.Banners.StartsWith("http"))
                    {
                        churchDTO.Banners = GetImageURL(churchDTO.Banners, churchDTO.BannerFormat);
                    }
                }
                if (!(churchDTO.Splashscreen.ToString() == string.Empty))
                {
                    if (!churchDTO.Splashscreen.StartsWith("http"))
                    {
                        churchDTO.Splashscreen = GetImageURL(churchDTO.Splashscreen, churchDTO.SplashScreenFormat);
                    }
                }
                if (churchDTO.Address != null)
                {
                    string address = string.Empty;
                    if (churchDTO.City != null && churchDTO.State != null && churchDTO.Country != null && churchDTO.Zip != null)
                    {
                        address = churchDTO.Address + "," + churchDTO.City + "," + churchDTO.Country + "," + churchDTO.Zip;
                    }
                    else
                    {
                        address = churchDTO.Address;
                    }
                    //Change this later 
                    //var locationService = new GoogleLocationService();
                    //var point = locationService.GetLatLongFromAddress(address);
                    //if (point != null)
                    //{
                    //    churchDTO.Longitude = Convert.ToString(point.Longitude);
                    //    churchDTO.Latitude = Convert.ToString(point.Latitude);
                    //}
                    churchDTO.Longitude = "";
                    churchDTO.Latitude = "";
                }
                if (churchDTO.Banners == string.Empty)
                {
                    churchDTO.Banners = "";
                    churchDTO.BannerFormat = "jpg";
                }
                if (churchDTO.Splashscreen == string.Empty)
                {
                    churchDTO.Splashscreen = "";
                    churchDTO.SplashScreenFormat = "jpg";
                }

                int churchid = ChurchDAL.CreateChurch(churchDTO);
                if (churchid == -1)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.UserExistWithEmail;
                }
                else
                    if (churchid > 0)
                {

                    var church = ChurchDAL.GetChurch(churchid);
                    if (!WebSecurity.UserExists(churchDTO.Username))
                    {
                        WebSecurity.CreateUserAndAccount(churchDTO.Username, churchDTO.Password);
                    }
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = church;
                    Result.Message = CustomConstants.ChurchAddedSuccessfully;
                }

                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }
                //}
                //else
                //{
                //    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                //    Result.Message = CustomConstants.User_Exist_With_Email;
                //}

                trans.Commit();

                return Result;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPut]
        public Utility.CustomResponse EditChurch(ChurchDTO churchDTO)
        {
            SqlConnection conn = ChurchDAL.ConnectTODB();
            SqlTransaction trans = conn.BeginTransaction();
            SqlCommand command = conn.CreateCommand();
            command.Transaction = trans;
            try
            {
                if (!(churchDTO.Banners.ToString() == string.Empty))
                {
                    if (!churchDTO.Banners.StartsWith("http"))
                    {
                        churchDTO.Banners = GetImageURL(churchDTO.Banners, churchDTO.BannerFormat);
                    }
                }
                if (!(churchDTO.Splashscreen.ToString() == string.Empty))
                {
                    if (!churchDTO.Splashscreen.StartsWith("http"))
                    {
                        churchDTO.Splashscreen = GetImageURL(churchDTO.Splashscreen, churchDTO.SplashScreenFormat);
                    }
                }

                if (WebSecurity.UserExists(churchDTO.Username))
                {

                    int churchid = ChurchDAL.EditChurch(churchDTO);
                    if (churchid == -1)
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.UserExistWithEmail;
                    }
                    else
                        if (churchid > 0)
                    {
                        var church = ChurchDAL.GetChurch(churchid);
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = church;
                        Result.Message = CustomConstants.ChurchUpdatedSuccessfully;
                    }

                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.AccessDenied;
                    }
                }

                trans.Commit();
                return Result;
            }
            catch (Exception ex)
            {
                trans.Rollback();
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpDelete]
        public Utility.CustomResponse DeleteChurch(int ChurchId)
        {
            try
            {
                int churchid = ChurchDAL.DeleteChurch(ChurchId);

                if (churchid > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchid;
                    Result.Message = CustomConstants.ChurchDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse CreateAnnouncement(AnnouncementDTO announcementDTO)
        {
            try
            {
                int announcementid = ChurchDAL.CreateAnnouncement(announcementDTO);

                if (announcementid > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = announcementid;
                    Result.Message = CustomConstants.AnnouncementsAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPut]
        public Utility.CustomResponse EditAnnouncement(AnnouncementDTO announcementDTO)
        {
            try
            {
                int announceId = ChurchDAL.EditAnnouncement(announcementDTO);

                if (announceId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = announceId;
                    Result.Message = CustomConstants.AnnouncementsUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteAnnouncement(int announcementId)
        {
            try
            {
                int announceId = ChurchDAL.DeleteAnnouncement(announcementId);

                if (announceId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = announceId;
                    Result.Message = CustomConstants.AnnouncementsDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse DBInsertVerseOftheDay(VerseDTO verseDTO)
        {
            try
            {

                ChurchDAL.InsertVerse(verseDTO);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.VerseInsertedSuccessfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpGet]
        public Utility.CustomResponse GetVerseOftheDay()
        {
            try
            {

                VerseDTO verse = ChurchDAL.GetVerseOftheDay();
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = verse;
                Result.Message = CustomConstants.DetailsGetSuccessfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        [HttpGet]
        public Utility.CustomResponse GetLastModifiedDate()
        {
            try
            {

                DateTime lastModifiedDate = ChurchDAL.GetLastModifiedDate();
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.VerseInsertedSuccessfully;
                Result.Response = lastModifiedDate;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
        [HttpPost]
        public Utility.CustomResponse InsertVerseOftheDay()
        {
            try
            {

                ChurchDAL.InsertVerseOftheDay();
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.VerseInsertedSuccessfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse RegisterDevice(DeviceDTO deviceDTO)
        {
            try
            {

                var deviceId = ChurchDAL.RegisterDevice(deviceDTO);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.DeviceRegisteredSuccessfully;
                Result.Response = deviceId;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddPushNotifications(string message, int badge)
        {

            // string filename = GetFileURL("D:\\iKKOSAdminPushCertificates.p12");
            try
            {
                //   rating.UserId =Convert.ToInt32(Userid);
                ChurchDAL.AddPushNotifications(message, badge);
                Result.Status = Utility.CustomResponseStatus.Successful;
                // Result.Message = CustomConstants.PushNotifications;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddSubscriptionEntry(SubscriptionInfo model)
        {
            try
            {
                if (model.SubscriptionMonth == DateTime.MinValue)
                {
                    List<SubscriptionInfo> subscriptionsInfo = ChurchSubscriptionsHelper.GetInstance().CalcSubscriptionMonthsFromPaidAmount(model);

                    subscriptionsInfo.ForEach(sub =>
                    {
                        ChurchDAL.AddSubscriptionInfo(sub);
                    });
                }
                else
                {
                    ChurchDAL.AddSubscriptionInfo(model);
                }

                Result.Status = Utility.CustomResponseStatus.Successful;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }

            return Result;
        }

        /// <summary>
        /// Updates Subscription transaction information
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public Utility.CustomResponse UpdateSubscriptionEntry(SubscriptionInfo model)
        {
            try
            {
                ChurchDAL.UpdateSubscriptionInfo(model);
                Result.Status = Utility.CustomResponseStatus.Successful;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }

            return Result;
        }

        /// <summary>
        /// Adds Member to church payment information to database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public Utility.CustomResponse AddMemberToChurchPaymentEntry(MemberToChurchPayment model)
        {
            try
            {
                ChurchDAL.AddMemberToChurchPaymentEntry(model);
                Result.Status = Utility.CustomResponseStatus.Successful;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }

            return Result;
        }

        /// <summary>
        /// Updates member to church payment entry
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public Utility.CustomResponse UpdateMemberToChurchPaymentEntry(MemberToChurchPayment model)
        {
            try
            {
                // TODO : need to calculate planEffectiveDate
                ChurchDAL.UpdateMemberToChurchPaymentInfo(model);
                Result.Status = Utility.CustomResponseStatus.Successful;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }

            return Result;
        }




        [HttpGet]
        public Utility.CustomResponse GetCountValues(int ChurchId)
        {
            try
            {

                var Counts = ChurchDAL.GetCountValues(ChurchId);
                if (Counts != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Counts;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        public string GetImageURL(string binary, string format)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond + format;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob = container.GetBlockBlobReference(filename);

                // Create or overwrite the "myblob" blob with contents from a local file.

                byte[] binarydata = Convert.FromBase64String(binary);

                //using (var fileStream = binarydata)
                //{
                blockBlob.Properties.ContentType = format;
                blockBlob.UploadFromByteArray(binarydata, 0, binarydata.Length);

                //UploadFromStream(fileStream);
                //}


                // Retrieve reference to a blob named "myblob".
                Microsoft.WindowsAzure.Storage.Blob.CloudBlockBlob blockBlob1 = container.GetBlockBlobReference(filename);

                imageurl = blockBlob1.Uri.ToString();

            }
            catch (Exception ex)
            {
                //lblstatus.Text = "Failed to upload image to Azure Server";
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                Result.Message = "Failed to add image";
            }

            return imageurl;
        }


        internal LatLongDTO GetLatLongValues(string address)
        {
            LatLongDTO latlong = new LatLongDTO();
            string url = "http://maps.googleapis.com/maps/api/geocode/" + "xml?address=" + address + "&sensor=false";

            WebResponse response = null;
            bool is_geocoded = true;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            response = request.GetResponse();
            string lat = "";
            string lng = "";
            string loc_type = "";
            if (response != null)
            {
                XPathDocument document = new XPathDocument(response.GetResponseStream());
                XPathNavigator navigator = document.CreateNavigator();

                // get response status
                XPathNodeIterator statusIterator = navigator.Select("/GeocodeResponse/status");
                while (statusIterator.MoveNext())
                {
                    if (statusIterator.Current.Value != "OK")
                    {
                        is_geocoded = false;
                    }
                }

                // get results
                if (is_geocoded)
                {
                    XPathNodeIterator resultIterator = navigator.Select("/GeocodeResponse/result");
                    while (resultIterator.MoveNext())
                    {


                        XPathNodeIterator geometryIterator = resultIterator.Current.Select("geometry");
                        while (geometryIterator.MoveNext())
                        {
                            XPathNodeIterator locationIterator = geometryIterator.Current.Select("location");
                            while (locationIterator.MoveNext())
                            {
                                XPathNodeIterator latIterator = locationIterator.Current.Select("lat");
                                while (latIterator.MoveNext())
                                {
                                    lat = latIterator.Current.Value;
                                    latlong.Latitude = lat;
                                }

                                XPathNodeIterator lngIterator = locationIterator.Current.Select("lng");
                                while (lngIterator.MoveNext())
                                {
                                    lng = lngIterator.Current.Value;
                                    latlong.Longitude = lng;

                                }
                                XPathNodeIterator locationTypeIterator = geometryIterator.Current.Select("location_type");
                                while (locationTypeIterator.MoveNext())
                                {
                                    loc_type = locationTypeIterator.Current.Value;
                                }
                            }

                        }
                    }

                }

            }

            return latlong;
        }



        internal string GetAddressWithLatLong(string latitude, string longitude)
        {

            string url = "http://maps.google.com/maps/api/geocode/xml?latlng=" + latitude + "," + longitude + "&sensor=false";

            WebResponse response = null;
            bool is_geocoded = true;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            response = request.GetResponse();
            string address = "";

            if (response != null)
            {
                XPathDocument document = new XPathDocument(response.GetResponseStream());
                XPathNavigator navigator = document.CreateNavigator();

                // get response status
                XPathNodeIterator statusIterator = navigator.Select("/GeocodeResponse/status");
                while (statusIterator.MoveNext())
                {
                    if (statusIterator.Current.Value != "OK")
                    {
                        is_geocoded = false;
                    }
                }

                // get results
                if (is_geocoded)
                {
                    XPathNodeIterator resultIterator = navigator.Select("/GeocodeResponse/result");
                    if (resultIterator.MoveNext())
                    {


                        XPathNodeIterator formatted_address_iterator = resultIterator.Current.Select("formatted_address");
                        if (formatted_address_iterator.MoveNext())
                        {

                            address = formatted_address_iterator.Current.Value;

                        }
                    }

                }

            }

            return address;
        }

        internal List<ApiDataDTO> GetZipDataFromLatLong(string latitude, string longitude)
        {
            List<ApiDataDTO> objApiDataDTO = new List<ApiDataDTO>();

            Newtonsoft.Json.Linq.JArray resp1 = null;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://api.zip-codes.com/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // HTTP GET
            string content = string.Empty;
            HttpResponseMessage response = client.GetAsync("/ZipCodesAPI.svc/1.0/FindZipCodesInRadius/ByLatLon?latitude=" + latitude + "&longitude=" + longitude + "&maximumradius=15&minimumradius=0&key=ITDS4E4MSYGL3LEJ7U8G").Result;
            if (response.IsSuccessStatusCode)
            {
                Stream st = response.Content.ReadAsStreamAsync().Result;
                StreamReader reader = new StreamReader(st);
                content = reader.ReadToEnd();
                var values = JsonConvert.DeserializeObject<dynamic>(content);
                var resp = values["DataList"];
                if (resp != null)
                {
                    foreach (var p in resp)
                    {
                        ApiDataDTO obj = new ApiDataDTO();
                        obj.Code = p.Code;
                        obj.City = p.City;
                        obj.State = p.State;
                        obj.Latitude = p.Latitude;
                        obj.Longitude = p.Longitude;
                        obj.Distance = p.Distance;
                        obj.County = p.County;
                        objApiDataDTO.Add(obj);

                    }
                }
            }
            return objApiDataDTO.ToList();
        }


        [HttpGet]
        public Utility.CustomResponse TermsandConditions()
        {
            try
            {
                string TermsURL = ConfigurationManager.AppSettings["TermsURL"].ToString();
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = TermsURL;
                Result.Message = CustomConstants.DetailsGetSuccessfully;


                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ProgramSheet(int ChurchId)
        {
            try
            {

                var prgSheet = ChurchDAL.GetProgramSheet(ChurchId);
                if (prgSheet.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = prgSheet;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = null;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ProgramSheetPortal(int ChurchId)
        {
            try
            {

                var prgSheet = ChurchDAL.GetProgramSheetPortal(ChurchId);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = prgSheet;
                Result.Message = CustomConstants.DetailsGetSuccessfully;


                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse ProgramSheetDetails(int ProgramSheetId)
        {
            try
            {

                var prgSheetDet = ChurchDAL.GetProgramSheetDetails(ProgramSheetId);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = prgSheetDet;
                Result.Message = CustomConstants.DetailsGetSuccessfully;


                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse CreateProgramSheet(ProgramSheetDTO programSheetDTO)
        {
            try
            {
                int programId = ChurchDAL.CreateProgramSheet(programSheetDTO);

                if (programId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = programId;
                    Result.Message = CustomConstants.ProgramSheetAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse CreateProgramSheetDetails(List<ProgramSheetDetailsDTO> programSheetDetailsDTO)
        {
            try
            {

                int numOfRows = ChurchDAL.DeleteProgramSheetDetails(programSheetDetailsDTO[0].ProgramSheetId);
                for (int i = 0; i < programSheetDetailsDTO.Count; i++)
                {
                    int programId = ChurchDAL.CreateProgramSheetDetails(programSheetDetailsDTO[i]);
                    if (programId > 0)
                    {
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = programId;
                        Result.Message = CustomConstants.ProgramSheetAddedSuccessfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.AccessDenied;
                    }
                }


                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteProgramSheet(int ProgramSheetId)
        {
            try
            {
                int programSheetId = ChurchDAL.DeleteProgramSheet(ProgramSheetId);

                if (programSheetId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = programSheetId;
                    Result.Message = CustomConstants.ProgramSheetDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse UpdateChurchEnabled(int churchId, Boolean enabled)
        {
            try
            {
                int churchid = ChurchDAL.UpdateChurchEnabled(churchId, enabled);

                if (churchid > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchid;
                    Result.Message = CustomConstants.ChurchEnableSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

     


        public Utility.CustomResponse GetUserSubscription(int ChurchId)
        {
            try
            {
                string customerkey = System.Configuration.ConfigurationManager.AppSettings["CustomerKey"].ToString();
                string customersecret = System.Configuration.ConfigurationManager.AppSettings["CustomerSecret"].ToString();

                bool isaccess = false;
                string content = string.Empty;
                string content1 = string.Empty;
                CustomerDTO customerDTO = new CustomerDTO();

                var church = ChurchDAL.GetChurch(ChurchId);
                string api = "wc-api/v2/customers/email/" + church.Username + "?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                //string api = "/demo/wc-api/v2/customers/email/" + church.Username + "?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                JavaScriptSerializer serializer1 = new JavaScriptSerializer();
                serializer1.MaxJsonLength = 1000000000;
                Newtonsoft.Json.Linq.JArray resp1 = null;
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri("http://www.rohanlam.com/churchnexus_copy"); //http:www.churchnexus.com
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = client.GetAsync(api).Result;
                if (response.IsSuccessStatusCode)
                {
                    Stream st = response.Content.ReadAsStreamAsync().Result;
                    StreamReader reader = new StreamReader(st);
                    content = reader.ReadToEnd();
                    if (content.Contains("errors"))
                    {
                        isaccess = false;
                    }
                    else
                    {
                        customerDTO = JsonDeserialize<CustomerDTO>(content);
                        if (customerDTO != null)
                        {
                            int custid = customerDTO.customer.id;
                            string downloadApi = "wc-api/v2/customers/" + custid + "/downloads?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                            // string downloadApi = "/demo/wc-api/v2/customers/" + custid + "/downloads?consumer_key=" + customerkey + "&consumer_secret=" + customersecret;
                            HttpResponseMessage response1 = client.GetAsync(downloadApi).Result;
                            if (response1.IsSuccessStatusCode)
                            {
                                Stream st1 = response1.Content.ReadAsStreamAsync().Result;
                                StreamReader reader1 = new StreamReader(st1);
                                content1 = reader1.ReadToEnd();
                                DownloadDTO downloadDTO = JsonDeserialize<DownloadDTO>(content1);
                                List<Download> download = downloadDTO.downloads.ToList();
                                if (download.Count == 0)
                                {
                                    isaccess = false;
                                }
                                else
                                {
                                    string accessExpries = download.First().access_expires;
                                    if (Convert.ToDateTime(accessExpries).AddDays(1) >= DateTime.Now)
                                    {
                                        isaccess = true;
                                    }
                                }

                            }

                        }
                    }

                }


                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = isaccess;


                // todo: add other conditions
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
        }

        public static T JsonDeserialize<T>(string jsonString)
        {
            System.Runtime.Serialization.Json.DataContractJsonSerializer ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(T));
            MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(jsonString));
            T obj = (T)ser.ReadObject(ms);
            return obj;
        }

        [HttpGet]
        public Utility.CustomResponse GetChurchByName(string churchName)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var church = ChurchDAL.GetChurchByName(churchName);
                if (church.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = church;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse UpdateAboutChurch(AboutChurch aboutChurch)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var Id = ChurchDAL.UpdateAboutChurch(aboutChurch);
                if (Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Id;
                    Result.Message = CustomConstants.AboutChurchUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse UpdateChurchBankDetails(ChurchBankDetailsDto churchBankDetailsDto)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var Id = ChurchDAL.UpdateChurchBankDetails(churchBankDetailsDto);
                if (Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Id;
                    Result.Message = CustomConstants.ChurchBankDetailsSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetStatesByCountryID(string CountryID)
        {
            try
            {
                List<StateDTO> States = ChurchDAL.GetStatesByCountryID(CountryID);

                if (States.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = States;
                    Result.Message = "Details_Get_Successfully";
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetChurchBankDetails(int ChurchId)
        {
            try
            {
                ChurchBankDetailsDto bankDetails = ChurchDAL.GetChurchBankDetails(ChurchId);

                if (bankDetails.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = bankDetails;
                    Result.Message = "Details_Get_Successfully";
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetChurchDataByChurchId(int ChurchId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var churchData = ChurchDAL.GetChurchDataByChurchId(ChurchId);
                if (churchData.UsersCount > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchData;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetSermonListByChurchId(int ChurchId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                List<ChurchSermonData> Sermons = ChurchDAL.GetSermonListByChurchId(ChurchId);
                if (Sermons.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Sermons;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetPaymentListByChurchId(int ChurchId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                List<ChurchPaymentData> Payments = ChurchDAL.GetPaymentListByChurchId(ChurchId);
                if (Payments.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Payments;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpGet]
        public Utility.CustomResponse GetUsersCountByChurchId(int ChurchId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                List<ChurchUsersList> lISTS = ChurchDAL.GetUsersListByChurchId(ChurchId);
                List<ChurchUsersList> lstSorted = getSortedList(lISTS);
                if (lstSorted.Count > 0)
                {
                    foreach (var a in lstSorted)
                    {
                        a.TotalCount = a.IOSCount + a.AndroidCount;
                    }

                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = lstSorted;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        private List<ChurchUsersList> getSortedList(List<ChurchUsersList> lISTS)
        {
            List<ChurchUsersList> newList = new List<ChurchUsersList>();
            if (lISTS.Count > 0)
            {
                for (int i = 0; i < lISTS.Count; i++)
                {
                    if (i == 0)
                    {
                        newList.Add(lISTS[i]);
                    }
                    else if (lISTS[i - 1].Month == lISTS[i].Month)
                    {
                        newList[newList.Count - 1].IOSCount = lISTS[i].IOSCount;
                    }
                    else
                    {
                        newList.Add(lISTS[i]);
                    }
                }
            }
            return newList;
        }

        [HttpPost]
        public Utility.CustomResponse AddContactUs(ContactUs contactUs)
        {
            try
            {
                var id = ChurchDAL.AddContactUs(contactUs);
                if (id > 0)
                {
                    EmailToSupport(contactUs);
                }
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = id;
                Result.Message = CustomConstants.Contactus_Added_Successfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        /// <summary>
        /// Adds donatino cause information to database
        /// </summary>
        /// <param name="donationCause"></param>
        /// <returns></returns>
        [HttpPost]
        public Utility.CustomResponse AddDonationCause(ChurchDonationCause donationCause)
        {
            try
            {
                ChurchDAL.CreateDonationCause(donationCause);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.DonationAdded;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        /// <summary>
        /// Adds donatino cause information to database
        /// </summary>
        /// <param name="donationCause"></param>
        /// <returns></returns>
        [HttpPost]
        public Utility.CustomResponse UpdateDonationCause(ChurchDonationCause donationCause)
        {
            try
            {
                ChurchDAL.UpdateDonationCause(donationCause);
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.DonationUpdated;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetChurchDonationCauses(int churchid)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = ChurchDAL.GetDonationCausesByChurchId(churchid);
                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpDelete]
        /// <summary>
        /// Deletes  Church donatin cause from db
        /// </summary>
        /// <param name="causeId"></param>
        /// <returns></returns>
        public CustomResponse DeleteChurchDonationCause(int churchId, int causeId)
        {
            try
            {
                ChurchDAL.DeleteDonationCause(causeId);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.DonationCauseDeleted;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }

            return Result;
        }

        /// <summary>
        /// Returns chuches with expired subscription
        /// </summary>
        /// <returns></returns>
        /// TODO : MUST Add unit test , as its more of business oriented
        [HttpGet]
        public CustomResponse GetChurchesWithExpiredSubscription()
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                List<ChurchDTO> churches = ChurchDAL.GetChurches();

                churches.ForEach(o =>
                {
                    ChurchSubscriptionsHelper.GetInstance().ValidateChurchSubscription(o);
                });

                res.Status = Utility.CustomResponseStatus.Successful;
                res.Response = churches.Where(o => o.HasValidSubscription == false);
                res.Message = CustomConstants.DetailsGetSuccessfully;

                return res;
            }
            catch (Exception ex)
            {
                res.Status = Utility.CustomResponseStatus.Exception;
                res.Message = ex.Message;
                return res;
            }
        }

        private int EmailToSupport(ContactUs contactUs)
        {
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ContactUsEmail.html"));
                string readFile = reader.ReadToEnd();
                string myString = string.Empty;
                myString = readFile;
                myString = myString.Replace("$$Name$$", contactUs.Name);
                myString = myString.Replace("$$Email$$", contactUs.Email);
                myString = myString.Replace("$$Subject$$", contactUs.Subject);
                myString = myString.Replace("$$Message$$", contactUs.Message);
                myString = myString.Replace("$$CountryCode$$", contactUs.CountryCode);
                myString = myString.Replace("$$Mobile$$", contactUs.MobileNumber);
                myString = myString.Replace("$$IPAddress$$", contactUs.IPAddress);


                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + "support@churchnexus.com" + ""));
                Msg.Subject = contactUs.Subject;
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password"]),
                    Timeout = 1000000
                };
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                smtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

        [HttpDelete]
        public Utility.CustomResponse DeletePdf(int Id)
        {
            try
            {
                int pdfId = ChurchDAL.DeletePdf(Id);
                if (pdfId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = pdfId;
                    Result.Message = CustomConstants.Pdf_File_Deleted_Successfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public CustomResponse GetSubscriptionsByChurch(int ChurchId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                List<SubscriptionInfo> churchSubInfo = ChurchDAL.GetSubscriptionsByChurch(ChurchId);  

                res.Status = Utility.CustomResponseStatus.Successful;
                res.Response = churchSubInfo;
                res.Message = CustomConstants.DetailsGetSuccessfully;

                return res;
            }
            catch (Exception ex)
            {
                res.Status = Utility.CustomResponseStatus.Exception;
                res.Message = ex.Message;
                return res;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ChurchYT(int ChurchId)
        {
            List<YoutubeVideosDTO> ytlist = new List<YoutubeVideosDTO>();
            try
            {
                YoutubeDto data = ChurchDAL.GetChurchYTDetails(ChurchId);
                List<string> totalurls = new List<string>();
                if (data.ChannelId == null || data.ChannelId == string.Empty)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = null;
                    Result.Message = "No Youtube Channel with this church!!!";
                }
                else
                {
                    if (data.IsUserName == 0)
                    {
                        var yt = new YouTubeService(new BaseClientService.Initializer() { ApiKey = "AIzaSyBrHqGmzavbt_nD6WKi6MEiQeL7E8Q7hEo" });
                        var searchListRequest = yt.Search.List("snippet");
                        searchListRequest.Order = SearchResource.ListRequest.OrderEnum.Date;
                        searchListRequest.MaxResults = 50;
                        searchListRequest.ChannelId = data.ChannelId;
                        var channelsListResponse = searchListRequest.Execute();
                        foreach (var video in channelsListResponse.Items)
                        {
                            var videoUrl = "https://www.youtube.com/watch?v=" + video.Id.VideoId;
                            totalurls.Add(videoUrl);
                            YoutubeVideosDTO ytvideo = new YoutubeVideosDTO();
                            ytvideo.VideoId = video.Id.VideoId;
                            ytvideo.VideoUrl = videoUrl;
                            ytvideo.ChannelId = video.Snippet.ChannelId;
                            ytvideo.ChannelTitle = video.Snippet.ChannelTitle;
                            if (video.Snippet.LiveBroadcastContent == "live")
                            {
                                ytvideo.IsLive = true;
                            }
                            else
                            {
                                ytvideo.IsLive = false;
                            }
                            ytvideo.Thumbnail = video.Snippet.Thumbnails.Default__.Url;
                            ytvideo.VideoName = video.Snippet.Title;
                            ytlist.Add(ytvideo);
                        }

                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Response = ytlist;
                        Result.Message = CustomConstants.DetailsGetSuccessfully;
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Some exception occured" + e);
            }
            return Result;
        }



    }

}
