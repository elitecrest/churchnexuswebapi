﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace NewAPIChurchAPP.Controllers
{
    public class PurchaseItemsController : ApiController
    {
        //
        // GET: /PurchaseItems/

        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetpurchasedItems(int churchId)
        {
            try
            {

                List<PurchaseItemsDto> items = ChurchDAL.GetItemsByChurchId(churchId);
                if (items.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = items;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpPost]
        public Utility.CustomResponse CreateItems(PurchaseItemsDto purchaseItemsDto)
        {
            try
            {
                SermonsController sermon = new SermonsController();
                 if (!purchaseItemsDto.ImageUrl.StartsWith("http"))
                 {
                    purchaseItemsDto.ContentType = "video/" + purchaseItemsDto.ImageFormat;
                    purchaseItemsDto.ImageUrl = sermon.GetImageURL(purchaseItemsDto.ImageUrl, purchaseItemsDto.ImageFormat, purchaseItemsDto.ImageFormat);
                 }
                var items = ChurchDAL.CreatePurchaseItems(purchaseItemsDto);

                if (items > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = items;
                    Result.Message = CustomConstants.ItemAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpDelete]
        public Utility.CustomResponse DeleteItem(int itemId)
        {
            try
            {
                int ItemId = ChurchDAL.DeleteItem(itemId);

                if (ItemId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = ItemId;
                    Result.Message = CustomConstants.ItemDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpPost]
        public Utility.CustomResponse UpdateItems(PurchaseItemsDto purchaseItemsDto)
        {
            try
            {
                SermonsController sermon = new SermonsController();
                if (!purchaseItemsDto.ImageUrl.StartsWith("http"))
                {
                    purchaseItemsDto.ContentType = "video/" + purchaseItemsDto.ImageFormat;
                    purchaseItemsDto.ImageUrl = sermon.GetImageURL(purchaseItemsDto.ImageUrl, purchaseItemsDto.ImageFormat, purchaseItemsDto.ContentType);
                }
                var items = ChurchDAL.UpdateChurchItems(purchaseItemsDto);

                if (items > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = items;
                    Result.Message = CustomConstants.ItemUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetItemsById(int itemId)
        {
            try
            {

                PurchaseItemsDto items = ChurchDAL.GetItemsByItemId(itemId);
                if (items != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = items;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [System.Web.Http.HttpGet]
        public Utility.CustomResponse GetStoreItems(int churchId)
        {
            try
            {
               
                List<PurchaseItemsDto> items = ChurchDAL.GetStoreItemsByChurchId(churchId);
                if (items.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = items;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }
    }
}
