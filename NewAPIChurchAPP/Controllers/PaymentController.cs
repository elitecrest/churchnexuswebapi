﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web.Http;
using static NewAPIChurchAPP.Models.Utility;
using paytm;
using System.Web.Script.Serialization;
using System.Net.Http.Headers;
using System.IO;

namespace NewAPIChurchAPP.Controllers
{
    public class PaymentController : ApiController
    {
        Utility.CustomResponse Result = new Utility.CustomResponse();
        [HttpGet]
        public CustomResponse getPaymentHash(String text)
        {
            CustomResponse response = new CustomResponse();
            try
            {
                String sha512HashString = GenerateSHA512String(text);
                response.Response = sha512HashString;
                response.Status = CustomResponseStatus.Successful;
                response.Message = "Successful!";
            }
            catch (Exception e)
            {
                response.Response = null;
                response.Status = CustomResponseStatus.Exception;
                response.Message = "Unable to deduce SHA512! please check input string!";
            }

            return response;

        }


        public string GenerateSHA512String(string inputString)
        {
            SHA512 sha512 = SHA512Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha512.ComputeHash(bytes);
            return GetStringFromHash(hash);
        }

        private static string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("X2"));
            }
            return result.ToString();
        }

        [HttpPost]
        public Utility.CustomResponse AddSuccessTransaction(PaymentDTO paymentDTO)
        {
            try
            {
                //string productInfo = paymentDTO.productinfo.Split('_')[1];
                //string churchId = paymentDTO.productinfo.Split('_')[0];
                //paymentDTO.productinfo = productInfo;
                //paymentDTO.ChurchId = Convert.ToInt32(churchId);
                paymentDTO.PaymentStatus = "Success";
                int Id = ChurchDAL.AddTransaction(paymentDTO);

                if (Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Id;
                    Result.Message = CustomConstants.TransactionAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse AddFailureTransaction(PaymentDTO paymentDTO)
        {
            try
            {
                string productInfo = paymentDTO.productinfo.Split('_')[1];
                string churchId = paymentDTO.productinfo.Split('_')[0];
                paymentDTO.productinfo = productInfo;
                paymentDTO.ChurchId = Convert.ToInt32(churchId);
                paymentDTO.PaymentStatus = "Failure";
                int Id = ChurchDAL.AddTransaction(paymentDTO);

                if (Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Id;
                    Result.Message = CustomConstants.TransactionAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetChurchesPaymentList(int ChurchId)
        {
            try
            {
                List<ChurchPaymentDTO> payments = ChurchDAL.GetChurchPaymentDetailsList(ChurchId);

                if (payments.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = payments;
                    Result.Message = "Details_Get_Successfully";
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse AddTransaction(PaymentDTO paymentDTO)
        {
            try
            {
                //string productInfo = paymentDTO.productinfo.Split('_')[1];
                //string churchId = paymentDTO.productinfo.Split('_')[0];
                //paymentDTO.productinfo = productInfo;
                //paymentDTO.ChurchId = Convert.ToInt32(churchId);
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("MID", Utility.PaytmConstants.MID);

                parameters.Add("ORDER_ID", paymentDTO.OrderId);

                parameters.Add("CUST_ID", paymentDTO.CustId);

                parameters.Add("CHANNEL_ID", Utility.PaytmConstants.CHANNEL_ID);

                parameters.Add("INDUSTRY_TYPE_ID", Utility.PaytmConstants.INDUSTRY_TYPE_ID);

                parameters.Add("WEBSITE", Utility.PaytmConstants.WEBSITE);

                parameters.Add("TXN_AMOUNT", paymentDTO.amount); 
              
                paymentDTO.PaymentStatus = "Success";
                paymentDTO.CheckHashSum = CheckSum.generateCheckSum(Utility.PaytmConstants.MERCHANT_KEY, parameters);
              
                int Id = ChurchDAL.AddTransaction(paymentDTO);

                if (Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Id;
                    Result.Message = CustomConstants.TransactionAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpPost]
        public Utility.CustomResponse UpdateTransaction(PaymentDTO paymentDTO)
        {
            try
            {
                //string productInfo = paymentDTO.productinfo.Split('_')[1];
                //string churchId = paymentDTO.productinfo.Split('_')[0];
                //paymentDTO.productinfo = productInfo;
                //paymentDTO.ChurchId = Convert.ToInt32(churchId); 
                int Id = 0;
                string checkSum = String.Empty; 
                //if (!(paymentDTO.status == "TXN_FAILURE")) -- Check for belwo SP Later
                //{
                //    checkSum = ChurchDAL.GetCheckSum(paymentDTO.OrderId);
                //    GetPaymentID(paymentDTO.OrderId, checkSum);
                //}

                Id = ChurchDAL.UpdateTransaction(paymentDTO);
                
                if (Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = Id;
                    Result.Message = CustomConstants.TransactionAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        public string GetPaymentID(string OrderId,string CheckSum)
        {
            string ReturnedString = ""; 

            string inputstring = "{\"MID\":\"" + PaytmConstants.MID;
            inputstring += "\",\"ORDERID\":" + OrderId;
            inputstring += "}";

            string accessUrl = "https://secure.paytm.in/oltp/HANDLER_INTERNAL/getTxnStatus?JsonData={%22MID%22:%22"+ PaytmConstants.MID + "%22,%22ORDERID%22:%22" + OrderId + "%22,%22CHECKSUMHASH%22:%22"+ CheckSum + "%22}";
            JavaScriptSerializer serializer2 = new JavaScriptSerializer();
            serializer2.MaxJsonLength = 1000000000;
            HttpClient client1 = new HttpClient();
            client1.DefaultRequestHeaders.Accept.Clear();
            client1.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response1 = client1.GetAsync(accessUrl).Result;        
            if (response1.IsSuccessStatusCode)
            {
                Stream st2 = response1.Content.ReadAsStreamAsync().Result;
                StreamReader reader2 = new StreamReader(st2);
                ReturnedString = reader2.ReadToEnd();
            }

            return ReturnedString;
        }

        [HttpPost]
        public Utility.CustomResponse CreatePayment(TransactionDTO paymentdto)
        {
            try
            {
                int PayId = ChurchDAL.CreatePayment(paymentdto);

                if (PayId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = PayId;
                    Result.Message = CustomConstants.PaymentIsDoneSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
    }
}