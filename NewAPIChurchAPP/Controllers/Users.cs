﻿using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using WebMatrix.WebData;

namespace NewAPIChurchAPP.Controllers
{
    public class UsersController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();

        [HttpPost]
        public Utility.CustomResponse CreateUser(UsersDTO usersDTO)
        {
            try
            {
                int userId = ChurchDAL.CreateUser(usersDTO);

                if (userId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = userId;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPost]
        public Utility.CustomResponse PaymentTracking(PaymentTrackingDTO paymentTrackingDTO)
        {
            try
            {
                int PaymentID = ChurchDAL.CreatePaymentTracking(paymentTrackingDTO);

                if (PaymentID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = PaymentID;
                    Result.Message = CustomConstants.PaymentTrackingAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        // Add Usage Tracking
        [HttpPost]
        public Utility.CustomResponse UsageTracking(IEnumerable<UsageDTO> usageDTO)
        {
            try
            {
                foreach (var u in usageDTO)
                {
                    ChurchDAL.AddUsageTracking(u);
                }


                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.UsageTrackingSuccessfully;


            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetUsersCount(int ChurchId)
        {
            try
            {

                int AppUsersCount = ChurchDAL.GetUsersCount(ChurchId);
                if (AppUsersCount != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = AppUsersCount;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse ForgotPassword(string userName)
        {
            //algoritham 1. we need to search for corresponding email id and prepare message and send it to related email.
            Utility.CustomResponse res = new Utility.CustomResponse();

            try
            {
                if (WebSecurity.UserExists(userName))
                {
                    string token = WebSecurity.GeneratePasswordResetToken(userName);

                    var emailAddress = ChurchDAL.GetEmailByUserId(userName);
                    //need to send a mail to user

                    if (SendMail(userName, token, emailAddress) == 0)
                    {
                        res.Status = Utility.CustomResponseStatus.Successful;
                        res.Message = CustomConstants.ForgotPasswordSuccessfully;
                    }
                    else
                    {
                        res.Status = Utility.CustomResponseStatus.UnSuccessful;
                        res.Message = Message;
                    }
                }
                else
                {
                    res.Status = Utility.CustomResponseStatus.UnSuccessful;
                    res.Message = CustomConstants.UserDoesNotExist;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }

            return res;
        }

        [HttpGet] //For Woocommerce site
        public Utility.CustomResponse UpdatePassword(string userName, string password)
        {

            bool isReset = false;
            try
            {
                if (WebSecurity.UserExists(userName))
                {
                    string token = WebSecurity.GeneratePasswordResetToken(userName);

                    isReset = WebSecurity.ResetPassword(token, password);
                    if (isReset)
                    {
                        ChurchDAL.UpdatePassword(userName, password);
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Message = CustomConstants.ResetPassword;

                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.ResetPasswordFailed;
                    }

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.UserDoesNotExist;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }

            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse ResetPassword(string token, string confirmPassword)
        {
            bool isReset = false;
            try
            {
                isReset = WebSecurity.ResetPassword(token, confirmPassword);
                if (isReset)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.ResetPassword;

                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.ResetPasswordFailed;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;// javaScriptSerializer.Serialize(Result);
            }
            return Result;
        }

        public static int SendMail(string username, string token, string emailAddress)
        {
            try
            {
                StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/ResetPassword.html"));
                string readFile = reader.ReadToEnd();
                string myString = "";
                myString = readFile;
                myString = myString.Replace("$$Name$$", username);
                myString = myString.Replace("$$Token$$", token);
                //   myString = myString.Replace("$$Password$$", password);

                MailMessage Msg = new MailMessage();
                MailAddress fromMail = new MailAddress(ConfigurationManager.AppSettings["EmailAddress"].ToString());
                Msg.From = fromMail;
                Msg.To.Add(new MailAddress("" + emailAddress + ""));
                Msg.Subject = " Password Reset Requested";
                Msg.Body = myString;
                Msg.IsBodyHtml = true;
                SmtpClient smtpMail = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    UseDefaultCredentials = false,
                    Credentials =
                        new NetworkCredential(ConfigurationManager.AppSettings["EmailAddress"],
                            ConfigurationManager.AppSettings["Password"]),
                    Timeout = 1000000
                };
                string domain;

                Uri url = HttpContext.Current.Request.Url;
                domain = url.AbsoluteUri.Replace(url.PathAndQuery, string.Empty);
                string body = string.Empty;
                smtpMail.Send(Msg);
                return 0;
            }
            catch (SmtpException exx)
            {
                Message = exx.Message;
                return 1;
            }
            catch (Exception ex)
            {
                Message = ex.Message;
                return 1;
            }
        }

        [HttpGet]
        public Utility.CustomResponse ChangePassword(string username, string currentpassword, string newpassword)
        {
            bool isReset = false;
            try
            {
                if (WebSecurity.UserExists(username))
                {
                    //string token = WebSecurity.GeneratePasswordResetToken(username);
                    isReset = WebSecurity.ChangePassword(username, currentpassword, newpassword);
                    //isReset = WebSecurity.ResetPassword(token, newpassword);
                    if (isReset)
                    {
                        var result = ChurchDAL.UpdatePassword(username, newpassword);
                        Result.Status = Utility.CustomResponseStatus.Successful;
                        Result.Message = CustomConstants.UpdateUserUpdateSuccessfully;
                    }
                    else
                    {
                        Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                        Result.Message = CustomConstants.ResetPasswordFailed;
                    }
                    //Result.Response = result;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.UserDoesNotExist;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse InAppPurchase(InAppPurchaseDTO inAppPurchaseDto)
        {
            try
            {
                int PaymentID = ChurchDAL.CreatePaymentsTracking(inAppPurchaseDto);

                if (PaymentID > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = PaymentID;
                    Result.Message = CustomConstants.PaymentTrackingAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse TrackAppUsage(int churchId, string deviceId, int deviceType)
        {
            try
            {

                int AppUsersCount = ChurchDAL.TrackAppUsage(churchId, deviceId, deviceType);
                if (AppUsersCount != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = AppUsersCount;
                    Result.Message = CustomConstants.AppUsageTrack;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }



        [HttpGet]
        public Utility.CustomResponse getChurchMembers(int churchId, int age)
        {
            try
            {

                List<ChurchMemberInfoDto> churchMembersList = ChurchDAL.GetChurchMembers(churchId, age);
                if (churchMembersList.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchMembersList;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = null;
                    Result.Message = CustomConstants.NoChurchMembersAvailableMsg;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse addChurchMemberInfo(ChurchMemberInfoDto churchMemberInfoDto)
        {
            try
            {

                int result = ChurchDAL.addMemberInfo(churchMemberInfoDto);
                if (result > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = result;
                    Result.Message = CustomConstants.UserInfoAdded;
                }
                else if (result == -1)
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = result;
                    Result.Message = "User already exists.";
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.Exception;
                    Result.Message = "Server error! please try later.";

                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }


        [HttpPost]
        public Utility.CustomResponse UpdateChurchMemberInfo(ChurchMemberInfoDto churchMemberInfoDto)
        {
            try
            {

                int result = ChurchDAL.UpdateMemberInfo(churchMemberInfoDto);
              
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = result;
                    Result.Message = CustomConstants.UserInfoUpdated;
                 

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse DeleteChurchMemberInfo(int MemberId)
        {
            try
            {

                int result = ChurchDAL.DeleteMemberInfo(MemberId);
                
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = result;
                    Result.Message = CustomConstants.UserInfoDeleted;
                

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse getChurchMembersById(int churchId, int memberId)
        {
            try
            {  
                ChurchMemberInfoDto  churchMembers  = ChurchDAL.GetChurchMembersById(churchId, memberId);
                if (churchMembers.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = churchMembers;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = null;
                    Result.Message = CustomConstants.NoChurchMembersAvailableMsg;
                }

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse sendWishesPushToChurchMembers()
        {
            try
            {  
                List<ChurchMemberInfoDto> churchMemberList = ChurchDAL.GetMembersHavingBirthday();

                foreach (ChurchMemberInfoDto birthDayMember in churchMemberList)
                {
                    if (birthDayMember.PushNotificationAccept)
                    {
                        string to_tag = birthDayMember.FBDeviceToken; //birthDayMember.surName + "_" + birthDayMember.name;
                                                                      //var response = PushNotifications(birthDayMember, "Hello, " + birthDayMember.surName + "." + birthDayMember.name + ". We wish you many more happy returns of the day. Thank you for engaging with us. ");
                        PushNotification(to_tag, "Hello, " + birthDayMember.surName + "." + birthDayMember.name + ". We wish you many more happy returns of the day. Thank you for engaging with us. ", string.Empty);
                    }
                }

                List<ChurchMemberInfoDto> weddingDayMembersList = ChurchDAL.GetMembersHavingWeddingDay();

                foreach (ChurchMemberInfoDto weddingDayMember in weddingDayMembersList)
                {
                    if (weddingDayMember.PushNotificationAccept)
                    {
                        string to_tag = weddingDayMember.FBDeviceToken; //weddingDayMember.surName + "_" + weddingDayMember.name;
                                                                        //var response = PushNotifications(weddingDayMember, "Hello, " + weddingDayMember.surName + "." + weddingDayMember.name + ". We wish you happy marriage anniversary. Thank you for engaging with us. ");
                        PushNotification(to_tag, "Hello, " + weddingDayMember.surName + "." + weddingDayMember.name + ". We wish you happy marriage anniversary. Thank you for engaging with us. ", string.Empty);
                    }
                }

                VerseDTO verse = ChurchDAL.GetVerseOftheDay();
                List<FirebaseDeviceDTO> firebaseDevices = ChurchDAL.GetFirebaseDevices();

                foreach (FirebaseDeviceDTO verseDevice in firebaseDevices)
                {
                    if (verseDevice.PushNotificationAccept)
                    {
                        string to_tag = verseDevice.FBDeviceToken;
                        PushNotification(to_tag, verse.Verse, string.Empty);
                    }
                }
                 
        

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = null;
                Result.Message = CustomConstants.PushToNotify;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
            return Result;

        }


        //public async Task<HttpResponseMessage> PushNotifications(ChurchMemberInfoDto churchMemberInfo, [FromBody]string message)
        //{
        //    string to_tag = churchMemberInfo.surName + "_" + churchMemberInfo.name;
        //    var user = HttpContext.Current.User.Identity.Name;
        //    string[] userTag = new string[2];
        //    userTag[0] = "username:" + to_tag;
        //    userTag[1] = "from:" + user;

        //    var usersTag = to_tag;

        //    Microsoft.Azure.NotificationHubs.NotificationOutcome outcome = null;
        //    HttpStatusCode ret = HttpStatusCode.InternalServerError;

        //    switch (churchMemberInfo.DeviceType)
        //    {
        //        case 3:
        //            // Windows 8.1 / Windows Phone 8.1
        //            var toast = @"<toast><visual><binding template=""ToastText01""><text id=""1"">" +
        //                        "From " + user + ": " + message + "</text></binding></visual></toast>";
        //            outcome = await Notifications.Instance.Hub.SendWindowsNativeNotificationAsync(toast, usersTag);
        //            break;
        //        case 1:
        //            // iOS
        //            var alert = "{\"aps\":{\"alert\":\"" + message + "\"}}";
        //            outcome = await Notifications.Instance.Hub.SendAppleNativeNotificationAsync(alert, to_tag);
        //            //outcome = await Notifications.Instance.Hub.SendAppleNativeNotificationAsync(alert);
        //            break;
        //        case 2:
        //            // Android

        //            var notif = "{\"data\":{\"message\":\"" + message + "\"}}";
        //            outcome = await Notifications.Instance.Hub.SendGcmNativeNotificationAsync(notif, usersTag);
        //            break;
        //    }

        //    if (outcome != null)
        //    {
        //        if (!((outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Abandoned) ||
        //            (outcome.State == Microsoft.Azure.NotificationHubs.NotificationOutcomeState.Unknown)))
        //        {
        //            ret = HttpStatusCode.OK;
        //        }
        //    }

        //    return Request.CreateResponse(ret);
        //}


        public bool PushNotification(string Name, string Message,string churchName)
        {
            bool IsSend = false;
            try
            {
                FirebaseDTO firebase = ChurchDAL.GetFirebaseCredentials();
                string applicationID = firebase.FirebaseAppId;
                string senderId = firebase.FirebaseSenderId;

                string deviceId = Name; 

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    to = deviceId,
                    notification = new
                    {
                        body = Message,
                        title = churchName,//"Note from Church",
                        sound = "Enabled"

                    }
                    ,
                    data = new
                    {
                        body = Message,
                        title = churchName,
                        sound = "Enabled"

                    }
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                                IsSend = true;
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
            return IsSend;
        }


        [HttpPost]
        public Utility.CustomResponse RegisterFirebaseDevice(FirebaseDeviceDTO fb)
        {
            try
            {
                var res = ChurchDAL.RegisterFirebaseDevice(fb);
                if (res != null)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = res;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Response = string.Empty;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }

                
            }
            catch (Exception ex)
            {
                 
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse AddImageUrl(Base64DTO base64DTO)
        {
            try
            {
                ChurchesController con = new ChurchesController();
                string result = con.GetImageURL(base64DTO.Base64String, base64DTO.format);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = result;
                Result.Message = CustomConstants.ImageUploadSuccessfully;

            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

    }
}
