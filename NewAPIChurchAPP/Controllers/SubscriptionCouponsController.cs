﻿using NewAPIChurchAPP.Models;
using NewAPIChurchAPP.SubscriptionCoupons;
using NewAPIChurchAPP.SubscriptionCoupons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static NewAPIChurchAPP.Models.Utility;

namespace NewAPIChurchAPP.Controllers
{
    /// <summary>
    /// API for Subscriptions
    /// </summary>
    public class SubscriptionCouponsController : ApiController
    {
        public static string Message = "";
        CustomResponse Result = new CustomResponse();

        [HttpGet]
        public Utility.CustomResponse GetSubscriptionCouponsByStatus(bool status)
        {
            try
            {
                var subscriptionCouponseDal = new SubscriptionCouponsDataAccess();

                List<SubscriptionCoupon> subscriptionCoupons = subscriptionCouponseDal.GetAllCouponsByStatus(status);
                
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = subscriptionCoupons;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpGet]
        public Utility.CustomResponse GetChurchSubscriptionCoupons(int churchId)
        {
            try
            {
                var subscriptionCouponseDal = new SubscriptionCouponsDataAccess();

                List<SubscriptionCoupon> subscriptionCoupons = subscriptionCouponseDal.GetChurchSubscriptionCoupons(churchId);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = subscriptionCoupons;
                Result.Message = CustomConstants.DetailsGetSuccessfully;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public CustomResponse CreateSubscriptionCoupon(SubscriptionCoupon coupon)
        {
            try
            {
                var subscriptionCouponseDal = new SubscriptionCouponsDataAccess();

                long couponId = subscriptionCouponseDal.AddCoupon(coupon);

                if (couponId > 0)
                {
                    Result.Status = CustomResponseStatus.Successful;
                    Result.Response = couponId;
                    Result.Message = CustomConstants.SubscriptionCouponAddedToChurch;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPut]
        public Utility.CustomResponse UpdateSubscriptionCoupon(SubscriptionCoupon coupon)
        {
            try
            {
                var subscriptionCouponseDal = new SubscriptionCouponsDataAccess();
                subscriptionCouponseDal.UpdateCoupon(coupon);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.AlbumsUpdatedSuccessfully;


                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPut]
        public Utility.CustomResponse AddSubsciptionCouponToChurch(ChurchCouponEntry entry)
        {
            try
            {
                var subscriptionCouponseDal = new SubscriptionCouponsDataAccess();
                var res=subscriptionCouponseDal.AddSubscriptionCouponToChurch(entry.ChurchId,entry.CouponCode);

                if (res == 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Message = CustomConstants.SubscriptionCouponAddedToChurch;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.SubscriptionCouponCantBeAddedToChurch;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }


        [HttpDelete]
        public Utility.CustomResponse DeleteSubscriptionCoupon(long couponId)
        {
            try
            {
                var subscriptionCouponseDal = new SubscriptionCouponsDataAccess();
                subscriptionCouponseDal.DeleteCoupon(couponId);

                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Message = CustomConstants.AlbumsDeletedSuccessfully;

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }
    }
}
