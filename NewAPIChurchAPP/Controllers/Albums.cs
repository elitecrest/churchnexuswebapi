﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using NewAPIChurchAPP.Models;

namespace NewAPIChurchAPP.Controllers
{
    public class AlbumsController : ApiController
    {
        public static string Message = "";
        Utility.CustomResponse Result = new Utility.CustomResponse();
        //
        // GET: /Albums/
        [HttpGet]
        public Utility.CustomResponse Albums(int churchId)
        {
            try
            {

                List<AlbumsDTO> albums = ChurchDAL.GetAllAlbums(churchId);
                if (albums.Count > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = albums;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
            }
            return Result;
        }

        [HttpPost]
        public Utility.CustomResponse CreateAlbums(AlbumsDTO albumsDTO)
        {
            try
            {
                ChurchesController churchcon = new ChurchesController();
                if (!albumsDTO.ThumbNail.StartsWith("http"))
                {
                    albumsDTO.ThumbNail = churchcon.GetImageURL(albumsDTO.ThumbNail, albumsDTO.ThumbNailFormat);
                }
                int albumId = ChurchDAL.CreateAlbums(albumsDTO);

                if (albumId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = albumId;
                    Result.Message = CustomConstants.AlbumsAddedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpGet]
        public Utility.CustomResponse GetAlbums(long albumId)
        {
            Utility.CustomResponse res = new Utility.CustomResponse();
            try
            {
                var albums = ChurchDAL.GetAlbums(albumId);
                if (albums.Id > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = albums;
                    Result.Message = CustomConstants.DetailsGetSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.NoRecordsFound;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpPut]
        public Utility.CustomResponse UpdateAlbum(AlbumsDTO albumsDTO)
        {
            try
            {
                ChurchesController churchcon = new ChurchesController();
                if (!albumsDTO.ThumbNail.StartsWith("http"))
                {
                    albumsDTO.ThumbNail = churchcon.GetImageURL(albumsDTO.ThumbNail, albumsDTO.ThumbNailFormat);
                }
                int albumId = ChurchDAL.EditAlbum(albumsDTO);

                if (albumId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = albumId;
                    Result.Message = CustomConstants.AlbumsUpdatedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

        [HttpDelete]
        public Utility.CustomResponse DeleteAlbum(int albumId)
        {
            try
            {
                int AlbumId = ChurchDAL.DeleteAlbum(albumId);

                if (AlbumId > 0)
                {
                    Result.Status = Utility.CustomResponseStatus.Successful;
                    Result.Response = AlbumId;
                    Result.Message = CustomConstants.AlbumsDeletedSuccessfully;
                }
                else
                {
                    Result.Status = Utility.CustomResponseStatus.UnSuccessful;
                    Result.Message = CustomConstants.AccessDenied;
                }

                return Result;
            }
            catch (Exception ex)
            {
                Result.Status = Utility.CustomResponseStatus.Exception;
                Result.Message = ex.Message;
                return Result;
            }
        }

    }
}
