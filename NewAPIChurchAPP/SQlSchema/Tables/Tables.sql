USE [ChurchDataBase]
GO
/****** Object:  Table [cp].[Announcements]    Script Date: 05-01-2015 17:55:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[Announcements](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[announcements_url] [varchar](2000) NULL,
	[facebook_url] [varchar](2000) NULL,
	[twitter_url] [varchar](2000) NULL,
	[website_url] [varchar](2000) NULL,
	[geomap] [nvarchar](40) NULL,
	[church_id] [int] NOT NULL,
 CONSTRAINT [PK_Announcements] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[churches]    Script Date: 05-01-2015 17:55:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[churches](
	[id] [int] IDENTITY(100000,1) NOT NULL,
	[name] [nvarchar](256) NOT NULL,
	[description] [nvarchar](max) NULL,
	[church_api_key] [varchar](64) NULL,
	[enabled] [bit] NOT NULL,
	[geomap] [nvarchar](40) NULL,
	[history] [nvarchar](max) NULL,
	[about] [nvarchar](max) NULL,
	[gallery_id] [smallint] NULL,
	[modified_on_utc] [datetime] NOT NULL,
	[banners] [nvarchar](2000) NULL,
	[splashscreen] [nvarchar](2000) NULL,
	[theme] [varchar](20) NULL,
	[phone_num] [varchar](50) NULL,
	[email] [varchar](100) NULL,
	[Address] [varchar](2000) NULL,
	[lat] [varchar](100) NULL,
	[long] [varchar](100) NULL,
	[planid] [int] NULL,
	[username] [varchar](100) NULL,
	[password] [varchar](50) NULL,
	[contactperson_name] [varchar](256) NULL,
	[contactperson_email] [varchar](100) NULL,
	[contactperson_phonenum] [varchar](50) NULL,
	[banner_format] [varchar](20) NULL,
	[splashscreen_format] [varchar](20) NULL,
 CONSTRAINT [PrimaryKey_eac0217f-5391-4664-a7ee-6ac307679730] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[Device]    Script Date: 05-01-2015 17:55:50 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[Device](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UDID] [varchar](100) NOT NULL,
	[DeviceToken] [varchar](100) NOT NULL,
	[DeviceType] [varchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[ChurchId] [int] NULL,
 CONSTRAINT [PrimaryKey_ff721ddf-8daa-49f8-a39d-3e2e8433cbf2] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[events]    Script Date: 05-01-2015 17:55:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[events](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[author] [nvarchar](64) NULL,
	[church_id] [int] NULL,
	[gallery_id] [smallint] NULL,
	[is_used] [bit] NOT NULL,
	[starts] [datetime] NULL,
	[ends] [datetime] NULL,
	[description] [nvarchar](512) NULL,
 CONSTRAINT [PrimaryKey_1c2dcf79-7571-47df-9291-0bf6630c1625] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[I_Hv_Decided]    Script Date: 05-01-2015 17:55:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[I_Hv_Decided](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](256) NULL,
	[email] [varchar](256) NULL,
	[phonenumber] [varchar](50) NULL,
	[accept_savior] [bit] NULL,
	[recommitted_life] [bit] NULL,
	[info_relationship] [bit] NULL,
	[church_id] [int] NOT NULL,
 CONSTRAINT [PK_I_Hv_Decided] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[Image_Category]    Script Date: 05-01-2015 17:55:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[Image_Category](
	[ID] [int] NOT NULL,
	[CategoryName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PrimaryKey_d8d4075b-8199-468b-8f67-94dba366378f] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[image_gallery]    Script Date: 05-01-2015 17:55:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[image_gallery](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](64) NOT NULL,
	[image_url] [nvarchar](512) NULL,
	[updated_on_utc] [datetime] NOT NULL,
	[image] [image] NULL,
	[image_hash] [varbinary](20) NULL,
	[image_size] [nvarchar](16) NULL,
	[gallery_id] [smallint] NULL,
	[theme] [varchar](max) NULL,
	[churchid] [int] NULL,
	[image_category] [int] NULL,
	[image_format] [varchar](20) NULL,
 CONSTRAINT [PrimaryKey_c9b0142f-6afa-4cf0-bbcd-a2ad70142b20] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[offerings]    Script Date: 05-01-2015 17:55:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[offerings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](500) NOT NULL,
	[enabled] [bit] NOT NULL,
	[starts] [datetime] NULL,
	[ends] [datetime] NULL,
	[createdDate] [datetime] NULL,
	[description] [varchar](2000) NULL,
	[church_id] [int] NULL,
 CONSTRAINT [PrimaryKey_e95912c9-0c2f-468b-afbb-eadaabcfc28c] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[planfeatures]    Script Date: 05-01-2015 17:55:51 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[planfeatures](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[plan_id] [int] NULL,
 CONSTRAINT [PrimaryKey_0dedb39e-13cc-4b8c-9900-e5cb89b862ad] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[plans]    Script Date: 05-01-2015 17:55:52 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[plans](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Amount] [nvarchar](50) NULL,
 CONSTRAINT [PrimaryKey_f5368461-ae41-4107-842b-31f393212e72] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[plays_bak]    Script Date: 05-01-2015 17:55:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[plays_bak](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[author] [nvarchar](64) NULL,
	[sermon_id] [int] NOT NULL,
	[play_url] [nvarchar](1024) NULL,
	[is_used] [bit] NOT NULL,
	[updated] [datetime] NULL,
	[duration] [datetime] NULL,
	[description] [nvarchar](512) NULL,
	[gallery_id] [smallint] NULL,
 CONSTRAINT [PrimaryKey_plays_bak] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[prayers]    Script Date: 05-01-2015 17:55:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[prayers](
	[id] [int] IDENTITY(100,1) NOT NULL,
	[name] [nvarchar](256) NOT NULL,
	[email] [nvarchar](256) NULL,
	[message] [nvarchar](max) NULL,
	[church_id] [int] NOT NULL,
	[enabled] [bit] NOT NULL,
	[confidential] [bit] NOT NULL,
	[contactme] [bit] NOT NULL,
	[phone] [nvarchar](max) NULL,
 CONSTRAINT [PrimaryKey_1da8fa76-93a9-46fe-b757-85e9b777335b] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[registration]    Script Date: 05-01-2015 17:55:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[registration](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](255) NULL,
	[password] [varchar](50) NULL,
	[conf_password] [varchar](50) NULL,
	[firstname] [varchar](100) NULL,
	[lastname] [varchar](100) NULL,
	[country] [varchar](200) NULL,
	[gender] [varchar](20) NULL,
	[age] [int] NULL,
	[church_id] [int] NULL,
 CONSTRAINT [pk_registration_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[series]    Script Date: 05-01-2015 17:55:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[series](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[author] [nvarchar](64) NULL,
	[church_id] [int] NULL,
	[gallery_id] [smallint] NULL,
	[is_used] [bit] NOT NULL,
	[created] [datetime] NULL,
	[updated] [datetime] NULL,
	[ends] [datetime] NULL,
	[description] [nvarchar](512) NULL,
	[ThumbNail] [nvarchar](2000) NULL,
	[ThumbNailFormat] [varchar](20) NULL,
 CONSTRAINT [PrimaryKey_2d1771ec-d9ca-4b55-af66-e954a4400264] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[sermons]    Script Date: 05-01-2015 17:55:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[sermons](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NULL,
	[author] [nvarchar](64) NULL,
	[series_id] [int] NOT NULL,
	[sermon_url] [nvarchar](1024) NULL,
	[is_used] [bit] NOT NULL,
	[updated] [datetime] NULL,
	[duration] [varchar](20) NULL,
	[description] [nvarchar](512) NULL,
	[gallery_id] [smallint] NULL,
	[sermon_format] [varchar](10) NULL,
 CONSTRAINT [PrimaryKey_sermons] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[sermons_bak]    Script Date: 05-01-2015 17:55:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[sermons_bak](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[author] [nvarchar](64) NULL,
	[church_id] [int] NULL,
	[gallery_id] [smallint] NULL,
	[is_used] [bit] NOT NULL,
	[created] [datetime] NULL,
	[updated] [datetime] NULL,
	[ends] [datetime] NULL,
	[description] [nvarchar](512) NULL,
	[ThumbNail] [nvarchar](2000) NULL,
 CONSTRAINT [PrimaryKey] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[Status]    Script Date: 05-01-2015 17:55:53 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[Status](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](50) NOT NULL,
 CONSTRAINT [PrimaryKey_689a7787-0263-43dd-b217-5c7969e65735] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[testimonials]    Script Date: 05-01-2015 17:55:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[testimonials](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](255) NOT NULL,
	[author] [nvarchar](256) NULL,
	[church_id] [int] NULL,
	[is_used] [bit] NOT NULL,
	[created] [datetime] NULL,
	[topic] [nvarchar](255) NULL,
	[description] [nvarchar](2048) NOT NULL,
	[gallery_id] [smallint] NULL,
	[status] [int] NULL,
	[location] [varchar](100) NULL,
 CONSTRAINT [PrimaryKey_d757c163-d400-4b89-b8aa-9e559f6a7751] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cp].[user_invites]    Script Date: 05-01-2015 17:55:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[user_invites](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](255) NOT NULL,
	[username] [nvarchar](256) NULL,
	[hash] [nvarchar](max) NOT NULL,
	[church_id] [int] NULL,
	[is_used] [bit] NOT NULL,
	[expires_on] [datetime] NULL,
 CONSTRAINT [pk_user_invites_Id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[users]    Script Date: 05-01-2015 17:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[users](
	[id] [int] IDENTITY(100000,1) NOT NULL,
	[church_id] [int] NOT NULL,
	[guid] [uniqueidentifier] NOT NULL,
	[joined_on_utc] [datetime] NOT NULL,
	[last_logged_in_on_utc] [datetime] NULL,
	[username] [nvarchar](256) NOT NULL,
	[email] [nvarchar](max) NOT NULL,
	[salt] [nvarchar](max) NOT NULL,
	[password] [nvarchar](max) NOT NULL,
	[first_name] [nvarchar](64) NULL,
	[last_name] [nvarchar](64) NULL,
	[church] [int] NULL,
 CONSTRAINT [pk_users_Id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[Verses]    Script Date: 05-01-2015 17:55:54 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [cp].[Verses](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Verse] [ntext] NOT NULL,
	[Author] [nvarchar](32) NOT NULL,
	[LastModified] [datetime] NULL,
 CONSTRAINT [PrimaryKey_b7dd4ab3-a212-4658-92ca-c2a35096173a] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
/****** Object:  Table [cp].[Words]    Script Date: 05-01-2015 17:55:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [cp].[Words](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[word] [varchar](4000) NOT NULL,
	[church_id] [int] NOT NULL,
 CONSTRAINT [PK_Words] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [cp].[events] ADD  CONSTRAINT [DF__events__is_used__3A81B327]  DEFAULT ((0)) FOR [is_used]
GO
ALTER TABLE [cp].[series] ADD  CONSTRAINT [DF__series__is_used__3E524401]  DEFAULT ((0)) FOR [is_used]
GO
ALTER TABLE [cp].[testimonials] ADD  CONSTRAINT [DF__testimoni__is_us__3F466844]  DEFAULT ((0)) FOR [is_used]
GO
ALTER TABLE [cp].[Announcements]  WITH CHECK ADD  CONSTRAINT [FK_Announcements_Churches1] FOREIGN KEY([church_id])
REFERENCES [cp].[churches] ([id])
GO
ALTER TABLE [cp].[Announcements] CHECK CONSTRAINT [FK_Announcements_Churches1]
GO
ALTER TABLE [cp].[events]  WITH CHECK ADD  CONSTRAINT [FK_events_Churches] FOREIGN KEY([church_id])
REFERENCES [cp].[churches] ([id])
GO
ALTER TABLE [cp].[events] CHECK CONSTRAINT [FK_events_Churches]
GO
ALTER TABLE [cp].[I_Hv_Decided]  WITH CHECK ADD  CONSTRAINT [FK_I_Hv_Decided_0] FOREIGN KEY([church_id])
REFERENCES [cp].[churches] ([id])
GO
ALTER TABLE [cp].[I_Hv_Decided] CHECK CONSTRAINT [FK_I_Hv_Decided_0]
GO
ALTER TABLE [cp].[image_gallery]  WITH CHECK ADD  CONSTRAINT [FK_image_gallery_0] FOREIGN KEY([churchid])
REFERENCES [cp].[churches] ([id])
GO
ALTER TABLE [cp].[image_gallery] CHECK CONSTRAINT [FK_image_gallery_0]
GO
ALTER TABLE [cp].[planfeatures]  WITH CHECK ADD  CONSTRAINT [FK_planfeatures_0] FOREIGN KEY([plan_id])
REFERENCES [cp].[plans] ([ID])
GO
ALTER TABLE [cp].[planfeatures] CHECK CONSTRAINT [FK_planfeatures_0]
GO
ALTER TABLE [cp].[registration]  WITH CHECK ADD  CONSTRAINT [FK_registration_0] FOREIGN KEY([church_id])
REFERENCES [cp].[churches] ([id])
GO
ALTER TABLE [cp].[registration] CHECK CONSTRAINT [FK_registration_0]
GO
ALTER TABLE [cp].[series]  WITH NOCHECK ADD  CONSTRAINT [FK_series_1] FOREIGN KEY([church_id])
REFERENCES [cp].[churches] ([id])
GO
ALTER TABLE [cp].[series] CHECK CONSTRAINT [FK_series_1]
GO
ALTER TABLE [cp].[testimonials]  WITH CHECK ADD  CONSTRAINT [FK_testimonials_0] FOREIGN KEY([church_id])
REFERENCES [cp].[churches] ([id])
GO
ALTER TABLE [cp].[testimonials] CHECK CONSTRAINT [FK_testimonials_0]
GO
ALTER TABLE [cp].[users]  WITH CHECK ADD  CONSTRAINT [FK_users_0] FOREIGN KEY([church])
REFERENCES [cp].[churches] ([id])
GO
ALTER TABLE [cp].[users] CHECK CONSTRAINT [FK_users_0]
GO
ALTER TABLE [cp].[Words]  WITH CHECK ADD  CONSTRAINT [FK_Words_0] FOREIGN KEY([church_id])
REFERENCES [cp].[churches] ([id])
GO
ALTER TABLE [cp].[Words] CHECK CONSTRAINT [FK_Words_0]
GO
