USE [ChurchDataBase]
GO
/****** Object:  StoredProcedure [cp].[ApproveTestimonial]    Script Date: 05-01-2015 17:56:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[ApproveTestimonial]
 @Id Int OUTPUT,
 @status int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Update testimonials set status=@status where id = @Id
set @Id = @Id
	 
END
 




GO
/****** Object:  StoredProcedure [cp].[CreateAnnouncements]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreateAnnouncements] 
   @announcements_url  varchar(2000),
   @facebook_url varchar(2000),
   @twitter_url varchar(2000),
   @website_url varchar(2000),
   @geomap nvarchar(40),
   @church_id int,  
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	 
			insert into Announcements(announcements_url,facebook_url,twitter_url,website_url,geomap,church_id)
			values(@announcements_url,@facebook_url,@twitter_url,@website_url,@geomap,@church_id)
           
		set @Id = @@IDENTITY
      

END








GO
/****** Object:  StoredProcedure [cp].[CreateChurch]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreateChurch] --'Church96','Church96','capkey',0,'geoma','hist','abtfgg',1,null,null,'theme','868678686','sdfgs@sdfq','dfgdfgfg',0
   @name nvarchar(256),
   @description nvarchar(max),
   @church_api_key varchar(64),
   @enabled bit,
   @geomap nvarchar(40),
   @history nvarchar(max),
   @about nvarchar(max),
   @gallery_id smallint,    
   @banners nvarchar(2000),
   @splashscreen nvarchar(2000),
   @theme varchar(20),
   @phone_num varchar(20),
   @email varchar(100),
   @Address varchar(2000),	 
   @lat varchar(100),
   @long varchar(100),
   @planid int,
   @username varchar(100),
   @password varchar(50),
   @contactperson_name varchar(256),
   @contactperson_email varchar(100),
   @contactperson_phonenum varchar(50),
   @banner_format varchar(20),
   @splashscreen_format varchar(20),
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	 
			insert into churches(name,description,church_api_key,enabled,geomap,history,about,
            gallery_id,modified_on_utc,banners,splashscreen,theme,phone_num,email,Address,lat,long,
			planid,username,password,contactperson_name,contactperson_email,contactperson_phonenum,banner_format,splashscreen_format)
			values(@name,@description,@church_api_key,@enabled,@geomap,@history,@about,
            @gallery_id,getdate(),@banners,@splashscreen,@theme,@phone_num,@email,@Address,@lat,@long,
			@planid,@username,@password,@contactperson_name,@contactperson_email,@contactperson_phonenum,@banner_format,@splashscreen_format)
		set @Id = @@IDENTITY
      

END











GO
/****** Object:  StoredProcedure [cp].[CreateEvent]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreateEvent] 
   @name nvarchar(255),
   @author nvarchar(64),
   @church_id int,
   @gallery_id int,
   @is_used bit,
   @starts datetime,
   @ends datetime,
   @description nvarchar(512),	 
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			insert into events(name,author,church_id,gallery_id,is_used,starts,ends,description)
            values(@name,@author,@church_id,@gallery_id,@is_used,@starts,@ends,@description)
            
		set @Id = @@IDENTITY
   

END







GO
/****** Object:  StoredProcedure [cp].[CreateIhaveDecided]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreateIhaveDecided] --'Church96','Church96','capkey',0,'geoma','hist','abtfgg',1,null,null,'theme','868678686','sdfgs@sdfq','dfgdfgfg',0
   @name nvarchar(256),    
   @email varchar(256),
   @phonenumber varchar(50),
   @accept_savior bit,
   @recommitted_life bit,
   @info_relationship bit,
   @church_id int,	 
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	   
			insert into I_Hv_Decided(name,email,phonenumber,accept_savior,recommitted_life,info_relationship,church_id)
			values(@name,@email,@phonenumber,@accept_savior,@recommitted_life,@info_relationship,@church_id)
		set @Id = @@IDENTITY
  

END







GO
/****** Object:  StoredProcedure [cp].[CreateImageGallery]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreateImageGallery]
 @name nvarchar(64),
 @imageurl nvarchar(512),
 @churchid int,
 @image_category int,
 @image_format varchar(20),
 @Id int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 

Insert into image_gallery(name,image_url,updated_on_utc,churchid,image_category,image_format) values(@name,@imageurl,getutcdate(),@churchid,@image_category,@image_format)
set @Id = @@IDENTITY
 
END
 






GO
/****** Object:  StoredProcedure [cp].[CreateOfferings]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreateOfferings]
@name varchar(500),
@enabled bit,
@Description varchar(2000),
@starts datetime,
@ends datetime,
@churchId int,
@Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
			 
			insert into offerings(name,enabled,starts,ends,createdDate,Description,church_id) 
			values(@name,@enabled,@starts,@ends,getutcdate(),@Description,@churchId)
		set @Id = @@IDENTITY
 

END






GO
/****** Object:  StoredProcedure [cp].[CreatePlan]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreatePlan]
 @name varchar(50),
 @Description varchar(100),
 @Amount nvarchar(50),
 @Id int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 

Insert into plans(Name,Description,Amount) values(@name,@Description,@Amount)
set @Id = @@IDENTITY
 
END
 




GO
/****** Object:  StoredProcedure [cp].[CreatePlanFeatures]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreatePlanFeatures]
 @Name varchar(50),
 @Description varchar(100),
 @plan_id int,
 @Id int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 

Insert into planfeatures(Name,Description,plan_id) values(@Name,@Description,@plan_id)
set @Id = @@IDENTITY
 
END
 




GO
/****** Object:  StoredProcedure [cp].[CreatePlays]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreatePlays] 
   @name nvarchar(255),
   @author nvarchar(64),
   @sermon_id int,
   @gallery_id int,  
   @play_url nvarchar(1024),
   @is_used bit,
   @updated datetime,
   @duration datetime,
   @description nvarchar(512),	 
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			insert into plays(name,author,sermon_id,gallery_id,play_url,is_used,updated,duration,description)
            values(@name,@author,@sermon_id,@gallery_id,@play_url,@is_used,@updated,@duration,@description)
            
		set @Id = @@IDENTITY
   

END








GO
/****** Object:  StoredProcedure [cp].[CreatePrayer]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreatePrayer]
@name varchar(256),
@email varchar(256),
@message nvarchar(max),
@church_id int,
@enabled bit,
@confidential bit,
@contactme bit,
@phone nvarchar(max),
@Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
			 
			insert into prayers(name,email,message,church_id,enabled,confidential,contactme,phone)
			values(@name,@email,@message,@church_id,@enabled,@confidential,@contactme,@phone)
		set @Id = @@IDENTITY
 

END




GO
/****** Object:  StoredProcedure [cp].[CreateSeries]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreateSeries] 
   @name nvarchar(255),
   @author nvarchar(64),
   @church_id int,
   @gallery_id int,     
   @is_used bit,
   @created datetime,
   @updated datetime,
   @ends datetime, 
   @description nvarchar(512),	 
   @ThumbNail nvarchar(2000),
   @ThumbNailFormat varchar(20),
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			insert into series(name,author,church_id,gallery_id,is_used,created,updated,ends,description,ThumbNail,ThumbNailFormat)
            values(@name,@author,@church_id,@gallery_id,@is_used,@created,@updated,@ends,@description,@ThumbNail,@ThumbNailFormat)
            
		set @Id = @@IDENTITY
   

END












GO
/****** Object:  StoredProcedure [cp].[CreateSermons]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO









-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreateSermons] 
   @name nvarchar(255),
   @author nvarchar(64),
   @series_id int,
   @gallery_id int,  
   @sermon_url nvarchar(1024),
   @is_used bit,
   @updated datetime,
   @duration varchar(20),
   @description nvarchar(512),	 
   @sermon_format varchar(20),
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			insert into sermons(name,author,series_id,gallery_id,sermon_url,is_used,updated,duration,description,sermon_format)
            values(@name,@author,@series_id,@gallery_id,@sermon_url,@is_used,@updated,@duration,@description,@sermon_format)
            
		set @Id = @@IDENTITY
   

END












GO
/****** Object:  StoredProcedure [cp].[CreateTestimonial]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[CreateTestimonial]
 
@email varchar(255),
@author nvarchar(256),
@church_id int,
@is_used bit, 
@topic nvarchar(255),
@description nvarchar(2048),
@gallery_id smallint,
@status int,
@location varchar(100),
@Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
			 
			insert into testimonials(email ,author,church_id ,is_used,created ,topic,description,gallery_id,status,location)
			values(@email ,@author,@church_id ,@is_used,getutcdate() ,@topic,@description,@gallery_id,@status,@location)
		 set @Id = @@IDENTITY
 

END







GO
/****** Object:  StoredProcedure [cp].[DeleteAnnouncements]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeleteAnnouncements] 
  
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	 
			Delete from Announcements where id = @Id
 
		set @Id = @Id
      

END









GO
/****** Object:  StoredProcedure [cp].[DeleteChurch]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeleteChurch] 
   @Id int output
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  Delete from churches where id = @Id
	 set @Id = @Id

END
 
GO
/****** Object:  StoredProcedure [cp].[DeleteEvent]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeleteEvent]   
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
		Delete from events where id = @Id
 
		set @Id = @Id
   

END







GO
/****** Object:  StoredProcedure [cp].[DeleteImageGallery]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeleteImageGallery] 
   
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			Delete from image_gallery where id = @Id
              
		set @Id = @Id
   

END


 

GO
/****** Object:  StoredProcedure [cp].[DeleteOfferings]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeleteOfferings]   
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
		Delete from offerings where id = @Id
 
		set @Id = @Id
   

END








GO
/****** Object:  StoredProcedure [cp].[DeletePlan]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeletePlan]
@Id int output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 
Delete from plans where id = @Id
 set @Id = @Id
END
 



GO
/****** Object:  StoredProcedure [cp].[DeletePlanFeatures]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeletePlanFeatures]
@Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Delete from planfeatures where id = @Id
set @Id = @Id
END
 




GO
/****** Object:  StoredProcedure [cp].[DeletePlays]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeletePlays] 
 
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
		Delete from plays where id=@Id
      
            
		set @Id =@Id
   

END








GO
/****** Object:  StoredProcedure [cp].[DeleteSeries]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeleteSeries] 
   
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			Delete from series where id = @Id
              
		set @Id = @Id
   

END


 

GO
/****** Object:  StoredProcedure [cp].[DeleteSermons]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO







-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[DeleteSermons] 
 
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
		Delete from sermons where id=@Id
      
            
		set @Id =@Id
   

END










GO
/****** Object:  StoredProcedure [cp].[GetAllChurches]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [cp].[GetAllChurches]
@geo nvarchar(40),
@churchid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


 if(@churchid = 0)
	Select * from cp.churches  
	order by name 
 else
  select * from cp.churches where id = @churchid  
  order by name 
END





 




GO
/****** Object:  StoredProcedure [cp].[GetAnnouncements]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [cp].[GetAnnouncements]
@Churchid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


 
	Select * from cp.Announcements where church_id = @Churchid
	 

END




GO
/****** Object:  StoredProcedure [cp].[GetChurches]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetChurches]
@geo nvarchar(40),
@churchid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


 if(@churchid = 0)
	Select * from cp.churches  where enabled=1
	order by name 
 else
  select * from cp.churches where id = @churchid  
  order by name 
END





 



GO
/****** Object:  StoredProcedure [cp].[GetCountValues]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetCountValues]
@churchid int  
AS
BEGIN
	 
	SET NOCOUNT ON;

	declare @eventsCnt int
	declare @seriesCnt int
	declare @TestPendingCnt int
	declare @TestApprovedCnt int

select @eventsCnt = count(*)  from cp.events where church_id = @churchid

select @seriesCnt = count(*)  from cp.series where church_id = @churchid

select @TestPendingCnt = count(*)  from cp.testimonials where status = 4 and church_id = @churchid

select @TestApprovedCnt = count(*)  from cp.testimonials where status = 1 and church_id = @churchid

select @eventsCnt eventsCnt ,@seriesCnt seriesCnt,@TestPendingCnt TestPendingCnt,@TestApprovedCnt TestApprovedCnt

END







GO
/****** Object:  StoredProcedure [cp].[GetEvents]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetEvents]
@churchid int,
@eventid int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 if(@eventid = 0)
	select * from cp.events where church_id = @churchid
	 order by starts
	else
	select * from cp.events where id=@eventid
	 order by starts
END






GO
/****** Object:  StoredProcedure [cp].[GetIhaveDecided]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [cp].[GetIhaveDecided]  
@churchid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 
  select * from cp.I_Hv_Decided where church_id = @churchid 
 
END




 



GO
/****** Object:  StoredProcedure [cp].[GetImages]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetImages]
@ChurchId int,
@imageid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	if(@imageid = 0)
	select id,name,image_url,updated_on_utc,gallery_id,theme,churchid,image_category,image_format from cp.image_gallery where churchid = @ChurchId
	else
	select id,name,image_url,updated_on_utc,gallery_id,theme,churchid,image_category,image_format from cp.image_gallery where id=@imageid
END







GO
/****** Object:  StoredProcedure [cp].[GetOfferings]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetOfferings]
 @churchid int
AS
BEGIN
 
	SET NOCOUNT ON;
 
	select * from cp.offerings where church_id = @churchid
	 order by name
END







GO
/****** Object:  StoredProcedure [cp].[GetPlanFeatures]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetPlanFeatures]
 @PlanId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 Select * from PlanFeatures where plan_id = @PlanId
END







GO
/****** Object:  StoredProcedure [cp].[GetPlans]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetPlans]
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 Select * from plans
END






GO
/****** Object:  StoredProcedure [cp].[GetPlansandFeatures]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetPlansandFeatures]
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select p.Id PlanID, p.Name [Plan],p.Amount,pf.Name PlanFeature from cp.plans p
inner join 
cp.planfeatures pf
on p.id=pf.plan_id
order by P.Name
END








GO
/****** Object:  StoredProcedure [cp].[GetPlays]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetPlays]
@sermonid int,
@playid int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 if(@playid = 0)
	select * from cp.plays where sermon_id = @sermonid
	else
	select * from cp.plays where id = @playid
END





GO
/****** Object:  StoredProcedure [cp].[GetPrayers]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetPrayers]
@churchid int,
@prayerid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


 if(@prayerid = 0)
	select * from cp.prayers where church_id = @churchid
	else
	select * from cp.prayers where id=@prayerid
END





GO
/****** Object:  StoredProcedure [cp].[GetSeries]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetSeries]
@churchid int,
@seriesId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 
	--SELECT id, name, gallery_id, (select count(*) from cp.plays p where p.sermon_id = s.id) as play_count
	--from cp.sermons s
	 
	 if(@seriesId = 0)
	select * from cp.series where church_id = @churchid
	else
	select * from cp.series where id=@seriesId

END






GO
/****** Object:  StoredProcedure [cp].[GetSermons]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetSermons]
@seriesid int,
@sermonid int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 if(@sermonid = 0)
	select * from cp.sermons where series_id = @seriesid
	else
	select * from cp.sermons where id = @sermonid
END








GO
/****** Object:  StoredProcedure [cp].[GetTestimonials]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetTestimonials]
@churchid int,
@testimonialid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	if(@testimonialid = 0) 
	select * from cp.testimonials where church_id = @churchid and status = 3
	else
	select * from cp.testimonials where id = @testimonialid and status = 3

END






GO
/****** Object:  StoredProcedure [cp].[GetTestimonialsByStatus]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetTestimonialsByStatus] 
@status int, 
@churchid int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 
	select * from cp.testimonials where [status] = @status and church_id = @churchid

END







GO
/****** Object:  StoredProcedure [cp].[GetVerseoftheDay]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[GetVerseoftheDay]
 AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	Select * from cp.verses 	 

END





GO
/****** Object:  StoredProcedure [cp].[GetWordoftheDay]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [cp].[GetWordoftheDay]
@ChurchId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON; 
	Select * from cp.Words where church_id = @ChurchId
	 

END




GO
/****** Object:  StoredProcedure [cp].[RegisterDevice]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[RegisterDevice] --100002,'DDC21E1C-C1F6-4DB7-9C68-82B66F94236F','1234','iOS',0
   @ChurchId int,
   @UDID  varchar(100),
   @DeviceToken varchar(100),
   @DeviceType varchar(50), 	 
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
		declare @cnt int
		select @cnt = count(*) from Device where UDID = @UDID and ChurchId = @ChurchId
	  
	  if(@cnt = 0)
	  Begin
			insert into Device(UDID,DeviceToken,DeviceType,ChurchId,CreatedDate)
            values(@UDID,@DeviceToken,@DeviceType,@ChurchId,getutcdate())
				set @Id = @@IDENTITY
      END
	ELSE
	BEGIN
	      Update Device set DeviceToken = @DeviceToken where UDID = @UDID and ChurchId = @ChurchId 
		  Select @Id = ID from Device  where UDID = @UDID and ChurchId = @ChurchId  and  DeviceToken = @DeviceToken
		  set @Id = @Id
	END
   

END









GO
/****** Object:  StoredProcedure [cp].[RegisterUsers]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[RegisterUsers]
 
	@email varchar(255),
	@password varchar(50),
	@conf_password varchar(50),
	@firstname varchar(100),
	@lastname varchar(100),
	@country varchar(200),
	@gender varchar(20),
	@age int,
	@church_id int,
    @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
			 
			insert into registration(email,password,conf_password,firstname,lastname,country,gender,age,church_id)
			values(@email,@password,@conf_password,@firstname,@lastname,@country,@gender,@age,@church_id)
		set @Id = @@IDENTITY
 

END





GO
/****** Object:  StoredProcedure [cp].[UpdateAnnouncements]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdateAnnouncements] 
   @announcements_url  varchar(2000),
   @facebook_url varchar(2000),
   @twitter_url varchar(2000),
   @website_url varchar(2000),
   @geomap nvarchar(40),
   @church_id int,  
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	 
			Update Announcements set announcements_url=@announcements_url,facebook_url=@facebook_url,
			twitter_url=@twitter_url,website_url=@website_url,geomap=@geomap,church_id=@church_id
            where id = @id
		set @Id = @Id
      

END









GO
/****** Object:  StoredProcedure [cp].[UpdateChurch]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


 
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdateChurch] -- 'Church96','Church96','capkey',0,'long:565','hist','abtfgg',1,null,null,'theme','868678686','sdfgs@sdfq','dfgdfgfg',100098
   @name nvarchar(256),
   @description nvarchar(max),
   @church_api_key varchar(64),
   @enabled bit,
   @geomap nvarchar(40),
   @history nvarchar(max),
   @about nvarchar(max),
   @gallery_id smallint,  
   @banners nvarchar(2000),
   @splashscreen nvarchar(2000),
   @theme varchar(20),
   @phone_num varchar(20),
   @email varchar(100),
   @Address varchar(2000),	
   @lat varchar(100),
   @long varchar(100), 
   @planid int,
   @username varchar(100),
   @password varchar(50),
   @contactperson_name varchar(256),
   @contactperson_email varchar(100),
   @contactperson_phonenum varchar(50),
   @banner_format varchar(20),
   @splashscreen_format varchar(20),
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
		    update churches set name = @name,description=@description,church_api_key=@church_api_key,enabled=@enabled,
			geomap=@geomap,history=@history,about=@about,gallery_id=@gallery_id,modified_on_utc=getdate(),
			banners=@banners,splashscreen=@splashscreen,theme=@theme,phone_num=@phone_num,email=@email,Address=@Address,
			lat=@lat,long=@long,planid=@planid, username=@username,password=@password,contactperson_name=@contactperson_name,
			contactperson_email=@contactperson_email,contactperson_phonenum=@contactperson_phonenum,banner_format=@banner_format,
			splashscreen_format=@splashscreen_format
			where id=@Id
		set @Id = @Id
	 

END











GO
/****** Object:  StoredProcedure [cp].[UpdateEvent]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdateEvent] 
   @name nvarchar(255),
   @author nvarchar(64),
   @church_id int,
   @gallery_id int,
   @is_used bit,
   @starts datetime,
   @ends datetime,
   @description nvarchar(512),	 
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			update events set name=@name,author=@author,church_id=@church_id
			,gallery_id=@gallery_id,is_used=@is_used,starts=@starts,ends=@ends,description=@description
            where  Id = @Id
		set @Id = @Id
   

END








GO
/****** Object:  StoredProcedure [cp].[UpdateImageGallery]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



 

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdateImageGallery]
 @name nvarchar(64),
 @imageurl nvarchar(512),
 @churchid int,
 @image_category int,
 @image_format varchar(20),
 @Id int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
 

Update  image_gallery
set name=@name,image_url=@imageurl,updated_on_utc=getutcdate(),churchid=@churchid,image_category=@image_category,image_format=@image_format
where id = @Id
set @Id = @@IDENTITY
 
END
 







GO
/****** Object:  StoredProcedure [cp].[UpdateOfferings]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdateOfferings]
@name varchar(500),
@enabled bit,
@Description varchar(2000),
@starts datetime,
@ends datetime,
@churchId int,
@Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
			 
			Update offerings set name=@name, enabled=@enabled,
			 Description=@Description, starts=@starts, ends=@ends, church_id = @churchId
			 where id = @id
		set @Id = @Id
 

END






GO
/****** Object:  StoredProcedure [cp].[UpdatePlan]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdatePlan]
 @name varchar(50),
 @Description varchar(100),
 @Amount nvarchar(50),
 @Id int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Update plans set Name=@name , Description = @Description ,@Amount =  Amount  where Id = @Id
set @Id = @Id
	 
END
 



GO
/****** Object:  StoredProcedure [cp].[UpdatePlanFeatures]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdatePlanFeatures]
 @name varchar(50),
 @Description varchar(100),
 @plan_id int,
 @ID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Update planfeatures set Name=@name , Description = @Description , plan_id = @plan_id where id = @ID
set @Id = @Id
	 
END
 




GO
/****** Object:  StoredProcedure [cp].[UpdatePlays]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdatePlays] 
   @name nvarchar(255),
   @author nvarchar(64),
   @sermon_id int,
   @gallery_id int,  
   @play_url nvarchar(1024),
   @is_used bit,
   @updated datetime,
   @duration datetime,
   @description nvarchar(512),	 
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			update plays set name=@name,author=@author,sermon_id=@sermon_id,gallery_id=@gallery_id,play_url=@play_url,
			is_used=@is_used,updated=@updated,duration=@duration,description=@description 
            where id=@Id
            
		set @Id =@Id
   

END









GO
/****** Object:  StoredProcedure [cp].[UpdateSeries]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdateSeries] 
   @name nvarchar(255),
   @author nvarchar(64),
   @church_id int,
   @gallery_id int,     
   @is_used bit,
   @created datetime,
   @updated datetime,
   @ends datetime, 
   @description nvarchar(512),	 
   @ThumbNail nvarchar(2000),
   @ThumbNailFormat varchar(20),
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			update series set name=@name,author=@author,church_id=@church_id,gallery_id=@gallery_id,
			is_used=@is_used,created=@created,updated=@updated,ends=@ends,description=@description,ThumbNail=@ThumbNail,ThumbNailFormat=@ThumbNailFormat
            where id=@id
		set @Id = @Id
   

END


 





GO
/****** Object:  StoredProcedure [cp].[UpdateSermons]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO










-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdateSermons] 
   @name nvarchar(255),
   @author nvarchar(64),
   @series_id int,
   @gallery_id int,  
   @sermon_url nvarchar(1024),
   @is_used bit,
   @updated datetime,
   @duration varchar(20),
   @description nvarchar(512),	 
   @sermon_format varchar(20),
   @Id int OUTPUT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
	  
			update sermons set name=@name,author=@author,series_id=@series_id,gallery_id=@gallery_id,sermon_url=@sermon_url,
			is_used=@is_used,updated=@updated,duration=@duration,description=@description,sermon_format=@sermon_format 
            where id=@Id
            
		set @Id =@Id
   

END













GO
/****** Object:  StoredProcedure [cp].[UpdateVerseOftheDay]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [cp].[UpdateVerseOftheDay]   
   @verse ntext,
   @author nvarchar(32) 
   
    
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
		SET NOCOUNT ON;
		declare @cnt int
		declare @LastModified Datetime
		select @cnt = count(*) from Verses
	  if(@cnt = 0)
	  Begin
			insert into Verses(Verse,Author,LastModified)
            values(@verse,@author,getutcdate())
        
	  End
	else
	Begin
		select @LastModified = LastModified from Verses
		 
		if(Convert(varchar,@LastModified,101) != Convert(varchar,getutcdate(),101))
		Begin
			Update Verses set Verse=@verse,Author=@author,LastModified=getutcdate()
		End
	End    
		
   

END









GO
/****** Object:  StoredProcedure [dbo].[LoginUser]    Script Date: 05-01-2015 17:56:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[LoginUser] --'fdsfa','fdsfa'
	@UserName [varchar](100),
	@Password [varchar](256)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
    -- Insert statements for procedure here
	SELECT * from cp.churches where username = @UserName and password = @Password 
END


GO
