﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using NewAPIChurchAPP.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.WindowsAzure.MediaServices.Client;
using System.Threading;

namespace NewAPIChurchAPP
{
    public partial class sampleUpload : System.Web.UI.Page
    {
        Utility.CustomResponse Result = new Utility.CustomResponse();
        protected void Page_Load(object sender, EventArgs e)
        {
           

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Handlefile();
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string jsonString = javaScriptSerializer.Serialize(Result);
            Response.Write(jsonString);

        }

        private Utility.CustomResponse Handlefile()
        {
            string baseURL = ConfigurationManager.AppSettings["BaseURL"].ToString();
            int length = (int)Request.InputStream.Length;
            byte[] buffer = new byte[length];
            Stream stream = Request.InputStream;
            Request.ContentType = "audio/mp4";
            Sermons sermons = new Sermons();

            string sermonurl = streamaudio(stream, "mp4");

            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(baseURL);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));


            sermons.Id = 0;//Convert.ToInt32(Request.Form["Id"]);
            sermons.Name = "Srmontest";
            sermons.Description = "Srmontest";
            sermons.Enabled = true;
            sermons.Author = "Srmontest";
            sermons.SeriesId = 3086;
            sermons.GalleryId = 0;// Convert.ToInt16(collection["GalleryId"]);
            sermons.SermonUrl = sermonurl;
            sermons.Updated = DateTime.Now;
            sermons.Duration = "1";// Convert.ToDateTime(Request.Form["Ends"].ToString()); 
            sermons.SermonFormat = "mp4";

            var response = client.PostAsJsonAsync("Sermons/CreateSermon", sermons).Result;

            if (response.IsSuccessStatusCode)
            {
                Result.Status = Utility.CustomResponseStatus.Successful;
                Result.Response = sermons;
                Result.Message = CustomConstants.DetailsGetSuccessfully;


            }
            else
            {
                Result.Status = Utility.CustomResponseStatus.UnSuccessful;

                Result.Message = CustomConstants.NoRecordsFound;
            }
            return Result;
        }
        //void Save(byte[] bytes)
        //{
        //    System.IO.File.WriteAllBytes(Path.Combine(HttpContext.Current.Server.MapPath("~/"), "FileName.3gp"), bytes);
        //}

        public string streamaudio(Stream stream, string format)
        {

            Random ran = new Random();
            var fileNamebanners = "fn" + ran.Next() + "." + format;
            var path = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/"), fileNamebanners);
            //if (!System.IO.File.Exists(path))
            //{
            //    Request.Files["files"].SaveAs(path);
            //}
            var ext = format;
            string mimeTypebanners = "audio/mp4";
            Stream fileStreambanners = stream;

            fileNamebanners = fileNamebanners.Trim().Replace(" ", "_");
            string blobpath = UploadImage(fileNamebanners, path, ext, fileStreambanners, mimeTypebanners);
            string sermonURL = UploadImage1(fileNamebanners, blobpath, ext, fileStreambanners, mimeTypebanners);
            //if (System.IO.File.Exists(path))
            //{
            //    System.IO.File.Delete(path);
            //}
            return sermonURL;
        }

        public string UploadImage(string fileName, string path, string ext, Stream InputStream, string ContentType)
        {
            string imageurl = string.Empty;
            try
            {
                Random r = new Random();

                string filename = r.Next(9999).ToString() + DateTime.Now.Millisecond + r.Next(9999) + r.Next(9999).ToString() + DateTime.Now.Millisecond;

                // Retrieve storage account from connection string.
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnectionString"].ToString());
                // Create the blob client.
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();



                // Retrieve a reference to a container. 
                Microsoft.WindowsAzure.Storage.Blob.CloudBlobContainer container = blobClient.GetContainerReference("mycontainer");

                // Create the container if it doesn't already exist.
                container.CreateIfNotExists();


                container.SetPermissions(new Microsoft.WindowsAzure.Storage.Blob.BlobContainerPermissions
                {
                    PublicAccess =
                        Microsoft.WindowsAzure.Storage.Blob.BlobContainerPublicAccessType.Blob
                });


                string uniqueBlobName = string.Format(fileName, Guid.NewGuid().ToString(), ext);
                CloudBlockBlob blob = container.GetBlockBlobReference(uniqueBlobName);
                blob.Properties.ContentType = ContentType;
                blob.UploadFromStream(InputStream);


                imageurl = blob.Uri.ToString();

            }
            catch (Exception ex)
            {

            }

            return imageurl;

        }

        public string UploadImage1(string fileName, string path, string ext, Stream InputStream, string ContentType)
        {
            string account = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountName"].ToString();
            string key = System.Configuration.ConfigurationManager.AppSettings["MediaServicesAccountKey"].ToString();

            CloudMediaContext context = new CloudMediaContext(account, key);
            // path = path + "." + ext;
            var uploadAsset = context.Assets.Create(Path.GetFileNameWithoutExtension(path), AssetCreationOptions.None);

            var assetFile = uploadAsset.AssetFiles.Create(Path.GetFileName(path));

            //var accessPolicy1 = context.AccessPolicies.Create(fileName, TimeSpan.FromDays(3), AccessPermissions.Write | AccessPermissions.List);

            //  var locator1 = context.Locators.CreateLocator(LocatorType.Sas, uploadAsset, accessPolicy1);
            //Convert.ToString(Path.GetFileNameWithoutExtension(uploadAsset.Name)).ToUpper()
            assetFile.Upload(path);
            //  locator1.Delete();
            // accessPolicy1.Delete();


            //var encodeAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            //// Preset reference documentation: http://msdn.microsoft.com/en-us/library/windowsazure/jj129582.aspx
            //var encodingPreset = "H264 Broadband 720p";
            //var assetToEncode = context.Assets.Where(a => a.Id == encodeAssetId).FirstOrDefault();
            //if (assetToEncode == null)
            //{
            //    throw new ArgumentException("Could not find assetId: " + encodeAssetId);
            //}

            //IJob job = context.Jobs.Create("Encoding " + assetToEncode.Name + " to " + encodingPreset);

            //IMediaProcessor latestWameMediaProcessor = (from p in context.MediaProcessors where p.Name == "Azure Media Encoder" select p).ToList().OrderBy(wame => new Version(wame.Version)).LastOrDefault();
            //ITask encodeTask = job.Tasks.AddNew("Encoding", latestWameMediaProcessor, encodingPreset, TaskOptions.None);
            //encodeTask.InputAssets.Add(assetToEncode);
            //encodeTask.OutputAssets.AddNew(assetToEncode.Name + " as " + encodingPreset, AssetCreationOptions.None);

            //job.StateChanged += new EventHandler<JobStateChangedEventArgs>((sender, jsc) => Console.WriteLine(string.Format("{0}\n  State: {1}\n  Time: {2}\n\n", ((IJob)sender).Name, jsc.CurrentState, DateTime.UtcNow.ToString(@"yyyy_M_d_hhmmss"))));
            //job.Submit();
            //job.GetExecutionProgressTask(CancellationToken.None).Wait();

            //var preparedAsset = job.OutputMediaAssets.FirstOrDefault();



            var streamingAssetId = uploadAsset.Id; // "YOUR ASSET ID";
            var daysForWhichStreamingUrlIsActive = 10000;
            var streamingAsset = context.Assets.Where(a => a.Id == streamingAssetId).FirstOrDefault();
            var accessPolicy = context.AccessPolicies.Create(streamingAsset.Name, TimeSpan.FromDays(daysForWhichStreamingUrlIsActive),
                                                     AccessPermissions.Read);
            string streamingUrl = string.Empty;
            var assetFiles = streamingAsset.AssetFiles.ToList();
            //var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith("m3u8-aapl.ism")).FirstOrDefault();
            //var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().Equals(streamingAsset.Name.ToLower())).FirstOrDefault();
            //if (streamingAssetFile != null)
            //{
            //    var locator = context.Locators.CreateLocator(LocatorType.OnDemandOrigin, streamingAsset, accessPolicy);
            //    Uri hlsUri = new Uri(locator.Path + streamingAssetFile.Name + "/manifest(format=m3u8-aapl)");
            //    streamingUrl = hlsUri.ToString();
            //}
            //streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".ism")).FirstOrDefault();
            //if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            //{
            //    var locator = context.Locators.CreateLocator(LocatorType.OnDemandOrigin, streamingAsset, accessPolicy);
            //    Uri smoothUri = new Uri(locator.Path + streamingAssetFile.Name + "/manifest");
            //    streamingUrl = smoothUri.ToString();
            //}
            var streamingAssetFile = assetFiles.Where(f => f.Name.ToLower().EndsWith(".mp4")).FirstOrDefault();
            if (string.IsNullOrEmpty(streamingUrl) && streamingAssetFile != null)
            {
                var locator = context.Locators.CreateLocator(LocatorType.Sas, streamingAsset, accessPolicy);
                var mp4Uri = new UriBuilder(locator.Path);
                mp4Uri.Path += "/" + streamingAssetFile.Name;
                streamingUrl = mp4Uri.ToString();
            }
            return streamingUrl;

        }

    }
}


    public partial class Sermons
    {
        public Int32 Id { get; set; }
        public Int32 SeriesId { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public Boolean Enabled { get; set; }
        public String Description { get; set; }
        public String SermonUrl { get; set; }
        public String Duration { get; set; }
        public DateTime? Updated { get; set; }
        public Int16? GalleryId { get; set; }
        public String SermonFormat { get; set; }
        public Stream stream { get; set; }
    }
 