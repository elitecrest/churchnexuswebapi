﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewAPIChurchAPP.SubscriptionCoupons.Models
{
    /// <summary>
    /// Model class for Church Coupon entry
    /// </summary>
    public class ChurchCouponEntry
    {
        /// <summary>
        /// Church Id
        /// </summary>
        public int ChurchId { get; set; }

        /// <summary>
        /// Coupon Code
        /// </summary>
        public string CouponCode { get; set; }
    }
}