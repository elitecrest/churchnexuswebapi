﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewAPIChurchAPP.SubscriptionCoupons.Models
{
    /// <summary>
    /// Subscription Coupon model
    /// </summary>
    public class SubscriptionCoupon
    {
        /// <summary>
        /// Unique Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Coupon Code
        /// </summary>
        public string CouponCode { get; set; }

        /// <summary>
        /// Days
        /// </summary>
        public int Days { get; set; }

        /// <summary>
        /// Is Coupon Reedemed
        /// </summary>
        public bool IsReedemed { get; set; }

        /// <summary>
        /// Coupon Expiry date
        /// </summary>
        public DateTime? ExpiryDate { get; set; }

        /// <summary>
        /// Created Date
        /// </summary>
        public DateTime? CreatedDate { get; set; }

        /// <summary>
        /// Redeemed Church Id
        /// </summary>
        public int RedeemedChurchId { get; set; }

        /// <summary>
        /// Staus
        /// </summary>
        public bool Status { get; set; }

        /// <summary>
        /// Reedemed church Name
        /// </summary>
        public string RedeemedChurchName { get; set; }
    }
}