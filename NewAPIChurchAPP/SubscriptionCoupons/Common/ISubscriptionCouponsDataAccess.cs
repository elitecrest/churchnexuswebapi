﻿using NewAPIChurchAPP.SubscriptionCoupons.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewAPIChurchAPP.SubscriptionCoupons.Common
{
    /// <summary>
    /// Data Access block for subscription coupons
    /// </summary>
    public interface ISubscriptionCouponsDataAccess
    {
        /// <summary>
        /// Adds coupon to database
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns></returns>
        long AddCoupon(SubscriptionCoupon coupon);

        /// <summary>
        /// Updates coupon in database
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns></returns>
        void UpdateCoupon(SubscriptionCoupon coupon);
        /// <summary>
        /// Deletes given coupon
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns></returns>
        void DeleteCoupon(long couponId);

        /// <summary>
        /// Gets all coupons by coupon status
        /// </summary>
        /// <param name="status">active/deleted</param>
        /// <returns></returns>
        List<SubscriptionCoupon> GetAllCouponsByStatus(bool status);

        /// <summary>
        /// Gets church subscription coupons
        /// </summary>
        /// <param name="churchId"></param>
        /// <returns></returns>
        List<SubscriptionCoupon> GetChurchSubscriptionCoupons(int churchId);

        /// <summary>
        /// Adds given coupon to church
        /// </summary>
        /// <param name="churchId"></param>
        /// <param name="couponId"></param>
        int AddSubscriptionCouponToChurch(int churchId, string couponId);

    }
}