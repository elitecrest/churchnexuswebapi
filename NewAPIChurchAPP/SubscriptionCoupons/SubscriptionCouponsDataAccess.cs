﻿using NewAPIChurchAPP.SubscriptionCoupons.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NewAPIChurchAPP.SubscriptionCoupons.Models;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace NewAPIChurchAPP.SubscriptionCoupons
{
    /// <summary>
    /// Data Access block for subscription coupons
    /// </summary>
    public class SubscriptionCouponsDataAccess : ISubscriptionCouponsDataAccess
    {

        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ChurchDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        /// <summary>
        /// Adds coupon to database
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns></returns>
        public long AddCoupon(SubscriptionCoupon coupon)
        {
            long id = 0;

            SqlConnection myConn = ConnectTODB();

            SqlCommand cmd = new SqlCommand("[cp].[AddSubscriptionCoupon]", myConn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter coupon_code = cmd.Parameters.Add("@coupon_code", System.Data.SqlDbType.VarChar, 25);
            coupon_code.Direction = System.Data.ParameterDirection.Input;
            coupon_code.Value = coupon.CouponCode;

            SqlParameter days = cmd.Parameters.Add("@days", System.Data.SqlDbType.Int);
            days.Direction = System.Data.ParameterDirection.Input;
            days.Value = coupon.Days; ;

            SqlParameter expiry_date = cmd.Parameters.Add("@expiry_date", System.Data.SqlDbType.DateTime2);
            expiry_date.Direction = System.Data.ParameterDirection.Input;
            expiry_date.Value = coupon.ExpiryDate;

            SqlParameter idParam = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
            idParam.Direction = System.Data.ParameterDirection.Output;
            idParam.Value = 0;
            cmd.ExecuteNonQuery();

            id = (int)cmd.Parameters["@Id"].Value;
            myConn.Close();

            return id;
        }

        /// <summary>
        /// Updates coupon in database
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns></returns>

        public int AddSubscriptionCouponToChurch(int churchId, string couponId)
        {
            int status = -1;

            SqlConnection myConn = ConnectTODB();

            SqlCommand cmd = new SqlCommand("[cp].[AddSubscriptionCouponToChurch]", myConn);
            cmd.CommandType = CommandType.StoredProcedure;

            SqlParameter _churchIdParam = cmd.Parameters.Add("@churchId", SqlDbType.Int);
            _churchIdParam.Direction = ParameterDirection.Input;
            _churchIdParam.Value = churchId;

            SqlParameter couponCodeParam = cmd.Parameters.Add("@coupon_code", SqlDbType.VarChar, 25);
            couponCodeParam.Direction = ParameterDirection.Input;
            couponCodeParam.Value = couponId;

            SqlParameter statusParam = cmd.Parameters.Add("@status", System.Data.SqlDbType.Int);
            statusParam.Direction = System.Data.ParameterDirection.Output;
            statusParam.Value = 0;
            cmd.ExecuteNonQuery();

            status = (int)cmd.Parameters["@status"].Value;
            myConn.Close();

            return status;
        }

        /// <summary>
        /// Deletes given coupon
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns></returns>
        public void DeleteCoupon(long couponId)
        {
            SqlConnection myConn = ConnectTODB();

            SqlCommand cmd = new SqlCommand("[cp].[DeleteSubscriptionCoupon]", myConn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter Idpar = cmd.Parameters.Add("@id", System.Data.SqlDbType.Int);
            Idpar.Direction = System.Data.ParameterDirection.Input;
            Idpar.Value = couponId;

            cmd.ExecuteNonQuery();
            myConn.Close();
        }

        /// <summary>
        /// Gets all coupons by coupon status
        /// </summary>
        /// <param name="status">active/deleted</param>
        /// <returns></returns>
        public List<SubscriptionCoupon> GetAllCouponsByStatus(bool status)
        {
            List<SubscriptionCoupon> subscriptionCoupons = new List<SubscriptionCoupon>();

            SqlConnection myConn = ConnectTODB();

            SqlCommand cmd = new SqlCommand("[cp].[GetSubscriptionCouponsByStatus]", myConn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter statusParam = cmd.Parameters.Add("@status", System.Data.SqlDbType.Bit);
            statusParam.Direction = System.Data.ParameterDirection.Input;
            statusParam.Value = status;

            SqlDataReader myData = cmd.ExecuteReader();

            subscriptionCoupons = this.PopulateSubscriptionModel(myData);

            myData.Close();
            myConn.Close();

            return subscriptionCoupons;
        }

        /// <summary>
        /// Gets church subscription coupons
        /// </summary>
        /// <param name="churchId"></param>
        /// <returns></returns>
        public List<SubscriptionCoupon> GetChurchSubscriptionCoupons(int churchId)
        {
            List<SubscriptionCoupon> subscriptionCoupons = new List<SubscriptionCoupon>();

            SqlConnection myConn = ConnectTODB();

            SqlCommand cmd = new SqlCommand("[cp].[GetChurchSubscriptionCoupons]", myConn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter churchIdParam = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
            churchIdParam.Direction = System.Data.ParameterDirection.Input;
            churchIdParam.Value = churchId;

            SqlDataReader myData = cmd.ExecuteReader();

            subscriptionCoupons = this.PopulateSubscriptionModel(myData);

            myData.Close();
            myConn.Close();

            return subscriptionCoupons;
        }

        /// <summary>
        /// Adds given coupon to church
        /// </summary>
        /// <param name="churchId"></param>
        /// <param name="couponId"></param>
        public void UpdateCoupon(SubscriptionCoupon coupon)
        {
            SqlConnection myConn = ConnectTODB();

            SqlCommand cmd = new SqlCommand("[cp].[UpdateSubscriptionCoupon]", myConn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;

            SqlParameter idParam = cmd.Parameters.Add("@id", System.Data.SqlDbType.Int);
            idParam.Direction = System.Data.ParameterDirection.Input;
            idParam.Value = coupon.Id;

            SqlParameter couponCodeParam = cmd.Parameters.Add("@coupon_code", System.Data.SqlDbType.VarChar, 25);
            couponCodeParam.Direction = System.Data.ParameterDirection.Input;
            couponCodeParam.Value = coupon.CouponCode;

            SqlParameter daysParam = cmd.Parameters.Add("@days", System.Data.SqlDbType.Int);
            daysParam.Direction = System.Data.ParameterDirection.Input;
            daysParam.Value = coupon.Days;

            SqlParameter expiryDateParam = cmd.Parameters.Add("@expiry_date", System.Data.SqlDbType.DateTime2);
            expiryDateParam.Direction = System.Data.ParameterDirection.Input;
            expiryDateParam.Value = coupon.ExpiryDate;

            cmd.ExecuteNonQuery();
            myConn.Close();
        }

        #region Private methods

        private List<SubscriptionCoupon> PopulateSubscriptionModel(SqlDataReader myData)
        {
            List<SubscriptionCoupon> subscriptionCoupons = new List<SubscriptionCoupon>();

            while (myData.Read())
            {
                SubscriptionCoupon coupon = new SubscriptionCoupon();
                coupon.Id = (myData["id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["id"]));
                coupon.CouponCode = (string)myData["coupon_code"].ToString();
                coupon.CreatedDate = (DateTime?)myData["created_date"];
                coupon.Days = (int)myData["days"];
                coupon.ExpiryDate = (DateTime?)myData["expiry_date"];
                coupon.IsReedemed = (bool)myData["is_redeemed"];
                coupon.RedeemedChurchId = (myData["redeemed_church_id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["redeemed_church_id"]));
                coupon.RedeemedChurchName = (string)myData["name"].ToString();

                subscriptionCoupons.Add(coupon);
            }

            return subscriptionCoupons;
        }

        #endregion Private methods
    }
}