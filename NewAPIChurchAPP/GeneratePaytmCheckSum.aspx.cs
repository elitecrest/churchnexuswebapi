﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using paytm;
using NewAPIChurchAPP.Models;

namespace NewAPIChurchAPP
{
    public partial class GeneratePaytmCheckSum : System.Web.UI.Page
    {
        //protected void Page_Load(object sender, EventArgs e)
        //{

        //    if (Request.Form.AllKeys.Length > 0)
        //    {
        //        try
        //        {
        //            PaytmResonse paytmResponse = new PaytmResonse();

        //            Dictionary<string, string> parameters = new Dictionary<string, string>();
        //            string paytmChecksum = "";
        //            string paytmInputParams = "";

        //            foreach (string key in Request.Form.Keys)
        //            {
        //                parameters.Add(key.Trim(), Request.Form[key].Trim());
        //                paytmInputParams = paytmInputParams + key + "=" + Request.Form[key] + ",";
        //            }

        //            paytmChecksum = CheckSum.generateCheckSum(Utility.PaytmConstants.MERCHANT_KEY, parameters);

        //            if (parameters.ContainsKey("ORDER_ID") && parameters.ContainsKey("MID"))
        //            {
        //                Response.AddHeader("Content-type", "application/json");

        //                ChurchDAL.InsertPaymentLog(paytmInputParams, parameters["ORDER_ID"] + "," + paytmChecksum + "," + "1", "Generate");

        //                Response.Write("{\"ORDER_ID\":\"" + parameters["ORDER_ID"] + "\",\"CHECKSUMHASH\":\"" + paytmChecksum + "\",\"payt_STATUS\":\"1\"}");
        //            }
        //            else
        //            {
        //                Response.AddHeader("Content-type", "application/json");
        //                ChurchDAL.InsertPaymentLog(paytmInputParams, parameters["ORDER_ID"] + "," + paytmChecksum + "," + "2", "Generate");
        //                Response.Write("{\"ORDER_ID\":\"" + parameters["ORDER_ID"] + "\",\"CHECKSUMHASH\":\"" + paytmChecksum + "\",\"payt_STATUS\":\"2\"}");
        //            }



        //        }
        //        catch (Exception)
        //        {
        //        }


        //    }
        //}

        //protected void Page_Load(object sender, EventArgs e)
        //{
        //    if (Request.Form.AllKeys.Length > 0)
        //    {
        //        try
        //        {
        //            PaytmResonse paytmResponse = new PaytmResonse();

        //            Dictionary<string, string> parameters = new Dictionary<string, string>();
        //            parameters.Add("MID", "");
        //            parameters.Add("ORDER_ID", "");
        //            parameters.Add("CUST_ID", "");
        //            parameters.Add("CHANNEL_ID", "");
        //            parameters.Add("INDUSTRY_TYPE_ID", "");
        //            parameters.Add("WEBSITE", "");
        //            parameters.Add("TXN_AMOUNT", "");

        //            string paytmInputParams = "";
        //            string paytmChecksum = "";

        //            foreach (string key in Request.Form.Keys)
        //            {
        //                // below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .
        //                if (Request.Form[key].Trim().ToUpper().Contains("REFUND"))
        //                {
        //                    continue;
        //                }

        //                if (parameters.ContainsKey(key.Trim()))
        //                {
        //                    parameters[key.Trim()] = Request.Form[key].Trim();
        //                }
        //                else
        //                {
        //                    parameters.Add(key.Trim(), Request.Form[key].Trim());
        //                }
        //                paytmInputParams = paytmInputParams + key + "=" + Request.Form[key] + ",";
        //            }

        //            paytmChecksum = CheckSum.generateCheckSum(Utility.PaytmConstants.MERCHANT_KEY, parameters);

        //            if (parameters.ContainsKey("ORDER_ID") && parameters.ContainsKey("MID"))
        //            {
        //                Response.AddHeader("Content-type", "application/json");
        //                ChurchDAL.InsertPaymentLog(paytmInputParams, parameters["ORDER_ID"] + "," + paytmChecksum + "," + "1", "Generate");
        //                Response.Write("{\"ORDER_ID\":\"" + parameters["ORDER_ID"] + "\",\"CHECKSUMHASH\":\"" + paytmChecksum + "\",\"payt_STATUS\":\"1\"}");
        //            }
        //            else
        //            {
        //                Response.AddHeader("Content-type", "application/json");
        //                ChurchDAL.InsertPaymentLog(paytmInputParams, parameters["ORDER_ID"] + "," + paytmChecksum + "," + "2", "Generate");
        //                Response.Write("{\"ORDER_ID\":\"" + parameters["ORDER_ID"] + "\",\"CHECKSUMHASH\":\"" + paytmChecksum + "\",\"payt_STATUS\":\"2\"}");
        //            }
        //        }
        //        catch (Exception)
        //        {
        //        }


        //    }
        //}


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Form.AllKeys.Length > 0)
            {
                try
                {

                    Dictionary<string, string> parameters = new Dictionary<string, string>();

                    parameters.Add("MID", "");

                    parameters.Add("ORDER_ID", "");

                    parameters.Add("CUST_ID", "");

                    parameters.Add("CHANNEL_ID", "");

                    parameters.Add("INDUSTRY_TYPE_ID", "");

                    parameters.Add("WEBSITE", "");

                    parameters.Add("TXN_AMOUNT", "");


                    string paytmInputParams = "";
                    string paytmChecksum = "";

                    foreach (string key in Request.Form.Keys) 
                    { 
                        // below code snippet is mandatory, so that no one can use your checksumgeneration url for other purpose .

                        if (Request.Form[key].Trim().ToUpper().Contains("REFUND") || Request.Form[key].Trim().Contains("|")) 
                        { 
                            continue; 
                        }
                         
                        if (parameters.ContainsKey(key.Trim()))
                        {

                            parameters[key.Trim()] = Request.Form[key].Trim();

                        }
                        else
                        { 
                            parameters.Add(key.Trim(), Request.Form[key].Trim()); 
                        }

                        paytmInputParams = paytmInputParams + key + "=" + Request.Form[key] + ",";

                    }



                    paytmChecksum = CheckSum.generateCheckSum(Utility.PaytmConstants.MERCHANT_KEY, parameters);



                    if (parameters.ContainsKey("ORDER_ID") && parameters.ContainsKey("MID")) 
                    { 
                        Response.AddHeader("Content-type", "application/json");
                        ChurchDAL.InsertPaymentLog(paytmInputParams, parameters["ORDER_ID"] + "," + "1", "Generate", paytmChecksum);
                        Response.Write("{\"ORDER_ID\":\"" + parameters["ORDER_ID"] + "\",\"CHECKSUMHASH\":\"" + paytmChecksum + "\",\"payt_STATUS\":\"1\"}"); 
                    }

                    else 
                    { 
                        Response.AddHeader("Content-type", "application/json");
                        ChurchDAL.InsertPaymentLog(paytmInputParams, parameters["ORDER_ID"]  + "," + "2", "Generate", paytmChecksum);
                        Response.Write("{\"ORDER_ID\":\"" + parameters["ORDER_ID"] + "\",\"CHECKSUMHASH\":\"" + paytmChecksum + "\",\"payt_STATUS\":\"2\"}"); 
                    }

                }

                catch (Exception)

                {

                } 

            }

        }

    }
}