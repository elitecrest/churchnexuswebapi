﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail; 
using System.Web;

namespace NewAPIChurchAPP.Models
{
    [AttributeUsage(AttributeTargets.Property,
                Inherited = false,
                AllowMultiple = false)]
    internal sealed class OptionalAttribute : Attribute
    {
    }

    public partial class ChurchDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Boolean Enabled { get; set; }
        public String Geomap { get; set; }
        public String History { get; set; }
        public String About { get; set; }
        public Int16? GalleryId { get; set; }
        public DateTime? LastModified { get; set; }
        public String Banners { get; set; }
        public String Splashscreen { get; set; }
        public String Theme { get; set; }
        public String PhoneNum { get; set; }
        public String Email { get; set; }
        public String Address { get; set; }
        public String ChurchAPIKey { get; set; }
        public String Latitude { get; set; }
        public String Longitude { get; set; }
        public Int32 PlanId { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String ContactPersonName { get; set; }
        public String ContactPersonEmail { get; set; }
        public String ContactPersonPhoneNum { get; set; }
        public String BannerFormat { get; set; }
        public String SplashScreenFormat { get; set; }
        public Boolean IsPushPay { get; set; }
        public Boolean IsPayPal { get; set; }
        public String GiveURL { get; set; }
        public String State { get; set; }
        public String City { get; set; }
        public String Zip { get; set; }
        public String Country { get; set; }
        public Boolean IsContent { get; set; }
        public Boolean EnableStatus { get; set; }
        public List<ChurchFeaturesDto> ChurchFeatures { get; set; }
        public string AdminFirstName { get; set; }
        public string AdminLastName { get; set; }
        public int PlanTypeId { get; set; }
        public int OrderId { get; set; }
        public int IsKidsEnabled { get; set; } 
        public bool HasValidSubscription { get; set; } 
 
        public int totalPromotionDays { get; set; } 
       
        public double TotalSubscriptionDue { get; set; }
         
        public double PlanAmount { get; set; } 
        public DateTime? SignupDate { get; set; }
        public int PaymentMode { get; set; }
        public bool PushNotificationAccept { get;   set; }
        public int DesignTemplateId { get;   set; }
        public int PlanFeatureId { get;   set; } 
        public List<ChurchPdfFileDto> PDFFiles { get;   set; } 
        public int OptKidsRegistration { get; set; }
        public int OptChurchStore { get; set; }
        public int OptLiveStream { get; set; } 
        public String Facebook_Url { get; set; }
        public String Twitter_Url { get; set; }
        public String Website_Url { get; set; } 
        public String Announcements_Url { get; set; }

    }

    public partial class GiveURLDTO
    {
        public Boolean IsPushPay { get; set; }
        public Boolean IsPayPal { get; set; }
        public String GiveURL { get; set; }
    }
    public partial class EventsDTO
    {
        public EventsDTO()
        {
            this.Author = string.Empty;
            this.Enabled = true;
            this.Description = string.Empty;
            this.GalleryId = 0;
        }
        public Int32 Id { get; set; }
        public Int32 ChurchId { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public Boolean Enabled { get; set; }
        public String Description { get; set; }
        public Int16? GalleryId { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public bool PushNotificationAccept { get;   set; }
        public string ChurchName { get;   set; }
    }

    public partial class ImageDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String ImageUrl { get; set; }
        public DateTime? Updated { get; set; }
        public Byte[] Img { get; set; }
        public String ImageSize { get; set; }
        public Int16? GalleryId { get; set; }
        public Byte[] ImageHash { get; set; }
        public String Theme { get; set; }
        public Int32 ChurchId { get; set; }
        public int ImageCategoryID { get; set; }
        public String ImageFormat { get; set; }
    }

    public partial class SermonDTO
    {
        public Int32 Id { get; set; }
        public Int32 ChurchId { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public Boolean Enabled { get; set; }
        public String Description { get; set; }
        public Int16? GalleryId { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Ends { get; set; }
        public String ThumbNail { get; set; }
    }

    public partial class PlaysDTO
    {
        public Int32 Id { get; set; }
        public Int32 SermonId { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public Boolean Enabled { get; set; }
        public String Description { get; set; }
        public String PlayUrl { get; set; }
        public DateTime? Duration { get; set; }
        public DateTime? Updated { get; set; }
        public Int16? GalleryId { get; set; }
    }

    public partial class PrayersDTO
    {
        public Int32 Id { get; set; }
        public Int32 ChurchId { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public String Message { get; set; }
        public Boolean Enabled { get; set; }
        public Boolean Confidential { get; set; }
        public Boolean ContactMe { get; set; }
        public String Phone { get; set; }

    }

    public partial class TestimonialsDTO
    {
        public Int32 Id { get; set; }
        public Int32 ChurchId { get; set; }
        public String Author { get; set; }
        public String Email { get; set; }
        public Int16? GalleryId { get; set; }
        public Boolean Enabled { get; set; }
        public DateTime? Created { get; set; }
        public String Topic { get; set; }
        public String Description { get; set; }
        public int Status { get; set; }
        public String Location { get; set; }
    }

    public partial class UsersDTO
    {
        public Int32 Id { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public String ConfirmPassword { get; set; }
        public String FirstName { get; set; }
        public String LastName { get; set; }
        public String Country { get; set; }
        public String Gender { get; set; }
        public Int32 Age { get; set; }
        public int ChurchID { get; set; }

    }

    public partial class DecidedDTO
    {

        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public String PhoneNumber { get; set; }
        public Boolean AcceptSavior { get; set; }
        public Boolean RecommittedLife { get; set; }
        public Boolean InfoRelationship { get; set; }
        public Int32 ChurchId { get; set; }


    }

    public partial class LatLongDTO
    {
        public String Latitude { get; set; }
        public String Longitude { get; set; }

    }

    public partial class ProgramSheetDTO
    {
        public int ProgramSheetId { get; set; }
        public string Name { get; set; }
        public DateTime HappenedDate { get; set; }
        public string Description { get; set; }
        public int ChurchId { get; set; }

    }

    public partial class ProgramSheetDetailsDTO
    {
        public string Type { get; set; }
        public int OrderNo { get; set; }
        public string Description { get; set; }
        public string Lyrics { get; set; }
        public int ProgramSheetId { get; set; }
        public int Id { get; set; }

    }

    public partial class WordDTO
    {
        public Int32 Id { get; set; }
        public String Word { get; set; }
        public Int32 ChurchId { get; set; }
    }

    public partial class AnnouncementDTO
    {
        public Int32 Id { get; set; }
        public String Announcements_Url { get; set; }
        public String Facebook_Url { get; set; }
        public String Twitter_Url { get; set; }
        public String Website_Url { get; set; }
        public String GeoMap { get; set; }
        public Int32 ChurchId { get; set; }


    }

    public partial class VerseDTO
    {
        public Int32 Id { get; set; }
        public String Verse { get; set; }
        public String Author { get; set; }
        public String Reference { get; set; }
        public DateTime LastModified { get; set; }
        public int Church_id { get; set; }
        public int Status { get; set; }

    }


    public partial class PlanDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public string Amount { get; set; }

        public string Country { get; set; }
    }


    public partial class PlanFeaturesDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public Int32 PlanId { get; set; }
    }


    public partial class PlanandFeaturesDTO
    {
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public string Amount { get; set; }
        public List<string> PlanFeaturesName { get; set; }
    }

    public partial class SeriesDTO
    {
        public Int32 Id { get; set; }
        public Int32 ChurchId { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public Boolean Enabled { get; set; }
        public String Description { get; set; }
        public Int16? GalleryId { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Ends { get; set; }
        public String ThumbNail { get; set; }
        public String ThumbNailFormat { get; set; }
    }

    public partial class SermonSeriesDTO
    {
        public Int32 Id { get; set; }
        public Int32 SeriesId { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public Boolean Enabled { get; set; }
        public String Description { get; set; }
        public String SermonUrl { get; set; }
        public String Duration { get; set; }
        public DateTime? Updated { get; set; }
        public Int16? GalleryId { get; set; }
        public String SermonFormat { get; set; }
        public Stream stream { get; set; }
        public DateTime? Created { get; set; }
        public int ChurchId { get;   set; }
        public bool PushNotificationAccept { get;   set; }
        public string FBDeviceToken { get;   set; }
        public string ChurchName { get;   set; }
        public string ContentType { get;   set; }
        public string UpdatedDateTime { get;   set; }
    }

    public partial class AnnouncmentDataDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AnnouncementUrl { get; set; }
        public bool IsUsed { get; set; }
        public DateTime Updated { get; set; }
        public string Duration { get; set; }
        public string Description { get; set; }
        public int FileTypeId { get; set; }
        public string FiletTypeName { get; set; }
        public int Status { get; set; }
        public DateTime ScheduledDate { get; set; }
        public int churchId { get; set; }
    }


    public partial class UploadImageDTO
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string format { get; set; }
        public string binary { get; set; }
        public int churchId { get; set; }
        public int ImageCategoryID { get; set; }
    }

    public partial class DeviceDTO
    {
        public Int32 Id { get; set; }
        public Int32 ChurchId { get; set; }
        public String UDID { get; set; }
        public String DeviceToken { get; set; }
        public String DeviceType { get; set; }

    }

    public partial class Base64DTO
    {
        public string Base64String { get; set; }
        public String format { get; set; }
        public string ContentType { get; set; }

    }

    public partial class CountDTO
    {
        public Int32 EventCount { get; set; }
        public Int32 SeriesCount { get; set; }
        public Int32 TestimonialPendingCount { get; set; }
        public Int32 TestimonialApprovedCount { get; set; }

    }

    public partial class OfferingsDTO
    {
        public Int32 Id { get; set; }
        public String Name { get; set; }
        public String Description { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public Boolean Enabled { get; set; }
        public int ChurchId { get; set; }
    }

    public partial class FeedbackDTO
    {
        public Int32 Id { get; set; }
        public String Username { get; set; }
        public String Email { get; set; }
        public String Comments { get; set; }
        public Int32 ChurchId { get; set; }

    }

    public partial class PaymentTrackingDTO
    {

        public Int32 Id { get; set; }
        public float Amount { get; set; }
        public String TransactionID { get; set; }
        public Int32 TransactionTypeID { get; set; }
        public Int32 ChurchId { get; set; }
        public String DeviceId { get; set; }
        public String UserName { get; set; }
        public String UserEmail { get; set; }
        public String TransactionTypeDesc { get; set; }

    }

    public partial class UsageDTO
    {
        public int FeatureId { get; set; }
        public int Count { get; set; }
        public int Value { get; set; }
        public int DeviceId { get; set; }

    }

    public partial class ApiDataDTO
    {
        public string Code { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string County { get; set; }
        public string Distance { get; set; }

    }

    #region GooglemapsAPI
    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public List<string> types { get; set; }
    }

    public class Northeast
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Bounds
    {
        public Northeast northeast { get; set; }
        public Southwest southwest { get; set; }
    }

    public class Location
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Northeast2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Southwest2
    {
        public double lat { get; set; }
        public double lng { get; set; }
    }

    public class Viewport
    {
        public Northeast2 northeast { get; set; }
        public Southwest2 southwest { get; set; }
    }

    public class Geometry
    {
        public Bounds bounds { get; set; }
        public Location location { get; set; }
        public string location_type { get; set; }
        public Viewport viewport { get; set; }
    }

    public class Result
    {
        public static Utility.CustomResponseStatus Status { get; internal set; }
        public List<AddressComponent> address_components { get; set; }
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string place_id { get; set; }
        public List<string> types { get; set; }
    }

    public class LocationDTO
    {
        public List<Result> results { get; set; }
        public string status { get; set; }
    }
    #endregion
    public class CountryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    #region Customer

    public class BillingAddress
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
    }

    public class ShippingAddress
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string postcode { get; set; }
        public string country { get; set; }
    }

    public class Customer
    {
        public int id { get; set; }
        public string created_at { get; set; }
        public string email { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string username { get; set; }
        public string role { get; set; }
        public string last_order_id { get; set; }
        public string last_order_date { get; set; }
        public int orders_count { get; set; }
        public string total_spent { get; set; }
        public string avatar_url { get; set; }
        public BillingAddress billing_address { get; set; }
        public ShippingAddress shipping_address { get; set; }
    }

    public class CustomerDTO
    {
        public Customer customer { get; set; }
    }


    public class CustomersDTO
    {
        public List<Customer> customers { get; set; }
    }

    public class File
    {
        public string name { get; set; }
        public string file { get; set; }
    }

    public class Download
    {
        public string download_url { get; set; }
        public string download_id { get; set; }
        public int product_id { get; set; }
        public string download_name { get; set; }
        public int order_id { get; set; }
        public string order_key { get; set; }
        public string downloads_remaining { get; set; }
        public string access_expires { get; set; }
        public File file { get; set; }
    }

    public class DownloadDTO
    {
        public List<Download> downloads { get; set; }
    }

    public class KidsListDto
    {
        public List<KidsDto> Kids { get; set; }
        public List<AdminClassDto> Classes { get; set; }
    }
    #endregion

    #region Children Registration
    public class AdminClassDto
    {
        public Int32 Id { get; set; }
        public string ClassName { get; set; }
        public string AgeGroup { get; set; }
        public Int32 MinAge { get; set; }
        public Int32 MaxAge { get; set; }
        public string Location { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Int32 CreatedBy { get; set; }
        public Int32 UpdatedBy { get; set; }
        public Int32 IsValid { get; set; }
        public Int32 AuthToken { get; set; }
        public string QrCode { get; set; }
    }

    public class AdminAuthTokenDto
    {
        public Int32 Id { get; set; }
        public Int32 TokenId { get; set; }
        public Int32 ClassId { get; set; }
        public DateTime ExpiryDate { get; set; }
    }

    public class ParentsDto
    {
        public Int32 Id { get; set; }
        public string ParentName { get; set; }
        public string EmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 ChurchId { get; set; }
        public Int32 Gender { get; set; }
        public string Country { get; set; }
        public string Password { get; set; }
        public Int32 DeviceType { get; set; }
    }

    public class KidsDto
    {
        public Int32 Id { get; set; }
        public string KidName { get; set; }
        public Int32 ParentId { get; set; }
        public Int32 ClassId { get; set; }
        public Int32 Age { get; set; }
        public Int32 Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public Int32 CreatedBy { get; set; }
        public string ParentName { get; set; }
        public string CategoryName { get; set; }//Church Class Name
    }

    public partial class SupportMailDTO
    {
        public string EmailAddress { get; set; }
        public string Password { get; set; }
        public string Host { get; set; }
        public string Port { get; set; }
        public string SSL { get; set; }

    }
    public class EmailDTO
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Subject { get; set; }
        public int Status { get; set; }
    }

    public class KidsCheckInDto
    {
        public List<Int32> KidId { get; set; }
        public string QrCode { get; set; }
        public Int32 Status { get; set; }
        public Int32 DeviceType { get; set; }
    }

    public class ChurchClassDto
    {
        public Int32 Id { get; set; }
        public string CategoryName { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public string Location { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public Int32 CreatedBy { get; set; }
        public Int32 UpdatedBy { get; set; }
    }

    public class ParentKidsDto
    {
        public Int32 Id { get; set; }
        public string KidName { get; set; }
        public string EmailAddress { get; set; }
        public Int32 DeviceType { get; set; }
        public Int32 ClassId { get; set; }
    }

    public partial class AlbumsDTO
    {
        public Int32 Id { get; set; }
        public Int32 ChurchId { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public Boolean Enabled { get; set; }
        public String Description { get; set; }
        public Int16? GalleryId { get; set; }
        public DateTime? Created { get; set; }
        public DateTime? Updated { get; set; }
        public DateTime? Ends { get; set; }
        public String ThumbNail { get; set; }
        public String ThumbNailFormat { get; set; }
    }

    public partial class TracksAlbumsDTO
    {
        public Int32 Id { get; set; }
        public Int32 AlbumId { get; set; }
        public String Name { get; set; }
        public String Author { get; set; }
        public Boolean Enabled { get; set; }
        public String Description { get; set; }
        public String TrackUrl { get; set; }
        public String Duration { get; set; }
        public DateTime? Updated { get; set; }
        public Int16? GalleryId { get; set; }
        public String TrackFormat { get; set; }
        public Stream stream { get; set; }
        public string ContentType { get;   set; }
    }
    #endregion

    public class ChurchFeaturesDto
    {
        public int Id { get; set; }
        public string FeatureName { get; set; }
        public int ChurchId { get; set; }
    }

    public class ChurchNotesDto
    {
        public int Id { get; set; }
        public int ChurchId { get; set; }
        public string Notes1 { get; set; }
        public string Notes2 { get; set; }
        public string Notes3 { get; set; }
        public string Notes4 { get; set; }
        public string Notes5 { get; set; }
        public DateTime CreatedDate { get; set; }
        public string AuthorName { get; set; }
        public string Reference { get; set; }
    }

    public class CustomWordOfDay
    {
        public int Id { get; set; }
        public int ChurchId { get; set; }
        public string CustomWord { get; set; }
        public int CustomWordType { get; set; }
        public DateTime CreatedDate { get; set; }
        public string AuthorName { get; set; }
        public string Reference { get; set; }
    }

    public class PurchaseItemsDto
    {
        public int Id { get; set; }
        public int ChurchId { get; set; }
        public string ImageUrl { get; set; }
        public int Quantity { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Amount { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string ImageFormat { get; set; }
        public string CurrencySymbol { get; set; }
        public string ContentType { get;   set; }
    }

    public class ChurchBranchesDto
    {
        public int Id { get; set; }
        public string CampusName { get; set; }
        public string Address { get; set; }
        public int ChurchId { get; set; }
    }

    public class CustomWordsDto
    {
        public Int32 Id { get; set; }
        public String Verse { get; set; }
        public String Author { get; set; }
        public String Reference { get; set; }
        public DateTime LastModified { get; set; }
        public int Church_id { get; set; }
        public int Status { get; set; }
    }

    public partial class InAppPurchaseDTO
    {

        public Int32 Id { get; set; }
        public float Amount { get; set; }
        public String TransactionID { get; set; }
        public Int32 ChurchId { get; set; }
        public String DeviceId { get; set; }
        public Int32 UserId { get; set; }
        public Int32 ItemId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PaymentType { get; set; }
        public Int32 Quantity { get; set; }
    }

    public partial class GetVideoUrlDTO
    {
        public Stream Request { get; set; }
        public string Extension { get; set; }
    }

    public partial class ChurchMemberInfoDto
    {
        public ChurchMemberInfoDto()
        {
            Gender = 0;
            PhoneNumber = string.Empty;
            UDID = string.Empty;
        }
        public int Id { get; set; }
        public string surName { get; set; }
        public string name { get; set; }
        public int churchId { get; set; }
        public DateTime dateOfBirth { get; set; }
        public DateTime weddingDate { get; set; }
        public DateTime createdDate { get; set; }
        public int DeviceType { get; set; }  
        [Optional]
        public int Gender { get; set; }
        [Optional] 
        public string PhoneNumber { get; set; }
        [Optional]
        public string UDID { get;   set; }
        public string FBDeviceToken { get;   set; }
        public bool PushNotificationAccept { get;   set; }
    }
    public partial class AboutChurch
    {
        public Int32 ChurchId { get; set; }
        public String About { get; set; }
    }


    public partial class ChurchBankDetailsDto
    {
        public Int32 Id { get; set; }
        public Int32 ChurchId { get; set; }
        public String BankName { get; set; }
        public String BranchName { get; set; }
        public String IFSC { get; set; }
        public String AccountNo { get; set; }
        public String AccountName { get; set; }
    }

    public class StateDTO
    {
        public int StateId { get; set; }
        public string StateName { get; set; }
        public string CountryID { get; set; }
    }

    public class ChurchPaymentDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ChurchId { get; set; }
        public string TransactionId { get; set; }
        public string PaymentType { get; set; }
        public string Amount { get; set; }
        public string PaymentDate { get; set; }
        public string Productinfo { get; set; }

        /// <summary>
        /// Donor address
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Donation Description
        /// </summary>
        public string DonationDescription { get; set; }

    }

    public class PaytmResonse
    {
        public string CHECKSUMHASH { get; set; }
        public string ORDER_ID { get; set; }
        public string payt_STATUS { get; set; }

    }

    public partial class ChurchReportData
    {
        public int UsersCount { get; set; }
        public int SermonsCount { get; set; }
        public int PaymentCount { get; set; }
    }

    /// <summary>
    /// Model class for UpdateChurchChurchPaymentMode
    /// </summary>
    public class UpdateChurchPaymetnModeModel
    {
        /// <summary>
        /// Church Id
        /// </summary>
        public int ChurchId { get; set; }

        /// <summary>
        /// Payment Mode
        /// </summary>
        public int PaymentMode { get; set; }
    }
    public partial class ChurchSermonData
    {
        public string SeriesName { get; set; }
        public string SermonName { get; set; }
        public string SermonFormat { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    public partial class ChurchPaymentData
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Amount { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PaymentType { get; set; }
    }

    public partial class ChurchUsersList
    {
        public int Year { get; set; }
        public string Month { get; set; }
        public int IOSCount { get; set; }
        public int AndroidCount { get; set; }
        public string DeviceType { get; set; }
        public int TotalCount { get; set; }
    }

    public partial class ContactUs
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
        public string CountryCode { get; set; }
        public string MobileNumber { get; set; }
        public string IPAddress { get; set; }
    }

    
   public partial class ChurchPdfFileDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string pdfFileURL { get; set; }
        public int ChurchId { get; set; } 
    }

    public partial class AboutChurchPdfFileDto
    { 
        public string About { get; set; }
        public List<ChurchPdfFileDto> ChurchPdfFiles { get; set; } 
        
    }


    /// <summary>
    /// Church to ChurchNexus payment info model
    /// </summary>
    public partial class ChurchChurchNexusPayments
    {
        /// <summary>
        /// Unique identifier
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Church Id
        /// </summary>
        public int ChurchId { get; set; }

        /// <summary>
        /// Plan for which the payment is made
        /// </summary>
        public int PlanId { get; set; }

        /// <summary>
        /// Amount paid
        /// </summary>
        public decimal PaidAmount { get; set; }

        /// <summary>
        /// Unique identifier for Payment order
        /// </summary>
        public string PaymentOrderId { get; set; }

        /// <summary>
        /// Transaction Id given by Payment Service
        /// </summary>
        public string PaymentServiceTransactionId { get; set; }

        /// <summary>
        /// Payment Date
        /// </summary>
        public DateTime? PaymentDate { get; set; }

        /// <summary>
        /// Payment Status
        /// </summary>
        public bool PaymentStatus { get; set; }

        /// <summary>
        /// Plan effective from
        /// </summary>
       // public DateTime? PlanEffectiveFrom { get; set; }
    }


    /// <summary>
    /// Member to church payment model class
    /// </summary>
    public partial class MemberToChurchPayment
    {
        /// <summary>
        /// ChurchId
        /// </summary>
        public int ChurchId { get; set; }

        /// <summary>
        /// Donor Name
        /// </summary>
        public string DonorName { get; set; }

        /// <summary>
        /// Donor Email
        /// </summary>
        public string DonorEmail { get; set; }

        /// <summary>
        /// Donor Mobile number
        /// </summary>
        public string DonorMobileNum { get; set; }

        /// <summary>
        /// Donor Address
        /// </summary>
        public string DonorAddress { get; set; }

        /// <summary>
        /// Donation cause
        /// </summary>
        public long DonationCause { get; set; }

        /// <summary>
        /// Donation description
        /// </summary>
        public string DonationDescription { get; set; }

        /// <summary>
        /// Payment orderid
        /// </summary>
        public string Payment_Order_id { get; set; }

        /// <summary>
        /// Payment Service transaction Id
        /// </summary>
        public string PaymentServiceTransactionId { get; set; }

        /// <summary>
        /// Payment Status
        /// </summary>
        public bool PaymentStatus { get; set; }

        /// <summary>
        /// Donation Amount
        /// </summary>
        public double DonationAmount { get; set; }

        /// <summary>
        /// Donation Date
        /// </summary>
        public DateTime DonationDate { get; set; }

        public int PaymentMode { get; set; }
    }



    /// <summary>
    /// Model class for ChurchDonation
    /// </summary>
    public partial class ChurchDonationCause
    {
        // Id
        public long Id { get; set; }

        /// <summary>
        /// ChurchId
        /// </summary>
        public int ChurchId { get; set; }

        /// <summary>
        /// DonationCause
        /// </summary>
        public string DonationCause { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        public bool Status { get; set; }
    }

    public class SubscriptionInfo
    {
        /// <summary>
        /// Id
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// PaymentServiceTransactionId
        /// </summary>
        public string PaymentServiceTransactionId { get; set; }

        /// <summary>
        /// PaymentOrder Id
        /// </summary>
        public string PaymentOrderId { get; set; }

        /// <summary>
        /// ChurchId
        /// </summary>
        public int ChurchId { get; set; }

        /// <summary>
        /// Plan Id
        /// </summary>
        public int PlanId { get; set; }

        /// <summary>
        /// Paid Plan Amount
        /// </summary>
        public double PaidPlanAmount { get; set; }

        /// <summary>
        /// Is Partial Payment
        /// </summary>
        public bool IsPartialPayment { get; set; }

        /// <summary>
        /// Subscription Month
        /// </summary>
        public DateTime SubscriptionMonth { get; set; }

        /// <summary>
        /// Remarks
        /// </summary>
        public string Remarks { get; set; }

        /// <summary>
        /// Payment status
        /// </summary>
        public bool PaymentStatus { get; set; }
        public DateTime PaymentDate { get;   set; }
    }
    public partial class ChurchPaymentModeDTO
    {
        public int ChurchId { get; set; }
        public int PaymentMode { get; set; }
        public string AccountPayee { get; set; }
        public string JsonResp { get; set; }
    }

    public partial class FirebaseDTO
    {
        public string FirebaseAppId { get; set; }
        public string FirebaseSenderId { get; set; }
    }

    public partial class FirebaseDeviceDTO
    {
        public int Id { get; set; }
        public string FBDeviceToken { get; set; }
        public int DeviceType { get; set; }
        public DateTime CreatedDate { get; set; }
        public int UserId { get; set; }
        public string RefDeviceId { get; set; }
        public int ReceiverType { get; set; }
        public bool PushNotificationAccept { get;  set; }
    }

    public partial class DesignDTO
    { 
        public int designTemplateId { get; set; }
        public int ChurchId { get; set; }

    }

    public partial class ChurchPlanFeatureDTO
    {
        public int planFeatureId { get; set; }
        public int ChurchId { get; set; }

    }

    public partial class PlanOptionalFeatures
    { 
        public int PlanId { get; set; }
        public int FeatureId { get; set; }
        public string FeatureName { get; set; }
        public int IsExists { get;   set; }
    }

    public partial class YoutubeDto
    {
        public int Id { get; set; }
        public string ChannelName { get; set; }
        public string ChannelId { get; set; }
        public int ChurchId { get; set; }
        public int IsUserName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
    public partial class YoutubeVideosDTO
    {
        public string VideoId { get; set; }
        public string VideoUrl { get; set; }
        public string ChannelId { get; set; }
        public string ChannelTitle { get; set; }
        public bool IsLive { get; set; }
        public string Thumbnail { get; set; }
        public string VideoName { get; set; }
    } 
   public partial class ChurchSignUpInfo
    {
        public int Id { get; set; }
        public string ChurchName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public int CountryId { get; set; }
        public int PlanId { get; set; }
        public bool SignUpCompleted { get; set; }
        public string CountryName { get;   set; }
        public int SignUpStepId { get;   set; }
        public int ChurchId { get; set; }
    }
}