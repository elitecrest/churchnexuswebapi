﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace NewAPIChurchAPP.Models
{
    public partial class PaymentDTO
    {
        public PaymentDTO()
        {
            mode = string.Empty;
            status = string.Empty;
            Key = string.Empty;
            Txnid = string.Empty;
            amount = string.Empty;
            productinfo = string.Empty;
            firstname = string.Empty;
            lastname = string.Empty;
            address1 = string.Empty;
            address2 = string.Empty;
            city = string.Empty;
            state = string.Empty;
            country = string.Empty;
            zipcode = string.Empty;
            email = string.Empty;
            phone = string.Empty;
            udf1 = string.Empty;
            udf2 = string.Empty;
            udf3 = string.Empty;
            udf4 = string.Empty;
            udf5 = string.Empty;
            hash = string.Empty;
            error = string.Empty;
            PG_TYPE = string.Empty;
            bank_ref_num = string.Empty;
            payumoneyId = string.Empty;
            additionalCharges = string.Empty;
            PaymentStatus = string.Empty;
            ChurchId = 0;
            CountryId = 0;
            StateId = 0;
            CityId = 0;
            CreatedDate = DateTime.Now;

            BANKTXNID = String.Empty;
        }
        public Int32 Id { get; set; }
        public String mode { get; set; }
        public String status { get; set; }
        public String Key { get; set; }
        public String Txnid { get; set; }
        public String amount { get; set; }
        public String productinfo { get; set; }
        public String firstname { get; set; }
        public String lastname { get; set; }
        public String address1 { get; set; }
        public String address2 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public String country { get; set; }
        public String zipcode { get; set; }
        public String email { get; set; }
        public String phone { get; set; }
        public String udf1 { get; set; }
        public String udf2 { get; set; }
        public String udf3 { get; set; }
        public String udf4 { get; set; }
        public String udf5 { get; set; }
        public String hash { get; set; }
        public String error { get; set; }
        public String PG_TYPE { get; set; }
        public String bank_ref_num { get; set; }
        public String payumoneyId { get; set; }
        public String additionalCharges { get; set; }
        public String PaymentStatus { get; set; }
        public Int32 ChurchId { get; set; }
        public Int32 CountryId { get; set; }
        public Int32 StateId { get; set; }
        public Int32 CityId { get; set; }
        public DateTime CreatedDate { get; set; }
        public String BANKTXNID { get; set; }
        public String OrderId { get; set; }
        public String CheckHashSum { get; set; }
        public String CustId { get; set; }

    }

    public partial class TransactionDTO
    {
        public int Id { get; set; }
        public string TransactionId { get; set; }
        public string TransactionStatus { get; set; }
        public string Amount { get; set; }
        public string Country { get; set; }
        public int ChurchId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string ProductInfo { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}