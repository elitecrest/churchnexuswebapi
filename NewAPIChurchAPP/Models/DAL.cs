﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;

namespace NewAPIChurchAPP.Models
{
    internal static class ChurchDAL
    {
        internal static SqlConnection ConnectTODB()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["ChurchDB"].ConnectionString;
            SqlConnection sqlconn = new SqlConnection(connectionString);
            sqlconn.Open();
            return sqlconn;
        }

        internal static CloudStorageAccount ConnectTOCloudDB()
        {
            var storageAccount =
                CloudStorageAccount.Parse(ConfigurationManager.AppSettings["StorageConnection"].ToString());
            return storageAccount;
        }

        #region Churches

        internal static int ChurchLoginValidation(string username, string password, int ChurchId)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[ChurchUserValidation]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrname = cmd.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar, 100);
                    usrname.Direction = System.Data.ParameterDirection.Input;
                    usrname.Value = username;

                    SqlParameter pwd = cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar, 256);
                    pwd.Direction = System.Data.ParameterDirection.Input;
                    pwd.Value = password;

                    SqlParameter churchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = ChurchId;

                    SqlParameter idParam = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    idParam.Direction = System.Data.ParameterDirection.Output;
                    idParam.Value = 0;
                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

     

        internal static ChurchDTO Login(string username, string password, int ChurchId)
        {
            ChurchDTO v = new ChurchDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                //check admin user ...

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[LoginUser]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrname = cmd.Parameters.Add("@UserName", System.Data.SqlDbType.VarChar, 100);
                    usrname.Direction = System.Data.ParameterDirection.Input;
                    usrname.Value = username;

                    SqlParameter pwd = cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar, 256);
                    pwd.Direction = System.Data.ParameterDirection.Input;
                    pwd.Value = password;

                    SqlParameter churchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        v.SignupDate = Convert.IsDBNull(myData["signup_date"])
                           ? System.DateTime.Now
                        : (DateTime?)myData["signup_date"];

                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];

                        v.totalPromotionDays = (myData["totalPromotionDays"] != DBNull.Value && myData["totalPromotionDays"] != null) ? (int)myData["totalPromotionDays"] : 0;
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        v.PaymentMode = myData["PaymentMode"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PaymentMode"]);
                        v.PushNotificationAccept = (myData["PushNotificationAccept"] == DBNull.Value) ? false : (bool)myData["PushNotificationAccept"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                          ? string.Empty
                          : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                          ? string.Empty
                          : myData["AdminLastName"].ToString();
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

       

        internal static List<ChurchDTO> GetAllChurches(string latitude, string longitude)
        {
            List<ChurchDTO> churches = new List<ChurchDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurches]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter lat1 = cmd.Parameters.Add("@lat1", System.Data.SqlDbType.VarChar, 100);
                    lat1.Direction = System.Data.ParameterDirection.Input;
                    lat1.Value = latitude;

                    SqlParameter long1 = cmd.Parameters.Add("@long1", System.Data.SqlDbType.VarChar, 100);
                    long1.Direction = System.Data.ParameterDirection.Input;
                    long1.Value = longitude;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchDTO v = new ChurchDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        v.SignupDate = Convert.IsDBNull(myData["signup_date"])
                           ? System.DateTime.Now
                           : (DateTime?)myData["modified_on_utc"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                           ? string.Empty
                           : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();
                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churches;
        }

        

        internal static int addMemberInfo(ChurchMemberInfoDto memberInfoDto)
        {
            int result = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[spAddChurchMembersInfo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter surName = cmd.Parameters.Add("@surName", System.Data.SqlDbType.NVarChar, 100);
                    surName.Direction = System.Data.ParameterDirection.Input;
                    surName.Value = memberInfoDto.surName;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar, 100);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = memberInfoDto.name;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = memberInfoDto.churchId;

                    SqlParameter deviceType = cmd.Parameters.Add("@deviceType", System.Data.SqlDbType.Int);
                    deviceType.Direction = System.Data.ParameterDirection.Input;
                    deviceType.Value = memberInfoDto.DeviceType;

                    SqlParameter dateOfbirth = cmd.Parameters.Add("@dateOfBirth", System.Data.SqlDbType.Date);
                    dateOfbirth.Direction = System.Data.ParameterDirection.Input;
                    dateOfbirth.Value = memberInfoDto.dateOfBirth; 

                    SqlParameter weddingDate = cmd.Parameters.Add("@weddingDate", System.Data.SqlDbType.Date);
                    weddingDate.Direction = System.Data.ParameterDirection.Input;
                    weddingDate.Value = memberInfoDto.weddingDate;

                    SqlParameter gender = cmd.Parameters.Add("@gender", System.Data.SqlDbType.Int);
                    gender.Direction = System.Data.ParameterDirection.Input;
                    gender.Value =  memberInfoDto.Gender;

                    SqlParameter phonenumber = cmd.Parameters.Add("@phonenumber", System.Data.SqlDbType.VarChar, 50);
                    phonenumber.Direction = System.Data.ParameterDirection.Input;
                    phonenumber.Value = memberInfoDto.PhoneNumber;

                    SqlParameter UDID = cmd.Parameters.Add("@UDID", System.Data.SqlDbType.VarChar, 100);
                    UDID.Direction = System.Data.ParameterDirection.Input;
                    UDID.Value = memberInfoDto.UDID;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        result = Convert.ToInt32(myData["id"]);
                    }
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


            return result;
        }

        internal static int UpdateMemberInfo(ChurchMemberInfoDto memberInfoDto)
        {
            int result = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[spUpdateChurchMembersInfo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter surName = cmd.Parameters.Add("@surName", System.Data.SqlDbType.NVarChar, 100);
                    surName.Direction = System.Data.ParameterDirection.Input;
                    surName.Value = memberInfoDto.surName;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.NVarChar, 100);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = memberInfoDto.name;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = memberInfoDto.churchId;

                    SqlParameter deviceType = cmd.Parameters.Add("@deviceType", System.Data.SqlDbType.Int);
                    deviceType.Direction = System.Data.ParameterDirection.Input;
                    deviceType.Value = memberInfoDto.DeviceType;

                    SqlParameter dateOfbirth = cmd.Parameters.Add("@dateOfBirth", System.Data.SqlDbType.Date);
                    dateOfbirth.Direction = System.Data.ParameterDirection.Input;
                    dateOfbirth.Value = memberInfoDto.dateOfBirth;


                    SqlParameter weddingDate = cmd.Parameters.Add("@weddingDate", System.Data.SqlDbType.Date);
                    weddingDate.Direction = System.Data.ParameterDirection.Input;
                    weddingDate.Value = memberInfoDto.weddingDate;

                    SqlParameter phonenumber = cmd.Parameters.Add("@phonenumber", System.Data.SqlDbType.VarChar,100);
                    phonenumber.Direction = System.Data.ParameterDirection.Input;
                    phonenumber.Value = memberInfoDto.PhoneNumber;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", System.Data.SqlDbType.Int);
                    gender.Direction = System.Data.ParameterDirection.Input;
                    gender.Value = memberInfoDto.Gender;

                    SqlParameter memberId = cmd.Parameters.Add("@memberId", System.Data.SqlDbType.Int);
                    memberId.Direction = System.Data.ParameterDirection.Input;
                    memberId.Value = memberInfoDto.Id;

                    SqlDataReader myData = cmd.ExecuteReader();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


            return result;
        }

        internal static List<FirebaseDeviceDTO> GetFirebaseDevices()
        {
            List<FirebaseDeviceDTO> devices = new List<FirebaseDeviceDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetFirebaseDevices]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        FirebaseDeviceDTO v = new FirebaseDeviceDTO();
                        v.Id = (int)myData["id"];
                        v.FBDeviceToken = (string)myData["FBDeviceToken"].ToString();
                        v.UserId = (int)myData["UserId"];
                        v.PushNotificationAccept = (bool)myData["PushNotificationAccept"];
                        devices.Add(v);
                    }
                }
                
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


            return devices;
        }

        internal static int DeleteMemberInfo(int MemberId)
        {
            int result = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[spDeleteChurchMembersInfo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter memberId = cmd.Parameters.Add("@memberId", System.Data.SqlDbType.Int);
                    memberId.Direction = System.Data.ParameterDirection.Input;
                    memberId.Value = MemberId;

                    SqlDataReader myData = cmd.ExecuteReader();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


            return result;
        }


        internal static void updateChurchPaymentMode(int churchId, int paymentMode)
        {

            SqlConnection myConn = ConnectTODB();

            if (null != myConn)
            {
                SqlCommand cmd = new SqlCommand("[dbo].[UpdateChurchPaymentMode]", myConn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter payment_Mode = cmd.Parameters.Add("@paymentMode", System.Data.SqlDbType.Int, 100);
                payment_Mode.Direction = System.Data.ParameterDirection.Input;
                payment_Mode.Value = paymentMode;


                SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                churchid.Direction = System.Data.ParameterDirection.Input;
                churchid.Value = churchId;

                cmd.ExecuteNonQuery();
            }

        }

        internal static int TrackAppUsage(int churchId, string deviceId, int deviceType)
        {
            int result = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[AppUsageTrack]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter devId = cmd.Parameters.Add("@deviceId", System.Data.SqlDbType.VarChar, 256);
                    devId.Direction = System.Data.ParameterDirection.Input;
                    devId.Value = deviceId;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlParameter devType = cmd.Parameters.Add("@deviceType", System.Data.SqlDbType.Int);
                    devType.Direction = System.Data.ParameterDirection.Input;
                    devType.Value = deviceType;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        result = Convert.ToInt32(myData["result"]);
                    }
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<ChurchDTO> GetAllChurches(string geo)
        {
            List<ChurchDTO> churches = new List<ChurchDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurches]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter geopar = cmd.Parameters.Add("@geo", System.Data.SqlDbType.VarChar, 40);
                    geopar.Direction = System.Data.ParameterDirection.Input;
                    geopar.Value = geo;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchDTO v = new ChurchDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["signup_date"];
                        v.SignupDate = Convert.IsDBNull(myData["signup_date"])
                           ? System.DateTime.Now
                           : (DateTime?)myData["modified_on_utc"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();

                        string fullAddress = v.Address;
                        if (v.City != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.City;
                        }
                        if (v.State != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.State;
                        }
                        if (v.Zip != string.Empty)
                        {
                            fullAddress = fullAddress + " " + v.Zip;
                        }
                        v.Address = fullAddress;
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                           ? string.Empty
                           : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();
                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        v.DesignTemplateId = (myData["DesignTemplateId"] == DBNull.Value) ? 1 : (int)myData["DesignTemplateId"];
                        v.PlanFeatureId = (myData["PlanFeatureId"] == DBNull.Value) ? 0 : (int)myData["PlanFeatureId"];
                        List<PlanOptionalFeatures> planOptFeatures =   ChurchDAL.GetPlansOptionalFeatures(v.PlanTypeId);
                        foreach (var opt in planOptFeatures)
                        {
                            if (opt.FeatureName == "Kids Registration") { v.OptKidsRegistration = opt.IsExists; }
                            if (opt.FeatureName == "Church Store") { v.OptChurchStore = opt.IsExists; }
                            if (opt.FeatureName == "Live Streaming") { v.OptLiveStream = opt.IsExists; }
                        }
                                 
                        List<ChurchPdfFileDto> pdfFiles = ChurchDAL.GetChurchPdfFiles(v.Id);
                        v.PDFFiles = pdfFiles;

                        List<AnnouncementDTO> announce = ChurchDAL.GetAnnouncements(v.Id);
                        foreach (var ann in announce)
                        {
                            v.Facebook_Url = ann.Facebook_Url;
                            v.Twitter_Url = ann.Twitter_Url;
                            v.Website_Url = ann.Website_Url;
                        }
                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churches;
        }

        internal static List<ChurchDTO> GetChurchesByCountry(string countryName)
        {
            List<ChurchDTO> churches = new List<ChurchDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchesByCountry]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter CountryName = cmd.Parameters.Add("@CountryName", System.Data.SqlDbType.VarChar, 100);
                    CountryName.Direction = System.Data.ParameterDirection.Input;
                    CountryName.Value = countryName;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchDTO v = new ChurchDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["signup_date"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["signup_date"];
                        v.SignupDate = Convert.IsDBNull(myData["modified_on_utc"])
                           ? System.DateTime.Now
                           : (DateTime?)myData["modified_on_utc"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();

                        string fullAddress = v.Address;
                        if (v.City != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.City;
                        }
                        if (v.State != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.State;
                        }
                        if (v.Zip != string.Empty)
                        {
                            fullAddress = fullAddress + " " + v.Zip;
                        }
                        v.Address = fullAddress;
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                           ? string.Empty
                           : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();
                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churches;
        }


        internal static List<ChurchDTO> GetChurchesByCountry(string countryName, string geo)
        {
            List<ChurchDTO> churches = new List<ChurchDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchesByCountrygeo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter CountryName = cmd.Parameters.Add("@CountryName", System.Data.SqlDbType.VarChar, 100);
                    CountryName.Direction = System.Data.ParameterDirection.Input;
                    CountryName.Value = countryName;

                    SqlParameter geoPar = cmd.Parameters.Add("@geo", System.Data.SqlDbType.VarChar, 100);
                    geoPar.Direction = System.Data.ParameterDirection.Input;
                    geoPar.Value = geo;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchDTO v = new ChurchDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["signup_date"];
                        v.LastModified = Convert.IsDBNull(myData["signup_date"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();

                        string fullAddress = v.Address;
                        if (v.City != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.City;
                        }
                        if (v.State != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.State;
                        }
                        if (v.Zip != string.Empty)
                        {
                            fullAddress = fullAddress + " " + v.Zip;
                        }
                        v.Address = fullAddress;
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                           ? string.Empty
                           : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();
                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        v.DesignTemplateId = (myData["DesignTemplateId"] == DBNull.Value) ? 1 : (int)myData["DesignTemplateId"];
                        v.PlanFeatureId = (myData["PlanFeatureId"] == DBNull.Value) ? 0 : (int)myData["PlanFeatureId"];
                        List<PlanOptionalFeatures> planOptFeatures = ChurchDAL.GetPlansOptionalFeatures(v.PlanTypeId);
                        foreach (var opt in planOptFeatures)
                        {
                            if (opt.FeatureName == "Kids Registration") { v.OptKidsRegistration = opt.IsExists; }
                            if (opt.FeatureName == "Church Store") { v.OptChurchStore = opt.IsExists; }
                            if (opt.FeatureName == "Live Streaming") { v.OptLiveStream = opt.IsExists; }
                        }

                        List<ChurchPdfFileDto> pdfFiles = ChurchDAL.GetChurchPdfFiles(v.Id);
                        v.PDFFiles = pdfFiles;
                        List<AnnouncementDTO> announce = ChurchDAL.GetAnnouncements(v.Id);
                        foreach (var ann in announce)
                        {
                            v.Facebook_Url = ann.Facebook_Url;
                            v.Twitter_Url = ann.Twitter_Url;
                            v.Website_Url = ann.Website_Url;
                        }

                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churches;
        }



        internal static List<ChurchDTO> GetChurchesBySearchCountry(string country, string searchtext)
        {
            List<ChurchDTO> churches = new List<ChurchDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchesBySearchCountry]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter CountryName = cmd.Parameters.Add("@CountryName", System.Data.SqlDbType.VarChar, 100);
                    CountryName.Direction = System.Data.ParameterDirection.Input;
                    CountryName.Value = country;

                    SqlParameter SearchText = cmd.Parameters.Add("@SearchText", System.Data.SqlDbType.VarChar, 500);
                    SearchText.Direction = System.Data.ParameterDirection.Input;
                    SearchText.Value = searchtext;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchDTO v = new ChurchDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        v.SignupDate = Convert.IsDBNull(myData["signup_date"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["signup_date"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();

                        string fullAddress = v.Address;
                        if (v.City != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.City;
                        }
                        if (v.State != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.State;
                        }
                        if (v.Zip != string.Empty)
                        {
                            fullAddress = fullAddress + " " + v.Zip;
                        }
                        v.Address = fullAddress;
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                           ? string.Empty
                           : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();
                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churches;
        }

        internal static List<ChurchDTO> GetChurchesByAddress(string cityname)
        {
            List<ChurchDTO> churches = new List<ChurchDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchesByAddress]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter cityName = cmd.Parameters.Add("@cityName", System.Data.SqlDbType.VarChar, 100);
                    cityName.Direction = System.Data.ParameterDirection.Input;
                    cityName.Value = cityname;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchDTO v = new ChurchDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        v.SignupDate = Convert.IsDBNull(myData["signup_date"])
                       ? System.DateTime.Now
                       : (DateTime?)myData["signup_date"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                          ? string.Empty
                          : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();
                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churches;
        }

        internal static List<ChurchDTO> GetChurchesBySearch(string searchText)
        {
            List<ChurchDTO> churches = new List<ChurchDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchesBySearchText]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter SearchText = cmd.Parameters.Add("@SearchText", System.Data.SqlDbType.VarChar, 500);
                    SearchText.Direction = System.Data.ParameterDirection.Input;
                    SearchText.Value = searchText;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchDTO v = new ChurchDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        v.SignupDate = Convert.IsDBNull(myData["signup_date"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["signup_date"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();
                        string fullAddress = v.Address;
                        if (v.City != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.City;
                        }
                        if (v.State != string.Empty)
                        {
                            fullAddress = fullAddress + "," + v.State;
                        }
                        if (v.Zip != string.Empty)
                        {
                            fullAddress = fullAddress + " " + v.Zip;
                        }
                        v.Address = fullAddress;
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                          ? string.Empty
                          : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();

                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        v.DesignTemplateId = (myData["DesignTemplateId"] == DBNull.Value) ? 1 :(int) myData["DesignTemplateId"];
                        v.PlanFeatureId = (myData["PlanFeatureId"] == DBNull.Value) ? 0 : (int)myData["PlanFeatureId"];
                        List<PlanOptionalFeatures> planOptFeatures = ChurchDAL.GetPlansOptionalFeatures(v.PlanTypeId);
                        foreach (var opt in planOptFeatures)
                        {
                            if (opt.FeatureName == "Kids Registration") { v.OptKidsRegistration = opt.IsExists; }
                            if (opt.FeatureName == "Church Store") { v.OptChurchStore = opt.IsExists; }
                            if (opt.FeatureName == "Live Streaming") { v.OptLiveStream = opt.IsExists; }
                        }

                        List<ChurchPdfFileDto> pdfFiles = ChurchDAL.GetChurchPdfFiles(v.Id);
                        v.PDFFiles = pdfFiles;
                        List<AnnouncementDTO> announce = ChurchDAL.GetAnnouncements(v.Id);
                        foreach (var ann in announce)
                        {
                            v.Facebook_Url = ann.Facebook_Url;
                            v.Twitter_Url = ann.Twitter_Url;
                            v.Website_Url = ann.Website_Url;
                        }
                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churches;
        }

        internal static GiveURLDTO GetGiveUrl(int ChurchId)
        {
            GiveURLDTO giveURLDTO = new GiveURLDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetGiveURL]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        giveURLDTO.IsPushPay = (bool)myData["isPushpay"];
                        giveURLDTO.IsPayPal = (bool)myData["isPayPal"];
                        string giveURL = GetChurchGiveUrl();
                        giveURL = giveURL.Replace("CHURCHID", ChurchId.ToString());
                        giveURLDTO.GiveURL = giveURL;
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return giveURLDTO;
        }


        internal static List<CountryDTO> GetCountryNames()
        {
            List<CountryDTO> countries = new List<CountryDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetCountryNames]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        CountryDTO v = new CountryDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();

                        countries.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return countries;
        }




        internal static List<ChurchDTO> GetAllChurchesForAdmin(string geo)
        {
            List<ChurchDTO> churches = new List<ChurchDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetAllChurches]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter geopar = cmd.Parameters.Add("@geo", System.Data.SqlDbType.VarChar, 40);
                    geopar.Direction = System.Data.ParameterDirection.Input;
                    geopar.Value = geo;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchDTO v = new ChurchDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                           ? string.Empty
                           : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();
                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churches;
        }

        internal static ChurchDTO GetChurch(int ChurchId)
        {

            ChurchDTO v = new ChurchDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurches]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter geopar = cmd.Parameters.Add("@geo", System.Data.SqlDbType.VarChar, 40);
                    geopar.Direction = System.Data.ParameterDirection.Input;
                    geopar.Value = string.Empty;


                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        v.SignupDate = Convert.IsDBNull(myData["signup_date"])
                           ? System.DateTime.Now
                           : (DateTime?)myData["signup_date"];
                        //v.Banners = Convert.IsDBNull(myData["banners"]) ? null : (byte[])myData["banners"];
                        //v.Splashscreen = Convert.IsDBNull(myData["splashscreen"]) ? null : (byte[])myData["splashscreen"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                           ? string.Empty
                           : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();
                        // v.Address = v.Address + v.City + v.State + v.Zip;
                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        v.PaymentMode = myData["PaymentMode"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PaymentMode"]);
                        v.PushNotificationAccept = (myData["PushNotificationAccept"] == DBNull.Value) ? false :(bool)myData["PushNotificationAccept"];
                        v.DesignTemplateId = (myData["DesignTemplateId"] == DBNull.Value) ? 1 : (int)myData["DesignTemplateId"];
                        v.PlanFeatureId = (myData["PlanFeatureId"] == DBNull.Value) ? 0 : (int)myData["PlanFeatureId"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static ChurchDTO GetChurchByName(string ChurchName)
        {

            ChurchDTO v = new ChurchDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchesByName]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter geopar = cmd.Parameters.Add("@geo", System.Data.SqlDbType.VarChar, 40);
                    geopar.Direction = System.Data.ParameterDirection.Input;
                    geopar.Value = string.Empty;


                    SqlParameter churchid = cmd.Parameters.Add("@churchname", System.Data.SqlDbType.VarChar, 50);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchName.Trim();

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        //v.Banners = Convert.IsDBNull(myData["banners"]) ? null : (byte[])myData["banners"];
                        //v.Splashscreen = Convert.IsDBNull(myData["splashscreen"]) ? null : (byte[])myData["splashscreen"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();
                        v.PlanTypeId = myData["PlanTypeId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["PlanTypeId"]);
                        v.OrderId = myData["OrderId"] == DBNull.Value ? 1 : Convert.ToInt32(myData["OrderId"]);
                        v.IsKidsEnabled = myData["IsKidsEnabled"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IsKidsEnabled"]);
                        // v.Address = v.Address + v.City + v.State + v.Zip;
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static string GetWordoftheDay(int ChurchId)
        {
            string word = string.Empty;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetWordoftheDay]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        word = (string)myData["word"].ToString();


                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return word;
        }




        internal static List<AnnouncementDTO> GetAnnouncements(int ChurchId)
        {
            List<AnnouncementDTO> announcement = new List<AnnouncementDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetAnnouncements]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@Churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AnnouncementDTO v = new AnnouncementDTO();
                        v.Id = (int)myData["id"];
                        v.Announcements_Url = (string)myData["announcements_url"].ToString();
                        v.Twitter_Url = (string)myData["twitter_url"].ToString();
                        v.Facebook_Url = (string)myData["facebook_url"].ToString();
                        v.Website_Url = (string)myData["website_url"].ToString();
                        v.GeoMap = (string)myData["geomap"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        announcement.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return announcement;
        }

        internal static int CreateChurch(ChurchDTO churchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[CreateChurch]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 256);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = (churchDTO.Name == null) ? string.Empty : churchDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = (churchDTO.Description == null) ? string.Empty : churchDTO.Description;

                    SqlParameter church_api_key = cmd.Parameters.Add("@church_api_key", System.Data.SqlDbType.VarChar,
                        64);
                    church_api_key.Direction = System.Data.ParameterDirection.Input;
                    church_api_key.Value = (churchDTO.ChurchAPIKey == null) ? string.Empty : churchDTO.ChurchAPIKey;

                    SqlParameter enabled = cmd.Parameters.Add("@enabled", System.Data.SqlDbType.Bit);
                    enabled.Direction = System.Data.ParameterDirection.Input;
                    enabled.Value = churchDTO.Enabled;

                    SqlParameter geomap = cmd.Parameters.Add("@geomap", System.Data.SqlDbType.VarChar, 40);
                    geomap.Direction = System.Data.ParameterDirection.Input;
                    geomap.Value = (churchDTO.Geomap == null) ? string.Empty : churchDTO.Geomap;

                    SqlParameter history = cmd.Parameters.Add("@history", System.Data.SqlDbType.VarChar, 4000);
                    history.Direction = System.Data.ParameterDirection.Input;
                    history.Value = (churchDTO.History == null) ? string.Empty : churchDTO.History;

                    SqlParameter about = cmd.Parameters.Add("@about", System.Data.SqlDbType.VarChar, 4000);
                    about.Direction = System.Data.ParameterDirection.Input;
                    about.Value = (churchDTO.About == null) ? string.Empty : churchDTO.About;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = (churchDTO.GalleryId == null) ? 0 : churchDTO.GalleryId;

                    SqlParameter banners = cmd.Parameters.Add("@banners", System.Data.SqlDbType.VarChar, 2000);
                    banners.Direction = System.Data.ParameterDirection.Input;
                    banners.Value = (churchDTO.Banners == null) ? string.Empty : churchDTO.Banners;

                    SqlParameter splashscreen = cmd.Parameters.Add("@splashscreen", System.Data.SqlDbType.VarChar, 2000);
                    splashscreen.Direction = System.Data.ParameterDirection.Input;
                    splashscreen.Value = (churchDTO.Splashscreen == null) ? string.Empty : churchDTO.Splashscreen;

                    SqlParameter theme = cmd.Parameters.Add("@theme", System.Data.SqlDbType.VarChar, 20);
                    theme.Direction = System.Data.ParameterDirection.Input;
                    theme.Value = (churchDTO.Theme == null) ? string.Empty : churchDTO.Theme;

                    SqlParameter phone_num = cmd.Parameters.Add("@phone_num", System.Data.SqlDbType.VarChar, 20);
                    phone_num.Direction = System.Data.ParameterDirection.Input;
                    phone_num.Value = (churchDTO.PhoneNum == null) ? string.Empty : churchDTO.PhoneNum;

                    SqlParameter email = cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 100);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = (churchDTO.Email == null) ? string.Empty : churchDTO.Email;

                    SqlParameter lat = cmd.Parameters.Add("@lat", System.Data.SqlDbType.VarChar, 100);
                    lat.Direction = System.Data.ParameterDirection.Input;
                    lat.Value = (churchDTO.Latitude == null) ? string.Empty : churchDTO.Latitude;

                    SqlParameter longitu = cmd.Parameters.Add("@long", System.Data.SqlDbType.VarChar, 100);
                    longitu.Direction = System.Data.ParameterDirection.Input;
                    longitu.Value = (churchDTO.Longitude == null) ? string.Empty : churchDTO.Longitude;

                    SqlParameter Address = cmd.Parameters.Add("@Address", System.Data.SqlDbType.VarChar, 2000);
                    Address.Direction = System.Data.ParameterDirection.Input;
                    Address.Value = (churchDTO.Address == null) ? string.Empty : churchDTO.Address;

                    SqlParameter Planid = cmd.Parameters.Add("@planid", System.Data.SqlDbType.Int);
                    Planid.Direction = System.Data.ParameterDirection.Input;
                    Planid.Value = (churchDTO.PlanId == null) ? 0 : churchDTO.PlanId;

                    SqlParameter Username = cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar, 100);
                    Username.Direction = System.Data.ParameterDirection.Input;
                    Username.Value = (churchDTO.Username == null) ? string.Empty : churchDTO.Username;

                    SqlParameter Password = cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar, 50);
                    Password.Direction = System.Data.ParameterDirection.Input;
                    Password.Value = (churchDTO.Password == null) ? string.Empty : churchDTO.Password;

                    SqlParameter ContactPersonName = cmd.Parameters.Add("@contactperson_name",
                        System.Data.SqlDbType.VarChar, 256);
                    ContactPersonName.Direction = System.Data.ParameterDirection.Input;
                    ContactPersonName.Value = (churchDTO.ContactPersonName == null)
                        ? string.Empty
                        : churchDTO.ContactPersonName;

                    SqlParameter ContactPersonPhone_num = cmd.Parameters.Add("@contactperson_phonenum",
                        System.Data.SqlDbType.VarChar, 50);
                    ContactPersonPhone_num.Direction = System.Data.ParameterDirection.Input;
                    ContactPersonPhone_num.Value = (churchDTO.ContactPersonPhoneNum == null)
                        ? string.Empty
                        : churchDTO.ContactPersonPhoneNum;

                    SqlParameter ContactPersonEmail = cmd.Parameters.Add("@contactperson_email",
                        System.Data.SqlDbType.VarChar, 100);
                    ContactPersonEmail.Direction = System.Data.ParameterDirection.Input;
                    ContactPersonEmail.Value = (churchDTO.ContactPersonEmail == null)
                        ? string.Empty
                        : churchDTO.ContactPersonEmail;

                    SqlParameter banner_format = cmd.Parameters.Add("@banner_format", System.Data.SqlDbType.VarChar, 20);
                    banner_format.Direction = System.Data.ParameterDirection.Input;
                    banner_format.Value = (churchDTO.BannerFormat == null) ? string.Empty : churchDTO.BannerFormat;

                    SqlParameter splashscreen_format = cmd.Parameters.Add("@splashscreen_format",
                        System.Data.SqlDbType.VarChar, 20);
                    splashscreen_format.Direction = System.Data.ParameterDirection.Input;
                    splashscreen_format.Value = (churchDTO.SplashScreenFormat == null)
                        ? string.Empty
                        : churchDTO.SplashScreenFormat;

                    SqlParameter isPayPal = cmd.Parameters.Add("@ispaypal", System.Data.SqlDbType.Bit);
                    isPayPal.Direction = System.Data.ParameterDirection.Input;
                    isPayPal.Value = churchDTO.IsPayPal;

                    SqlParameter isPushPay = cmd.Parameters.Add("@ispushpay", System.Data.SqlDbType.Bit);
                    isPushPay.Direction = System.Data.ParameterDirection.Input;
                    isPushPay.Value = churchDTO.IsPushPay;

                    SqlParameter GiveUrl = cmd.Parameters.Add("@giveurl", System.Data.SqlDbType.VarChar, 500);
                    GiveUrl.Direction = System.Data.ParameterDirection.Input;
                    GiveUrl.Value = churchDTO.GiveURL;

                    SqlParameter State = cmd.Parameters.Add("@State", System.Data.SqlDbType.VarChar, 50);
                    State.Direction = System.Data.ParameterDirection.Input;
                    State.Value = churchDTO.State;

                    SqlParameter City = cmd.Parameters.Add("@City", System.Data.SqlDbType.VarChar, 50);
                    City.Direction = System.Data.ParameterDirection.Input;
                    City.Value = churchDTO.City;

                    SqlParameter Zip = cmd.Parameters.Add("@Zip", System.Data.SqlDbType.VarChar, 50);
                    Zip.Direction = System.Data.ParameterDirection.Input;
                    Zip.Value = churchDTO.Zip;

                    SqlParameter Country = cmd.Parameters.Add("@Country", System.Data.SqlDbType.VarChar, 50);
                    Country.Direction = System.Data.ParameterDirection.Input;
                    Country.Value = churchDTO.Country;

                    SqlParameter firstname = cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.VarChar, 100);
                    firstname.Direction = System.Data.ParameterDirection.Input;
                    firstname.Value = (churchDTO.AdminFirstName == null) ? string.Empty : churchDTO.AdminFirstName;

                    SqlParameter lastname = cmd.Parameters.Add("@LastName", System.Data.SqlDbType.VarChar, 100);
                    lastname.Direction = System.Data.ParameterDirection.Input;
                    lastname.Value = (churchDTO.AdminLastName == null) ? string.Empty : churchDTO.AdminLastName;

                    SqlParameter planType = cmd.Parameters.Add("@PlanTypeId", System.Data.SqlDbType.Int);
                    planType.Direction = System.Data.ParameterDirection.Input;
                    planType.Value = churchDTO.PlanTypeId;

                    SqlParameter orderType = cmd.Parameters.Add("@OrderId", System.Data.SqlDbType.VarChar, 100);
                    orderType.Direction = System.Data.ParameterDirection.Input;
                    orderType.Value = churchDTO.OrderId;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int EditChurch(ChurchDTO churchDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateChurch]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 256);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = (churchDTO.Name == null) ? string.Empty : churchDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = (churchDTO.Description == null) ? string.Empty : churchDTO.Description;

                    SqlParameter church_api_key = cmd.Parameters.Add("@church_api_key", System.Data.SqlDbType.VarChar,
                        64);
                    church_api_key.Direction = System.Data.ParameterDirection.Input;
                    church_api_key.Value = (churchDTO.ChurchAPIKey == null) ? string.Empty : churchDTO.ChurchAPIKey;

                    SqlParameter enabled = cmd.Parameters.Add("@enabled", System.Data.SqlDbType.Bit);
                    enabled.Direction = System.Data.ParameterDirection.Input;
                    enabled.Value = churchDTO.Enabled;

                    SqlParameter geomap = cmd.Parameters.Add("@geomap", System.Data.SqlDbType.VarChar, 40);
                    geomap.Direction = System.Data.ParameterDirection.Input;
                    geomap.Value = (churchDTO.Geomap == null) ? string.Empty : churchDTO.Geomap;

                    SqlParameter history = cmd.Parameters.Add("@history", System.Data.SqlDbType.VarChar, 4000);
                    history.Direction = System.Data.ParameterDirection.Input;
                    history.Value = (churchDTO.History == null) ? string.Empty : churchDTO.History;

                    SqlParameter about = cmd.Parameters.Add("@about", System.Data.SqlDbType.VarChar, 4000);
                    about.Direction = System.Data.ParameterDirection.Input;
                    about.Value = (churchDTO.About == null) ? string.Empty : churchDTO.About;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = (churchDTO.GalleryId == null) ? 0 : churchDTO.GalleryId;

                    SqlParameter banners = cmd.Parameters.Add("@banners", System.Data.SqlDbType.VarChar, 2000);
                    banners.Direction = System.Data.ParameterDirection.Input;
                    banners.Value = (churchDTO.Banners == null) ? string.Empty : churchDTO.Banners;

                    SqlParameter splashscreen = cmd.Parameters.Add("@splashscreen", System.Data.SqlDbType.VarChar, 2000);
                    splashscreen.Direction = System.Data.ParameterDirection.Input;
                    splashscreen.Value = (churchDTO.Splashscreen == null) ? string.Empty : churchDTO.Splashscreen;

                    SqlParameter theme = cmd.Parameters.Add("@theme", System.Data.SqlDbType.VarChar, 20);
                    theme.Direction = System.Data.ParameterDirection.Input;
                    theme.Value = (churchDTO.Theme == null) ? string.Empty : churchDTO.Theme;

                    SqlParameter phone_num = cmd.Parameters.Add("@phone_num", System.Data.SqlDbType.VarChar, 20);
                    phone_num.Direction = System.Data.ParameterDirection.Input;
                    phone_num.Value = (churchDTO.PhoneNum == null) ? string.Empty : churchDTO.PhoneNum;

                    SqlParameter email = cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 100);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = (churchDTO.Email == null) ? string.Empty : churchDTO.Email;

                    SqlParameter lat = cmd.Parameters.Add("@lat", System.Data.SqlDbType.VarChar, 100);
                    lat.Direction = System.Data.ParameterDirection.Input;
                    lat.Value = (churchDTO.Latitude == null) ? string.Empty : churchDTO.Latitude;

                    SqlParameter longitu = cmd.Parameters.Add("@long", System.Data.SqlDbType.VarChar, 100);
                    longitu.Direction = System.Data.ParameterDirection.Input;
                    longitu.Value = (churchDTO.Longitude == null) ? string.Empty : churchDTO.Longitude;

                    SqlParameter Address = cmd.Parameters.Add("@Address", System.Data.SqlDbType.VarChar, 2000);
                    Address.Direction = System.Data.ParameterDirection.Input;
                    Address.Value = (churchDTO.Address == null) ? string.Empty : churchDTO.Address;

                    SqlParameter Planid = cmd.Parameters.Add("@planid", System.Data.SqlDbType.Int);
                    Planid.Direction = System.Data.ParameterDirection.Input;
                    Planid.Value = (churchDTO.PlanId == null) ? 0 : churchDTO.PlanId;

                    SqlParameter Username = cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar, 100);
                    Username.Direction = System.Data.ParameterDirection.Input;
                    Username.Value = (churchDTO.Username == null) ? string.Empty : churchDTO.Username;

                    SqlParameter Password = cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar, 50);
                    Password.Direction = System.Data.ParameterDirection.Input;
                    Password.Value = (churchDTO.Password == null) ? string.Empty : churchDTO.Password;

                    SqlParameter ContactPersonName = cmd.Parameters.Add("@contactperson_name",
                        System.Data.SqlDbType.VarChar, 256);
                    ContactPersonName.Direction = System.Data.ParameterDirection.Input;
                    ContactPersonName.Value = (churchDTO.ContactPersonName == null)
                        ? string.Empty
                        : churchDTO.ContactPersonName;

                    SqlParameter ContactPersonPhone_num = cmd.Parameters.Add("@contactperson_phonenum",
                        System.Data.SqlDbType.VarChar, 50);
                    ContactPersonPhone_num.Direction = System.Data.ParameterDirection.Input;
                    ContactPersonPhone_num.Value = (churchDTO.ContactPersonPhoneNum == null)
                        ? string.Empty
                        : churchDTO.ContactPersonPhoneNum;

                    SqlParameter ContactPersonEmail = cmd.Parameters.Add("@contactperson_email",
                        System.Data.SqlDbType.VarChar, 100);
                    ContactPersonEmail.Direction = System.Data.ParameterDirection.Input;
                    ContactPersonEmail.Value = (churchDTO.ContactPersonEmail == null)
                        ? string.Empty
                        : churchDTO.ContactPersonEmail;

                    SqlParameter banner_format = cmd.Parameters.Add("@banner_format", System.Data.SqlDbType.VarChar, 20);
                    banner_format.Direction = System.Data.ParameterDirection.Input;
                    banner_format.Value = (churchDTO.BannerFormat == null) ? string.Empty : churchDTO.BannerFormat;

                    SqlParameter splashscreen_format = cmd.Parameters.Add("@splashscreen_format",
                        System.Data.SqlDbType.VarChar, 20);
                    splashscreen_format.Direction = System.Data.ParameterDirection.Input;
                    splashscreen_format.Value = (churchDTO.SplashScreenFormat == null)
                        ? string.Empty
                        : churchDTO.SplashScreenFormat;

                    SqlParameter isPayPal = cmd.Parameters.Add("@ispaypal", System.Data.SqlDbType.Bit);
                    isPayPal.Direction = System.Data.ParameterDirection.Input;
                    isPayPal.Value = churchDTO.IsPayPal;

                    SqlParameter isPushPay = cmd.Parameters.Add("@ispushpay", System.Data.SqlDbType.Bit);
                    isPushPay.Direction = System.Data.ParameterDirection.Input;
                    isPushPay.Value = churchDTO.IsPushPay;

                    SqlParameter GiveUrl = cmd.Parameters.Add("@giveurl", System.Data.SqlDbType.VarChar, 500);
                    GiveUrl.Direction = System.Data.ParameterDirection.Input;
                    GiveUrl.Value = churchDTO.GiveURL;

                    SqlParameter State = cmd.Parameters.Add("@State", System.Data.SqlDbType.VarChar, 50);
                    State.Direction = System.Data.ParameterDirection.Input;
                    State.Value = churchDTO.State;

                    SqlParameter City = cmd.Parameters.Add("@City", System.Data.SqlDbType.VarChar, 50);
                    City.Direction = System.Data.ParameterDirection.Input;
                    City.Value = churchDTO.City;

                    SqlParameter Zip = cmd.Parameters.Add("@Zip", System.Data.SqlDbType.VarChar, 50);
                    Zip.Direction = System.Data.ParameterDirection.Input;
                    Zip.Value = churchDTO.Zip;

                    SqlParameter Country = cmd.Parameters.Add("@Country", System.Data.SqlDbType.VarChar, 50);
                    Country.Direction = System.Data.ParameterDirection.Input;
                    Country.Value = churchDTO.Country;

                    SqlParameter firstname = cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.VarChar, 100);
                    firstname.Direction = System.Data.ParameterDirection.Input;
                    firstname.Value = (churchDTO.AdminFirstName == null) ? string.Empty : churchDTO.AdminFirstName;

                    SqlParameter lastname = cmd.Parameters.Add("@LastName", System.Data.SqlDbType.VarChar, 100);
                    lastname.Direction = System.Data.ParameterDirection.Input;
                    lastname.Value = (churchDTO.AdminLastName == null) ? string.Empty : churchDTO.AdminLastName;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = churchDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int DeleteChurch(int churchid)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteChurch]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = churchid;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int CreateAnnouncement(AnnouncementDTO announcementDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[CreateAnnouncements]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter announcements_url = cmd.Parameters.Add("@announcements_url",
                        System.Data.SqlDbType.VarChar, 2000);
                    announcements_url.Direction = System.Data.ParameterDirection.Input;
                    announcements_url.Value = announcementDTO.Announcements_Url;

                    SqlParameter facebook_url = cmd.Parameters.Add("@facebook_url", System.Data.SqlDbType.VarChar, 2000);
                    facebook_url.Direction = System.Data.ParameterDirection.Input;
                    facebook_url.Value = announcementDTO.Facebook_Url;

                    SqlParameter twitter_url = cmd.Parameters.Add("@twitter_url", System.Data.SqlDbType.VarChar, 2000);
                    twitter_url.Direction = System.Data.ParameterDirection.Input;
                    twitter_url.Value = announcementDTO.Twitter_Url;

                    SqlParameter website_url = cmd.Parameters.Add("@website_url", System.Data.SqlDbType.VarChar, 2000);
                    website_url.Direction = System.Data.ParameterDirection.Input;
                    website_url.Value = announcementDTO.Website_Url;

                    SqlParameter geomap = cmd.Parameters.Add("@geomap", System.Data.SqlDbType.VarChar, 40);
                    geomap.Direction = System.Data.ParameterDirection.Input;
                    geomap.Value = announcementDTO.GeoMap;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = announcementDTO.ChurchId;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }
        internal static AnnouncmentDataDTO EditCustomAnnouncement(AnnouncmentDataDTO announcementDto)
        {
            int id = 0;
            AnnouncmentDataDTO updatedAnnDto = null;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[spUpdateCustomAnnouncement]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = announcementDto.Name;



                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.NVarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = announcementDto.Description;

                    SqlParameter announcementUrl = cmd.Parameters.Add("@announcementUrl", System.Data.SqlDbType.NVarChar, 1024);
                    announcementUrl.Direction = System.Data.ParameterDirection.Input;
                    announcementUrl.Value = announcementDto.AnnouncementUrl;


                    SqlParameter church_id = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = announcementDto.churchId;

                    SqlParameter fileTypeId = cmd.Parameters.Add("@fileTypeId", System.Data.SqlDbType.Int);
                    fileTypeId.Direction = System.Data.ParameterDirection.Input;
                    fileTypeId.Value = announcementDto.FileTypeId;

                    SqlParameter duration = cmd.Parameters.Add("@duration", System.Data.SqlDbType.VarChar, 20);
                    duration.Direction = System.Data.ParameterDirection.Input;
                    duration.Value = announcementDto.Duration;


                    SqlParameter created = cmd.Parameters.Add("@scheduledDate", System.Data.SqlDbType.DateTime);
                    created.Direction = System.Data.ParameterDirection.Input;
                    created.Value = announcementDto.ScheduledDate;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Input;
                    Idpar.Value = announcementDto.Id;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        AnnouncmentDataDTO v = new AnnouncmentDataDTO();
                        v.Id = (int)myData["Id"];
                        v.Name = myData["name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["name"]);
                        v.AnnouncementUrl = myData["announcement_url"] == DBNull.Value ? string.Empty : Convert.ToString(myData["announcement_url"]);

                        v.Description = myData["description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["description"]);
                        v.Duration = myData["duration"] == DBNull.Value ? string.Empty : Convert.ToString(myData["duration"]);
                        v.FileTypeId = myData["fileTypeId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["fileTypeId"]);
                        v.FiletTypeName = myData["fileTypeName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["fileTypeName"]);
                        v.churchId = (int)myData["church_id"];
                        v.ScheduledDate = myData["scheduledDate"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["scheduledDate"]);
                        v.Status = myData["status"] == DBNull.Value ? 0 : Convert.ToInt32(myData["status"]);
                        v.IsUsed = Convert.ToBoolean(myData["is_used"]);
                        v.Updated = myData["updated"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["updated"]);

                        updatedAnnDto = v;
                    }
                    myData.Close();
                    myConn.Close();

                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return updatedAnnDto;
        }



        internal static int EditAnnouncement(AnnouncementDTO announcementDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[UpdateAnnouncements]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter announcements_url = cmd.Parameters.Add("@announcements_url",
                        System.Data.SqlDbType.VarChar, 2000);
                    announcements_url.Direction = System.Data.ParameterDirection.Input;
                    announcements_url.Value = announcementDTO.Announcements_Url;

                    SqlParameter facebook_url = cmd.Parameters.Add("@facebook_url", System.Data.SqlDbType.VarChar, 2000);
                    facebook_url.Direction = System.Data.ParameterDirection.Input;
                    facebook_url.Value = announcementDTO.Facebook_Url;

                    SqlParameter twitter_url = cmd.Parameters.Add("@twitter_url", System.Data.SqlDbType.VarChar, 2000);
                    twitter_url.Direction = System.Data.ParameterDirection.Input;
                    twitter_url.Value = announcementDTO.Twitter_Url;

                    SqlParameter website_url = cmd.Parameters.Add("@website_url", System.Data.SqlDbType.VarChar, 2000);
                    website_url.Direction = System.Data.ParameterDirection.Input;
                    website_url.Value = announcementDTO.Website_Url;

                    SqlParameter geomap = cmd.Parameters.Add("@geomap", System.Data.SqlDbType.VarChar, 40);
                    geomap.Direction = System.Data.ParameterDirection.Input;
                    geomap.Value = announcementDTO.GeoMap;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = announcementDTO.ChurchId;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = announcementDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeleteAnnouncement(int AnnouncementId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteAnnouncements]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = AnnouncementId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static string VerseOftheDay()
        {
            Newtonsoft.Json.Linq.JArray resp1 = null;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://labs.bible.org/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            // HTTP GET
            string content = string.Empty;
            HttpResponseMessage response = client.GetAsync("api/?passage=random").Result;
            if (response.IsSuccessStatusCode)
            {
                Stream st = response.Content.ReadAsStreamAsync().Result;
                StreamReader reader = new StreamReader(st);
                content = reader.ReadToEnd();


            }
            return content;
        }


        internal static string AddVerse()
        {
            string strVerse = string.Empty;
            try
            {
                SqlConnection myConn = ConnectTODB();
                string FirstString = "<b>";
                string LastString = "</b>";
                string FinalString = string.Empty;
                strVerse = VerseOftheDay();
                int Pos1 = strVerse.IndexOf(FirstString) + FirstString.Length;
                int Pos2 = strVerse.IndexOf(LastString);


                string strReference = strVerse.Substring(Pos1, Pos2 - Pos1);
                string strMessage = strVerse.Substring(Pos2 + 5, strVerse.Length - (Pos2 + 5));



                int wordcount = strMessage.Trim().Split(' ').Count();
                if (wordcount >= 30 && wordcount <= 50)
                {
                    VerseDTO verseDTO = new VerseDTO();
                    verseDTO.Reference = strReference;
                    verseDTO.Verse = strMessage;
                    verseDTO.Status = 1;
                    verseDTO.Author = strReference;
                    verseDTO.Church_id = 0;
                    DateTime date = new DateTime();
                    date = (GetLastModifiedDate() == Convert.ToDateTime("01/01/1900"))
                        ? Convert.ToDateTime(DateTime.Now)
                        : Convert.ToDateTime(GetLastModifiedDate()).AddDays(1);
                    verseDTO.LastModified = date;

                    if (null != myConn)
                    {

                        SqlCommand cmd = new SqlCommand("[cp].[UpdateVerseOftheDay]", myConn);
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;

                        SqlParameter verse = cmd.Parameters.Add("@Verse", System.Data.SqlDbType.NText);
                        verse.Direction = System.Data.ParameterDirection.Input;
                        verse.Value = verseDTO.Verse;

                        SqlParameter author = cmd.Parameters.Add("@Author", System.Data.SqlDbType.VarChar, 50);
                        author.Direction = System.Data.ParameterDirection.Input;
                        author.Value = verseDTO.Author;

                        SqlParameter LastModified = cmd.Parameters.Add("@LastModified", System.Data.SqlDbType.DateTime);
                        LastModified.Direction = System.Data.ParameterDirection.Input;
                        LastModified.Value = verseDTO.LastModified;

                        SqlParameter Reference = cmd.Parameters.Add("@Reference", System.Data.SqlDbType.VarChar, 50);
                        Reference.Direction = System.Data.ParameterDirection.Input;
                        Reference.Value = verseDTO.Reference;


                        SqlParameter Church_id = cmd.Parameters.Add("@Church_id", System.Data.SqlDbType.Int);
                        Church_id.Direction = System.Data.ParameterDirection.Input;
                        Church_id.Value = verseDTO.Church_id;

                        SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                        Status.Direction = System.Data.ParameterDirection.Input;
                        Status.Value = verseDTO.Status;

                        cmd.ExecuteNonQuery();


                        myConn.Close();
                    }
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return strVerse;
        }

        internal static void InsertVerse(VerseDTO verseDTO)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();


                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[UpdateVerseOftheDay]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter verse = cmd.Parameters.Add("@Verse", System.Data.SqlDbType.NText);
                    verse.Direction = System.Data.ParameterDirection.Input;
                    verse.Value = verseDTO.Verse;

                    SqlParameter author = cmd.Parameters.Add("@Author", System.Data.SqlDbType.VarChar, 50);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = verseDTO.Author;

                    SqlParameter LastModified = cmd.Parameters.Add("@LastModified", System.Data.SqlDbType.DateTime);
                    LastModified.Direction = System.Data.ParameterDirection.Input;
                    LastModified.Value = verseDTO.LastModified;

                    SqlParameter Reference = cmd.Parameters.Add("@Reference", System.Data.SqlDbType.VarChar, 50);
                    Reference.Direction = System.Data.ParameterDirection.Input;
                    Reference.Value = verseDTO.Reference;


                    SqlParameter Church_id = cmd.Parameters.Add("@Church_id", System.Data.SqlDbType.Int);
                    Church_id.Direction = System.Data.ParameterDirection.Input;
                    Church_id.Value = verseDTO.Church_id;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = verseDTO.Status;

                    cmd.ExecuteNonQuery();


                    myConn.Close();
                }


            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

        }


        internal static DateTime GetLastModifiedDate()
        {
            DateTime dtModified = new DateTime();
            try
            {
                SqlConnection myConn = ConnectTODB();
                //string verse = AddVerse();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetLastModifiedDate]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        dtModified = (myData["LastModified"] == null)
                            ? Convert.ToDateTime("01/01/1900")
                            : Convert.ToDateTime(myData["LastModified"]);

                    }
                    myData.Close();
                    myConn.Close();



                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return dtModified;
        }

        internal static VerseDTO GetVerseOftheDay()
        {
            VerseDTO v = new VerseDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();
                //string verse = AddVerse();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetVerseoftheDay]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Verse = (myData["Verse"].ToString() == null)
                            ? string.Empty
                            : (string)myData["Verse"].ToString();
                        v.Author = (myData["Author"].ToString() == null)
                            ? string.Empty
                            : (string)myData["Author"].ToString();
                        v.LastModified = (DateTime)myData["LastModified"];
                        v.Verse = System.Net.WebUtility.HtmlDecode(UppercaseFirst(v.Verse));
                        v.Reference = (myData["Reference"].ToString() == null)
                            ? string.Empty
                            : (string)myData["Reference"].ToString();
                        v.Church_id = 0;
                        v.Status = 0;

                    }
                    myData.Close();
                    myConn.Close();



                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        internal static void InsertVerseOftheDay()
        {

            int versecount = 0;

            for (int i = 0; i < 150; i++)
            {
                // versecount = GetVerseCount();
                AddVerse();
            }


        }

        internal static int GetVerseCount()
        {
            int VerseCount = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetVerseCount]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        VerseCount = (int)myData["VerseCount"];

                    }


                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return VerseCount;
        }


        internal static CountDTO GetCountValues(int ChurchId)
        {

            CountDTO v = new CountDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetCountValues]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.SeriesCount = (int)myData["seriesCnt"];
                        v.EventCount = (int)myData["eventsCnt"];
                        v.TestimonialPendingCount = (int)myData["TestPendingCnt"];
                        v.TestimonialApprovedCount = (int)myData["TestApprovedCnt"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static List<ChurchMemberInfoDto> GetMembersHavingBirthday()
        {

            List<ChurchMemberInfoDto> churchMembersList = new List<ChurchMemberInfoDto>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[spGetChurchMembersHavingBirthDay]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    //SqlParameter churchid = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    //churchid.Direction = System.Data.ParameterDirection.Input;
                    //churchid.Value = ChurchId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchMemberInfoDto infoDto = new ChurchMemberInfoDto();
                        infoDto.Id = (int)myData["id"];
                        infoDto.churchId = (int)myData["churchId"];
                        infoDto.DeviceType = (int)myData["deviceType"];
                        infoDto.surName = myData["surName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["surName"]);
                        infoDto.name = myData["name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["name"]);
                        infoDto.dateOfBirth = Convert.IsDBNull(myData["dateOfBirth"]) ? System.DateTime.Now : (DateTime)myData["dateOfBirth"];
                        infoDto.weddingDate = Convert.IsDBNull(myData["weddingDate"]) ? System.DateTime.Now : (DateTime)myData["weddingDate"];
                        infoDto.createdDate = Convert.IsDBNull(myData["createdDate"]) ? System.DateTime.Now : (DateTime)myData["createdDate"];
                        infoDto.FBDeviceToken = myData["FBDeviceToken"] == DBNull.Value ? string.Empty : Convert.ToString(myData["FBDeviceToken"]);
                        infoDto.PushNotificationAccept = myData["PushNotificationAccept"] == DBNull.Value ? false : Convert.ToBoolean(myData["PushNotificationAccept"]);
                        churchMembersList.Add(infoDto);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churchMembersList;
        }


        internal static List<ChurchMemberInfoDto> GetMembersHavingWeddingDay()
        {

            List<ChurchMemberInfoDto> churchMembersList = new List<ChurchMemberInfoDto>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[spGetChurchMembersHavingWeddingDay]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    //SqlParameter churchid = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    //churchid.Direction = System.Data.ParameterDirection.Input;
                    //churchid.Value = ChurchId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchMemberInfoDto infoDto = new ChurchMemberInfoDto();
                        infoDto.Id = (int)myData["id"];
                        infoDto.churchId = (int)myData["churchId"];
                        infoDto.DeviceType = (int)myData["deviceType"];
                        infoDto.surName = myData["surName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["surName"]);
                        infoDto.name = myData["name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["name"]);
                        infoDto.dateOfBirth = Convert.IsDBNull(myData["dateOfBirth"]) ? System.DateTime.Now : (DateTime)myData["dateOfBirth"];
                        infoDto.weddingDate = Convert.IsDBNull(myData["weddingDate"]) ? System.DateTime.Now : (DateTime)myData["weddingDate"];
                        infoDto.createdDate = Convert.IsDBNull(myData["createdDate"]) ? System.DateTime.Now : (DateTime)myData["createdDate"];
                        infoDto.FBDeviceToken = myData["FBDeviceToken"] == DBNull.Value ? string.Empty : Convert.ToString(myData["FBDeviceToken"]);
                        infoDto.PushNotificationAccept = myData["PushNotificationAccept"] == DBNull.Value ? false : Convert.ToBoolean(myData["PushNotificationAccept"]);
                        churchMembersList.Add(infoDto);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churchMembersList;
        }


        internal static List<ChurchMemberInfoDto> GetChurchMembers(int ChurchId, int age)
        {

            List<ChurchMemberInfoDto> churchMembersList = new List<ChurchMemberInfoDto>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[spGetChurchMembers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter churchid = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlParameter ageParam = cmd.Parameters.Add("@age", System.Data.SqlDbType.Int);
                    ageParam.Direction = System.Data.ParameterDirection.Input;
                    ageParam.Value = age;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchMemberInfoDto infoDto = new ChurchMemberInfoDto();
                        infoDto.Id = (int)myData["id"];
                        infoDto.churchId = (int)myData["churchId"];
                        infoDto.DeviceType = (int)myData["deviceType"];
                        infoDto.surName = myData["surName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["surName"]);
                        infoDto.name = myData["name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["name"]);
                        infoDto.dateOfBirth = Convert.IsDBNull(myData["dateOfBirth"]) ? System.DateTime.Now : (DateTime)myData["dateOfBirth"];
                        infoDto.weddingDate = Convert.IsDBNull(myData["weddingDate"]) ? System.DateTime.Now : (DateTime)myData["weddingDate"];
                        infoDto.createdDate = Convert.IsDBNull(myData["createdDate"]) ? System.DateTime.Now : (DateTime)myData["createdDate"];
                        infoDto.Gender = myData["Gender"] == DBNull.Value ? 0 : Convert.ToInt16(myData["Gender"]);
                        infoDto.PhoneNumber = myData["PhoneNumber"] == DBNull.Value ? string.Empty : Convert.ToString(myData["PhoneNumber"]);
                        churchMembersList.Add(infoDto);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churchMembersList;
        }

        internal static int GetUsersCount(int ChurchId)
        {
            int CountUsers = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetAppUsersCount]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        CountUsers = (int)myData["UsersCnt"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return CountUsers;
        }


        internal static int RegisterDevice(DeviceDTO deviceDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[RegisterDevice]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ChurchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    ChurchId.Direction = System.Data.ParameterDirection.Input;
                    ChurchId.Value = deviceDTO.ChurchId;

                    SqlParameter UDID = cmd.Parameters.Add("@UDID", System.Data.SqlDbType.VarChar, 100);
                    UDID.Direction = System.Data.ParameterDirection.Input;
                    UDID.Value = deviceDTO.UDID;

                    SqlParameter DeviceToken = cmd.Parameters.Add("@DeviceToken", System.Data.SqlDbType.VarChar, 100);
                    DeviceToken.Direction = System.Data.ParameterDirection.Input;
                    DeviceToken.Value = deviceDTO.DeviceToken;

                    SqlParameter DeviceType = cmd.Parameters.Add("@DeviceType", System.Data.SqlDbType.VarChar, 100);
                    DeviceType.Direction = System.Data.ParameterDirection.Input;
                    DeviceType.Value = deviceDTO.DeviceType;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int UpdatePassword(string userName, string newPassword)
        {
            int churchId = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdatePassword]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@userName", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = userName;

                    SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 100);
                    Password.Direction = ParameterDirection.Input;
                    Password.Value = newPassword;
                    //Password.Value = UsersDalV4.Encrypt(confirmPassword);

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        churchId = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return churchId;
        }
        #endregion



        #region Events

        internal static List<EventsDTO> GetAllEvents(int ChurchId)
        {
            List<EventsDTO> events = new List<EventsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetEvents]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlParameter evtid = cmd.Parameters.Add("@eventid", System.Data.SqlDbType.Int);
                    evtid.Direction = System.Data.ParameterDirection.Input;
                    evtid.Value = 0;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        EventsDTO v = new EventsDTO();
                        v.Id = (int)myData["id"];
                        v.ChurchId = (int)myData["church_id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Author = (string)(myData["author"]).ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Starts = Convert.IsDBNull(myData["starts"])? System.DateTime.Now : (DateTime?)myData["starts"];
                        v.Ends = Convert.IsDBNull(myData["ends"]) ? System.DateTime.Now : (DateTime?)myData["ends"];


                        events.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return events;
        }

        internal static EventsDTO GetEvent(long eventId)
        {
            EventsDTO v = new EventsDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetEvents]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = 0;


                    SqlParameter evtid = cmd.Parameters.Add("@eventid", System.Data.SqlDbType.Int);
                    evtid.Direction = System.Data.ParameterDirection.Input;
                    evtid.Value = eventId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.ChurchId = (int)myData["church_id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Author = (string)(myData["author"]).ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Starts = Convert.IsDBNull(myData["starts"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["starts"];
                        v.Ends = Convert.IsDBNull(myData["ends"]) ? System.DateTime.Now : (DateTime?)myData["ends"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static EventsDTO CreateEvent(EventsDTO eventsDTO)
        {
            EventsDTO v = new EventsDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateEvent]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = eventsDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = eventsDTO.Description;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = eventsDTO.Author;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = eventsDTO.Enabled;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = eventsDTO.GalleryId;

                    SqlParameter starts = cmd.Parameters.Add("@starts", System.Data.SqlDbType.DateTime);
                    starts.Direction = System.Data.ParameterDirection.Input;
                    starts.Value = eventsDTO.Starts;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = eventsDTO.Ends;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = eventsDTO.ChurchId;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Input;
                    Idpar.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                        v.Id = (int)myData["id"];
                        v.PushNotificationAccept = (myData["PushNotificationAccept"] == DBNull.Value) ? false : (bool)myData["PushNotificationAccept"];
                        v.ChurchId = eventsDTO.ChurchId;
                        v.ChurchName = (string) myData["ChurchName"];
                    }
                    myData.Close();
                    myConn.Close();


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }


        internal static int EditEvent(EventsDTO eventsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateEvent]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 256);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = eventsDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = eventsDTO.Description;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = eventsDTO.Author;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = eventsDTO.Enabled;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = eventsDTO.GalleryId;

                    SqlParameter starts = cmd.Parameters.Add("@starts", System.Data.SqlDbType.DateTime);
                    starts.Direction = System.Data.ParameterDirection.Input;
                    starts.Value = eventsDTO.Starts;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = eventsDTO.Ends;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = eventsDTO.ChurchId;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = eventsDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeleteEvent(int eventId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteEvent]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = eventId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        #endregion

        #region Images

        internal static List<ImageDTO> GetAllImages(int ChurchId)
        {
            List<ImageDTO> images = new List<ImageDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetImages]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = ChurchId;

                    SqlParameter imageid = cmd.Parameters.Add("@imageid", System.Data.SqlDbType.Int);
                    imageid.Direction = System.Data.ParameterDirection.Input;
                    imageid.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ImageDTO v = new ImageDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.ImageUrl = (string)myData["image_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated_on_utc"];
                        //v.Img = Convert.IsDBNull(myData["image"]) ? null : (byte[])myData["image"]; 
                        // v.ImageSize = (string)myData["image_size"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        //v.ImageHash = Convert.IsDBNull(myData["image_hash"]) ? null : (byte[])myData["image_hash"]; 
                        v.Theme = (string)myData["theme"].ToString();
                        v.ChurchId = Convert.IsDBNull(myData["churchid"]) ? 0 : (int)myData["churchid"];
                        v.ImageCategoryID = Convert.IsDBNull(myData["image_category"])
                            ? 0
                            : (int)myData["image_category"];
                        v.ImageFormat = (string)myData["image_format"].ToString();
                        images.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return images;
        }

        internal static ImageDTO GetImage(int ImageId)
        {
            ImageDTO v = new ImageDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetImages]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = 0;

                    SqlParameter imageid = cmd.Parameters.Add("@imageid", System.Data.SqlDbType.Int);
                    imageid.Direction = System.Data.ParameterDirection.Input;
                    imageid.Value = ImageId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.ImageUrl = (string)myData["image_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated_on_utc"];
                        v.Img = Convert.IsDBNull(myData["image"]) ? null : (byte[])myData["image"];
                        v.ImageSize = (string)myData["image_size"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.ImageHash = Convert.IsDBNull(myData["image_hash"]) ? null : (byte[])myData["image_hash"];
                        v.Theme = (string)myData["theme"].ToString();
                        v.ChurchId = Convert.IsDBNull(myData["churchid"]) ? 0 : (int)myData["churchid"];
                        v.ImageCategoryID = Convert.IsDBNull(myData["image_category"])
                            ? 0
                            : (int)myData["image_category"];
                        v.ImageFormat = (string)myData["image_format"].ToString();

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        #endregion


        #region Sermons

        internal static List<SermonDTO> GetAllSermons(int Churchid)
        {
            List<SermonDTO> Sermons = new List<SermonDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@Churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = Churchid;


                    SqlParameter sermonId = cmd.Parameters.Add("@sermonId", System.Data.SqlDbType.Int);
                    sermonId.Direction = System.Data.ParameterDirection.Input;
                    sermonId.Value = 0;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SermonDTO v = new SermonDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Ends = Convert.IsDBNull(myData["Ends"]) ? System.DateTime.Now : (DateTime?)myData["Ends"];
                        v.ThumbNail = (string)myData["ThumbNail"].ToString();
                        Sermons.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Sermons;
        }

        internal static SermonDTO GetSermon(long SermonId)
        {
            SermonDTO v = new SermonDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@Churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = 0;

                    SqlParameter sermonId = cmd.Parameters.Add("@sermonId", System.Data.SqlDbType.Int);
                    sermonId.Direction = System.Data.ParameterDirection.Input;
                    sermonId.Value = SermonId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Ends = Convert.IsDBNull(myData["Ends"]) ? System.DateTime.Now : (DateTime?)myData["Ends"];
                        v.ThumbNail = (string)myData["ThumbNail"].ToString();

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static int CreateSermons(SermonDTO sermonDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = sermonDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = sermonDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = sermonDTO.Description;

                    SqlParameter ThumbNail = cmd.Parameters.Add("@ThumbNail", System.Data.SqlDbType.VarChar, 2000);
                    ThumbNail.Direction = System.Data.ParameterDirection.Input;
                    ThumbNail.Value = sermonDTO.ThumbNail;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = sermonDTO.ChurchId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = sermonDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = sermonDTO.Enabled;

                    SqlParameter created = cmd.Parameters.Add("@created", System.Data.SqlDbType.DateTime);
                    created.Direction = System.Data.ParameterDirection.Input;
                    created.Value = sermonDTO.Created;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = sermonDTO.Updated;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = sermonDTO.Ends;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int EditSermon(SermonDTO sermonDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = sermonDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = sermonDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = sermonDTO.Description;

                    SqlParameter ThumbNail = cmd.Parameters.Add("@ThumbNail", System.Data.SqlDbType.VarChar, 2000);
                    ThumbNail.Direction = System.Data.ParameterDirection.Input;
                    ThumbNail.Value = sermonDTO.ThumbNail;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = sermonDTO.ChurchId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = sermonDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = sermonDTO.Enabled;

                    SqlParameter created = cmd.Parameters.Add("@created", System.Data.SqlDbType.DateTime);
                    created.Direction = System.Data.ParameterDirection.Input;
                    created.Value = sermonDTO.Created;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = sermonDTO.Updated;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = sermonDTO.Ends;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = sermonDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeleteSermon(int sermonId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = sermonId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        #endregion

        #region Plays

        internal static List<PlaysDTO> GetAllPlays(int sermonid)
        {
            List<PlaysDTO> Plays = new List<PlaysDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPlays]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@sermonid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = sermonid;

                    SqlParameter playid = cmd.Parameters.Add("@playid", System.Data.SqlDbType.Int);
                    playid.Direction = System.Data.ParameterDirection.Input;
                    playid.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        PlaysDTO v = new PlaysDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.SermonId = (int)myData["sermon_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.PlayUrl = (string)myData["play_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = Convert.IsDBNull(myData["duration"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["duration"];

                        Plays.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Plays;
        }

        internal static PlaysDTO GetPlay(int playId)
        {

            PlaysDTO v = new PlaysDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPlays]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@sermonid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = 0;

                    SqlParameter playid = cmd.Parameters.Add("@playid", System.Data.SqlDbType.Int);
                    playid.Direction = System.Data.ParameterDirection.Input;
                    playid.Value = playId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.SermonId = (int)myData["sermon_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.PlayUrl = (string)myData["play_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = Convert.IsDBNull(myData["duration"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["duration"];


                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static int CreatePlay(PlaysDTO playsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreatePlays]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = playsDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = playsDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = playsDTO.Description;

                    SqlParameter sermon_id = cmd.Parameters.Add("@sermon_id", System.Data.SqlDbType.Int);
                    sermon_id.Direction = System.Data.ParameterDirection.Input;
                    sermon_id.Value = playsDTO.SermonId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = playsDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = playsDTO.Enabled;

                    SqlParameter play_url = cmd.Parameters.Add("@play_url", System.Data.SqlDbType.VarChar, 1024);
                    play_url.Direction = System.Data.ParameterDirection.Input;
                    play_url.Value = playsDTO.PlayUrl;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = playsDTO.Updated;

                    SqlParameter duration = cmd.Parameters.Add("@duration", System.Data.SqlDbType.DateTime);
                    duration.Direction = System.Data.ParameterDirection.Input;
                    duration.Value = playsDTO.Duration;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int EditPlay(PlaysDTO playsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdatePlays]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = playsDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = playsDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = playsDTO.Description;

                    SqlParameter sermon_id = cmd.Parameters.Add("@sermon_id", System.Data.SqlDbType.Int);
                    sermon_id.Direction = System.Data.ParameterDirection.Input;
                    sermon_id.Value = playsDTO.SermonId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = playsDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = playsDTO.Enabled;

                    SqlParameter play_url = cmd.Parameters.Add("@play_url", System.Data.SqlDbType.VarChar, 1024);
                    play_url.Direction = System.Data.ParameterDirection.Input;
                    play_url.Value = playsDTO.PlayUrl;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = playsDTO.Updated;

                    SqlParameter duration = cmd.Parameters.Add("@duration", System.Data.SqlDbType.DateTime);
                    duration.Direction = System.Data.ParameterDirection.Input;
                    duration.Value = playsDTO.Duration;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = playsDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeletePlay(int playId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeletePlays]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = playId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        #endregion

        #region Prayers

        internal static List<PrayersDTO> GetAllPrayers(int churchid)
        {
            List<PrayersDTO> Prayers = new List<PrayersDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPrayers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = churchid;

                    SqlParameter prayer = cmd.Parameters.Add("@prayerid", System.Data.SqlDbType.Int);
                    prayer.Direction = System.Data.ParameterDirection.Input;
                    prayer.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        PrayersDTO v = new PrayersDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Email = (string)myData["email"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Message = (string)myData["message"].ToString();
                        v.Confidential = (bool)myData["confidential"];
                        v.ContactMe = (bool)myData["contactme"];
                        v.Phone = (string)myData["phone"].ToString();

                        Prayers.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Prayers;
        }

        internal static PrayersDTO GetPrayer(long prayerId)
        {
            PrayersDTO v = new PrayersDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPrayers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = 0;


                    SqlParameter prayer = cmd.Parameters.Add("@prayerid", System.Data.SqlDbType.Int);
                    prayer.Direction = System.Data.ParameterDirection.Input;
                    prayer.Value = prayerId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Email = (string)myData["email"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Message = (string)myData["message"].ToString();
                        v.Confidential = (bool)myData["confidential"];
                        v.ContactMe = (bool)myData["contactme"];
                        v.Phone = (string)myData["phone"].ToString();


                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static int CreatePrayer(PrayersDTO prayersDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreatePrayer]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 256);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = prayersDTO.Name;

                    SqlParameter email = cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 256);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = prayersDTO.Email;

                    SqlParameter message = cmd.Parameters.Add("@message", System.Data.SqlDbType.VarChar, 8000);
                    message.Direction = System.Data.ParameterDirection.Input;
                    message.Value = prayersDTO.Message;

                    SqlParameter churchid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = prayersDTO.ChurchId;

                    SqlParameter enabled = cmd.Parameters.Add("@enabled", System.Data.SqlDbType.Bit);
                    enabled.Direction = System.Data.ParameterDirection.Input;
                    enabled.Value = prayersDTO.Enabled;

                    SqlParameter confidential = cmd.Parameters.Add("@confidential", System.Data.SqlDbType.Bit);
                    confidential.Direction = System.Data.ParameterDirection.Input;
                    confidential.Value = prayersDTO.Confidential;

                    SqlParameter contactme = cmd.Parameters.Add("@contactme", System.Data.SqlDbType.Bit);
                    contactme.Direction = System.Data.ParameterDirection.Input;
                    contactme.Value = prayersDTO.ContactMe;

                    SqlParameter phone = cmd.Parameters.Add("@phone", System.Data.SqlDbType.VarChar, 4000);
                    phone.Direction = System.Data.ParameterDirection.Input;
                    phone.Value = prayersDTO.Phone;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        #endregion

        #region Testimonials

        internal static List<TestimonialsDTO> GetAllTestimonials(int churchid)
        {
            List<TestimonialsDTO> Testimonials = new List<TestimonialsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetTestimonials]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = churchid;


                    SqlParameter testimonialid = cmd.Parameters.Add("@testimonialid", System.Data.SqlDbType.Int);
                    testimonialid.Direction = System.Data.ParameterDirection.Input;
                    testimonialid.Value = 0;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        TestimonialsDTO v = new TestimonialsDTO();
                        v.Id = (int)myData["id"];
                        v.Email = (string)myData["email"].ToString();
                        v.Author = (string)myData["author"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Enabled = (bool)myData["is_used"];
                        v.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        v.Topic = (string)myData["topic"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Location = (string)myData["location"].ToString();
                        v.Status = (int)myData["status"];

                        Testimonials.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Testimonials;
        }

        internal static TestimonialsDTO GetTestimonial(int TestimonialId)
        {
            TestimonialsDTO v = new TestimonialsDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetTestimonials]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = 0;


                    SqlParameter testimonialid = cmd.Parameters.Add("@testimonialid", System.Data.SqlDbType.Int);
                    testimonialid.Direction = System.Data.ParameterDirection.Input;
                    testimonialid.Value = TestimonialId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Email = (string)myData["email"].ToString();
                        v.Author = (string)myData["author"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Enabled = (bool)myData["is_used"];
                        v.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        v.Topic = (string)myData["topic"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Location = (string)myData["location"].ToString();
                        v.Status = (int)myData["status"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }


        internal static List<TestimonialsDTO> GetTotalTestimonials(int ChurchId)
        {
            List<TestimonialsDTO> Testimonials = new List<TestimonialsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetALLTestimonials]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = ChurchId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        TestimonialsDTO v = new TestimonialsDTO();
                        v.Id = (int)myData["id"];
                        v.Email = (string)myData["email"].ToString();
                        v.Author = (string)myData["author"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Enabled = (bool)myData["is_used"];
                        v.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        v.Topic = (string)myData["topic"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Location = (string)myData["location"].ToString();
                        v.Status = (int)myData["status"];

                        Testimonials.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Testimonials;
        }

        internal static List<TestimonialsDTO> GetTestimonialByStatus(int churchid, int status)
        {
            List<TestimonialsDTO> Testimonials = new List<TestimonialsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetTestimonialsByStatus]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = churchid;


                    SqlParameter Status = cmd.Parameters.Add("@status", System.Data.SqlDbType.Int);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = status;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        TestimonialsDTO v = new TestimonialsDTO();
                        v.Id = (int)myData["id"];
                        v.Email = (string)myData["email"].ToString();
                        v.Author = (string)myData["author"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Enabled = (bool)myData["is_used"];
                        v.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        v.Topic = (string)myData["topic"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Location = (string)myData["location"].ToString();
                        v.Status = (int)myData["status"];
                        Testimonials.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Testimonials;
        }

        internal static int CreateTestimonial(TestimonialsDTO testimonialsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateTestimonial]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 255);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = testimonialsDTO.Email;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 256);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = testimonialsDTO.Author;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = testimonialsDTO.ChurchId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = testimonialsDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = testimonialsDTO.Enabled;

                    //SqlParameter  created = cmd.Parameters.Add("@created", System.Data.SqlDbType.DateTime);
                    //created.Direction = System.Data.ParameterDirection.Input;
                    //created.Value = testimonialsDTO.Created ;

                    SqlParameter topic = cmd.Parameters.Add("@topic", System.Data.SqlDbType.VarChar, 255);
                    topic.Direction = System.Data.ParameterDirection.Input;
                    topic.Value = testimonialsDTO.Topic;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 8000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = testimonialsDTO.Description;

                    SqlParameter status = cmd.Parameters.Add("@status", System.Data.SqlDbType.Int);
                    status.Direction = System.Data.ParameterDirection.Input;
                    status.Value = testimonialsDTO.Status;

                    SqlParameter location = cmd.Parameters.Add("@location", System.Data.SqlDbType.VarChar, 100);
                    location.Direction = System.Data.ParameterDirection.Input;
                    location.Value = testimonialsDTO.Location;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int EditTestimonial(TestimonialsDTO testimonialsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UdpateTestimonial]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 255);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = testimonialsDTO.Email;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 256);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = testimonialsDTO.Author;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = testimonialsDTO.ChurchId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = testimonialsDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = testimonialsDTO.Enabled;


                    SqlParameter topic = cmd.Parameters.Add("@topic", System.Data.SqlDbType.VarChar, 255);
                    topic.Direction = System.Data.ParameterDirection.Input;
                    topic.Value = testimonialsDTO.Topic;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 8000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = testimonialsDTO.Description;

                    SqlParameter status = cmd.Parameters.Add("@status", System.Data.SqlDbType.Int);
                    status.Direction = System.Data.ParameterDirection.Input;
                    status.Value = testimonialsDTO.Status;

                    SqlParameter location = cmd.Parameters.Add("@location", System.Data.SqlDbType.VarChar, 100);
                    location.Direction = System.Data.ParameterDirection.Input;
                    location.Value = testimonialsDTO.Location;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = testimonialsDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int ApproveTestimonial(int testimonialId, int status)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[ApproveTestimonial]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter statusPar = cmd.Parameters.Add("@status", System.Data.SqlDbType.Int);
                    statusPar.Direction = System.Data.ParameterDirection.Input;
                    statusPar.Value = status;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = testimonialId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        #endregion


        #region Registration

        internal static int CreateUser(UsersDTO usersDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[RegisterUsers]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 255);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = usersDTO.Email;

                    SqlParameter password = cmd.Parameters.Add("@password", System.Data.SqlDbType.VarChar, 50);
                    password.Direction = System.Data.ParameterDirection.Input;
                    password.Value = usersDTO.Password;

                    SqlParameter conf_password = cmd.Parameters.Add("@conf_password", System.Data.SqlDbType.VarChar, 50);
                    conf_password.Direction = System.Data.ParameterDirection.Input;
                    conf_password.Value = usersDTO.ConfirmPassword;

                    SqlParameter firstname = cmd.Parameters.Add("@firstname", System.Data.SqlDbType.VarChar, 100);
                    firstname.Direction = System.Data.ParameterDirection.Input;
                    firstname.Value = usersDTO.FirstName;

                    SqlParameter lastname = cmd.Parameters.Add("@lastname", System.Data.SqlDbType.VarChar, 100);
                    lastname.Direction = System.Data.ParameterDirection.Input;
                    lastname.Value = usersDTO.LastName;

                    SqlParameter country = cmd.Parameters.Add("@country", System.Data.SqlDbType.VarChar, 200);
                    country.Direction = System.Data.ParameterDirection.Input;
                    country.Value = usersDTO.Country;

                    SqlParameter gender = cmd.Parameters.Add("@gender", System.Data.SqlDbType.VarChar, 20);
                    gender.Direction = System.Data.ParameterDirection.Input;
                    gender.Value = usersDTO.Gender;

                    SqlParameter age = cmd.Parameters.Add("@age", System.Data.SqlDbType.Int);
                    age.Direction = System.Data.ParameterDirection.Input;
                    age.Value = usersDTO.Age;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = usersDTO.ChurchID;


                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        #endregion


        #region IhaveDecided

        internal static int SubmitIHavedecided(DecidedDTO decidedDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateIhaveDecided]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 256);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = decidedDTO.Email;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 256);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = decidedDTO.Name;

                    SqlParameter phonenumber = cmd.Parameters.Add("@phonenumber", System.Data.SqlDbType.VarChar, 50);
                    phonenumber.Direction = System.Data.ParameterDirection.Input;
                    phonenumber.Value = decidedDTO.PhoneNumber;


                    SqlParameter accept_savior = cmd.Parameters.Add("@accept_savior", System.Data.SqlDbType.Bit);
                    accept_savior.Direction = System.Data.ParameterDirection.Input;
                    accept_savior.Value = decidedDTO.AcceptSavior;

                    SqlParameter recommitted_life = cmd.Parameters.Add("@recommitted_life", System.Data.SqlDbType.Bit);
                    recommitted_life.Direction = System.Data.ParameterDirection.Input;
                    recommitted_life.Value = decidedDTO.RecommittedLife;

                    SqlParameter info_relationship = cmd.Parameters.Add("@info_relationship", System.Data.SqlDbType.Bit);
                    info_relationship.Direction = System.Data.ParameterDirection.Input;
                    info_relationship.Value = decidedDTO.InfoRelationship;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = decidedDTO.ChurchId;


                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static List<DecidedDTO> GetIHaveDecided(int ChurchId)
        {
            List<DecidedDTO> decidedDTO = new List<DecidedDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetIhaveDecided]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        DecidedDTO v = new DecidedDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.PhoneNumber = (string)myData["phonenumber"].ToString();
                        v.AcceptSavior = (bool)myData["accept_savior"];
                        v.RecommittedLife = (bool)myData["recommitted_life"];
                        v.InfoRelationship = (bool)myData["info_relationship"];
                        v.ChurchId = (int)myData["church_id"];
                        decidedDTO.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return decidedDTO;
        }


        #endregion


        #region Plans

        internal static List<PlanandFeaturesDTO> GetPlansandFeatures()
        {
            List<PlanandFeaturesDTO> planandFeaturesDTO = new List<PlanandFeaturesDTO>();


            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPlansandFeatures]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();
                    string planName = null;
                    while (myData.Read())
                    {

                        List<string> planFeaturesDTO;
                        string tempName = (string)myData["Plan"].ToString();
                        if (planName != null && tempName.Equals(planName))
                        {

                            //planFeaturesDTO.Add(planfeature);

                            PlanandFeaturesDTO dto = planandFeaturesDTO[planandFeaturesDTO.Count - 1];
                            dto.PlanFeaturesName.Add((string)myData["PlanFeature"].ToString());

                        }
                        else
                        {

                            planName = (string)myData["Plan"].ToString();
                            planFeaturesDTO = new List<string>();

                            string planFeature = (string)myData["PlanFeature"].ToString();
                            planFeaturesDTO.Add(planFeature);
                            PlanandFeaturesDTO dto = new PlanandFeaturesDTO();
                            dto.PlanFeaturesName = planFeaturesDTO;
                            dto.PlanName = planName;
                            dto.PlanId = (int)myData["PlanID"];
                            dto.Amount = (string)myData["Amount"].ToString();
                            planandFeaturesDTO.Add(dto);

                        }





                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return planandFeaturesDTO;
        }


        internal static List<PlanDTO> GetAllPlans()
        {
            List<PlanDTO> Plans = new List<PlanDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPlans]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        PlanDTO v = new PlanDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["Description"].ToString();
                        v.Amount = myData["Amount"].ToString();
                        Plans.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Plans;
        }

        internal static int CreatePlan(PlanDTO planDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[CreatePlan]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 50);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = planDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 100);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = planDTO.Description;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int EditPlan(PlanDTO planDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdatePlan]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 50);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = planDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 100);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = planDTO.Description;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int DeletePlan(int planid)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeletePlan]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = planid;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        /// <summary>
        /// Returns Plan Amount for a given church
        /// </summary>
        /// <returns></returns>
        internal static double GetPlanAmount(int planId, string country)
        {
            double planAmount = 0;

            try
            {
                var _countryCode = (country.Equals("United States", StringComparison.InvariantCultureIgnoreCase) ? "US" : "IN");

                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPlanAmount]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter _planId = cmd.Parameters.Add("@planid", System.Data.SqlDbType.Int);
                    _planId.Direction = System.Data.ParameterDirection.Input;
                    _planId.Value = planId;

                    SqlParameter ccode = cmd.Parameters.Add("@country", System.Data.SqlDbType.VarChar, 4000);
                    ccode.Direction = System.Data.ParameterDirection.Input;
                    ccode.Value = _countryCode;

                    planAmount = Convert.ToDouble(cmd.ExecuteScalar().ToString().TrimStart('$'));


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return planAmount;
        }

        #endregion

        #region planFeatures

        internal static List<PlanFeaturesDTO> GetPlanFeatures(int planid)
        {
            List<PlanFeaturesDTO> PlanFeatures = new List<PlanFeaturesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPlanFeatures]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@PlanId", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Input;
                    Idpar.Value = planid;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        PlanFeaturesDTO v = new PlanFeaturesDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["Description"].ToString();
                        v.PlanId = (int)myData["plan_id"];
                        PlanFeatures.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return PlanFeatures;
        }


        internal static int CreatePlanFeatures(PlanFeaturesDTO planFeaturesDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[CreatePlanFeatures]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 50);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = planFeaturesDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 100);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = planFeaturesDTO.Description;

                    SqlParameter planid = cmd.Parameters.Add("@plan_id", System.Data.SqlDbType.Int);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = planFeaturesDTO.PlanId;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int EditPlanFeatures(PlanFeaturesDTO planFeaturesDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdatePlanFeatures]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 50);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = planFeaturesDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 100);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = planFeaturesDTO.Description;

                    SqlParameter planid = cmd.Parameters.Add("@plan_id", System.Data.SqlDbType.Int);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = planFeaturesDTO.PlanId;



                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int DeletePlanFeatures(int planfeatureid)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeletePlanFeatures]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = planfeatureid;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        #endregion


        #region Series

        internal static List<SeriesDTO> GetAllSeries(int Churchid)
        {
            List<SeriesDTO> Series = new List<SeriesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetSeries]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@Churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = Churchid;


                    SqlParameter sermonId = cmd.Parameters.Add("@seriesId", System.Data.SqlDbType.Int);
                    sermonId.Direction = System.Data.ParameterDirection.Input;
                    sermonId.Value = 0;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SeriesDTO v = new SeriesDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Ends = Convert.IsDBNull(myData["Ends"]) ? System.DateTime.Now : (DateTime?)myData["Ends"];
                        v.ThumbNail = (string)myData["ThumbNail"].ToString();
                        v.ThumbNailFormat = (string)myData["ThumbNailFormat"].ToString();
                        Series.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Series;
        }

        internal static SeriesDTO GetSeries(long SeriesId)
        {
            SeriesDTO v = new SeriesDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetSeries]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = 0;

                    SqlParameter seriesId = cmd.Parameters.Add("@seriesId", System.Data.SqlDbType.Int);
                    seriesId.Direction = System.Data.ParameterDirection.Input;
                    seriesId.Value = SeriesId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Ends = Convert.IsDBNull(myData["Ends"]) ? System.DateTime.Now : (DateTime?)myData["Ends"];
                        v.ThumbNail = (string)myData["ThumbNail"].ToString();
                        v.ThumbNailFormat = (string)myData["ThumbNailFormat"].ToString();
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static int CreateSeries(SeriesDTO seriesDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateSeries]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = seriesDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = seriesDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = seriesDTO.Description;

                    SqlParameter ThumbNail = cmd.Parameters.Add("@ThumbNail", System.Data.SqlDbType.VarChar, 2000);
                    ThumbNail.Direction = System.Data.ParameterDirection.Input;
                    ThumbNail.Value = seriesDTO.ThumbNail;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = seriesDTO.ChurchId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = seriesDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = seriesDTO.Enabled;

                    SqlParameter created = cmd.Parameters.Add("@created", System.Data.SqlDbType.DateTime);
                    created.Direction = System.Data.ParameterDirection.Input;
                    created.Value = seriesDTO.Created;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = seriesDTO.Updated;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = seriesDTO.Ends;

                    SqlParameter ThumbNailFormat = cmd.Parameters.Add("@ThumbNailFormat", System.Data.SqlDbType.VarChar,
                        20);
                    ThumbNailFormat.Direction = System.Data.ParameterDirection.Input;
                    ThumbNailFormat.Value = (seriesDTO.ThumbNailFormat == null)
                        ? string.Empty
                        : seriesDTO.ThumbNailFormat;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int EditSeries(SeriesDTO seriesDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateSeries]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = seriesDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = seriesDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = seriesDTO.Description;

                    SqlParameter ThumbNail = cmd.Parameters.Add("@ThumbNail", System.Data.SqlDbType.VarChar, 2000);
                    ThumbNail.Direction = System.Data.ParameterDirection.Input;
                    ThumbNail.Value = seriesDTO.ThumbNail;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = seriesDTO.ChurchId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = seriesDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = seriesDTO.Enabled;

                    SqlParameter created = cmd.Parameters.Add("@created", System.Data.SqlDbType.DateTime);
                    created.Direction = System.Data.ParameterDirection.Input;
                    created.Value = seriesDTO.Created;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = seriesDTO.Updated;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = seriesDTO.Ends;

                    SqlParameter ThumbNailFormat = cmd.Parameters.Add("@ThumbNailFormat", System.Data.SqlDbType.VarChar,
                        20);
                    ThumbNailFormat.Direction = System.Data.ParameterDirection.Input;
                    ThumbNailFormat.Value = (seriesDTO.ThumbNailFormat == null)
                        ? string.Empty
                        : seriesDTO.ThumbNailFormat;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = seriesDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeleteSeries(int seriesId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteSeries]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = seriesId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static List<SeriesDTO> GetTopSeriesSermons(int churchid)
        {
            List<SeriesDTO> series = new List<SeriesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetTopSeriesSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    Churchid.Direction = System.Data.ParameterDirection.Input;
                    Churchid.Value = churchid;


                    //SqlDataAdapter mydataadp = new SqlDataAdapter(cmd);
                    //DataSet ds = new DataSet();
                    //mydataadp.Fill(ds);
                    //DataTable dt = ds.Tables[0];
                    //DataTable dtnew = dt.Clone();
                    //DataView dv = new DataView(dt);
                    //dtnew = dv.ToTable(true, "id","name","author","church_id","is_used","description","gallery_id","created","updated","Ends","ThumbNail","ThumbNailFormat");
                    //int count = dtnew.Rows.Count;                                         
                    //for (int i = 0; i < dtnew.Rows.Count; i++)

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SeriesDTO v = new SeriesDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.ChurchId = (int)myData["church_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Ends = Convert.IsDBNull(myData["Ends"]) ? System.DateTime.Now : (DateTime?)myData["Ends"];
                        v.ThumbNail = (string)myData["ThumbNail"].ToString();
                        v.ThumbNailFormat = (string)myData["ThumbNailFormat"].ToString();
                        series.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return series;
        }


        #endregion


        #region SermonSeries

        internal static List<SermonSeriesDTO> GetAllSermonSeries(int seriesid)
        {
            List<SermonSeriesDTO> sermonSeries = new List<SermonSeriesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter seriesidpar = cmd.Parameters.Add("@seriesid", System.Data.SqlDbType.Int);
                    seriesidpar.Direction = System.Data.ParameterDirection.Input;
                    seriesidpar.Value = seriesid;

                    SqlParameter sermonid = cmd.Parameters.Add("@sermonid", System.Data.SqlDbType.Int);
                    sermonid.Direction = System.Data.ParameterDirection.Input;
                    sermonid.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SermonSeriesDTO v = new SermonSeriesDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.SeriesId = (int)myData["series_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.SermonUrl = (string)myData["sermon_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                         v.UpdatedDateTime =  String.Format("{0:u}", v.Updated); 
                        v.Duration = (string)myData["duration"].ToString();
                        v.SermonFormat = (string)myData["sermon_format"].ToString();
                        sermonSeries.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return sermonSeries;
        }

        internal static SermonSeriesDTO GetSermonSeries(int sermonId)
        {

            SermonSeriesDTO v = new SermonSeriesDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter seriesidpar = cmd.Parameters.Add("@seriesid", System.Data.SqlDbType.Int);
                    seriesidpar.Direction = System.Data.ParameterDirection.Input;
                    seriesidpar.Value = 0;

                    SqlParameter sermonid = cmd.Parameters.Add("@sermonid", System.Data.SqlDbType.Int);
                    sermonid.Direction = System.Data.ParameterDirection.Input;
                    sermonid.Value = sermonId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.SeriesId = (int)myData["series_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.SermonUrl = (string)myData["sermon_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = (string)myData["duration"].ToString();
                        v.SermonFormat = (string)myData["sermon_format"].ToString();

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static SermonSeriesDTO CreateSermonSeries(SermonSeriesDTO sermonSeriesDTO)
        {
            DateTime utcTime = DateTime.UtcNow;

            TimeZoneInfo tzi = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
            DateTime localTime = TimeZoneInfo.ConvertTimeFromUtc(utcTime, tzi);

            SermonSeriesDTO v = new SermonSeriesDTO();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = sermonSeriesDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = sermonSeriesDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = sermonSeriesDTO.Description;

                    SqlParameter series_id = cmd.Parameters.Add("@series_id", System.Data.SqlDbType.Int);
                    series_id.Direction = System.Data.ParameterDirection.Input;
                    series_id.Value = sermonSeriesDTO.SeriesId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = sermonSeriesDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = sermonSeriesDTO.Enabled;

                    SqlParameter sermon_url = cmd.Parameters.Add("@sermon_url", System.Data.SqlDbType.VarChar, 1024);
                    sermon_url.Direction = System.Data.ParameterDirection.Input;
                    sermon_url.Value = (sermonSeriesDTO.SermonUrl == null) ? string.Empty : sermonSeriesDTO.SermonUrl;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = sermonSeriesDTO.Updated;

                    SqlParameter Created = cmd.Parameters.Add("@Created", System.Data.SqlDbType.DateTime);
                    Created.Direction = System.Data.ParameterDirection.Input;
                    Created.Value = localTime;

                    SqlParameter duration = cmd.Parameters.Add("@duration", System.Data.SqlDbType.VarChar, 20);
                    duration.Direction = System.Data.ParameterDirection.Input;
                    duration.Value = (sermonSeriesDTO.Duration == null) ? string.Empty : sermonSeriesDTO.Duration;

                    SqlParameter sermon_format = cmd.Parameters.Add("@sermon_format", System.Data.SqlDbType.VarChar, 20);
                    sermon_format.Direction = System.Data.ParameterDirection.Input;
                    sermon_format.Value = (sermonSeriesDTO.SermonFormat == null)
                        ? string.Empty
                        : sermonSeriesDTO.SermonFormat;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.SeriesId = (int)myData["series_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.SermonUrl = (string)myData["sermon_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = (string)myData["duration"].ToString();
                        v.SermonFormat = (string)myData["sermon_format"].ToString();
                        v.Created = Convert.IsDBNull(myData["Created"])
                           ? System.DateTime.Now
                           : (DateTime?)myData["Created"];
                        v.ChurchId = (int)myData["church_id"];
                        v.PushNotificationAccept = (myData["PushNotificationAccept"] == DBNull.Value) ? false : (bool)myData["PushNotificationAccept"];
                        //v.FBDeviceToken = (string)myData["FBDeviceToken"].ToString();
                        v.ChurchName = (string)myData["ChurchName"].ToString();
                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static SermonSeriesDTO EditSermonSeries(SermonSeriesDTO sermonSeriesDTO)
        {
            SermonSeriesDTO v = new SermonSeriesDTO();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = sermonSeriesDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = sermonSeriesDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = sermonSeriesDTO.Description;

                    SqlParameter series_id = cmd.Parameters.Add("@series_id", System.Data.SqlDbType.Int);
                    series_id.Direction = System.Data.ParameterDirection.Input;
                    series_id.Value = sermonSeriesDTO.SeriesId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = sermonSeriesDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = sermonSeriesDTO.Enabled;

                    SqlParameter sermon_url = cmd.Parameters.Add("@sermon_url", System.Data.SqlDbType.VarChar, 1024);
                    sermon_url.Direction = System.Data.ParameterDirection.Input;
                    sermon_url.Value = (sermonSeriesDTO.SermonUrl == null) ? string.Empty : sermonSeriesDTO.SermonUrl;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = sermonSeriesDTO.Updated;

                    SqlParameter duration = cmd.Parameters.Add("@duration", System.Data.SqlDbType.VarChar, 20);
                    duration.Direction = System.Data.ParameterDirection.Input;
                    duration.Value = (sermonSeriesDTO.Duration == null) ? string.Empty : sermonSeriesDTO.Duration;

                    SqlParameter sermon_format = cmd.Parameters.Add("@sermon_format", System.Data.SqlDbType.VarChar, 20);
                    sermon_format.Direction = System.Data.ParameterDirection.Input;
                    sermon_format.Value = (sermonSeriesDTO.SermonFormat == null)
                        ? string.Empty
                        : sermonSeriesDTO.SermonFormat;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = sermonSeriesDTO.Id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.SeriesId = (int)myData["series_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.SermonUrl = (string)myData["sermon_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = (string)myData["duration"].ToString();
                        v.SermonFormat = (string)myData["sermon_format"].ToString();

                    }

                    myData.Close();
                    myConn.Close();



                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static int DeleteSermonSeries(int sermonSeriesId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = sermonSeriesId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static SermonSeriesDTO CreateAdminSermon(SermonSeriesDTO sermonSeriesDTO)
        {
            SermonSeriesDTO v = new SermonSeriesDTO();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateAdminSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = sermonSeriesDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = sermonSeriesDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = sermonSeriesDTO.Description;

                    SqlParameter series_id = cmd.Parameters.Add("@series_id", System.Data.SqlDbType.Int);
                    series_id.Direction = System.Data.ParameterDirection.Input;
                    series_id.Value = sermonSeriesDTO.SeriesId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = sermonSeriesDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = sermonSeriesDTO.Enabled;

                    SqlParameter sermon_url = cmd.Parameters.Add("@sermon_url", System.Data.SqlDbType.VarChar, 1024);
                    sermon_url.Direction = System.Data.ParameterDirection.Input;
                    sermon_url.Value = (sermonSeriesDTO.SermonUrl == null) ? string.Empty : sermonSeriesDTO.SermonUrl;

                    SqlParameter duration = cmd.Parameters.Add("@duration", System.Data.SqlDbType.VarChar, 20);
                    duration.Direction = System.Data.ParameterDirection.Input;
                    duration.Value = (sermonSeriesDTO.Duration == null) ? string.Empty : sermonSeriesDTO.Duration;

                    SqlParameter sermon_format = cmd.Parameters.Add("@sermon_format", System.Data.SqlDbType.VarChar, 20);
                    sermon_format.Direction = System.Data.ParameterDirection.Input;
                    sermon_format.Value = (sermonSeriesDTO.SermonFormat == null)
                        ? string.Empty
                        : sermonSeriesDTO.SermonFormat;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = sermonSeriesDTO.Updated == null ? DateTime.Now : sermonSeriesDTO.Updated;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.SeriesId = (int)myData["series_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.SermonUrl = (string)myData["sermon_url"].ToString();
                        v.Duration = (string)myData["duration"].ToString();
                        v.SermonFormat = (string)myData["sermon_format"].ToString();

                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        #endregion

        #region ImageGallery

        internal static int UploadToImageGallery(UploadImageDTO uploadImageDTO)
        {

            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateImageGallery]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 64);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = uploadImageDTO.name;

                    SqlParameter author = cmd.Parameters.Add("@imageurl", System.Data.SqlDbType.VarChar, 512);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = uploadImageDTO.binary;

                    SqlParameter description = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = uploadImageDTO.churchId;

                    SqlParameter series_id = cmd.Parameters.Add("@image_category", System.Data.SqlDbType.Int);
                    series_id.Direction = System.Data.ParameterDirection.Input;
                    series_id.Value = uploadImageDTO.ImageCategoryID;

                    SqlParameter image_format = cmd.Parameters.Add("@image_format", System.Data.SqlDbType.VarChar, 20);
                    image_format.Direction = System.Data.ParameterDirection.Input;
                    image_format.Value = (uploadImageDTO.format == null) ? string.Empty : uploadImageDTO.format;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int UpdateToImageGallery(UploadImageDTO uploadImageDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateImageGallery]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 64);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = uploadImageDTO.name;

                    SqlParameter author = cmd.Parameters.Add("@imageurl", System.Data.SqlDbType.VarChar, 512);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = uploadImageDTO.binary;

                    SqlParameter description = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = uploadImageDTO.churchId;

                    SqlParameter series_id = cmd.Parameters.Add("@image_category", System.Data.SqlDbType.Int);
                    series_id.Direction = System.Data.ParameterDirection.Input;
                    series_id.Value = uploadImageDTO.ImageCategoryID;

                    SqlParameter image_format = cmd.Parameters.Add("@image_format", System.Data.SqlDbType.VarChar, 20);
                    image_format.Direction = System.Data.ParameterDirection.Input;
                    image_format.Value = (uploadImageDTO.format == null) ? string.Empty : uploadImageDTO.format;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int DeleteImageGallery(int imageId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteImageGallery]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = imageId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        #endregion

        #region Feedback

        internal static int CreateFeedback(FeedbackDTO feedbackDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[CreateFeedback]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter username = cmd.Parameters.Add("@username", System.Data.SqlDbType.VarChar, 255);
                    username.Direction = System.Data.ParameterDirection.Input;
                    username.Value = feedbackDTO.Username;

                    SqlParameter email = cmd.Parameters.Add("@email", System.Data.SqlDbType.VarChar, 100);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = feedbackDTO.Email;

                    SqlParameter comments = cmd.Parameters.Add("@comments", System.Data.SqlDbType.VarChar, 4000);
                    comments.Direction = System.Data.ParameterDirection.Input;
                    comments.Value = feedbackDTO.Comments;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = feedbackDTO.ChurchId;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static string GetChurchName(int churchId)
        {
            string churchName = string.Empty;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchNameById]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        churchName = myData["name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["name"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churchName;
        }
        #endregion

        #region  PaymentTracking

        internal static int CreatePaymentTracking(PaymentTrackingDTO paymentTrackingDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[CreateUserPaymentTracking]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter amount = cmd.Parameters.Add("@amount", System.Data.SqlDbType.Float);
                    amount.Direction = System.Data.ParameterDirection.Input;
                    amount.Value = paymentTrackingDTO.Amount;

                    SqlParameter transaction_id = cmd.Parameters.Add("@transaction_id", System.Data.SqlDbType.VarChar,
                        50);
                    transaction_id.Direction = System.Data.ParameterDirection.Input;
                    transaction_id.Value = paymentTrackingDTO.TransactionID;

                    SqlParameter transaction_type = cmd.Parameters.Add("@transaction_type_id", System.Data.SqlDbType.Int);
                    transaction_type.Direction = System.Data.ParameterDirection.Input;
                    transaction_type.Value = paymentTrackingDTO.TransactionTypeID;

                    SqlParameter transaction_type_desc = cmd.Parameters.Add("@transaction_type_desc",
                        System.Data.SqlDbType.VarChar, 50);
                    transaction_type_desc.Direction = System.Data.ParameterDirection.Input;
                    transaction_type_desc.Value = paymentTrackingDTO.TransactionTypeDesc;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = paymentTrackingDTO.ChurchId;

                    SqlParameter device_id = cmd.Parameters.Add("@device_id", System.Data.SqlDbType.VarChar, 200);
                    device_id.Direction = System.Data.ParameterDirection.Input;
                    device_id.Value = paymentTrackingDTO.DeviceId;

                    SqlParameter user_name = cmd.Parameters.Add("@user_name", System.Data.SqlDbType.VarChar, 255);
                    user_name.Direction = System.Data.ParameterDirection.Input;
                    user_name.Value = paymentTrackingDTO.UserName;

                    SqlParameter user_email = cmd.Parameters.Add("@user_email", System.Data.SqlDbType.VarChar, 50);
                    user_email.Direction = System.Data.ParameterDirection.Input;
                    user_email.Value = paymentTrackingDTO.UserEmail;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int CreatePaymentsTracking(InAppPurchaseDTO inAppPurchaseDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[CreateUsersPaymentsTracking]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter amount = cmd.Parameters.Add("@amount", System.Data.SqlDbType.Float);
                    amount.Direction = System.Data.ParameterDirection.Input;
                    amount.Value = inAppPurchaseDto.Amount;

                    SqlParameter transaction_id = cmd.Parameters.Add("@transaction_id", System.Data.SqlDbType.VarChar,
                        50);
                    transaction_id.Direction = System.Data.ParameterDirection.Input;
                    transaction_id.Value = inAppPurchaseDto.TransactionID;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = inAppPurchaseDto.ChurchId;

                    SqlParameter device_id = cmd.Parameters.Add("@device_id", System.Data.SqlDbType.VarChar, 200);
                    device_id.Direction = System.Data.ParameterDirection.Input;
                    device_id.Value = inAppPurchaseDto.DeviceId;

                    SqlParameter user_name = cmd.Parameters.Add("@user_id", System.Data.SqlDbType.VarChar, 20);
                    user_name.Direction = System.Data.ParameterDirection.Input;
                    user_name.Value = Convert.ToString(inAppPurchaseDto.UserId);

                    SqlParameter user_email = cmd.Parameters.Add("@payment_type", System.Data.SqlDbType.VarChar, 50);
                    user_email.Direction = System.Data.ParameterDirection.Input;
                    user_email.Value = inAppPurchaseDto.PaymentType;

                    SqlParameter itemId = cmd.Parameters.Add("@ItemId", System.Data.SqlDbType.Int);
                    itemId.Direction = System.Data.ParameterDirection.Input;
                    itemId.Value = inAppPurchaseDto.ItemId;

                    SqlParameter quantity = cmd.Parameters.Add("@Quantity", System.Data.SqlDbType.Int);
                    quantity.Direction = System.Data.ParameterDirection.Input;
                    quantity.Value = inAppPurchaseDto.Quantity;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        #endregion

        static void DeviceSubscriptionChanged(object sender,
            string oldSubscriptionId, string newSubscriptionId, INotification notification)
        {
            //Do something here
        }

        //this even raised when a notification is successfully sent
        static void NotificationSent(object sender, INotification notification)
        {
            //Do something here
        }

        //this is raised when a notification is failed due to some reason
        static void NotificationFailed(object sender,
            INotification notification, Exception notificationFailureException)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the channel
        static void ChannelException
            (object sender, IPushChannel channel, Exception exception)
        {
            //Do something here
        }

        //this is fired when there is exception is raised by the service
        static void ServiceException(object sender, Exception exception)
        {
            //Do something here
        }

        //this is raised when the particular device subscription is expired
        static void DeviceSubscriptionExpired(object sender,
            string expiredDeviceSubscriptionId,
            DateTime timestamp, INotification notification)
        {
            //Do something here
        }

        //this is raised when the channel is destroyed
        static void ChannelDestroyed(object sender)
        {
            //Do something here
        }

        //this is raised when the channel is created
        static void ChannelCreated(object sender, IPushChannel pushChannel)
        {
            //Do something here
        }

        internal static void AddPushNotifications(string message, int badge)
        {
            //create the puchbroker object
            var push = new PushBroker();
            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;

            try
            {


                var path = HttpContext.Current.Server.MapPath("~/iKKOSAdminPushCertificates.p12");

                var appleCert = System.IO.File.ReadAllBytes(path);
                //IMPORTANT: If you are using a Development provisioning Profile, you must use
                // the Sandbox push notification server 
                //  (so you would leave the first arg in the ctor of ApplePushChannelSettings as
                // 'false')
                //  If you are using an AdHoc or AppStore provisioning profile, you must use the 
                //Production push notification server
                //  (so you would change the first arg in the ctor of ApplePushChannelSettings to 
                //'true')

                var settings = new PushServiceSettings();
                settings.AutoScaleChannels = false;
                settings.Channels = 1;
                settings.MaxAutoScaleChannels = 1;

                push.RegisterAppleService(new ApplePushChannelSettings(true, appleCert, "123456Aa"));
                //Extension method
                //Fluent construction of an iOS notification
                //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets
                // generated within your iOS app itself when the Application Delegate
                //  for registered for remote notifications is called, 
                // and the device token is passed back to you
                push.QueueNotification(new AppleNotification()
                    .ForDeviceToken("546e9a4b9a3a135f8fad07d168523ea3f99189dd79d96682fa260d3d8e9192f3")
                    //the recipient device id
                    .WithAlert(message) //the message
                    .WithBadge(badge)
                    );

                push.StopAllServices(waitForQueuesToFinish: true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Give

        internal static List<OfferingsDTO> GetAllOfferings(int ChurchId, int Flag)
        {
            List<OfferingsDTO> Offerings = new List<OfferingsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetOfferings]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlParameter offeringsId = cmd.Parameters.Add("@offeringsId", System.Data.SqlDbType.Int);
                    offeringsId.Direction = System.Data.ParameterDirection.Input;
                    offeringsId.Value = 0;

                    SqlParameter flag = cmd.Parameters.Add("@flag", System.Data.SqlDbType.Int);
                    flag.Direction = System.Data.ParameterDirection.Input;
                    flag.Value = Flag;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        OfferingsDTO v = new OfferingsDTO();
                        v.Id = (int)myData["id"];
                        v.ChurchId = (int)myData["church_id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Description = (string)myData["description"].ToString();
                        v.Starts = Convert.ToDateTime(myData["starts"].ToString());
                        v.Ends = Convert.ToDateTime(myData["ends"].ToString());

                        Offerings.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Offerings;
        }


        internal static OfferingsDTO GetOfferings(int OfferingsId)
        {
            OfferingsDTO v = new OfferingsDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetOfferings]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = 0;

                    SqlParameter offeringsId = cmd.Parameters.Add("@offeringsId", System.Data.SqlDbType.Int);
                    offeringsId.Direction = System.Data.ParameterDirection.Input;
                    offeringsId.Value = OfferingsId;

                    SqlParameter flag = cmd.Parameters.Add("@flag", System.Data.SqlDbType.Int);
                    flag.Direction = System.Data.ParameterDirection.Input;
                    flag.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.ChurchId = (int)myData["church_id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Description = (string)myData["description"].ToString();
                        v.Starts = Convert.ToDateTime(myData["starts"].ToString());
                        v.Ends = Convert.ToDateTime(myData["ends"].ToString());


                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static int CreateOfferings(OfferingsDTO offeringsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateOfferings]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 500);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = offeringsDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 2000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = offeringsDTO.Description;

                    SqlParameter is_used = cmd.Parameters.Add("@enabled", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = offeringsDTO.Enabled;

                    SqlParameter starts = cmd.Parameters.Add("@starts", System.Data.SqlDbType.DateTime);
                    starts.Direction = System.Data.ParameterDirection.Input;
                    starts.Value = offeringsDTO.Starts;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = offeringsDTO.Ends;

                    SqlParameter churchId = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = offeringsDTO.ChurchId;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int EditOfferings(OfferingsDTO offeringsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateOfferings]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 500);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = offeringsDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 2000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = offeringsDTO.Description;

                    SqlParameter is_used = cmd.Parameters.Add("@enabled", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = offeringsDTO.Enabled;

                    SqlParameter starts = cmd.Parameters.Add("@starts", System.Data.SqlDbType.DateTime);
                    starts.Direction = System.Data.ParameterDirection.Input;
                    starts.Value = offeringsDTO.Starts;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = offeringsDTO.Ends;

                    SqlParameter churchId = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = offeringsDTO.ChurchId;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = offeringsDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeleteOfferings(int OfferingsId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteOfferings]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = OfferingsId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        #endregion

        internal static void AddUsageTracking(UsageDTO usageDTO)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UsersUsageTracking]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter featureID = cmd.Parameters.Add("@FeatureID", System.Data.SqlDbType.Int);
                    featureID.Direction = System.Data.ParameterDirection.Input;
                    featureID.Value = usageDTO.FeatureId;

                    SqlParameter count = cmd.Parameters.Add("@Count", System.Data.SqlDbType.Int);
                    count.Direction = System.Data.ParameterDirection.Input;
                    count.Value = usageDTO.Count;

                    SqlParameter value = cmd.Parameters.Add("@Value", System.Data.SqlDbType.Int);
                    value.Direction = System.Data.ParameterDirection.Input;
                    value.Value = usageDTO.Value;

                    SqlParameter user = cmd.Parameters.Add("@DeviceID", System.Data.SqlDbType.VarChar, 100);
                    user.Direction = System.Data.ParameterDirection.Input;
                    user.Value = usageDTO.DeviceId;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }


        #region ProgramSheet

        internal static int DeleteProgramSheetDetails(int ProgramSheetId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteProgramSheetDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ProgramId = cmd.Parameters.Add("@ProgramId", System.Data.SqlDbType.Int);
                    ProgramId.Direction = System.Data.ParameterDirection.InputOutput;
                    ProgramId.Value = ProgramSheetId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@ProgramId"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int CreateProgramSheet(ProgramSheetDTO programSheetDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[CreateProgramSheet]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", System.Data.SqlDbType.VarChar, 100);
                    Name.Direction = System.Data.ParameterDirection.Input;
                    Name.Value = programSheetDTO.Name;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 8000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = programSheetDTO.Description;

                    SqlParameter HappenedDate = cmd.Parameters.Add("@HappenedDate", System.Data.SqlDbType.DateTime);
                    HappenedDate.Direction = System.Data.ParameterDirection.Input;
                    HappenedDate.Value = programSheetDTO.HappenedDate;


                    SqlParameter churchId = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = programSheetDTO.ChurchId;

                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = programSheetDTO.ProgramSheetId;


                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int CreateProgramSheetDetails(ProgramSheetDetailsDTO programSheetDetailsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {

                    SqlCommand cmd = new SqlCommand("[cp].[CreateProgramSheetDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ProgramId = cmd.Parameters.Add("@ProgramId", System.Data.SqlDbType.Int);
                    ProgramId.Direction = System.Data.ParameterDirection.Input;
                    ProgramId.Value = programSheetDetailsDTO.ProgramSheetId;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 8000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = programSheetDetailsDTO.Description;

                    SqlParameter Type = cmd.Parameters.Add("@Type", System.Data.SqlDbType.NVarChar, 15);
                    Type.Direction = System.Data.ParameterDirection.Input;
                    Type.Value = programSheetDetailsDTO.Type;

                    SqlParameter lyrics = cmd.Parameters.Add("@Lyrics", System.Data.SqlDbType.VarChar, 4000);
                    lyrics.Direction = System.Data.ParameterDirection.Input;
                    lyrics.Value = programSheetDetailsDTO.Lyrics;


                    SqlParameter OrderNo = cmd.Parameters.Add("@OrderNo", System.Data.SqlDbType.Int);
                    OrderNo.Direction = System.Data.ParameterDirection.Input;
                    OrderNo.Value = programSheetDetailsDTO.OrderNo;


                    SqlParameter Id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.InputOutput;
                    Id.Value = programSheetDetailsDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static List<ProgramSheetDTO> GetProgramSheet(int ChurchId)
        {
            List<ProgramSheetDTO> ProgramSheet = new List<ProgramSheetDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetProgramSheet]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ProgramSheetDTO v = new ProgramSheetDTO();
                        v.ProgramSheetId = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        v.Description = (string)myData["Description"].ToString();
                        v.HappenedDate = (DateTime)myData["HappenedDate"];
                        v.ChurchId = ChurchId;
                        ProgramSheet.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return ProgramSheet;
        }

        internal static List<ProgramSheetDTO> GetProgramSheetPortal(int ChurchId)
        {
            List<ProgramSheetDTO> ProgramSheet = new List<ProgramSheetDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetProgramSheetPortal]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ProgramSheetDTO v = new ProgramSheetDTO();
                        v.ProgramSheetId = (int)myData["id"];
                        v.Name = (string)myData["Name"].ToString();
                        v.Description = (string)myData["Description"].ToString();
                        v.HappenedDate = (DateTime)myData["HappenedDate"];
                        v.ChurchId = ChurchId;
                        ProgramSheet.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return ProgramSheet;
        }


        internal static List<ProgramSheetDetailsDTO> GetProgramSheetDetails(int ProgramSheetId)
        {
            List<ProgramSheetDetailsDTO> ProgramSheetDetails = new List<ProgramSheetDetailsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetProgramSheetDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter programSheetId = cmd.Parameters.Add("@ProgramSheetId", System.Data.SqlDbType.Int);
                    programSheetId.Direction = System.Data.ParameterDirection.Input;
                    programSheetId.Value = ProgramSheetId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ProgramSheetDetailsDTO v = new ProgramSheetDetailsDTO();
                        v.ProgramSheetId = (int)myData["id"];
                        v.Type = (string)myData["Type"].ToString();
                        v.Description = (string)myData["Description"].ToString();
                        v.Lyrics = (string)myData["Lyrics"].ToString();
                        v.OrderNo = (int)myData["OrderNo"];
                        ProgramSheetDetails.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return ProgramSheetDetails;
        }



        internal static int DeleteProgramSheet(int programId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteProgramSheet]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = programId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int UpdateChurchEnabled(int churchId, bool enabled)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateChurchEnabled]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter enabledPar = cmd.Parameters.Add("@enabled", System.Data.SqlDbType.Bit);
                    enabledPar.Direction = System.Data.ParameterDirection.Input;
                    enabledPar.Value = enabled;

                    SqlParameter idPar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    idPar.Direction = System.Data.ParameterDirection.InputOutput;
                    idPar.Value = churchId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        } 


        #endregion


        internal static List<SermonSeriesDTO> GetAllAdminSermonSeries(int seriesid)
        {
            List<SermonSeriesDTO> sermonSeries = new List<SermonSeriesDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetAdminSermons]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter seriesidpar = cmd.Parameters.Add("@seriesid", System.Data.SqlDbType.Int);
                    seriesidpar.Direction = System.Data.ParameterDirection.Input;
                    seriesidpar.Value = seriesid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        SermonSeriesDTO v = new SermonSeriesDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.SeriesId = (int)myData["series_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.SermonUrl = (string)myData["sermon_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = (string)myData["duration"].ToString();
                        v.SermonFormat = (string)myData["sermon_format"].ToString();
                        v.Created = Convert.IsDBNull(myData["created"])
                          ? Convert.ToDateTime("1900-01-01") : (DateTime?)myData["created"];
                        sermonSeries.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return sermonSeries;
        }

        #region Admin Classes

        internal static int CreateClassesByAdmin(AdminClassDto adminClasses)
        {
            int result = 0;
            try
            {
                //Random rand = new Random();
                //var random = rand.Next(999, 10000);
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[CreateAdminClasses]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter className = cmd.Parameters.Add("@ClassName", System.Data.SqlDbType.VarChar, 50);
                    className.Direction = System.Data.ParameterDirection.Input;
                    className.Value = adminClasses.ClassName;

                    SqlParameter minAge = cmd.Parameters.Add("@MinAge", System.Data.SqlDbType.Int);
                    minAge.Direction = System.Data.ParameterDirection.Input;
                    minAge.Value = adminClasses.MinAge;

                    SqlParameter maxAge = cmd.Parameters.Add("@MaxAge", System.Data.SqlDbType.Int);
                    maxAge.Direction = System.Data.ParameterDirection.Input;
                    maxAge.Value = adminClasses.MaxAge;

                    SqlParameter location = cmd.Parameters.Add("@Location", System.Data.SqlDbType.VarChar, 50);
                    location.Direction = System.Data.ParameterDirection.Input;
                    location.Value = adminClasses.Location;

                    SqlParameter createdBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    createdBy.Direction = System.Data.ParameterDirection.Input;
                    createdBy.Value = adminClasses.CreatedBy;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = adminClasses.Id;

                    cmd.ExecuteNonQuery();

                    result = Convert.ToInt32(cmd.Parameters["@Id"].Value);

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<AdminClassDto> GetAllClassInfo(int churchId)
        {
            List<AdminClassDto> classes = new List<AdminClassDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetAllClasses]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    church.Direction = System.Data.ParameterDirection.Input;
                    church.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AdminClassDto v = new AdminClassDto();

                        v.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        v.ClassName = myData["Classname"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Classname"]);
                        v.MinAge = myData["MinAge"] == DBNull.Value
                           ? 0
                           : Convert.ToInt32(myData["MinAge"]);
                        v.MaxAge = myData["MaxAge"] == DBNull.Value
                           ? 0
                           : Convert.ToInt32(myData["MaxAge"]);
                        v.Location = myData["Location"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Location"]);
                        v.AuthToken = myData["TokenID"] == DBNull.Value ? 0 : Convert.ToInt32(myData["TokenID"]);
                        v.QrCode = myData["UniqueGUID"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["UniqueGUID"]);
                        classes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return classes;
        }

        internal static AdminAuthTokenDto GenerateClassAuthCode(int ClassId, int CreatedBy)
        {
            AdminAuthTokenDto generateCode = new AdminAuthTokenDto();

            Random random = new Random();
            var randomNumber = random.Next(1000, 9999);

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GenerateAuthCode]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter classId = cmd.Parameters.Add("@AdminClassId", System.Data.SqlDbType.Int);
                    classId.Direction = System.Data.ParameterDirection.Input;
                    classId.Value = ClassId;

                    SqlParameter createdBy = cmd.Parameters.Add("@CreatedBy", System.Data.SqlDbType.Int);
                    createdBy.Direction = System.Data.ParameterDirection.Input;
                    createdBy.Value = CreatedBy;

                    SqlParameter authCode = cmd.Parameters.Add("@AuthToken", System.Data.SqlDbType.Int);
                    authCode.Direction = System.Data.ParameterDirection.Input;
                    authCode.Value = randomNumber;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        generateCode.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        generateCode.ClassId = myData["ChurchClassId"] == DBNull.Value
                            ? 0 : Convert.ToInt32(myData["ChurchClassId"]);
                        generateCode.TokenId = myData["TokenId"] == DBNull.Value
                            ? 0
                            : Convert.ToInt32(myData["TokenId"]);
                        generateCode.ExpiryDate = myData["ExpiryDate"] == DBNull.Value
                            ? System.DateTime.Now
                            : Convert.ToDateTime(myData["ExpiryDate"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return generateCode;
        }

        internal static int ValidateClassPin(int ClassId, int Pin)
        {
            int result = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[ValidateClassPin]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter classId = cmd.Parameters.Add("@ClassId", System.Data.SqlDbType.Int);
                    classId.Direction = System.Data.ParameterDirection.Input;
                    classId.Value = ClassId;

                    SqlParameter authCode = cmd.Parameters.Add("@AuthToken", System.Data.SqlDbType.Int);
                    authCode.Direction = System.Data.ParameterDirection.Input;
                    authCode.Value = Pin;

                    SqlParameter id = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    id.Direction = System.Data.ParameterDirection.InputOutput;
                    id.Value = 0;


                    cmd.ExecuteNonQuery();

                    result = (int)cmd.Parameters["@Id"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return result;
        }


        internal static List<AdminClassDto> GetClassesInfoByChurch(int churchId)
        {
            List<AdminClassDto> classes = new List<AdminClassDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetClassesByChurchId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    church.Direction = System.Data.ParameterDirection.Input;
                    church.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AdminClassDto v = new AdminClassDto();

                        v.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        v.ClassName = myData["Classname"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Classname"]);
                        v.MinAge = myData["MinAge"] == DBNull.Value
                            ? 0
                            : Convert.ToInt32(myData["MinAge"]);
                        v.MaxAge = myData["MaxAge"] == DBNull.Value
                           ? 0
                           : Convert.ToInt32(myData["MaxAge"]);
                        v.Location = myData["Location"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Location"]);
                        v.CreatedDate = myData["CreatedDate"] == DBNull.Value
                            ? System.DateTime.Now
                            : Convert.ToDateTime(myData["CreatedDate"]);
                        v.CreatedBy = myData["CreatedBy"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CreatedBy"]);
                        classes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return classes;
        }

        internal static ParentsDto CreateParents(ParentsDto parent)
        {
            ParentsDto v = new ParentsDto();
            int result = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[UserRegistration]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter parentName = cmd.Parameters.Add("@Name", System.Data.SqlDbType.VarChar, 100);
                    parentName.Direction = System.Data.ParameterDirection.Input;
                    parentName.Value = parent.ParentName;

                    SqlParameter pwd = cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar, 50);
                    pwd.Direction = System.Data.ParameterDirection.Input;
                    pwd.Value = parent.Password;

                    SqlParameter email = cmd.Parameters.Add("@EmailAddress", System.Data.SqlDbType.VarChar, 50);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = parent.EmailAddress;

                    SqlParameter mobile = cmd.Parameters.Add("@Mobile", System.Data.SqlDbType.VarChar, 50);
                    mobile.Direction = System.Data.ParameterDirection.Input;
                    mobile.Value = parent.MobileNumber;

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    church.Direction = System.Data.ParameterDirection.Input;
                    church.Value = parent.ChurchId;

                    SqlParameter gender = cmd.Parameters.Add("@Gender", System.Data.SqlDbType.Bit);
                    gender.Direction = System.Data.ParameterDirection.Input;
                    gender.Value = parent.Gender;

                    SqlParameter country = cmd.Parameters.Add("@Country", System.Data.SqlDbType.VarChar, 50);
                    country.Direction = System.Data.ParameterDirection.Input;
                    country.Value = parent.Country;

                    SqlParameter device = cmd.Parameters.Add("@DeviceType", SqlDbType.Int);
                    device.Direction = ParameterDirection.Input;
                    device.Value = parent.DeviceType;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = parent.Id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        v.ParentName = myData["Name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Name"]);
                        v.EmailAddress = myData["EmailAddress"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["EmailAddress"]);
                        v.MobileNumber = myData["MobileNumber"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["MobileNumber"]);
                        v.CreatedDate = myData["CreatedDate"] == DBNull.Value
                            ? System.DateTime.Now
                            : Convert.ToDateTime(myData["CreatedDate"]);
                        v.ChurchId = myData["ChurchId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ChurchId"]);
                        v.Gender = myData["Gender"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Gender"]);
                        v.Country = myData["Country"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Country"]);
                        v.Password = myData["Password"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Password"]);
                        v.DeviceType = myData["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["DeviceType"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return v;
        }

        internal static int DeleteClass(int ClassId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[DeleteClasses]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = ClassId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static KidsDto CreateKids(KidsDto kid)
        {
            var kids = new KidsDto();
            try
            {
                var myConn = ConnectTODB();

                if (null != myConn)
                {
                    var cmd = new SqlCommand("[dbo].[CreateKidswithClasses]", myConn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    var kidName = cmd.Parameters.Add("@KidName", SqlDbType.VarChar, 100);
                    kidName.Direction = ParameterDirection.Input;
                    kidName.Value = kid.KidName;

                    var parentId = cmd.Parameters.Add("@ParentId", SqlDbType.Int);
                    parentId.Direction = ParameterDirection.Input;
                    parentId.Value = kid.ParentId;

                    var classId = cmd.Parameters.Add("@ClassId", SqlDbType.Int);
                    classId.Direction = ParameterDirection.Input;
                    classId.Value = kid.ClassId;

                    var kidStatus = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    kidStatus.Direction = ParameterDirection.Input;
                    kidStatus.Value = kid.Status;

                    var idpar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    idpar.Direction = ParameterDirection.InputOutput;
                    idpar.Value = kid.Id;

                    var myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        kids.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        kids.KidName = myData["KidName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["KidName"]);
                        kids.ParentId = myData["ParentId"] == DBNull.Value
                            ? 0
                            : Convert.ToInt32(myData["ParentId"]);
                        kids.ClassId = myData["ClassId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ClassId"]);
                        kids.Status = myData["Status"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Status"]);
                        kids.CreatedDate = myData["CreatedDate"] == DBNull.Value
                            ? DateTime.Now
                            : Convert.ToDateTime(myData["CreatedDate"]);
                        kids.CreatedBy = myData["CreatedBy"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CreatedBy"]);
                        kids.CategoryName = myData["ClassName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ClassName"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return kids;
        }

        //internal static List<KidsDto> GetKidsByParentId(int parentId)
        //{
        //    List<KidsDto> kids = new List<KidsDto>();

        //    try
        //    {
        //        SqlConnection myConn = ConnectTODB();

        //        if (null != myConn)
        //        {
        //            SqlCommand cmd = new SqlCommand("[dbo].[GetAllKids]", myConn);
        //            cmd.CommandType = System.Data.CommandType.StoredProcedure;

        //            SqlParameter parent = cmd.Parameters.Add("@ParentId", System.Data.SqlDbType.Int);
        //            parent.Direction = System.Data.ParameterDirection.Input;
        //            parent.Value = parentId;

        //            SqlDataReader myData = cmd.ExecuteReader();

        //            while (myData.Read())
        //            {
        //                KidsDto v = new KidsDto();
        //                v.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
        //                v.KidName = myData["Name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Name"]);
        //                v.ImageUrl = myData["ImageUrl"] == DBNull.Value
        //                    ? string.Empty
        //                    : Convert.ToString(myData["ImageUrl"]);
        //                v.CreatedDate = myData["CreatedDate"] == DBNull.Value
        //                    ? System.DateTime.Now
        //                    : Convert.ToDateTime(myData["CreatedDate"]);
        //                v.ParentId = myData["ParentId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ParentId"]);
        //                kids.Add(v);
        //            }
        //            myData.Close();
        //            myConn.Close();
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        string s = e.Message;
        //        throw;
        //    }

        //    return kids;
        //}

        internal static int UserValidation(string emailAddress)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[UserValidation]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@EmailAddress", System.Data.SqlDbType.VarChar, 50);
                    email.Direction = ParameterDirection.Input;
                    email.Value = emailAddress.Trim();

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = ParameterDirection.InputOutput;
                    Idpar.Value = 0;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static ParentsDto UserLoginValidation(string emailAddress, string password, int churchID)
        {
            ParentsDto v = new ParentsDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[UserLoginValidation]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@EmailAddress", System.Data.SqlDbType.VarChar, 50);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = emailAddress.Trim();

                    SqlParameter pwd = cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar, 50);
                    pwd.Direction = System.Data.ParameterDirection.Input;
                    pwd.Value = password.Trim();

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    church.Direction = System.Data.ParameterDirection.Input;
                    church.Value = churchID;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        if (v.Id > 0)
                        {
                            v.ParentName = myData["Name"] == DBNull.Value
                                ? string.Empty
                                : Convert.ToString(myData["Name"]);
                            v.EmailAddress = myData["EmailAddress"] == DBNull.Value
                                ? string.Empty
                                : Convert.ToString(myData["EmailAddress"]);
                            v.MobileNumber = myData["MobileNumber"] == DBNull.Value
                                ? string.Empty
                                : Convert.ToString(myData["MobileNumber"]);
                            v.CreatedDate = myData["CreatedDate"] == DBNull.Value
                                ? System.DateTime.Now
                                : Convert.ToDateTime(myData["CreatedDate"]);
                            v.ChurchId = myData["ChurchId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ChurchId"]);
                            v.Gender = myData["Gender"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Gender"]);
                            v.Country = myData["Country"] == DBNull.Value
                                ? string.Empty
                                : Convert.ToString(myData["Country"]);
                        }
                    }

                    myData.Close();
                    myConn.Close();

                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return v;
        }

        internal static SupportMailDTO GetSupportMails(string name)
        {

            SupportMailDTO s = new SupportMailDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetSupportMails]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter namePar = cmd.Parameters.Add("@Name", SqlDbType.VarChar, 500);
                    namePar.Direction = ParameterDirection.Input;
                    namePar.Value = name;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        s.EmailAddress = (string)myData["EmailAddress"];
                        s.Password = (string)myData["Password"];
                        s.Host = (string)myData["Host"];
                        s.Port = (string)myData["Port"];
                        s.SSL = (string)myData["SSL"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return s;
        }

        internal static string GetUserExists(string emailaddress)
        {
            string code = string.Empty;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetLoginUserExists]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter emailaddressPar = cmd.Parameters.Add("@emailaddress", SqlDbType.VarChar, 100);
                    emailaddressPar.Direction = ParameterDirection.Input;
                    emailaddressPar.Value = emailaddress;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        code = (string)myData["Code"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return code;
        }

        internal static string UpdateParentPassword(string userName, string token, string confirmPassword)
        {

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[UpdateParentPassword]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter Token = cmd.Parameters.Add("@Token", SqlDbType.VarChar, 500);
                    Token.Direction = ParameterDirection.Input;
                    Token.Value = token;

                    SqlParameter Emailaddress = cmd.Parameters.Add("@Emailaddress", SqlDbType.VarChar, 100);
                    Emailaddress.Direction = ParameterDirection.Input;
                    Emailaddress.Value = userName;

                    SqlParameter Password = cmd.Parameters.Add("@Password", SqlDbType.VarChar, 100);
                    Password.Direction = ParameterDirection.Input;
                    Password.Value = confirmPassword;
                    //Password.Value = UsersDalV4.Encrypt(confirmPassword);

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        userName = Convert.ToString(myData["Email"]);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return userName;
        }

        internal static int AddEmailLog(EmailDTO emailDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[AddEmailLog]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter from = cmd.Parameters.Add("@From", SqlDbType.VarChar, 1000);
                    from.Direction = ParameterDirection.Input;
                    from.Value = emailDTO.From;

                    SqlParameter to = cmd.Parameters.Add("@To", SqlDbType.VarChar, 1000);
                    to.Direction = ParameterDirection.Input;
                    to.Value = emailDTO.To;

                    SqlParameter subject = cmd.Parameters.Add("@Subject", SqlDbType.VarChar, 1000);
                    subject.Direction = ParameterDirection.Input;
                    subject.Value = emailDTO.Subject;

                    SqlParameter status = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    status.Direction = ParameterDirection.Input;
                    status.Value = emailDTO.Status;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        id = Convert.ToInt32(myData["Id"]);
                    }

                    myConn.Close();
                }
            }
            catch (Exception e)
            {

                throw;
            }

            return id;
        }

        internal static int CheckInKids(int kidId, string qrCode, int status, int deviceType)
        {
            int result = 0;
            try
            {
                Random rand = new Random();
                var random = rand.Next(999, 10000);
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    var cmd = new SqlCommand("[dbo].[ChildCheckIn]", myConn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    var kid = cmd.Parameters.Add("@KidId", SqlDbType.Int);
                    kid.Direction = ParameterDirection.Input;
                    kid.Value = kidId;

                    var code = cmd.Parameters.Add("@QrCode", SqlDbType.VarChar, 100);
                    code.Direction = ParameterDirection.Input;
                    code.Value = qrCode;

                    SqlParameter kidStatus = cmd.Parameters.Add("@Status", SqlDbType.Int);
                    kidStatus.Direction = ParameterDirection.Input;
                    kidStatus.Value = status;

                    SqlParameter deviceId = cmd.Parameters.Add("@DeviceType", SqlDbType.Int);
                    deviceId.Direction = ParameterDirection.Input;
                    deviceId.Value = deviceType;

                    SqlParameter idpar = cmd.Parameters.Add("@Id", SqlDbType.Int);
                    idpar.Direction = ParameterDirection.InputOutput;
                    idpar.Value = 0;

                    cmd.ExecuteNonQuery();

                    result = Convert.ToInt32(cmd.Parameters["@Id"].Value);

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return result;
        }

        internal static List<KidsDto> GetKidsByClass(int classId, int tokenId)
        {
            List<KidsDto> kids = new List<KidsDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetKidsByClass]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter clas = cmd.Parameters.Add("@ClassId", System.Data.SqlDbType.Int);
                    clas.Direction = System.Data.ParameterDirection.Input;
                    clas.Value = classId;

                    SqlParameter token = cmd.Parameters.Add("@tokenId", System.Data.SqlDbType.Int);
                    token.Direction = System.Data.ParameterDirection.Input;
                    token.Value = tokenId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        KidsDto v = new KidsDto
                        {
                            Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]),
                            KidName = myData["KidName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["KidName"]),
                            //ImageUrl =
                            //    myData["ImageUrl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ImageUrl"]),
                            //CreatedDate =
                            //    myData["CreatedDate"] == DBNull.Value
                            //        ? System.DateTime.Now
                            //        : Convert.ToDateTime(myData["CreatedDate"]),
                            //Age = myData["Age"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Age"]),
                            ParentName = myData["Name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Name"])
                        };
                        kids.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return kids;
        }

        internal static int UserExits(string emailAddress, string password, int churchID)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[UserExists]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter email = cmd.Parameters.Add("@EmailAddress", System.Data.SqlDbType.VarChar, 50);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = emailAddress.Trim();

                    SqlParameter pwd = cmd.Parameters.Add("@Password", System.Data.SqlDbType.VarChar, 50);
                    pwd.Direction = System.Data.ParameterDirection.Input;
                    pwd.Value = password.Trim();

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    church.Direction = System.Data.ParameterDirection.Input;
                    church.Value = churchID;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = 0;

                    cmd.ExecuteNonQuery();
                    id = Convert.ToInt32(cmd.Parameters["@Id"].Value);

                    myConn.Close();

                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return id;
        }

        internal static List<ChurchClassDto> GetClassInfoByChurch(int churchId)
        {
            List<ChurchClassDto> classes = new List<ChurchClassDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetClassByChurchId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    church.Direction = System.Data.ParameterDirection.Input;
                    church.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchClassDto v = new ChurchClassDto();

                        v.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        v.CategoryName = myData["Classname"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Classname"]);
                        v.MinAge = myData["MinAge"] == DBNull.Value
                            ? 0
                            : Convert.ToInt32(myData["MinAge"]);
                        v.MaxAge = myData["MaxAge"] == DBNull.Value
                            ? 0
                            : Convert.ToInt32(myData["MaxAge"]);
                        v.Location = myData["Location"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Location"]);
                        v.CreatedDate = myData["CreatedDate"] == DBNull.Value
                            ? System.DateTime.Now
                            : Convert.ToDateTime(myData["CreatedDate"]);
                        v.CreatedBy = myData["CreatedBy"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CreatedBy"]);
                        classes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return classes;
        }

        internal static List<KidsDto> GetKidsCheckedOut(int churchId)
        {
            List<KidsDto> kids = new List<KidsDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetCheckedOutKids]", myConn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", SqlDbType.Int);
                    church.Direction = ParameterDirection.Input;
                    church.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        KidsDto v = new KidsDto
                        {
                            Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]),
                            KidName = myData["KidName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["KidName"]),
                            ParentName = myData["Name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Name"])
                        };
                        kids.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return kids;
        }

        internal static string GetEmailByUserId(string userName)
        {
            string email = string.Empty;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetEmailAddress]", myConn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter user = cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 50);
                    user.Direction = ParameterDirection.Input;
                    user.Value = userName;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        email = Convert.ToString(myData["email"]);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return email;
        }

        internal static List<KidsDto> GetKidsByClassId(int classId, int churchId)
        {
            List<KidsDto> kids = new List<KidsDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetAllKidsByClass]", myConn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };
                    SqlParameter classesId = cmd.Parameters.Add("@ClassId", SqlDbType.Int);
                    classesId.Direction = ParameterDirection.Input;
                    classesId.Value = classId;

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", SqlDbType.Int);
                    church.Direction = ParameterDirection.Input;
                    church.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        KidsDto v = new KidsDto
                        {
                            Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]),
                            KidName = myData["KidName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["KidName"]),
                            ParentName = myData["Name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Name"])
                        };
                        kids.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return kids;
        }
        internal static ParentKidsDto GetParentEmail(int kidId)
        {
            ParentKidsDto parentKids = new ParentKidsDto();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetParentEmail]", myConn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter kid = cmd.Parameters.Add("@KidId", SqlDbType.Int);
                    kid.Direction = ParameterDirection.Input;
                    kid.Value = kidId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        parentKids.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        parentKids.EmailAddress = myData["EmailAddress"] == DBNull.Value ? string.Empty : Convert.ToString(myData["EmailAddress"]);
                        parentKids.KidName = myData["KidName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["KidName"]);
                        parentKids.DeviceType = myData["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(myData["DeviceType"]);
                        parentKids.ClassId = myData["ClassId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["ClassId"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return parentKids;
        }

        internal static List<ChurchBranchesDto> GetChurchBranchByChurch(int churchId)
        {
            List<ChurchBranchesDto> churches = new List<ChurchBranchesDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[GetChurchBranches]", myConn)
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", SqlDbType.Int);
                    church.Direction = ParameterDirection.Input;
                    church.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchBranchesDto v = new ChurchBranchesDto
                        {
                            Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]),
                            CampusName = myData["CampusName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["CampusName"]),
                            Address = myData["Address"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Address"]),
                            ChurchId = myData["church_id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["church_id"]),
                        };
                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return churches;
        }

        internal static int CreateChurchBranches(ChurchBranchesDto churchBranchesDto)
        {
            PurchaseItemsDto v = new PurchaseItemsDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[AddChurchBranches]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = churchBranchesDto.ChurchId;

                    SqlParameter campus = cmd.Parameters.Add("@CampusName", System.Data.SqlDbType.VarChar, 50);
                    campus.Direction = System.Data.ParameterDirection.Input;
                    campus.Value = churchBranchesDto.CampusName;

                    SqlParameter address = cmd.Parameters.Add("@Address", System.Data.SqlDbType.VarChar, 1024);
                    address.Direction = System.Data.ParameterDirection.Input;
                    address.Value = churchBranchesDto.Address;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int UpdateChurchBranches(ChurchBranchesDto churchBranchesDto)
        {
            PurchaseItemsDto v = new PurchaseItemsDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[EditChurchBranches]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = churchBranchesDto.ChurchId;

                    SqlParameter campus = cmd.Parameters.Add("@CampusName", System.Data.SqlDbType.VarChar, 50);
                    campus.Direction = System.Data.ParameterDirection.Input;
                    campus.Value = churchBranchesDto.CampusName;

                    SqlParameter address = cmd.Parameters.Add("@Address", System.Data.SqlDbType.VarChar, 1024);
                    address.Direction = System.Data.ParameterDirection.Input;
                    address.Value = churchBranchesDto.Address;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Input;
                    Idpar.Value = churchBranchesDto.Id;

                    SqlParameter output = cmd.Parameters.Add("@Output", System.Data.SqlDbType.Int);
                    output.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Output"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeleteChurchBranch(int branchId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[DeleteChurchBranch]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = branchId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        #endregion

        #region Music
        internal static List<AlbumsDTO> GetAllAlbums(int churchId)
        {
            List<AlbumsDTO> albums = new List<AlbumsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetAlbums]", myConn)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    SqlParameter usrid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = churchId;


                    SqlParameter sermonId = cmd.Parameters.Add("@albumId", System.Data.SqlDbType.Int);
                    sermonId.Direction = System.Data.ParameterDirection.Input;
                    sermonId.Value = 0;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        AlbumsDTO album = new AlbumsDTO();
                        album.Id = (int)myData["id"];
                        album.Name = (string)myData["name"].ToString();
                        album.ChurchId = (int)myData["church_id"];
                        album.Author = (string)myData["author"].ToString();
                        album.Enabled = (bool)myData["is_used"];
                        album.Description = (string)myData["description"].ToString();
                        album.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        album.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        album.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        album.Ends = Convert.IsDBNull(myData["Ends"]) ? System.DateTime.Now : (DateTime?)myData["Ends"];
                        album.ThumbNail = (string)myData["ThumbNail"].ToString();
                        album.ThumbNailFormat = (string)myData["ThumbNailFormat"].ToString();
                        albums.Add(album);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return albums;
        }

        internal static int CreateAlbums(AlbumsDTO albumsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateAlbums]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = albumsDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = albumsDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = albumsDTO.Description;

                    SqlParameter ThumbNail = cmd.Parameters.Add("@ThumbNail", System.Data.SqlDbType.VarChar, 2000);
                    ThumbNail.Direction = System.Data.ParameterDirection.Input;
                    ThumbNail.Value = albumsDTO.ThumbNail;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = albumsDTO.ChurchId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = albumsDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = albumsDTO.Enabled;

                    SqlParameter created = cmd.Parameters.Add("@created", System.Data.SqlDbType.DateTime);
                    created.Direction = System.Data.ParameterDirection.Input;
                    created.Value = albumsDTO.Created;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = albumsDTO.Updated;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = albumsDTO.Ends;

                    SqlParameter ThumbNailFormat = cmd.Parameters.Add("@ThumbNailFormat", System.Data.SqlDbType.VarChar,
                        20);
                    ThumbNailFormat.Direction = System.Data.ParameterDirection.Input;
                    ThumbNailFormat.Value = (albumsDTO.ThumbNailFormat == null)
                        ? string.Empty
                        : albumsDTO.ThumbNailFormat;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static AlbumsDTO GetAlbums(long albumId)
        {
            AlbumsDTO album = new AlbumsDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetAlbums]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = 0;

                    SqlParameter albumsId = cmd.Parameters.Add("@albumId", System.Data.SqlDbType.Int);
                    albumsId.Direction = System.Data.ParameterDirection.Input;
                    albumsId.Value = albumId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        album.Id = (int)myData["id"];
                        album.Name = (string)myData["name"].ToString();
                        album.ChurchId = (int)myData["church_id"];
                        album.Author = (string)myData["author"].ToString();
                        album.Enabled = (bool)myData["is_used"];
                        album.Description = (string)myData["description"].ToString();
                        album.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        album.Created = Convert.IsDBNull(myData["created"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["created"];
                        album.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        album.Ends = Convert.IsDBNull(myData["Ends"]) ? System.DateTime.Now : (DateTime?)myData["Ends"];
                        album.ThumbNail = (string)myData["ThumbNail"].ToString();
                        album.ThumbNailFormat = (string)myData["ThumbNailFormat"].ToString();
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return album;
        }

        internal static int EditAlbum(AlbumsDTO albumsDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateAlbums]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = albumsDTO.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = albumsDTO.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 512);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = albumsDTO.Description;

                    SqlParameter ThumbNail = cmd.Parameters.Add("@ThumbNail", System.Data.SqlDbType.VarChar, 2000);
                    ThumbNail.Direction = System.Data.ParameterDirection.Input;
                    ThumbNail.Value = albumsDTO.ThumbNail;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = albumsDTO.ChurchId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = albumsDTO.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = albumsDTO.Enabled;

                    SqlParameter created = cmd.Parameters.Add("@created", System.Data.SqlDbType.DateTime);
                    created.Direction = System.Data.ParameterDirection.Input;
                    created.Value = albumsDTO.Created;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = albumsDTO.Updated;

                    SqlParameter ends = cmd.Parameters.Add("@ends", System.Data.SqlDbType.DateTime);
                    ends.Direction = System.Data.ParameterDirection.Input;
                    ends.Value = albumsDTO.Ends;

                    SqlParameter ThumbNailFormat = cmd.Parameters.Add("@ThumbNailFormat", System.Data.SqlDbType.VarChar,
                        20);
                    ThumbNailFormat.Direction = System.Data.ParameterDirection.Input;
                    ThumbNailFormat.Value = (albumsDTO.ThumbNailFormat == null)
                        ? string.Empty
                        : albumsDTO.ThumbNailFormat;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = albumsDTO.Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeleteAlbum(int albumId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteAlbum]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = albumId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static List<TracksAlbumsDTO> GetAllAlbumsTracks(int albumid)
        {
            List<TracksAlbumsDTO> sermonSeries = new List<TracksAlbumsDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetTracks]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter albumidpar = cmd.Parameters.Add("@albumid", System.Data.SqlDbType.Int);
                    albumidpar.Direction = System.Data.ParameterDirection.Input;
                    albumidpar.Value = albumid;

                    SqlParameter trackidpar = cmd.Parameters.Add("@trackid", System.Data.SqlDbType.Int);
                    trackidpar.Direction = System.Data.ParameterDirection.Input;
                    trackidpar.Value = 0;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        TracksAlbumsDTO v = new TracksAlbumsDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AlbumId = (int)myData["album_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.TrackUrl = (string)myData["audio_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = (string)myData["duration"].ToString();
                        v.TrackFormat = (string)myData["audio_format"].ToString();
                        sermonSeries.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return sermonSeries;
        }

        internal static TracksAlbumsDTO GetTrackByAlbum(int albumId)
        {

            TracksAlbumsDTO v = new TracksAlbumsDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetTracks]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter seriesidpar = cmd.Parameters.Add("@seriesid", System.Data.SqlDbType.Int);
                    seriesidpar.Direction = System.Data.ParameterDirection.Input;
                    seriesidpar.Value = 0;

                    SqlParameter sermonid = cmd.Parameters.Add("@sermonid", System.Data.SqlDbType.Int);
                    sermonid.Direction = System.Data.ParameterDirection.Input;
                    sermonid.Value = albumId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AlbumId = (int)myData["album_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.TrackUrl = (string)myData["audio_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = (string)myData["duration"].ToString();
                        v.TrackFormat = (string)myData["audio_format"].ToString();

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static TracksAlbumsDTO EditTrackAlbums(TracksAlbumsDTO trackAlbumsDto)
        {
            TracksAlbumsDTO v = new TracksAlbumsDTO();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateTracks]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = trackAlbumsDto.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = trackAlbumsDto.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = trackAlbumsDto.Description;

                    SqlParameter album_id = cmd.Parameters.Add("@album_id", System.Data.SqlDbType.Int);
                    album_id.Direction = System.Data.ParameterDirection.Input;
                    album_id.Value = trackAlbumsDto.AlbumId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = trackAlbumsDto.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = trackAlbumsDto.Enabled;

                    SqlParameter track_url = cmd.Parameters.Add("@audio_url", System.Data.SqlDbType.VarChar, 1024);
                    track_url.Direction = System.Data.ParameterDirection.Input;
                    track_url.Value = (trackAlbumsDto.TrackUrl == null) ? string.Empty : trackAlbumsDto.TrackUrl;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = trackAlbumsDto.Updated;

                    SqlParameter duration = cmd.Parameters.Add("@duration", System.Data.SqlDbType.VarChar, 20);
                    duration.Direction = System.Data.ParameterDirection.Input;
                    duration.Value = (trackAlbumsDto.Duration == null) ? string.Empty : trackAlbumsDto.Duration;

                    SqlParameter track_format = cmd.Parameters.Add("@audio_format", System.Data.SqlDbType.VarChar, 20);
                    track_format.Direction = System.Data.ParameterDirection.Input;
                    track_format.Value = (trackAlbumsDto.TrackFormat == null)
                        ? string.Empty
                        : trackAlbumsDto.TrackFormat;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = trackAlbumsDto.Id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AlbumId = (int)myData["album_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.TrackUrl = (string)myData["audio_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = (string)myData["duration"].ToString();
                        v.TrackFormat = (string)myData["audio_format"].ToString();
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static int DeleteTrackLabel(int trackId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteTracks]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = trackId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static TracksAlbumsDTO GetTrackAlbums(int trackId)
        {
            TracksAlbumsDTO v = new TracksAlbumsDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetTracks]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter albumidpar = cmd.Parameters.Add("@albumid", System.Data.SqlDbType.Int);
                    albumidpar.Direction = System.Data.ParameterDirection.Input;
                    albumidpar.Value = 0;

                    SqlParameter trackid = cmd.Parameters.Add("@trackid", System.Data.SqlDbType.Int);
                    trackid.Direction = System.Data.ParameterDirection.Input;
                    trackid.Value = trackId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AlbumId = (int)myData["album_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.TrackUrl = (string)myData["audio_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = (string)myData["duration"].ToString();
                        v.TrackFormat = (string)myData["audio_format"].ToString();

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static TracksAlbumsDTO CreateTrackAlbums(TracksAlbumsDTO trackAlbumDto)
        {
            TracksAlbumsDTO v = new TracksAlbumsDTO();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateTracks]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = trackAlbumDto.Name;

                    SqlParameter author = cmd.Parameters.Add("@author", System.Data.SqlDbType.VarChar, 64);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = trackAlbumDto.Author;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = trackAlbumDto.Description;

                    SqlParameter album_id = cmd.Parameters.Add("@album_id", System.Data.SqlDbType.Int);
                    album_id.Direction = System.Data.ParameterDirection.Input;
                    album_id.Value = trackAlbumDto.AlbumId;

                    SqlParameter gallery_id = cmd.Parameters.Add("@gallery_id", System.Data.SqlDbType.Int);
                    gallery_id.Direction = System.Data.ParameterDirection.Input;
                    gallery_id.Value = trackAlbumDto.GalleryId;

                    SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    is_used.Direction = System.Data.ParameterDirection.Input;
                    is_used.Value = trackAlbumDto.Enabled;

                    SqlParameter track_url = cmd.Parameters.Add("@track_url", System.Data.SqlDbType.VarChar, 1024);
                    track_url.Direction = System.Data.ParameterDirection.Input;
                    track_url.Value = (trackAlbumDto.TrackUrl == null) ? string.Empty : trackAlbumDto.TrackUrl;

                    SqlParameter updated = cmd.Parameters.Add("@updated", System.Data.SqlDbType.DateTime);
                    updated.Direction = System.Data.ParameterDirection.Input;
                    updated.Value = trackAlbumDto.Updated;

                    SqlParameter duration = cmd.Parameters.Add("@duration", System.Data.SqlDbType.VarChar, 20);
                    duration.Direction = System.Data.ParameterDirection.Input;
                    duration.Value = (trackAlbumDto.Duration == null) ? string.Empty : trackAlbumDto.Duration;

                    SqlParameter track_format = cmd.Parameters.Add("@track_format", System.Data.SqlDbType.VarChar, 20);
                    track_format.Direction = System.Data.ParameterDirection.Input;
                    track_format.Value = (trackAlbumDto.TrackFormat == null)
                        ? string.Empty
                        : trackAlbumDto.TrackFormat;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.AlbumId = (int)myData["album_id"];
                        v.Author = (string)myData["author"].ToString();
                        v.Enabled = (bool)myData["is_used"];
                        v.Description = (string)myData["description"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.TrackUrl = (string)myData["audio_url"].ToString();
                        v.Updated = Convert.IsDBNull(myData["updated"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["updated"];
                        v.Duration = (string)myData["duration"].ToString();
                        v.TrackFormat = (string)myData["audio_format"].ToString();

                    }

                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        #endregion

        #region Church Features

        internal static List<ChurchFeaturesDto> GetFeaturesByChurchId(int churchId)
        {
            List<ChurchFeaturesDto> features = new List<ChurchFeaturesDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchFeatures]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchFeaturesDto v = new ChurchFeaturesDto();
                        v.Id = (int)myData["ID"];
                        v.FeatureName = (string)myData["Name"].ToString();
                        v.ChurchId = (int)myData["Church_Id"];
                        features.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return features;
        }

        #endregion

        #region Church Notes

        internal static List<ChurchNotesDto> GetNotesByChurchId(int churchId)
        {
            List<ChurchNotesDto> notes = new List<ChurchNotesDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchNotes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        ChurchNotesDto v = new ChurchNotesDto();
                        v.Id = (int)myData["ID"];
                        v.Notes1 = myData["Notes1"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes1"]);
                        v.Notes2 = myData["Notes2"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes2"]);
                        v.Notes3 = myData["Notes3"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes3"]);
                        v.Notes4 = myData["Notes4"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes4"]);
                        v.Notes5 = myData["Notes5"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes5"]);
                        v.ChurchId = (int)myData["Church_Id"];
                        v.CreatedDate = Convert.ToDateTime(myData["CreatedDate"]);
                        v.AuthorName = myData["Author"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Author"]);
                        v.Reference = myData["Reference"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Reference"]);
                        notes.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return notes;
        }

        internal static int CreateChurchNotes(ChurchNotesDto churchNotesDto)
        {
            ChurchNotesDto v = new ChurchNotesDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[AddChurchNotes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = churchNotesDto.ChurchId;

                    SqlParameter notes1 = cmd.Parameters.Add("@Notes1", System.Data.SqlDbType.VarChar, 512);
                    notes1.Direction = System.Data.ParameterDirection.Input;
                    notes1.Value = churchNotesDto.Notes1;

                    SqlParameter notes2 = cmd.Parameters.Add("@Notes2", System.Data.SqlDbType.VarChar, 512);
                    notes2.Direction = System.Data.ParameterDirection.Input;
                    notes2.Value = churchNotesDto.Notes2;

                    SqlParameter notes3 = cmd.Parameters.Add("@Notes3", System.Data.SqlDbType.VarChar, 512);
                    notes3.Direction = System.Data.ParameterDirection.Input;
                    notes3.Value = churchNotesDto.Notes3;

                    SqlParameter notes4 = cmd.Parameters.Add("@Notes4", System.Data.SqlDbType.VarChar, 512);
                    notes4.Direction = System.Data.ParameterDirection.Input;
                    notes4.Value = churchNotesDto.Notes4;

                    SqlParameter notes5 = cmd.Parameters.Add("@Notes5", System.Data.SqlDbType.VarChar, 512);
                    notes5.Direction = System.Data.ParameterDirection.Input;
                    notes5.Value = churchNotesDto.Notes5;

                    SqlParameter author = cmd.Parameters.Add("@Author", System.Data.SqlDbType.VarChar, 50);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = churchNotesDto.AuthorName;

                    SqlParameter reference = cmd.Parameters.Add("@Reference", System.Data.SqlDbType.VarChar, 50);
                    reference.Direction = System.Data.ParameterDirection.Input;
                    reference.Value = churchNotesDto.Reference;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int UpdateChurchNotes(ChurchNotesDto churchNotesDto)
        {
            ChurchNotesDto v = new ChurchNotesDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[EditChurchNotes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = churchNotesDto.ChurchId;

                    SqlParameter notes1 = cmd.Parameters.Add("@Notes1", System.Data.SqlDbType.VarChar, 512);
                    notes1.Direction = System.Data.ParameterDirection.Input;
                    notes1.Value = churchNotesDto.Notes1;

                    SqlParameter notes2 = cmd.Parameters.Add("@Notes2", System.Data.SqlDbType.VarChar, 512);
                    notes2.Direction = System.Data.ParameterDirection.Input;
                    notes2.Value = churchNotesDto.Notes2;

                    SqlParameter notes3 = cmd.Parameters.Add("@Notes3", System.Data.SqlDbType.VarChar, 512);
                    notes3.Direction = System.Data.ParameterDirection.Input;
                    notes3.Value = churchNotesDto.Notes3;

                    SqlParameter notes4 = cmd.Parameters.Add("@Notes4", System.Data.SqlDbType.VarChar, 512);
                    notes4.Direction = System.Data.ParameterDirection.Input;
                    notes4.Value = churchNotesDto.Notes4;

                    SqlParameter notes5 = cmd.Parameters.Add("@Notes5", System.Data.SqlDbType.VarChar, 512);
                    notes5.Direction = System.Data.ParameterDirection.Input;
                    notes5.Value = churchNotesDto.Notes5;

                    SqlParameter author = cmd.Parameters.Add("@Author", System.Data.SqlDbType.VarChar, 50);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = churchNotesDto.AuthorName;

                    SqlParameter reference = cmd.Parameters.Add("@Reference", System.Data.SqlDbType.VarChar, 50);
                    reference.Direction = System.Data.ParameterDirection.Input;
                    reference.Value = churchNotesDto.Reference;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Input;
                    Idpar.Value = churchNotesDto.Id;

                    SqlParameter output = cmd.Parameters.Add("@output", System.Data.SqlDbType.Int);
                    output.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@output"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static ChurchNotesDto GetChurchNotesByChurchId(int churchId)
        {
            ChurchNotesDto v = new ChurchNotesDto();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetNotesByChurchId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        v.Id = (int)myData["ID"];
                        v.Notes1 = myData["Notes1"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes1"]);
                        v.Notes2 = myData["Notes2"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes2"]);
                        v.Notes3 = myData["Notes3"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes3"]);
                        v.Notes4 = myData["Notes4"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes4"]);
                        v.Notes5 = myData["Notes5"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Notes5"]);
                        v.ChurchId = (int)myData["Church_Id"];
                        v.CreatedDate = Convert.ToDateTime(myData["CreatedDate"]);
                        v.AuthorName = myData["Author"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Author"]);
                        v.Reference = myData["Reference"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Reference"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static int DeleteChurchNotes(int NotesId, int ChurchId)
        {
            int result = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteChurchNotes]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = ChurchId;

                    SqlParameter notesId = cmd.Parameters.Add("@NotesId", System.Data.SqlDbType.Int);
                    notesId.Direction = System.Data.ParameterDirection.Input;
                    notesId.Value = NotesId;

                    SqlDataReader myData = cmd.ExecuteReader();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


            return result;
        }
        #endregion

        #region Custom Words

        internal static List<CustomWordOfDay> GetCustomWordsByChurchId(int churchId)
        {
            List<CustomWordOfDay> words = new List<CustomWordOfDay>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetCustomWords]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        CustomWordOfDay v = new CustomWordOfDay();
                        v.Id = (int)myData["ID"];
                        v.CustomWord = myData["CustomWord"] == DBNull.Value ? string.Empty : Convert.ToString(myData["CustomWord"]);
                        v.ChurchId = (int)myData["Church_Id"];
                        v.CustomWordType = myData["CustomWord_Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CustomWord_Id"]);
                        v.CreatedDate = Convert.ToDateTime(myData["CreatedDate"]);
                        v.AuthorName = myData["AuthorName"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["AuthorName"]);
                        v.Reference = myData["Reference"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Reference"]);
                        words.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return words;
        }

        internal static int CreateCustomWords(CustomWordOfDay customWordsDto)
        {
            ChurchNotesDto v = new ChurchNotesDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[AddCustomWords]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = customWordsDto.ChurchId;

                    SqlParameter customWord = cmd.Parameters.Add("@CustomWord", System.Data.SqlDbType.VarChar, 512);
                    customWord.Direction = System.Data.ParameterDirection.Input;
                    customWord.Value = customWordsDto.CustomWord;

                    SqlParameter customWordType = cmd.Parameters.Add("@CustomWordType", System.Data.SqlDbType.Int);
                    customWordType.Direction = System.Data.ParameterDirection.Input;
                    customWordType.Value = customWordsDto.CustomWordType;

                    SqlParameter createdDate = cmd.Parameters.Add("@CreatedDate", System.Data.SqlDbType.DateTime);
                    createdDate.Direction = System.Data.ParameterDirection.Input;
                    createdDate.Value = customWordsDto.CreatedDate;

                    SqlParameter author = cmd.Parameters.Add("@AuthorName", System.Data.SqlDbType.VarChar, 50);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = customWordsDto.AuthorName;

                    SqlParameter reference = cmd.Parameters.Add("@Reference", System.Data.SqlDbType.VarChar, 50);
                    reference.Direction = System.Data.ParameterDirection.Input;
                    reference.Value = customWordsDto.Reference;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int UpdateCustomWords(CustomWordOfDay customWordsDto)
        {
            ChurchNotesDto v = new ChurchNotesDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[EditCustomWords]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = customWordsDto.ChurchId;

                    SqlParameter customWord = cmd.Parameters.Add("@CustomWord", System.Data.SqlDbType.VarChar, 512);
                    customWord.Direction = System.Data.ParameterDirection.Input;
                    customWord.Value = customWordsDto.CustomWord;

                    SqlParameter customWordType = cmd.Parameters.Add("@CustomWordType", System.Data.SqlDbType.Int);
                    customWordType.Direction = System.Data.ParameterDirection.Input;
                    customWordType.Value = customWordsDto.CustomWordType;

                    SqlParameter createdDate = cmd.Parameters.Add("@CreatedDate", System.Data.SqlDbType.DateTime);
                    createdDate.Direction = System.Data.ParameterDirection.Input;
                    createdDate.Value = customWordsDto.CreatedDate;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Input;
                    Idpar.Value = customWordsDto.Id;

                    SqlParameter author = cmd.Parameters.Add("@AuthorName", System.Data.SqlDbType.VarChar, 50);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = customWordsDto.AuthorName;

                    SqlParameter reference = cmd.Parameters.Add("@Reference", System.Data.SqlDbType.VarChar, 50);
                    reference.Direction = System.Data.ParameterDirection.Input;
                    reference.Value = customWordsDto.Reference;

                    SqlParameter output = cmd.Parameters.Add("@output", System.Data.SqlDbType.Int);
                    output.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@output"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static CustomWordOfDay GetCustomWordsByDay(int churchId)
        {
            CustomWordOfDay words = new CustomWordOfDay();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetCustomWordsByToday]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        words.Id = (int)myData["ID"];
                        words.CustomWord = myData["CustomWord"] == DBNull.Value ? string.Empty : Convert.ToString(myData["CustomWord"]);
                        words.ChurchId = (int)myData["Church_Id"];
                        words.CustomWordType = myData["CustomWord_Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["CustomWord_Id"]);
                        words.AuthorName = myData["AuthorName"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["AuthorName"]);
                        words.Reference = myData["Reference"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Reference"]);
                        words.CreatedDate = Convert.ToDateTime(myData["CreatedDate"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return words;
        }

        public static CustomWordsDto GetGenericWordOftheDay()
        {
            CustomWordsDto v = new CustomWordsDto();
            try
            {
                SqlConnection myConn = ConnectTODB();
                //string verse = AddVerse();
                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetVerseoftheDay]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        v.Id = (int)myData["id"];
                        v.Verse = (myData["Verse"].ToString() == null)
                            ? string.Empty
                            : (string)myData["Verse"].ToString();
                        v.Author = (myData["Author"].ToString() == null)
                            ? string.Empty
                            : (string)myData["Author"].ToString();
                        v.LastModified = (DateTime)myData["LastModified"];
                        v.Verse = System.Net.WebUtility.HtmlDecode(UppercaseFirst(v.Verse));
                        v.Reference = (myData["Reference"].ToString() == null)
                            ? string.Empty
                            : (string)myData["Reference"].ToString();
                        v.Church_id = 0;
                        v.Status = 0;
                        if (v.Author != v.Reference)
                        {
                            v.Author = v.Author + " - " + v.Reference;
                        }
                    }
                    myData.Close();
                    myConn.Close();

                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }

        internal static CustomWordsDto GetCustomWordOftheDay(int churchId)
        {
            CustomWordsDto words = new CustomWordsDto();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetCustomWordsByToday]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        words.Id = (int)myData["ID"];
                        words.Verse = myData["CustomWord"] == DBNull.Value ? string.Empty : Convert.ToString(myData["CustomWord"]);
                        words.Church_id = (int)myData["Church_Id"];

                        words.Author = myData["AuthorName"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["AuthorName"]);
                        words.Reference = myData["Reference"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Reference"]);
                        words.LastModified = Convert.ToDateTime(myData["CreatedDate"]);
                        if (words.Author != words.Reference)
                        {
                            words.Author = words.Author + " - " + words.Reference;
                        }
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            if (words.Verse != null)
            {
                return words;
            }
            else
            {
                return null;
            }
        }

        internal static CustomWordsDto GetNotesOftheDay(int churchId)
        {
            CustomWordsDto words = new CustomWordsDto();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CustomNotesWords]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        words.Id = (int)myData["ID"];
                        words.Verse = myData["Word"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Word"]);
                        words.Church_id = (int)myData["Church_Id"];

                        words.Author = myData["Author"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Author"]);
                        words.Reference = myData["Reference"] == DBNull.Value
                            ? string.Empty
                            : Convert.ToString(myData["Reference"]);
                        words.LastModified = Convert.ToDateTime(myData["CreatedDate"]);
                        if (words.Author != words.Reference)
                        {
                            words.Author = words.Author + " - " + words.Reference;
                        }
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return words;
        }
        #endregion


        #region PurchasedItems

        internal static List<PurchaseItemsDto> GetItemsByChurchId(int churchId)
        {
            List<PurchaseItemsDto> items = new List<PurchaseItemsDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetItems]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        PurchaseItemsDto v = new PurchaseItemsDto();
                        v.Id = (int)myData["ID"];
                        v.ImageUrl = myData["ImageUrl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ImageUrl"]);
                        v.Title = myData["Title"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Title"]);
                        v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                        v.Amount = myData["Price"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Price"]);
                        v.Quantity = myData["Quantity"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Quantity"]);
                        v.ChurchId = (int)myData["Church_Id"];
                        v.CreatedDate = myData["CreatedDate"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                        v.UpdatedDate = myData["UpdatedDate"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["UpdatedDate"]);
                        v.CurrencySymbol = myData["CurrencySymbol"] == DBNull.Value ? string.Empty : Convert.ToString(myData["CurrencySymbol"]);
                        items.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return items;
        }

        internal static int CreatePurchaseItems(PurchaseItemsDto purchaseItemsDto)
        {
            PurchaseItemsDto v = new PurchaseItemsDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[AddChurchItems]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = purchaseItemsDto.ChurchId;

                    SqlParameter title = cmd.Parameters.Add("@Title", System.Data.SqlDbType.VarChar, 512);
                    title.Direction = System.Data.ParameterDirection.Input;
                    title.Value = purchaseItemsDto.Title;

                    SqlParameter desc = cmd.Parameters.Add("@Description", System.Data.SqlDbType.VarChar, 512);
                    desc.Direction = System.Data.ParameterDirection.Input;
                    desc.Value = purchaseItemsDto.Description;

                    SqlParameter image = cmd.Parameters.Add("@ImageUrl", System.Data.SqlDbType.VarChar, 512);
                    image.Direction = System.Data.ParameterDirection.Input;
                    image.Value = purchaseItemsDto.ImageUrl;

                    SqlParameter price = cmd.Parameters.Add("@Price", System.Data.SqlDbType.VarChar, 50);
                    price.Direction = System.Data.ParameterDirection.Input;
                    price.Value = purchaseItemsDto.Amount;

                    SqlParameter quantity = cmd.Parameters.Add("@Quantity", System.Data.SqlDbType.Int);
                    quantity.Direction = System.Data.ParameterDirection.Input;
                    quantity.Value = purchaseItemsDto.Quantity;

                    SqlParameter currency = cmd.Parameters.Add("@CurrencySymbol", System.Data.SqlDbType.VarChar, 50);
                    currency.Direction = System.Data.ParameterDirection.Input;
                    currency.Value = purchaseItemsDto.CurrencySymbol;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int DeleteItem(int itemId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteItem]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = itemId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int UpdateChurchItems(PurchaseItemsDto purchaseItemsDto)
        {
            PurchaseItemsDto v = new PurchaseItemsDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[EditChurchItems]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = purchaseItemsDto.ChurchId;

                    SqlParameter title = cmd.Parameters.Add("@Title", System.Data.SqlDbType.VarChar, 512);
                    title.Direction = System.Data.ParameterDirection.Input;
                    title.Value = purchaseItemsDto.Title;

                    SqlParameter desc = cmd.Parameters.Add("@Description", System.Data.SqlDbType.VarChar, 512);
                    desc.Direction = System.Data.ParameterDirection.Input;
                    desc.Value = purchaseItemsDto.Description;

                    SqlParameter image = cmd.Parameters.Add("@ImageUrl", System.Data.SqlDbType.VarChar, 512);
                    image.Direction = System.Data.ParameterDirection.Input;
                    image.Value = purchaseItemsDto.ImageUrl;

                    SqlParameter price = cmd.Parameters.Add("@Price", System.Data.SqlDbType.VarChar, 50);
                    price.Direction = System.Data.ParameterDirection.Input;
                    price.Value = purchaseItemsDto.Amount;

                    SqlParameter quantity = cmd.Parameters.Add("@Quantity", System.Data.SqlDbType.Int);
                    quantity.Direction = System.Data.ParameterDirection.Input;
                    quantity.Value = purchaseItemsDto.Quantity;

                    SqlParameter currency = cmd.Parameters.Add("@CurrencySymbol", System.Data.SqlDbType.VarChar, 50);
                    currency.Direction = System.Data.ParameterDirection.Input;
                    currency.Value = purchaseItemsDto.CurrencySymbol;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Input;
                    Idpar.Value = purchaseItemsDto.Id;

                    SqlParameter output = cmd.Parameters.Add("@output", System.Data.SqlDbType.Int);
                    output.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@output"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static PurchaseItemsDto GetItemsByItemId(int itemId)
        {
            PurchaseItemsDto items = new PurchaseItemsDto();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetItemsById]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter item = cmd.Parameters.Add("@itemId", System.Data.SqlDbType.Int);
                    item.Direction = System.Data.ParameterDirection.Input;
                    item.Value = itemId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {

                        items.Id = (int)myData["ID"];
                        items.ImageUrl = myData["ImageUrl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ImageUrl"]);
                        items.Title = myData["Title"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Title"]);
                        items.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                        items.Amount = myData["Price"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Price"]);
                        items.Quantity = myData["Quantity"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Quantity"]);
                        items.ChurchId = (int)myData["Church_Id"];
                        items.CreatedDate = myData["CreatedDate"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                        items.UpdatedDate = myData["UpdatedDate"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["UpdatedDate"]);
                        items.CurrencySymbol = myData["CurrencySymbol"] == DBNull.Value ? string.Empty : Convert.ToString(myData["CurrencySymbol"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return items;
        }

        internal static List<PurchaseItemsDto> GetStoreItemsByChurchId(int churchId)
        {
            List<PurchaseItemsDto> items = new List<PurchaseItemsDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetItems]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        PurchaseItemsDto v = new PurchaseItemsDto();
                        v.Id = (int)myData["ID"];
                        v.ImageUrl = myData["ImageUrl"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ImageUrl"]);
                        v.Title = myData["Title"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Title"]);
                        v.Description = myData["Description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Description"]);
                        v.Amount = myData["Price"] == DBNull.Value ? string.Empty : Convert.ToString(myData["Price"]);
                        v.Quantity = myData["Quantity"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Quantity"]);
                        v.ChurchId = (int)myData["Church_Id"];
                        v.CreatedDate = myData["CreatedDate"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["CreatedDate"]);
                        v.UpdatedDate = myData["UpdatedDate"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["UpdatedDate"]);
                        v.CurrencySymbol = myData["CurrencySymbol"] == DBNull.Value ? string.Empty : Convert.ToString(myData["CurrencySymbol"]);
                        var regions = System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.SpecificCultures).Select(x => new System.Globalization.RegionInfo(x.LCID));
                        var englishRegion = regions.FirstOrDefault(region => region.ISOCurrencySymbol.Contains(v.CurrencySymbol));
                        string symbol = string.Empty;
                        if (englishRegion != null)
                        {
                            symbol = englishRegion.CurrencySymbol;
                        }
                        v.Amount = symbol + " " + v.Amount;
                        items.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return items;
        }


        public static List<AnnouncmentDataDTO> getAnnouncementsByChurchId(int churchId)
        {

            List<AnnouncmentDataDTO> items = new List<AnnouncmentDataDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[spGetAllCustomAnnouncements]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        AnnouncmentDataDTO v = new AnnouncmentDataDTO();
                        v.Id = (int)myData["Id"];
                        v.Name = myData["name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["name"]);
                        v.AnnouncementUrl = myData["announcement_url"] == DBNull.Value ? string.Empty : Convert.ToString(myData["announcement_url"]);

                        v.Description = myData["description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["description"]);
                        v.Duration = myData["duration"] == DBNull.Value ? string.Empty : Convert.ToString(myData["duration"]);
                        v.FileTypeId = myData["fileTypeId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["fileTypeId"]);
                        v.FiletTypeName = myData["fileTypeName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["fileTypeName"]);
                        v.churchId = (int)myData["church_id"];
                        v.ScheduledDate = myData["scheduledDate"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["scheduledDate"]);
                        v.Status = myData["status"] == DBNull.Value ? 0 : Convert.ToInt32(myData["status"]);
                        v.IsUsed = Convert.ToBoolean(myData["is_used"]);
                        v.Updated = myData["updated"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["updated"]);

                        items.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return items;
        }

        public static AnnouncmentDataDTO getAnnouncementById(int id)
        {

            AnnouncmentDataDTO items = null;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[spGetCustomAnnouncementById]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchid = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = id;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read() && myData != null)
                    {
                        AnnouncmentDataDTO v = new AnnouncmentDataDTO();
                        v.Id = (int)myData["Id"];
                        v.Name = myData["name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["name"]);
                        v.AnnouncementUrl = myData["announcement_url"] == DBNull.Value ? string.Empty : Convert.ToString(myData["announcement_url"]);

                        v.Description = myData["description"] == DBNull.Value ? string.Empty : Convert.ToString(myData["description"]);
                        v.Duration = myData["duration"] == DBNull.Value ? string.Empty : Convert.ToString(myData["duration"]);
                        v.FileTypeId = myData["fileTypeId"] == DBNull.Value ? 0 : Convert.ToInt32(myData["fileTypeId"]);
                        v.FiletTypeName = myData["fileTypeName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["fileTypeName"]);
                        v.churchId = (int)myData["church_id"];
                        v.ScheduledDate = myData["scheduledDate"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["scheduledDate"]);
                        v.Status = myData["status"] == DBNull.Value ? 0 : Convert.ToInt32(myData["status"]);
                        v.IsUsed = Convert.ToBoolean(myData["is_used"]);
                        v.Updated = myData["updated"] == DBNull.Value ? System.DateTime.Now : Convert.ToDateTime(myData["updated"]);

                        items = v;
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return items;
        }
        internal static int DeleteCustomAnnouncement(int Id)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeleteAnnouncement]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = Id;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        internal static int CreateCustomAnnouncement(AnnouncmentDataDTO announcementDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[spInsertCustomAnnouncement]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = announcementDto.Name;



                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.NVarChar, 4000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = announcementDto.Description;

                    SqlParameter announcementUrl = cmd.Parameters.Add("@announcementUrl", System.Data.SqlDbType.NVarChar, 1024);
                    announcementUrl.Direction = System.Data.ParameterDirection.Input;
                    announcementUrl.Value = announcementDto.AnnouncementUrl;


                    SqlParameter church_id = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = announcementDto.churchId;

                    SqlParameter fileTypeId = cmd.Parameters.Add("@fileTypeId", System.Data.SqlDbType.Int);
                    fileTypeId.Direction = System.Data.ParameterDirection.Input;
                    fileTypeId.Value = announcementDto.FileTypeId;

                    SqlParameter duration = cmd.Parameters.Add("@duration", System.Data.SqlDbType.VarChar, 20);
                    duration.Direction = System.Data.ParameterDirection.Input;
                    duration.Value = announcementDto.Duration;

                    //SqlParameter is_used = cmd.Parameters.Add("@is_used", System.Data.SqlDbType.Bit);
                    //is_used.Direction = System.Data.ParameterDirection.Input;
                    //is_used.Value = announcementDto.IsUsed;

                    SqlParameter created = cmd.Parameters.Add("@scheduledDate", System.Data.SqlDbType.DateTime);
                    created.Direction = System.Data.ParameterDirection.Input;
                    created.Value = announcementDto.ScheduledDate;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;



                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }



        #endregion

        #region donation causes

        /// <summary>
        /// Adds a new donation cause to db
        /// </summary>
        /// <param name="eventsDTO"></param>
        /// <returns></returns>
        internal static void CreateDonationCause(ChurchDonationCause donationCause)
        {
            SqlConnection myConn = ConnectTODB();

            if (null != myConn)
            {
                SqlCommand cmd = new SqlCommand("[cp].[CreateChurchDonationCause]", myConn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter cause = cmd.Parameters.Add("@cause", System.Data.SqlDbType.VarChar, 500);
                cause.Direction = System.Data.ParameterDirection.Input;
                cause.Value = donationCause.DonationCause;

                SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                church_id.Direction = System.Data.ParameterDirection.Input;
                church_id.Value = donationCause.ChurchId;

                SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.NVarChar, 2000);
                description.Direction = System.Data.ParameterDirection.Input;
                description.Value = donationCause.Description;
                cmd.ExecuteNonQuery();

                myConn.Close();
            }
        }

        /// <summary>
        /// Updates Donation Cause information into database
        /// </summary>
        /// <param name="donationCause"></param>
        internal static void UpdateDonationCause(ChurchDonationCause donationCause)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateChurchDonationCause]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter causeId = cmd.Parameters.Add("@causeId", System.Data.SqlDbType.Int);
                    causeId.Direction = System.Data.ParameterDirection.Input;
                    causeId.Value = donationCause.Id;


                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = donationCause.ChurchId;


                    SqlParameter causeName = cmd.Parameters.Add("@cause_name", System.Data.SqlDbType.VarChar, 500);
                    causeName.Direction = System.Data.ParameterDirection.Input;
                    causeName.Value = donationCause.DonationCause;

                    SqlParameter description = cmd.Parameters.Add("@description", System.Data.SqlDbType.VarChar, 2000);
                    description.Direction = System.Data.ParameterDirection.Input;
                    description.Value = donationCause.DonationCause;

                    SqlParameter status = cmd.Parameters.Add("@status", System.Data.SqlDbType.Bit);
                    status.Direction = System.Data.ParameterDirection.Input;
                    status.Value = donationCause.DonationCause;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }


        /// <summary>
        /// Gets all donation causes of a given church
        /// </summary>
        /// <param name="churchId"></param>
        /// <returns></returns>
        internal static List<ChurchDonationCause> GetDonationCausesByChurchId(int churchId)
        {
            List<ChurchDonationCause> donationCauses = new List<ChurchDonationCause>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchDonationCauses]", myConn)
                    {
                        CommandType = System.Data.CommandType.StoredProcedure
                    };

                    SqlParameter usrid = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchDonationCause cause = new ChurchDonationCause();
                        cause.Id = (long)myData["id"];
                        cause.DonationCause = (string)myData["donation_cause"].ToString();
                        cause.ChurchId = (int)myData["church_id"];
                        cause.Description = (string)myData["description"].ToString();
                        cause.Status = (bool)myData["status"];
                        donationCauses.Add(cause);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return donationCauses;
        }

        /// <summary>
        /// Deletes church donation cause from db
        /// </summary>
        /// <param name="id"></param>
        internal static void DeleteDonationCause(long donatinCauseId)
        {
            SqlConnection myConn = ConnectTODB();

            if (null != myConn)
            {
                SqlCommand cmd = new SqlCommand("[cp].[DeleteChurchDonatinCause]", myConn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                Idpar.Value = donatinCauseId;

                cmd.ExecuteNonQuery();
                myConn.Close();
            }
        }

        #endregion donation causes



        internal static int AddTransaction(PaymentDTO paymentDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateTransactionDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter Mode = cmd.Parameters.Add("@Mode", System.Data.SqlDbType.NVarChar, 10);
                    Mode.Direction = System.Data.ParameterDirection.Input;
                    Mode.Value = paymentDTO.mode;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.NVarChar, 10);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = paymentDTO.status;

                    SqlParameter Key = cmd.Parameters.Add("@Key", System.Data.SqlDbType.NVarChar, 20);
                    Key.Direction = System.Data.ParameterDirection.Input;
                    Key.Value = paymentDTO.Key;

                    SqlParameter Txnid = cmd.Parameters.Add("@Txnid", System.Data.SqlDbType.NVarChar, 100);
                    Txnid.Direction = System.Data.ParameterDirection.Input;
                    Txnid.Value = paymentDTO.Txnid;

                    SqlParameter Amount = cmd.Parameters.Add("@Amount", System.Data.SqlDbType.NVarChar, 100);
                    Amount.Direction = System.Data.ParameterDirection.Input;
                    Amount.Value = paymentDTO.amount;

                    SqlParameter Productinfo = cmd.Parameters.Add("@Productinfo", System.Data.SqlDbType.NVarChar, 500);
                    Productinfo.Direction = System.Data.ParameterDirection.Input;
                    Productinfo.Value = paymentDTO.productinfo;

                    SqlParameter Firstname = cmd.Parameters.Add("@Firstname", System.Data.SqlDbType.NVarChar, 50);
                    Firstname.Direction = System.Data.ParameterDirection.Input;
                    Firstname.Value = paymentDTO.firstname;

                    SqlParameter Lastname = cmd.Parameters.Add("@Lastname", System.Data.SqlDbType.NVarChar, 50);
                    Lastname.Direction = System.Data.ParameterDirection.Input;
                    Lastname.Value = paymentDTO.lastname;

                    SqlParameter Address1 = cmd.Parameters.Add("@Address1", System.Data.SqlDbType.NVarChar, 100);
                    Address1.Direction = System.Data.ParameterDirection.Input;
                    Address1.Value = paymentDTO.address1;

                    SqlParameter Address2 = cmd.Parameters.Add("@Address2", System.Data.SqlDbType.NVarChar, 100);
                    Address2.Direction = System.Data.ParameterDirection.Input;
                    Address2.Value = paymentDTO.address2;

                    SqlParameter City = cmd.Parameters.Add("@City", System.Data.SqlDbType.NVarChar, 100);
                    City.Direction = System.Data.ParameterDirection.Input;
                    City.Value = paymentDTO.city;

                    SqlParameter State = cmd.Parameters.Add("@State", System.Data.SqlDbType.NVarChar, 100);
                    State.Direction = System.Data.ParameterDirection.Input;
                    State.Value = paymentDTO.state;

                    SqlParameter BANKTXNID = cmd.Parameters.Add("@BANKTXNID", System.Data.SqlDbType.NVarChar, 2000);
                    BANKTXNID.Direction = System.Data.ParameterDirection.Input;
                    BANKTXNID.Value = paymentDTO.BANKTXNID;

                    SqlParameter Country = cmd.Parameters.Add("@Country", System.Data.SqlDbType.NVarChar, 100);
                    Country.Direction = System.Data.ParameterDirection.Input;
                    Country.Value = paymentDTO.country;

                    SqlParameter Zipcode = cmd.Parameters.Add("@Zipcode", System.Data.SqlDbType.NVarChar, 20);
                    Zipcode.Direction = System.Data.ParameterDirection.Input;
                    Zipcode.Value = paymentDTO.zipcode;

                    SqlParameter Email = cmd.Parameters.Add("@Email", System.Data.SqlDbType.NVarChar, 100);
                    Email.Direction = System.Data.ParameterDirection.Input;
                    Email.Value = paymentDTO.email;

                    SqlParameter Phone = cmd.Parameters.Add("@Phone", System.Data.SqlDbType.NVarChar, 15);
                    Phone.Direction = System.Data.ParameterDirection.Input;
                    Phone.Value = paymentDTO.phone;

                    SqlParameter udf1 = cmd.Parameters.Add("@udf1", System.Data.SqlDbType.NVarChar, 100);
                    udf1.Direction = System.Data.ParameterDirection.Input;
                    udf1.Value = paymentDTO.udf1;

                    SqlParameter udf2 = cmd.Parameters.Add("@udf2", System.Data.SqlDbType.NVarChar, 100);
                    udf2.Direction = System.Data.ParameterDirection.Input;
                    udf2.Value = paymentDTO.udf2;

                    SqlParameter udf3 = cmd.Parameters.Add("@udf3", System.Data.SqlDbType.NVarChar, 100);
                    udf3.Direction = System.Data.ParameterDirection.Input;
                    udf3.Value = paymentDTO.udf3;

                    SqlParameter udf4 = cmd.Parameters.Add("@udf4", System.Data.SqlDbType.NVarChar, 100);
                    udf4.Direction = System.Data.ParameterDirection.Input;
                    udf4.Value = paymentDTO.udf4;

                    SqlParameter udf5 = cmd.Parameters.Add("@udf5", System.Data.SqlDbType.NVarChar, 100);
                    udf5.Direction = System.Data.ParameterDirection.Input;
                    udf5.Value = paymentDTO.udf5;

                    SqlParameter Hash = cmd.Parameters.Add("@Hash", System.Data.SqlDbType.NVarChar, 200);
                    Hash.Direction = System.Data.ParameterDirection.Input;
                    Hash.Value = paymentDTO.hash;

                    SqlParameter Error = cmd.Parameters.Add("@Error", System.Data.SqlDbType.NVarChar, 10);
                    Error.Direction = System.Data.ParameterDirection.Input;
                    Error.Value = paymentDTO.error;

                    SqlParameter PGTYPE = cmd.Parameters.Add("@PGTYPE", System.Data.SqlDbType.NVarChar, 20);
                    PGTYPE.Direction = System.Data.ParameterDirection.Input;
                    PGTYPE.Value = paymentDTO.PG_TYPE;

                    SqlParameter bank_ref_num = cmd.Parameters.Add("@bank_ref_num", System.Data.SqlDbType.NVarChar, 20);
                    bank_ref_num.Direction = System.Data.ParameterDirection.Input;
                    bank_ref_num.Value = paymentDTO.bank_ref_num;

                    SqlParameter payumoneyId = cmd.Parameters.Add("@payumoneyId", System.Data.SqlDbType.NVarChar, 20);
                    payumoneyId.Direction = System.Data.ParameterDirection.Input;
                    payumoneyId.Value = paymentDTO.payumoneyId;

                    SqlParameter additionalCharges = cmd.Parameters.Add("@additionalCharges", System.Data.SqlDbType.NVarChar, 200);
                    additionalCharges.Direction = System.Data.ParameterDirection.Input;
                    additionalCharges.Value = paymentDTO.additionalCharges;

                    SqlParameter Payment_Status = cmd.Parameters.Add("@Payment_Status", System.Data.SqlDbType.NVarChar, 20);
                    Payment_Status.Direction = System.Data.ParameterDirection.Input;
                    Payment_Status.Value = paymentDTO.PaymentStatus;

                    SqlParameter ChurchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.NVarChar, 10);
                    ChurchId.Direction = System.Data.ParameterDirection.Input;
                    ChurchId.Value = paymentDTO.ChurchId;

                    SqlParameter CityId = cmd.Parameters.Add("@CityId", System.Data.SqlDbType.Int);
                    CityId.Direction = System.Data.ParameterDirection.Input;
                    CityId.Value = paymentDTO.CityId;

                    SqlParameter StateId = cmd.Parameters.Add("@StateId", System.Data.SqlDbType.Int);
                    StateId.Direction = System.Data.ParameterDirection.Input;
                    StateId.Value = paymentDTO.StateId;

                    SqlParameter CountryId = cmd.Parameters.Add("@CountryId", System.Data.SqlDbType.Int);
                    CountryId.Direction = System.Data.ParameterDirection.Input;
                    CountryId.Value = paymentDTO.CountryId;

                    SqlParameter CreatedDate = cmd.Parameters.Add("@CreatedDate", System.Data.SqlDbType.DateTime);
                    CreatedDate.Direction = System.Data.ParameterDirection.Input;
                    CreatedDate.Value = paymentDTO.CreatedDate;

                    SqlParameter OrderId = cmd.Parameters.Add("@OrderId", System.Data.SqlDbType.VarChar, 1000);
                    OrderId.Direction = System.Data.ParameterDirection.Input;
                    OrderId.Value = paymentDTO.OrderId;

                    SqlParameter CheckHashSum = cmd.Parameters.Add("@CheckHashSum", System.Data.SqlDbType.VarChar, 1000);
                    CheckHashSum.Direction = System.Data.ParameterDirection.Input;
                    CheckHashSum.Value = paymentDTO.CheckHashSum;

                    SqlParameter CustId = cmd.Parameters.Add("@CustId", System.Data.SqlDbType.VarChar, 1000);
                    CustId.Direction = System.Data.ParameterDirection.Input;
                    CustId.Value = paymentDTO.CustId;

                    id = Convert.ToInt32(cmd.ExecuteScalar());


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int UpdateAboutChurch(AboutChurch aboutChurch)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateAboutChurch]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ChurchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    ChurchId.Direction = System.Data.ParameterDirection.Input;
                    ChurchId.Value = aboutChurch.ChurchId;

                    SqlParameter about = cmd.Parameters.Add("@about", System.Data.SqlDbType.VarChar, 4000);
                    about.Direction = System.Data.ParameterDirection.Input;
                    about.Value = aboutChurch.About;

                    id = Convert.ToInt32(cmd.ExecuteScalar());


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int UpdateChurchBankDetails(ChurchBankDetailsDto churchBankDetailsDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateChurchBankDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ChurchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    ChurchId.Direction = System.Data.ParameterDirection.Input;
                    ChurchId.Value = churchBankDetailsDto.ChurchId;

                    SqlParameter BankName = cmd.Parameters.Add("@BankName", System.Data.SqlDbType.VarChar, 100);
                    BankName.Direction = System.Data.ParameterDirection.Input;
                    BankName.Value = churchBankDetailsDto.BankName;

                    SqlParameter BranchName = cmd.Parameters.Add("@BranchName", System.Data.SqlDbType.VarChar, 100);
                    BranchName.Direction = System.Data.ParameterDirection.Input;
                    BranchName.Value = churchBankDetailsDto.BranchName;

                    SqlParameter AccountNo = cmd.Parameters.Add("@AccountNo", System.Data.SqlDbType.VarChar, 100);
                    AccountNo.Direction = System.Data.ParameterDirection.Input;
                    AccountNo.Value = churchBankDetailsDto.AccountNo;

                    SqlParameter IFSCCode = cmd.Parameters.Add("@IFSCCode", System.Data.SqlDbType.VarChar, 100);
                    IFSCCode.Direction = System.Data.ParameterDirection.Input;
                    IFSCCode.Value = churchBankDetailsDto.IFSC;

                    SqlParameter AccountName = cmd.Parameters.Add("@AccountName", System.Data.SqlDbType.VarChar, 100);
                    AccountName.Direction = System.Data.ParameterDirection.Input;
                    AccountName.Value = churchBankDetailsDto.AccountName;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }




        internal static List<StateDTO> GetStatesByCountryID(string CountryID)
        {
            List<StateDTO> States = new List<StateDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetStatesByCountryID]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter StateParameter = cmd.Parameters.Add("@CountryID", System.Data.SqlDbType.VarChar);
                    StateParameter.Direction = System.Data.ParameterDirection.Input;
                    StateParameter.Value = CountryID;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        StateDTO s = new StateDTO();
                        s.StateId = (int)myData["StateID"];
                        s.StateName = (string)myData["StateName"].ToString();
                        s.CountryID = (string)myData["CountryID"].ToString();

                        States.Add(s);
                    }
                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return States;
        }

        internal static List<ChurchPaymentDTO> GetChurchPaymentDetailsList(int ChurchId)
        {
            List<ChurchPaymentDTO> payments = new List<ChurchPaymentDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchPaymentDetailsList]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ChurchIdPar = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    ChurchIdPar.Direction = System.Data.ParameterDirection.Input;
                    ChurchIdPar.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchPaymentDTO s = new ChurchPaymentDTO();
                        s.Id = Convert.ToInt32(myData["id"]);
                        s.ChurchId = myData["ChurchId"].ToString();
                        s.Name = myData["Name"].ToString();
                        s.Email = myData["Email"].ToString();
                        s.TransactionId = myData["TransactionId"].ToString();
                        s.PaymentType = myData["PaymentType"].ToString();
                        s.Amount = myData["Amount"].ToString();
                        s.PaymentDate = myData["CreatedDate"].ToString();
                        s.Productinfo = myData["Productinfo"].ToString();
                        s.DonationDescription = myData["DonationDescription"] == DBNull.Value ? string.Empty : (myData["DonationDescription"].ToString());
                        s.Address = myData["Address"] == DBNull.Value ? string.Empty : (myData["Address"].ToString());
                        payments.Add(s);
                    }
                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return payments;
        }

        internal static ChurchBankDetailsDto GetChurchBankDetails(int ChurchId)
        {
            ChurchBankDetailsDto bankDet = new ChurchBankDetailsDto();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchBankDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter ChurchIdPar = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    ChurchIdPar.Direction = System.Data.ParameterDirection.Input;
                    ChurchIdPar.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        bankDet.Id = (int)myData["id"];
                        bankDet.ChurchId = Convert.ToInt32(myData["ChurchId"]);
                        bankDet.BankName = myData["BankName"].ToString();
                        bankDet.BranchName = myData["BranchName"].ToString();
                        bankDet.AccountName = myData["AccountName"].ToString();
                        bankDet.AccountNo = myData["AccountNo"].ToString();
                        bankDet.IFSC = myData["IFSCCode"].ToString();
                    }
                    myData.Close();
                    myConn.Close();
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return bankDet;
        }


        internal static void InsertPaymentLog(string input, string output, string Page, string CheckSum)
        {


            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreatePaymentLog]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter inputPar = cmd.Parameters.Add("@input", System.Data.SqlDbType.VarChar, 4000);
                    inputPar.Direction = System.Data.ParameterDirection.Input;
                    inputPar.Value = input;

                    SqlParameter outputPar = cmd.Parameters.Add("@output", System.Data.SqlDbType.VarChar, 4000);
                    outputPar.Direction = System.Data.ParameterDirection.Input;
                    outputPar.Value = output;

                    SqlParameter checkSum = cmd.Parameters.Add("@CheckSumHash", System.Data.SqlDbType.VarChar, 1000);
                    checkSum.Direction = System.Data.ParameterDirection.Input;
                    checkSum.Value = CheckSum;

                    SqlParameter PagePar = cmd.Parameters.Add("@Page", System.Data.SqlDbType.VarChar, 100);
                    PagePar.Direction = System.Data.ParameterDirection.Input;
                    PagePar.Value = Page;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }

            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }


        }


        internal static ChurchReportData GetChurchDataByChurchId(int ChurchId)
        {

            ChurchReportData Data = new ChurchReportData();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchDataByChurchId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter idpar = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    idpar.Direction = System.Data.ParameterDirection.Input;
                    idpar.Value = ChurchId;



                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        Data.UsersCount = (int)myData["NoOfUsers"];
                        Data.SermonsCount = (int)myData["NoOfSermons"];
                        Data.PaymentCount = (int)myData["NoOfPayments"];

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Data;
        }

        internal static List<ChurchSermonData> GetSermonListByChurchId(int ChurchId)
        {

            List<ChurchSermonData> Sermons = new List<ChurchSermonData>();


            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetSermonListByChurchId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter idpar = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    idpar.Direction = System.Data.ParameterDirection.Input;
                    idpar.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchSermonData SermonData = new ChurchSermonData();
                        SermonData.SeriesName = (string)myData["SeriesName"];
                        SermonData.SermonName = (string)myData["SermonName"];
                        SermonData.SermonFormat = myData["sermon_format"] == DBNull.Value ? string.Empty : (myData["sermon_format"].ToString());
                        SermonData.CreatedDate = myData["created"] == DBNull.Value ? DateTime.Now : (DateTime)myData["created"];
                        Sermons.Add(SermonData);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Sermons;
        }

        internal static List<ChurchPaymentData> GetPaymentListByChurchId(int ChurchId)
        {

            List<ChurchPaymentData> Payments = new List<ChurchPaymentData>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPaymentListByChurchId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter idpar = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    idpar.Direction = System.Data.ParameterDirection.Input;
                    idpar.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchPaymentData Payment = new ChurchPaymentData();
                        Payment.Name = (string)myData["Name"];
                        Payment.Email = (string)myData["Email"];
                        Payment.Amount = (string)myData["Amount"];
                        Payment.CreatedDate = myData["CreatedDate"] == DBNull.Value ? DateTime.Now : (DateTime)myData["CreatedDate"];
                        Payment.PaymentType = (string)myData["PaymentType"];
                        Payments.Add(Payment);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Payments;
        }

        internal static List<ChurchUsersList> GetUsersListByChurchId(int ChurchId)
        {

            List<ChurchUsersList> ListCount = new List<ChurchUsersList>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetUsersListByChurchId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter idpar = cmd.Parameters.Add("@churchid", System.Data.SqlDbType.Int);
                    idpar.Direction = System.Data.ParameterDirection.Input;
                    idpar.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchUsersList UserCount = new ChurchUsersList();

                        UserCount.Year = (int)myData["Year"];
                        UserCount.Month = (string)myData["month"];
                        UserCount.IOSCount = myData["IOSCnt"] == DBNull.Value ? 0 : Convert.ToInt32(myData["IOSCnt"]);
                        UserCount.AndroidCount = myData["AndCnt"] == DBNull.Value ? 0 : Convert.ToInt32(myData["AndCnt"]);
                        UserCount.DeviceType = (string)myData["Devicetype"];
                        ListCount.Add(UserCount);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return ListCount;
        }

        internal static int UpdateTransaction(PaymentDTO paymentDTO)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateTransactionDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter Id = cmd.Parameters.Add("@id", System.Data.SqlDbType.Int);
                    Id.Direction = System.Data.ParameterDirection.Input;
                    Id.Value = paymentDTO.Id;

                    SqlParameter Status = cmd.Parameters.Add("@Status", System.Data.SqlDbType.NVarChar, 10);
                    Status.Direction = System.Data.ParameterDirection.Input;
                    Status.Value = paymentDTO.status;


                    SqlParameter Txnid = cmd.Parameters.Add("@Txnid", System.Data.SqlDbType.NVarChar, 100);
                    Txnid.Direction = System.Data.ParameterDirection.Input;
                    Txnid.Value = paymentDTO.Txnid;

                    SqlParameter BANKTXNID = cmd.Parameters.Add("@BANKTXNID", System.Data.SqlDbType.NVarChar, 2000);
                    BANKTXNID.Direction = System.Data.ParameterDirection.Input;
                    BANKTXNID.Value = paymentDTO.BANKTXNID;

                    SqlParameter OrderId = cmd.Parameters.Add("@OrderId", System.Data.SqlDbType.VarChar, 1000);
                    OrderId.Direction = System.Data.ParameterDirection.Input;
                    OrderId.Value = paymentDTO.OrderId;

                    id = Convert.ToInt32(cmd.ExecuteScalar());


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static string GetCheckSum(string orderId)
        {
            string chkSum = string.Empty;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetCheckSum]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter orderIdPar = cmd.Parameters.Add("@orderId", System.Data.SqlDbType.VarChar, 1000);
                    orderIdPar.Direction = System.Data.ParameterDirection.Input;
                    orderIdPar.Value = orderId;


                    chkSum = cmd.ExecuteScalar().ToString();


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return chkSum;
        }

        internal static List<PlanDTO> GetCountryPlans(string Country)
        {
            List<PlanDTO> Plans = new List<PlanDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetCountryPlans]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter country = cmd.Parameters.Add("@Country", System.Data.SqlDbType.VarChar, 20);
                    country.Direction = System.Data.ParameterDirection.Input;
                    country.Value = Country;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        PlanDTO v = new PlanDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Amount = (string)myData["Amount"].ToString();

                        Plans.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return Plans;
        }

        internal static int AddContactUs(ContactUs contactUs)
        {
            int id = 0;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[AddContactUs]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter Name = cmd.Parameters.Add("@Name", System.Data.SqlDbType.VarChar, 100);
                    Name.Direction = System.Data.ParameterDirection.Input;
                    Name.Value = contactUs.Name;

                    SqlParameter Email = cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100);
                    Email.Direction = System.Data.ParameterDirection.Input;
                    Email.Value = contactUs.Email;

                    SqlParameter Subject = cmd.Parameters.Add("@Subject", System.Data.SqlDbType.VarChar, 100);
                    Subject.Direction = System.Data.ParameterDirection.Input;
                    Subject.Value = contactUs.Subject;

                    SqlParameter Message = cmd.Parameters.Add("@Message", System.Data.SqlDbType.VarChar, 4000);
                    Message.Direction = System.Data.ParameterDirection.Input;
                    Message.Value = contactUs.Message;

                    SqlParameter CountryCode = cmd.Parameters.Add("@CountryCode", System.Data.SqlDbType.VarChar, 20);
                    CountryCode.Direction = System.Data.ParameterDirection.Input;
                    CountryCode.Value = contactUs.CountryCode;

                    SqlParameter MobileNumber = cmd.Parameters.Add("@MobileNumber", System.Data.SqlDbType.VarChar, 50);
                    MobileNumber.Direction = System.Data.ParameterDirection.Input;
                    MobileNumber.Value = contactUs.MobileNumber;

                    SqlParameter IPAddress = cmd.Parameters.Add("@IPAddress", System.Data.SqlDbType.VarChar, 50);
                    IPAddress.Direction = System.Data.ParameterDirection.Input;
                    IPAddress.Value = contactUs.IPAddress;

                    id = Convert.ToInt16(cmd.ExecuteScalar());
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static PlanDTO GetChurchPlanDetails(int churchId)
        {
            PlanDTO plan = null;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchPlanDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter _churchIdParam = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.VarChar, 20);
                    _churchIdParam.Direction = System.Data.ParameterDirection.Input;
                    _churchIdParam.Value = churchId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        plan = new PlanDTO();
                        plan.Id = (int)myData["id"];
                        plan.Name = (string)myData["name"].ToString();
                        plan.Amount = (string)myData["Amount"].ToString();
                        plan.Country = (string)myData["Country"].ToString();
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return plan;
        }

        /// <summary>
        /// Adds Payment information from church to church nexus in database
        /// </summary>
        /// <param name="churchNexusChurchPaymentEntry"></param>
        internal static void AddSubscriptionInfo(SubscriptionInfo subscriptionInfo)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[AddSubscriptionInfo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = subscriptionInfo.ChurchId;

                    SqlParameter planId = cmd.Parameters.Add("@planid", System.Data.SqlDbType.Int);
                    planId.Direction = System.Data.ParameterDirection.Input;
                    planId.Value = subscriptionInfo.PlanId;

                    SqlParameter paidAmount = cmd.Parameters.Add("@paidamount", System.Data.SqlDbType.Decimal, 100);
                    paidAmount.Direction = System.Data.ParameterDirection.Input;
                    paidAmount.Value = subscriptionInfo.PaidPlanAmount;

                    SqlParameter paymentOrderId = cmd.Parameters.Add("@paymentorderid", System.Data.SqlDbType.VarChar, 4000);
                    paymentOrderId.Direction = System.Data.ParameterDirection.Input;
                    paymentOrderId.Value = subscriptionInfo.PaymentOrderId;

                    SqlParameter subscriptionMonth = cmd.Parameters.Add("@subscriptionMonth", System.Data.SqlDbType.DateTime2);
                    subscriptionMonth.Direction = System.Data.ParameterDirection.Input;
                    subscriptionMonth.Value = subscriptionInfo.SubscriptionMonth;

                    SqlParameter isPartialPayment = cmd.Parameters.Add("@isPartialPayment", System.Data.SqlDbType.Bit);
                    isPartialPayment.Direction = System.Data.ParameterDirection.Input;
                    isPartialPayment.Value = subscriptionInfo.IsPartialPayment;


                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Updates churchtochurchnexus payment info
        /// </summary>
        /// <param name="churchNexusChurchPaymentEntry"></param>
        internal static void UpdateSubscriptionInfo(SubscriptionInfo subscriptionInfo)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateSubscriptionInfo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter paymentOrderId = cmd.Parameters.Add("@paymentOrderId", System.Data.SqlDbType.VarChar, 256);
                    paymentOrderId.Direction = System.Data.ParameterDirection.Input;
                    paymentOrderId.Value = subscriptionInfo.PaymentOrderId;

                    SqlParameter paymentTransactionId = cmd.Parameters.Add("@paymentServiceTransactionId", System.Data.SqlDbType.VarChar, 256);
                    paymentTransactionId.Direction = System.Data.ParameterDirection.Input;
                    paymentTransactionId.Value = subscriptionInfo.PaymentServiceTransactionId;

                    SqlParameter paymentStatus = cmd.Parameters.Add("@paymentStatus", System.Data.SqlDbType.Bit);
                    paymentStatus.Direction = System.Data.ParameterDirection.Input;
                    paymentStatus.Value = subscriptionInfo.PaymentStatus;

                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Adds Payment information from member to church in database
        /// </summary>
        /// <param name="churchNexusChurchPaymentEntry"></param>
        internal static void AddMemberToChurchPaymentEntry(MemberToChurchPayment memberToChurchPaymentEntry)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[AddMembertoChurchPaymentsEntry]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = memberToChurchPaymentEntry.ChurchId;

                    SqlParameter donor_name = cmd.Parameters.Add("@donor_name", System.Data.SqlDbType.VarChar, 100);
                    donor_name.Direction = System.Data.ParameterDirection.Input;
                    donor_name.Value = memberToChurchPaymentEntry.DonorName;

                    SqlParameter donorEmail = cmd.Parameters.Add("@donor_email", System.Data.SqlDbType.VarChar, 100);
                    donorEmail.Direction = System.Data.ParameterDirection.Input;
                    donorEmail.Value = memberToChurchPaymentEntry.DonorEmail;

                    SqlParameter donorMobile = cmd.Parameters.Add("@donor_mobile", System.Data.SqlDbType.VarChar, 25);
                    donorMobile.Direction = System.Data.ParameterDirection.Input;
                    donorMobile.Value = memberToChurchPaymentEntry.DonorMobileNum;

                    SqlParameter donorAddress = cmd.Parameters.Add("@donor_address", System.Data.SqlDbType.VarChar, 300);
                    donorAddress.Direction = System.Data.ParameterDirection.Input;
                    donorAddress.Value = memberToChurchPaymentEntry.DonorAddress;

                    SqlParameter donation_Description = cmd.Parameters.Add("@donation_description", System.Data.SqlDbType.VarChar, 500);
                    donation_Description.Direction = System.Data.ParameterDirection.Input;
                    donation_Description.Value = memberToChurchPaymentEntry.DonationDescription;

                    SqlParameter donation_Amount = cmd.Parameters.Add("@donation_amount", System.Data.SqlDbType.BigInt);
                    donation_Amount.Direction = System.Data.ParameterDirection.Input;
                    donation_Amount.Value = memberToChurchPaymentEntry.DonationAmount;

                    SqlParameter donation_cause = cmd.Parameters.Add("@donation_cause", System.Data.SqlDbType.BigInt);
                    donation_cause.Direction = System.Data.ParameterDirection.Input;
                    donation_cause.Value = memberToChurchPaymentEntry.DonationCause;

                    SqlParameter paymentOrderId = cmd.Parameters.Add("@payment_order_id", System.Data.SqlDbType.VarChar, 255);
                    paymentOrderId.Direction = System.Data.ParameterDirection.Input;
                    paymentOrderId.Value = memberToChurchPaymentEntry.Payment_Order_id;


                    SqlParameter paymentmode = cmd.Parameters.Add("@PaymentMode", System.Data.SqlDbType.Int);
                    paymentmode.Direction = System.Data.ParameterDirection.Input;
                    paymentmode.Value = memberToChurchPaymentEntry.PaymentMode;

                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Updates membertoChurch payment info
        /// </summary>
        /// <param name="memberToChurchPaymentEntry"></param>
        internal static void UpdateMemberToChurchPaymentInfo(MemberToChurchPayment memberToChurchPaymentEntry)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateMembertoChurchPaymentsEntry]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter paymentOrderId = cmd.Parameters.Add("@payment_order_id", System.Data.SqlDbType.VarChar, 256);
                    paymentOrderId.Direction = System.Data.ParameterDirection.Input;
                    paymentOrderId.Value = memberToChurchPaymentEntry.Payment_Order_id;

                    SqlParameter paymentTransactionId = cmd.Parameters.Add("@payment_service_transactionId", System.Data.SqlDbType.VarChar, 256);
                    paymentTransactionId.Direction = System.Data.ParameterDirection.Input;
                    paymentTransactionId.Value = memberToChurchPaymentEntry.PaymentServiceTransactionId;

                    SqlParameter paymentStatus = cmd.Parameters.Add("@payment_status", System.Data.SqlDbType.Bit);
                    paymentStatus.Direction = System.Data.ParameterDirection.Input;
                    paymentStatus.Value = memberToChurchPaymentEntry.PaymentStatus;

                    cmd.ExecuteNonQuery();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Checks if a given church has valid subscription for the given month
        /// </summary>
        /// <param name="churchId"></param>
        /// <param name="subscriptionDate"></param>
        /// <returns></returns>
        internal static bool HasValidSubscription(int churchId, DateTime subscriptionDate)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[HasSubscription]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter _churchId = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    _churchId.Direction = System.Data.ParameterDirection.Input;
                    _churchId.Value = churchId;

                    SqlParameter _subscriptionDate = cmd.Parameters.Add("@date", System.Data.SqlDbType.DateTime);
                    _subscriptionDate.Direction = System.Data.ParameterDirection.Input;
                    _subscriptionDate.Value = subscriptionDate;

                    var res = Convert.ToInt32(cmd.ExecuteScalar());
                    myConn.Close();

                    return res != 0 ? true : false;
                }

                return false;
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
        }

        /// <summary>
        /// Returns all churches
        /// </summary>
        /// <param name="paymentMode">Payment Mode {1- subscriptions,2-one time}</param>
        /// <returns></returns>
        internal static List<ChurchDTO> GetChurches()
        {
            List<ChurchDTO> churches = new List<ChurchDTO>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchesByPaymentMode]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        ChurchDTO v = new ChurchDTO();
                        v.Id = (int)myData["id"];
                        v.Name = (string)myData["name"].ToString();
                        v.Description = (string)myData["description"].ToString();
                        v.Enabled = (bool)myData["enabled"];
                        v.Geomap = (string)myData["geomap"].ToString();
                        v.History = (string)myData["history"].ToString();
                        v.About = (string)myData["about"].ToString();
                        v.GalleryId = Convert.IsDBNull(myData["gallery_id"]) ? 0 : (short?)myData["gallery_id"];
                        v.LastModified = Convert.IsDBNull(myData["modified_on_utc"])
                            ? System.DateTime.Now
                            : (DateTime?)myData["modified_on_utc"];
                        v.Banners = (string)myData["banners"].ToString();
                        v.Splashscreen = (string)myData["splashscreen"].ToString();
                        v.Theme = (string)myData["theme"].ToString();
                        v.PhoneNum = (string)myData["phone_num"].ToString();
                        v.Email = (string)myData["email"].ToString();
                        v.Address = (string)myData["Address"].ToString();
                        v.ChurchAPIKey = (string)myData["church_api_key"].ToString();
                        v.Latitude = (string)myData["lat"].ToString();
                        v.Longitude = (string)myData["long"].ToString();
                        v.PlanId = (int)myData["planid"];
                        v.Username = (string)myData["username"].ToString();
                        v.Password = (string)myData["password"].ToString();
                        v.ContactPersonName = (string)myData["contactperson_name"].ToString();
                        v.ContactPersonEmail = (string)myData["contactperson_email"].ToString();
                        v.ContactPersonPhoneNum = (string)myData["contactperson_phonenum"].ToString();
                        v.BannerFormat = (string)myData["banner_format"].ToString();
                        v.SplashScreenFormat = (string)myData["splashscreen_format"].ToString();
                        v.IsPushPay = (bool)myData["isPushpay"];
                        v.IsPayPal = (bool)myData["isPayPal"];
                        v.GiveURL = myData["giveURL"].ToString();
                        v.State = (myData["State"] == DBNull.Value || myData["State"] == string.Empty)
                            ? string.Empty
                            : myData["State"].ToString();
                        v.City = (myData["City"] == DBNull.Value || myData["City"] == string.Empty)
                            ? string.Empty
                            : myData["City"].ToString();
                        v.Zip = (myData["Zip"] == DBNull.Value || myData["Zip"] == string.Empty)
                            ? string.Empty
                            : myData["Zip"].ToString();
                        v.Country = (myData["Country"] == DBNull.Value || myData["Country"] == string.Empty)
                            ? string.Empty
                            : myData["Country"].ToString();
                        v.IsContent = (myData["IsContent"] == DBNull.Value) ? false : (Boolean)myData["IsContent"];
                        v.EnableStatus = (myData["EnableStatus"] == DBNull.Value)
                            ? false
                            : (Boolean)myData["EnableStatus"];
                        v.AdminFirstName = (myData["AdminFirstName"] == DBNull.Value || myData["AdminFirstName"] == string.Empty)
                           ? string.Empty
                           : myData["AdminFirstName"].ToString();
                        v.AdminLastName = (myData["AdminLastName"] == DBNull.Value || myData["AdminLastName"] == string.Empty)
                            ? string.Empty
                            : myData["AdminLastName"].ToString();

                        v.totalPromotionDays = (myData["totalPromotionDays"] != DBNull.Value && myData["totalPromotionDays"] != null) ? (int)myData["totalPromotionDays"] : 0;
                    
                        churches.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churches;
        }

        /// <summary>
        /// Returns all churches
        /// </summary>
        /// <param name="paymentMode">Payment Mode {1- subscriptions,2-one time}</param>
        /// <returns></returns>
        internal static List<SubscriptionInfo> GetSubscriptionsByChurch(int churchId)
        {
            List<SubscriptionInfo> subscriptions = new List<SubscriptionInfo>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetSubscriptionsByChurchId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter _churchId = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    _churchId.Direction = System.Data.ParameterDirection.Input;
                    _churchId.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData != null && myData.Read())
                    {
                        SubscriptionInfo v = new SubscriptionInfo();
                        var _id = myData["id"];
                        v.Id = Convert.ToInt32(myData["id"]);
                        v.PaymentServiceTransactionId = (string)myData["paymentServiceTransactionId"].ToString();
                        v.PaymentOrderId = (string)myData["payment_order_id"].ToString();
                        v.ChurchId = Convert.ToInt32(myData["churchId"]);
                        v.PlanId = Convert.ToInt32(myData["planId"]);
                        v.PaidPlanAmount = Convert.ToDouble(myData["paidPlanAmount"]);
                        v.IsPartialPayment = (bool)myData["is_partial_payment"];
                        v.SubscriptionMonth = Convert.ToDateTime(myData["subscriptionMonth"]);
                        v.Remarks = (string)myData["remarks"].ToString();
                        v.PaymentStatus = (bool)myData["payment_status"];
                        v.PaymentDate = Convert.ToDateTime(myData["payment_date"]);
                        subscriptions.Add(v);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return subscriptions;
        }
        internal static int CreatePayment(TransactionDTO payment)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreatePaymentInfo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter tran = cmd.Parameters.Add("@TransactionId", System.Data.SqlDbType.NVarChar, 500);
                    tran.Direction = System.Data.ParameterDirection.Input;
                    tran.Value = payment.TransactionId;

                    SqlParameter status = cmd.Parameters.Add("@TransactionStatus", System.Data.SqlDbType.VarChar, 100);
                    status.Direction = System.Data.ParameterDirection.Input;
                    status.Value = payment.TransactionStatus;

                    SqlParameter amt = cmd.Parameters.Add("@TransactionAmount", System.Data.SqlDbType.VarChar, 100);
                    amt.Direction = System.Data.ParameterDirection.Input;
                    amt.Value = payment.Amount;

                    SqlParameter country = cmd.Parameters.Add("@Country", System.Data.SqlDbType.VarChar, 100);
                    country.Direction = System.Data.ParameterDirection.Input;
                    country.Value = payment.Country;

                    SqlParameter church = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    church.Direction = System.Data.ParameterDirection.Input;
                    church.Value = payment.ChurchId;

                    SqlParameter email = cmd.Parameters.Add("@Email", System.Data.SqlDbType.NVarChar, 100);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = payment.Email;

                    SqlParameter fname = cmd.Parameters.Add("@FirstName", System.Data.SqlDbType.VarChar, 100);
                    fname.Direction = System.Data.ParameterDirection.Input;
                    fname.Value = payment.FirstName;

                    SqlParameter pinfo = cmd.Parameters.Add("@ProductInfo", System.Data.SqlDbType.VarChar, 500);
                    pinfo.Direction = System.Data.ParameterDirection.Input;
                    pinfo.Value = payment.ProductInfo;

                    id = Convert.ToInt32(cmd.ExecuteScalar());


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static string GetChurchGiveUrl()
        {
            string churchGiveURL = string.Empty;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchGiveUrl]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    churchGiveURL = cmd.ExecuteScalar().ToString();


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return churchGiveURL;
        }

        internal static int CreateChurchPdfFiles(ChurchPdfFileDto churchPdfFileDto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[CreateChurchPdfFiles]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter name = cmd.Parameters.Add("@name", System.Data.SqlDbType.VarChar, 255);
                    name.Direction = System.Data.ParameterDirection.Input;
                    name.Value = churchPdfFileDto.Name;

                    SqlParameter author = cmd.Parameters.Add("@PdfFileURL", System.Data.SqlDbType.VarChar, 1024);
                    author.Direction = System.Data.ParameterDirection.Input;
                    author.Value = churchPdfFileDto.pdfFileURL;

                    SqlParameter church_id = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = churchPdfFileDto.ChurchId;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static List<ChurchPdfFileDto> GetChurchPdfFiles(int churchid)
        {
            List<ChurchPdfFileDto> lstPdf = new List<ChurchPdfFileDto>();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchPdfFiles]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = churchid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchPdfFileDto Pdf = new ChurchPdfFileDto();

                        Pdf.Id = (int)myData["id"];
                        Pdf.Name = (string)myData["name"].ToString();
                        Pdf.ChurchId = (int)myData["church_id"];
                        Pdf.pdfFileURL = (string)myData["pdf_url"].ToString();
                        lstPdf.Add(Pdf);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return lstPdf;
        }

        internal static string GetAboutChurch(int churchid)
        {
            string about = string.Empty;

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetAboutChurch]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = churchid;

                    about = Convert.ToString(cmd.ExecuteScalar());

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return about;
        }

        internal static ChurchMemberInfoDto GetChurchMembersById(int ChurchId, int memberId)
        {

            ChurchMemberInfoDto infoDto = new ChurchMemberInfoDto();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[spGetChurchMembersById]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter churchid = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;

                    SqlParameter memberIdPar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    memberIdPar.Direction = System.Data.ParameterDirection.Input;
                    memberIdPar.Value = memberId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        infoDto.Id = (int)myData["id"];
                        infoDto.churchId = (int)myData["churchId"];
                        infoDto.DeviceType = (int)myData["deviceType"];
                        infoDto.surName = myData["surName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["surName"]);
                        infoDto.name = myData["name"] == DBNull.Value ? string.Empty : Convert.ToString(myData["name"]);
                        infoDto.dateOfBirth = Convert.IsDBNull(myData["dateOfBirth"]) ? System.DateTime.Now : (DateTime)myData["dateOfBirth"];
                        infoDto.weddingDate = Convert.IsDBNull(myData["weddingDate"]) ? System.DateTime.Now : (DateTime)myData["weddingDate"];
                        infoDto.createdDate = Convert.IsDBNull(myData["createdDate"]) ? System.DateTime.Now : (DateTime)myData["createdDate"];
                        infoDto.Gender = Convert.IsDBNull(myData["Gender"]) ? 0 : (int)myData["Gender"];
                        infoDto.PhoneNumber = Convert.IsDBNull(myData["PhoneNumber"]) ? string.Empty : (string)myData["PhoneNumber"];
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return infoDto;
        }
        internal static int UpdateChurchPaymentMode(ChurchPaymentModeDTO churchpaymentdto)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateChurchPaymentMode]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter church_id = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    church_id.Direction = System.Data.ParameterDirection.Input;
                    church_id.Value = churchpaymentdto.ChurchId;

                    SqlParameter paymode = cmd.Parameters.Add("@PaymentMode", System.Data.SqlDbType.Int);
                    paymode.Direction = System.Data.ParameterDirection.Input;
                    paymode.Value = churchpaymentdto.PaymentMode;

                    SqlParameter accpayee = cmd.Parameters.Add("@AccountPayee", System.Data.SqlDbType.VarChar, 100);
                    accpayee.Direction = System.Data.ParameterDirection.Input;
                    accpayee.Value = (churchpaymentdto.AccountPayee == null) ? string.Empty : churchpaymentdto.AccountPayee;

                    SqlParameter json = cmd.Parameters.Add("@JsonResp", System.Data.SqlDbType.VarChar, 4048);
                    json.Direction = System.Data.ParameterDirection.Input;
                    json.Value = (churchpaymentdto.JsonResp == null) ? string.Empty : churchpaymentdto.JsonResp;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static ChurchPaymentModeDTO GetPaymentModebyChurchId(int churchid)
        {
            ChurchPaymentModeDTO pm = new ChurchPaymentModeDTO();

            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPaymentModeByChurchId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter usrid = cmd.Parameters.Add("@church_id", System.Data.SqlDbType.Int);
                    usrid.Direction = System.Data.ParameterDirection.Input;
                    usrid.Value = churchid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        pm.ChurchId = (int)myData["id"];
                        pm.PaymentMode = myData["PaymentMode"] == DBNull.Value ? 0 : Convert.ToInt32(myData["PaymentMode"]);
                        pm.AccountPayee = myData["AccountPayee"] == DBNull.Value ? string.Empty : Convert.ToString(myData["AccountPayee"]);
                        pm.JsonResp = myData["JsonString"] == DBNull.Value ? string.Empty : Convert.ToString(myData["JsonString"]);

                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return pm;
        }

        internal static int EliteChurchEnable()
        {
            int result = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[EnableEliteChurches]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                    
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return result;
        }

        internal static int EliteChurchDisable()
        {
            int result = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[dbo].[DisableEliteChurches]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    cmd.ExecuteNonQuery();
                   
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return result;
        }


        internal static int DeletePdf(int PdfId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeletePdfFile]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
 

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = PdfId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static int DeletePrayer(int PrayerId)
        {
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[DeletePrayerRequest]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.InputOutput;
                    Idpar.Value = PrayerId;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;


                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static FirebaseDTO GetFirebaseCredentials()
        {
            FirebaseDTO firebaseDTO = new FirebaseDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[GetFirebaseCredentials]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    { 
                        firebaseDTO.FirebaseAppId = (myData["FirebaseAPPID"] == DBNull.Value) ? "" : (string)myData["FirebaseAPPID"];
                        firebaseDTO.FirebaseSenderId = (myData["FirebaseSenderId"] == DBNull.Value) ? "" : (string)myData["FirebaseSenderId"]; 
                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {
                
            }

            return firebaseDTO;
        }

        internal static object RegisterFirebaseDevice(FirebaseDeviceDTO deviceInfo)
        {
            FirebaseDeviceDTO deviceInfoDto = null;
            try
            {
                 
                SqlConnection sqlConnection = ConnectTODB();
                SqlCommand sqlCommand = new SqlCommand("[RegisterFirebaseDeviceDetails]", sqlConnection);
                sqlCommand.CommandType = System.Data.CommandType.StoredProcedure;

                SqlParameter DevToken = sqlCommand.Parameters.Add("@FBDevicetoken", System.Data.SqlDbType.NVarChar, 1024);
                DevToken.Direction = System.Data.ParameterDirection.Input;
                DevToken.Value = deviceInfo.FBDeviceToken.Trim();

                SqlParameter deviceType = sqlCommand.Parameters.Add("@DeviceType", System.Data.SqlDbType.Int);
                deviceType.Direction = System.Data.ParameterDirection.Input;
                deviceType.Value = deviceInfo.DeviceType;

                SqlParameter Userid = sqlCommand.Parameters.Add("@UserId", System.Data.SqlDbType.Int);
                Userid.Direction = System.Data.ParameterDirection.Input;
                Userid.Value = deviceInfo.UserId;

                SqlParameter Refddevid = sqlCommand.Parameters.Add("@RefDeviceId", System.Data.SqlDbType.NVarChar, 1024);
                Refddevid.Direction = System.Data.ParameterDirection.Input;
                Refddevid.Value = deviceInfo.RefDeviceId;

                SqlParameter rectype = sqlCommand.Parameters.Add("@RecieverType", System.Data.SqlDbType.Int);
                rectype.Direction = System.Data.ParameterDirection.Input;
                rectype.Value = deviceInfo.ReceiverType;


                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    deviceInfoDto = new FirebaseDeviceDTO();
                    deviceInfoDto.Id = sqlDataReader["id"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["id"]);
                    deviceInfoDto.FBDeviceToken = sqlDataReader["FBDeviceToken"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["FBDeviceToken"]);
                    deviceInfoDto.DeviceType = sqlDataReader["DeviceType"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["DeviceType"]);
                    deviceInfoDto.CreatedDate = Convert.ToDateTime(sqlDataReader["CreatedDate"]);
                    deviceInfoDto.UserId = Convert.ToInt32(sqlDataReader["UserId"]);
                    deviceInfoDto.RefDeviceId = sqlDataReader["RefDeviceId"] == DBNull.Value ? string.Empty : Convert.ToString(sqlDataReader["RefDeviceId"]);
                    deviceInfoDto.ReceiverType = sqlDataReader["ReceiverType"] == DBNull.Value ? 0 : Convert.ToInt32(sqlDataReader["ReceiverType"]);
                }
                sqlDataReader.Close();
                sqlConnection.Close();
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }
            return deviceInfoDto;
        }

        internal static void UpdateChurchPushNotificationAccept(int churchId, bool pushNotificationAccept)
        {
            FirebaseDTO firebaseDTO = new FirebaseDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateChurchPushNotificationAccept]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter pushNotificationAcceptPar = cmd.Parameters.Add("@PushNotificationAccept", System.Data.SqlDbType.Bit);
                    pushNotificationAcceptPar.Direction = System.Data.ParameterDirection.Input;
                    pushNotificationAcceptPar.Value = pushNotificationAccept;

                    SqlParameter churchIdPar = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchIdPar.Direction = System.Data.ParameterDirection.Input;
                    churchIdPar.Value = churchId;

                     cmd.ExecuteNonQuery();   

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

           
        }

        

        internal static List<string> GetChurchFBDeviceTokens(int churchId)
        {
            List<string> fbTokens = new List<string>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchFBDeviceTokens]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter churchIdPar = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchIdPar.Direction = System.Data.ParameterDirection.Input;
                    churchIdPar.Value = churchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        
                        string token = (string)myData["FBDeviceToken"];
                        fbTokens.Add(token); 
                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return fbTokens;

        }

        internal static List<string> GetEventFBDeviceTokens(int eventId)
        {
            List<string> fbTokens = new List<string>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetEventFBDeviceToken]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter EventId = cmd.Parameters.Add("@EventId", System.Data.SqlDbType.Int);
                    EventId.Direction = System.Data.ParameterDirection.Input;
                    EventId.Value = eventId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        string token = (string)myData["FBDeviceToken"];
                        fbTokens.Add(token);
                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return fbTokens;

        }

        internal static DesignDTO UpdateTemplate(DesignDTO designDTO)
        { 
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateTemplate]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter designTemplateId = cmd.Parameters.Add("@DesignTemplateId", System.Data.SqlDbType.Int);
                    designTemplateId.Direction = System.Data.ParameterDirection.Input;
                    designTemplateId.Value = designDTO.designTemplateId;

                    SqlParameter churchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = designDTO.ChurchId;

                    cmd.ExecuteNonQuery();  

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return designDTO;

        }

        internal static DesignDTO GetTemplate(int ChurchId)
        {
            DesignDTO design = new DesignDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetTemplate]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure; 
                    
                    SqlParameter churchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        design.designTemplateId = (int)myData["DesignTemplateId"];
                        design.ChurchId = (int)myData["ChurchId"];
                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return design;

        }

        internal static ChurchPlanFeatureDTO GetPlanFeature(int ChurchId)
        {
            ChurchPlanFeatureDTO plan = new ChurchPlanFeatureDTO();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPlanFeature]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter churchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = ChurchId;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {

                        plan.planFeatureId = (int)myData["planFeatureId"];
                        plan.ChurchId = (int)myData["ChurchId"];
                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return plan;
        }

        internal static ChurchPlanFeatureDTO UpdatePlanFeature(ChurchPlanFeatureDTO planFeature)
        {
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdatePlanFeature]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter planFeatureId = cmd.Parameters.Add("@PlanFeatureId", System.Data.SqlDbType.Int);
                    planFeatureId.Direction = System.Data.ParameterDirection.Input;
                    planFeatureId.Value = planFeature.planFeatureId;

                    SqlParameter churchId = cmd.Parameters.Add("@ChurchId", System.Data.SqlDbType.Int);
                    churchId.Direction = System.Data.ParameterDirection.Input;
                    churchId.Value = planFeature.ChurchId;

                    cmd.ExecuteNonQuery();

                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return planFeature;

        }


        internal static List<PlanOptionalFeatures> GetPlansOptionalFeatures(int Planid)
        {
            List<PlanOptionalFeatures> plans = new List<PlanOptionalFeatures>();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetPlansOptionalFeatures]", myConn);
                    cmd.CommandType = CommandType.StoredProcedure;

                    SqlParameter planid = cmd.Parameters.Add("@planid", System.Data.SqlDbType.Int);
                    planid.Direction = System.Data.ParameterDirection.Input;
                    planid.Value = Planid;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        PlanOptionalFeatures plan = new PlanOptionalFeatures();
                        plan.PlanId = (int)myData["PlanId"];
                        plan.FeatureId = (int)myData["FeatureId"];
                        plan.FeatureName = (string)myData["FeatureName"];
                        plan.IsExists = (int)myData["isExists"];
                        plans.Add(plan);
                    }
                    myData.Close();


                    myConn.Close();
                }
            }
            catch (Exception ex)
            {

            }

            return plans;
        }

        internal static YoutubeDto GetChurchYTDetails(int ChurchId)
        {

            YoutubeDto yt = new YoutubeDto();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchYTDetails]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;


                    SqlParameter churchid = cmd.Parameters.Add("@churchId", System.Data.SqlDbType.Int);
                    churchid.Direction = System.Data.ParameterDirection.Input;
                    churchid.Value = ChurchId;


                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        yt.Id = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        yt.ChannelName = myData["ChannelName"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ChannelName"]);
                        yt.ChannelId = myData["ChannelId"] == DBNull.Value ? string.Empty : Convert.ToString(myData["ChannelId"]);
                        yt.ChurchId = myData["Id"] == DBNull.Value ? 0 : Convert.ToInt32(myData["Id"]);
                        yt.CreatedDate = myData["created"] == DBNull.Value ? DateTime.Now : Convert.ToDateTime(myData["created"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return yt;
        }


       
      internal static int CreateChurchSignUpInfo(ChurchSignUpInfo churchSignUpInfo)
        {
            PurchaseItemsDto v = new PurchaseItemsDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[AddChurchSignupInfo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchName = cmd.Parameters.Add("@ChurchName", System.Data.SqlDbType.VarChar, 255);
                    churchName.Direction = System.Data.ParameterDirection.Input;
                    churchName.Value = churchSignUpInfo.ChurchName;

                    SqlParameter email = cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = churchSignUpInfo.Email;

                    SqlParameter mobileNumber = cmd.Parameters.Add("@MobileNumber", System.Data.SqlDbType.VarChar, 50);
                    mobileNumber.Direction = System.Data.ParameterDirection.Input;
                    mobileNumber.Value = churchSignUpInfo.MobileNumber;

                    SqlParameter countryId = cmd.Parameters.Add("@CountryId", System.Data.SqlDbType.Int);
                    countryId.Direction = System.Data.ParameterDirection.Input;
                    countryId.Value = churchSignUpInfo.CountryId;

                    SqlParameter planId = cmd.Parameters.Add("@PlanId", System.Data.SqlDbType.Int);
                    planId.Direction = System.Data.ParameterDirection.Input;
                    planId.Value = churchSignUpInfo.PlanId;

                    SqlParameter signUpCompleted = cmd.Parameters.Add("@SignUpCompleted", System.Data.SqlDbType.Bit);
                    signUpCompleted.Direction = System.Data.ParameterDirection.Input;
                    signUpCompleted.Value = churchSignUpInfo.SignUpCompleted; 
                  

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@Id"].Value;
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }

        internal static int UpdateChurchSignUpInfo(ChurchSignUpInfo churchSignUpInfo)
        {
            PurchaseItemsDto v = new PurchaseItemsDto();
            int id = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateChurchSignupInfo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchName = cmd.Parameters.Add("@ChurchName", System.Data.SqlDbType.VarChar, 255);
                    churchName.Direction = System.Data.ParameterDirection.Input;
                    churchName.Value = churchSignUpInfo.ChurchName;

                    SqlParameter email = cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = churchSignUpInfo.Email;

                    SqlParameter mobileNumber = cmd.Parameters.Add("@MobileNumber", System.Data.SqlDbType.VarChar, 50);
                    mobileNumber.Direction = System.Data.ParameterDirection.Input;
                    mobileNumber.Value = churchSignUpInfo.MobileNumber;

                    SqlParameter countryId = cmd.Parameters.Add("@CountryId", System.Data.SqlDbType.Int);
                    countryId.Direction = System.Data.ParameterDirection.Input;
                    countryId.Value = churchSignUpInfo.CountryId;

                    SqlParameter planId = cmd.Parameters.Add("@PlanId", System.Data.SqlDbType.Int);
                    planId.Direction = System.Data.ParameterDirection.Input;
                    planId.Value = churchSignUpInfo.PlanId;

                    SqlParameter signUpCompleted = cmd.Parameters.Add("@SignUpCompleted", System.Data.SqlDbType.Bit);
                    signUpCompleted.Direction = System.Data.ParameterDirection.Input;
                    signUpCompleted.Value = churchSignUpInfo.SignUpCompleted; 

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Input;
                    Idpar.Value = churchSignUpInfo.Id;  

                    SqlParameter output = cmd.Parameters.Add("@output", System.Data.SqlDbType.Int);
                    output.Direction = System.Data.ParameterDirection.Output;

                    cmd.ExecuteNonQuery();

                    id = (int)cmd.Parameters["@output"].Value;
                    myConn.Close();
                   
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return id;
        }


        internal static ChurchSignUpInfo GetChurchSignUpInfo(int ChurchSignUpId)
        {
            ChurchSignUpInfo v = new ChurchSignUpInfo();
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchSignupInfo]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter  idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    idpar.Direction = System.Data.ParameterDirection.Input;
                    idpar.Value = ChurchSignUpId; 
                   

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        v.Id = (int)myData["id"];
                        v.ChurchName = (string)myData["ChurchName"].ToString();
                        v.Email = (string)myData["Email"];
                        v.MobileNumber = (string)myData["MobileNumber"].ToString();
                        v.CountryId = (int)myData["CountryId"];
                        v.PlanId = (int)myData["PlanId"];
                        v.SignUpCompleted = Convert.ToBoolean(myData["SignUpCompleted"]); 
                        v.CountryName = (string)myData["CountryName"].ToString();
                        v.SignUpStepId = (myData["SignUpStepId"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["SignUpStepId"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return v;
        }


        internal static int UpdateChurchSigupInfoStatus(int ChurchSignUpId , bool SignUpCompleted)
        {
             try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateChurchSingupInfoStatus]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                     
                    SqlParameter signUpCompleted = cmd.Parameters.Add("@SignUpCompleted", System.Data.SqlDbType.Bit);
                    signUpCompleted.Direction = System.Data.ParameterDirection.Input;
                    signUpCompleted.Value =  SignUpCompleted;

                    SqlParameter Idpar = cmd.Parameters.Add("@Id", System.Data.SqlDbType.Int);
                    Idpar.Direction = System.Data.ParameterDirection.Input;
                    Idpar.Value = ChurchSignUpId;
 

                    cmd.ExecuteNonQuery(); 
                  
                    myConn.Close();

                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return ChurchSignUpId;
        }


        internal static int UpdateChurchSignupStepId(int ChurchSignUpId, int StepId)
        {
           
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[UpdateChurchSignupStepId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchSignupIdPar = cmd.Parameters.Add("@ChurchSignupId", System.Data.SqlDbType.Int);
                    churchSignupIdPar.Direction = System.Data.ParameterDirection.Input;
                    churchSignupIdPar.Value = ChurchSignUpId;

                    SqlParameter stepIdPar = cmd.Parameters.Add("@StepId", System.Data.SqlDbType.Int);
                    stepIdPar.Direction = System.Data.ParameterDirection.Input;
                    stepIdPar.Value = StepId;

                    cmd.ExecuteNonQuery(); 

                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return ChurchSignUpId;
        }

        internal static int GetChurchIdByChurchSignUpId(string ChurchName,string Email)
        {
            int ChurchId = 0;
            try
            {
                SqlConnection myConn = ConnectTODB();

                if (null != myConn)
                {
                    SqlCommand cmd = new SqlCommand("[cp].[GetChurchIdByChurchSignUpId]", myConn);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;

                    SqlParameter churchname = cmd.Parameters.Add("@ChurchName", System.Data.SqlDbType.VarChar,100);
                    churchname.Direction = System.Data.ParameterDirection.Input;
                    churchname.Value = ChurchName;

                    SqlParameter email = cmd.Parameters.Add("@Email", System.Data.SqlDbType.VarChar, 100);
                    email.Direction = System.Data.ParameterDirection.Input;
                    email.Value = Email;

                    SqlDataReader myData = cmd.ExecuteReader();

                    while (myData.Read())
                    {
                        ChurchId = (myData["id"] == DBNull.Value) ? 0 : Convert.ToInt32(myData["id"]);
                    }
                    myData.Close();
                    myConn.Close();
                }
            }
            catch (Exception e)
            {
                string s = e.Message;
                throw;
            }

            return ChurchId;
        }
    }


}