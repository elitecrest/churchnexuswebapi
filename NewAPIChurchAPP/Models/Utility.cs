﻿using System;

namespace NewAPIChurchAPP.Models
{
    public class Utility
    {
        public class CustomResponse
        {
            public CustomResponseStatus Status;
            public Object Response;
            public string Message;
        }

        public enum CustomResponseStatus
        {
            Successful,
            UnSuccessful,
            Exception
        }


        public static class PaytmConstants
        {
            //Staging
            //public static string MID = "EliteC91485049938595";
            //public static string MERCHANT_KEY = "qhdJFT01zO1k1Hbh";
            //public static string INDUSTRY_TYPE_ID = "Retail";
            //public static string CHANNEL_ID = "WAP";
            //public static string WEBSITE = "APP_STAGING";


            //Production
            public static string MID = "EliCre21316159415223";
            public static string MERCHANT_KEY = "6&z&vYCUlx2GNI0F";
            public static string INDUSTRY_TYPE_ID = "Retail109";
            public static string CHANNEL_ID = "WAP";
            public static string WEBSITE = "EliCreWAP";

        }

    }
    public static class CustomConstants
    {

        public static string UserDoesNotExist = "User does not exist";
        public static string UserExistWithEmail = "User already exist";
        public static string InValidCredential = "InValid credential";
        public static string RegisteredSuccessfully = "Registered successfully";
        public static string PaymentIsDoneSuccessfully = "Payment is processed successfully";
        public static string VideoAddedSuccessfully = "Video added succcessfully";
        public static string VideoUpdatedSuccessfully = "Video updated Succcessfully";
        public static string AccessDenied = "Error in retrieving data";
        public static string SeriesDeletionFailureMsg = "Unable to delete. Please make sure Series is empty!";
        public static string VideoDoesNotExists = "Video does not exist";
        public static string VideoDeletedSuccessfully = "Video deleted succcessfully";
        public static string VideoRatingAddedSuccessfully = "Video rating succcessfully added";
        public static string DetailsGetSuccessfully = "Data retrieved successfully";
        public static string NoChurchMembersAvailableMsg = "This church don't have registered members yet!";
        public static string UpdateUserUpdateSuccessfully = "User update successfully";
        public static string VisibilityUpdateSuccessfully = "Video visibility updated successfully";
        public static string VisibilityUpdateFail = "Failed to update video visibility";

        public static string ImageUploadSuccessfully = "Image uploaded successfully";
        public static string ImageUpdateSuccessfully = "Image updated successfully";
        public static string ImageDeleteSuccessfully = "Image deleted successfully";
        public static string ImagesMaximumNumberExceeds = "Maximum number of images exceeded";

        public static string TestimonialAddedSuccessfully = "Testimonial added successfully";
        public static string TestimonialApprovedSuccessfully = "Testimonial approved successfully";
        public static string TestimonialRejectedSuccessfully = "Testimonial rejected";
        public static string TestimonialPublishedSuccessfully = "Testimonial published successfully";
        public static string TestimonialDeletedSuccessfully = "Testimonial deleted successfully";
        public static string TestimonialUpdatedSuccessfully = "Testimonial updated successfully";

        public static string SermonAddedSuccessfully = "Sermon added successfully";
        public static string SermonUpdatedSuccessfully = "Sermon updated successfully";
        public static string SermonDeletedSuccessfully = "Sermon deleted successfully";


        public static string AnnouncementAddedSuccessfully = "Announcement added successfully";
        public static string AnnouncementUpdatedSuccessfully = "Announcement updated successfully";
        public static string AnnouncementDeletedSuccessfully = "Announcement deleted successfully";

        public static string ChurchAddedSuccessfully = "Church added successfully";
        public static string ChurchUpdatedSuccessfully = "Church updated successfully";
        public static string ChurchDeletedSuccessfully = "Church deleted successfully";

        public static string AnnouncementsAddedSuccessfully = "Announcements added successfully";
        public static string AnnouncementsUpdatedSuccessfully = "Announcements updated successfully";
        public static string AnnouncementsDeletedSuccessfully = "Announcements deleted successfully";

        public static string EventsAddedSuccessfully = "Event added successfully";
        public static string EventsUpdatedSuccessfully = "Event updated successfully";
        public static string EventsDeletedSuccessfully = "Event deleted successfully";

        public static string HaveDecidedAddedSuccessfully = "IHaveDecided details added successfully";

        public static string PlanAddedSuccessfully = "Plan added successfully";
        public static string PlanUpdatedSuccessfully = "Plan updated successfully";
        public static string PlanDeletedSuccessfully = "Plan deleted successfully";

        public static string PlanFeaturesAddedSuccessfully = "PlanFeatures added successfully";
        public static string PlanFeaturesUpdatedSuccessfully = "PlanFeatures updated successfully";
        public static string PlanFeaturesDeletedSuccessfully = "PlanFeatures deleted successfully";

        public static string PlaysAddedSuccessfully = "Plays added successfully";
        public static string PlaysUpdatedSuccessfully = "Plays updated successfully";
        public static string PlaysDeletedSuccessfully = "Plays deleted successfully";

        public static string PrayerAddedSuccessfully = "Prayer added successfully";

        public static string SeriesAddedSuccessfully = "Series added successfully";
        public static string SeriesAddedUnSuccessfully = "Series not added";
        public static string SeriesUpdatedSuccessfully = "Series updated successfully";
        public static string SeriesDeletedSuccessfully = "Series deleted successfully";
        public static string SeriesUpdatedUnSuccessfully = "Series not updated";

        public static string NoRecordsFound = "No records found";

        public static string DeviceRegisteredSuccessfully = "Device registered successfully";

        public static string OfferingsAddedSuccessfully = "Special offerings added successfully";
        public static string OfferingsUpdatedSuccessfully = "Special offerings updated successfully";
        public static string OfferingsDeletedSuccessfully = "Special offerings deleted successfully";

        public static string FeedbackAddedSuccessfully = "Feedback added successfully";

        public static string PaymentTrackingAddedSuccessfully = "Payment tracking added successfully";

        public static string UsageTrackingSuccessfully = "Usage tracking inserted successfully";

        public static string VerseInsertedSuccessfully = "Verses inserted successfully";

        public static string PasswordMisMatch = "Password mismatched";

        public static string ResetPassword = "Password has been reset successfully";

        public static string ResetPasswordFailed = "Resetting password has been failed.Try again!";

        public static string ForgotPasswordSuccessfully = "Mail sent successfully to registered email address";

        public static string ProgramSheetAddedSuccessfully = "Program sheet added successfully";
        public static string ProgramSheetDeletedSuccessfully = "Program sheet deleted successfully";

        public static string ChurchEnableSuccessfully = "Church enable updated successfully";
        public static string ClassCreatedSuccessfully = "Class created successfully";
        public static string UserCreatedSuccessfully = "User created successfully";
        public static string KidRegisteredSuccessfully = "Kid created successfully";
        public static string KidRetrieved = "Kids Retrieved Successfully";
        public static string ClassGenerated = "Pin generated Successfully";
        public static string ClassValidated = "Class Validated Successfully";
        public static string ClassRetrieved = "Classes Retrieved Successfully";
        public static string UserValidated = "User Validated Successfully";
        public static string ClassDeleted = "Class Deleted successfully";
        public static string ClassNotExists = "No Classes Exist for this Church";
        public static string KidsCheckIn = "Kids Checked in successfully";
        public static string KidsCheckOut = "Kids Checked out successfully";
        public static string QrCodeError = "QR Code MisMatched with the class";
        public static string KidsNotExists = "No Kids available for this class";
        public static string ClassInvalid = "Class is not validated";
        public static string PushToNotify = "Message sent to the parent";
        public static string PushNotNotify = "Could not able to send message to parent";

        public static string AlbumsAddedSuccessfully = "Albums added successfully";
        public static string AlbumsUpdatedSuccessfully = "Albums updated successfully";
        public static string AlbumsDeletedSuccessfully = "Albums deleted successfully";

        public static string TrackAddedSuccessfully = "Track added successfully";
        public static string TrackUpdatedSuccessfully = "Track updated successfully";
        public static string TrackDeletedSuccessfully = "Track deleted successfully";

        public static string NotesAddedSuccessfully = "Notes added successfully";
        public static string CustomWordsAddedSuccessfully = "Custom Word created successfully";
        public static string NotesUpdatedSuccessfully = "Notes Updated successfully";
        public static string CustomWordsUpdatedSuccessfully = "Custom Word updated successfully";
        public static string ItemAddedSuccessfully = "Item added successfully";
        public static string ItemUpdatedSuccessfully = "Item Updated successfully";
        public static string ItemDeletedSuccessfully = "Item Deleted successfully";
        public static string ChurchesRetrieved = "Church Branches Info retrieved successfully";
        public static string SubscriptionCouponAddedToChurch = "Subscription coupon added successfully";
        public static string BranchesNotExists = "No Branches Exist for this Church";
        public static string CreatedChurchSuccessfully = "Church Branches Created successfully";
        public static string UpdateChurchSuccessfully = "Church Branches updated successfully";
        public static string DeleteChurchSuccessfully = "Church Branches deleted successfully";
        public static string AppUsageTrack = "This device has been tracked";
        public static string UserInfoAdded = "Member info added successfully";
        public static string TransactionAddedSuccessfully = "Transaction added successfully";
        public static string AboutChurchUpdatedSuccessfully = "About church updated successfully";
        public static string ChurchBankDetailsSuccessfully = "Church bank details updated successfully";
        public static string Contactus_Added_Successfully = "ContactUs added successfully";
        public static string Church_PaymentMode_Updated_Successfully = "Church PaymentMode updated successfully";

        public static string DonationAdded = "Donation type added successfully";
        public static string DonationUpdated = "Donation type updated successfully";
        public static string DonationCauseDeleted = "Donation cause deleted successfully";
        public static string PdfFilesAddedSuccessfully = "Pdf files added successfully";
        public static string UserInfoDeleted = "Member info deleted successfully"; 
        public static string UserInfoUpdated = "Member info updated successfully"; 
        public static string NotesDeletedSuccessfully = "Notes Deleted Successfully";
        public static string UpdateChurchPaymentMode = "Church Payment Mode Updated Successfully";
        public static string Elite_Church_Enable = "Churches Enabled Successfully";
        public static string Elite_Church_Disable = "Churches Disabled Successfully";
        public static string Pdf_File_Deleted_Successfully = "PDF File Deleted successfully";
        public static string Prayer_Request_Deleted_Successfully = "Prayer Request Deleted successfully"; 
        public static string SubscriptionCouponCantBeAddedToChurch = "Subscription coupon cannot be added"; 
        public static string TemplateUpdatedSuccessfully = "Template updated successfully"; 
        public static string ChurchSignUpAddedSuccessfully = "Church signUp info added successfully";
        public static string ChurchSignUpUpdatedSuccessfully = "Church signUp info updated successfully"; 
        public static string ChurchSignUpStatusUpdatedSuccessfully =  "Church signUp info status successfully";
    }
}